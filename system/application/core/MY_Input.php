<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Input extends CI_Input {

	public function __construct()
	{		
 		parent::__construct();		
	}
	
	public function is_ajax_request()
	{
		$CI =& get_instance();
		$CI->output->enable_profiler(FALSE);
		return ($this->server('HTTP_X_REQUESTED_WITH') === 'XMLHttpRequest');
	}
}
/* End of file MY_Input.php */
/* Location: ./appication/core/MY_Input.php */