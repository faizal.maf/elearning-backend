<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class MY_Exceptions extends CI_Exceptions {
	
	public function __construct()
	{		
 		parent::__construct();		
	}
  	
	// --------------------------------------------------------------------

	/**
	 * 404 Page Not Found Handler
	 *
	 * @access	private
	 * @param	string
	 * @return	string
	 */
	function show_404($page = '', $log_error = TRUE)
	{	
		$heading = "Halaman tidak ditemukan";
		$message = "Halaman yang anda minta tidak ditemukan.";

		// By default we log this, but allow a dev to skip it
		if ($log_error)
		{
			// log_message('error', '404 Page Not Found --> '.$page, FALSE, TRUE);
			log_message('error', '404 Page Not Found --> '.$page);
		}

		echo $this->show_error($heading, $message, 'error_404', 404);
		exit;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * General Error Page
	 *
	 * This function takes an error message as input
	 * (either as a string or an array) and displays
	 * it using the specified template.
	 *
	 * @access	private
	 * @param	string	the heading
	 * @param	string	the message
	 * @param	string	the template name
	 * @param 	int		the status code
	 * @return	string
	 */
	function show_error($heading, $message, $template = 'error_general', $status_code = 500)
	{
		if (class_exists('CI_Controller', FALSE))
		{ // Check jika instance/ controller diload
			
		}
		return parent::show_error($heading, $message, $template, $status_code);
	}
	
	// --------------------------------------------------------------------

	/**
	 * Native PHP error handler
	 *
	 * @access	private
	 * @param	string	the error severity
	 * @param	string	the error string
	 * @param	string	the error filepath
	 * @param	string	the error line number
	 * @return	string
	 */
	// // function show_php_error($severity, $message, $filepath, $line)
	// // {
		// // if (class_exists('CI_Controller', FALSE))
		// // { // Check jika instance/ controller diload			
			// // /**
				// // Selama ada throw Exception disini, maka aplikasi akan terhenti. 
				// // Maka akan dicek apakah ada transaksi database atau tidak.
				// // Kalau ada, rollback transaksi tersebut.
			// // */
			// // $CI =& get_instance();
			// // if (isset($CI->db))
			// // {
				// // if ($CI->db->is_trans_opened())
					// // $CI->db->trans_rollback();
			// // }
		// // }
		
		// // $severity = ( ! isset($this->levels[$severity])) ? $severity : $this->levels[$severity];

		// // $filepath = str_replace("\\", "/", $filepath);

		// // // For safety reasons we do not show the full file path
		// // if (FALSE !== strpos($filepath, '/'))
		// // {
			// // $x = explode('/', $filepath);
			// // $filepath = $x[count($x)-2].'/'.end($x);
		// // }

		// // if (ob_get_level() > $this->ob_level + 1)
		// // {
			// // ob_end_flush();
		// // }
		// // ob_start();
		// // include(APPPATH.'errors/error_php.php');
		// // $buffer = ob_get_contents();
		// // ob_end_clean();
		// // //echo $buffer;
		// // set_exception_handler('exception_handler');
		// // log_message('error', 'Severity: '.$severity.'  --> '.$message. ' '.$filepath.' '.$line, TRUE);
		// // throw new Exception($buffer);
	// // }
}
// END MY_Exceptions Class

/* End of file MY_Exceptions.php */
/* Location: ./system/application/core/MY_Exceptions.php */