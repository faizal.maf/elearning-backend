<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model 
{

	public $table_name;
	public $field_id;
	public $flag_delete_field = '';
	public $set_order = array();
	public $set_sort;
	public $set_sort_field;

	public $columns = array();
	public $fields = array();
	protected $CI;
	
	public function __construct()
	{
 		parent::__construct();
		$this->CI =& get_instance();
	}

	public function get_fields()
	{
		$results = array();
		$fields_name = NULL;
		$not_exception_field = TRUE;
		
		if ($fields_name != NULL)
		{
			if (!is_array($fields_name))
				$fields_name = array($fields_name);
			
			foreach ($fields_name as $_field_name)
			{
				$fields = array();
				$is_found = FALSE;
				foreach ($this->fields as $_index=>$_field)
				{
					if ($_field['field'] == $_field_name)
					{
						$fields = $_field;
						$is_found = TRUE;
						break;
					}
				}
				
				if ($is_found == $not_exception_field)
					$results[] = $fields;
			}
		}
		else
			$results = $this->fields;
		
		return $results;
	}

	public function remove_fields($fields_name)
	{
		if (!is_array($fields_name))
			$fields_name = array($fields_name);
		
		foreach ($fields_name as $_field_name)
		{
			$fields = $this->fields;
			foreach ($fields as $_index=>$_fields)
			{
				if ($_fields['field'] == $_field_name)
				{
					unset($this->fields[$_index]);
					break;
				}
			}
		}
		
		return $this->fields;
	}

	public function modify_value_fields($field, $value)
	{
		/**
		 *  set change value @ref _set_property_fields('value', $field, $value)
		 */
		return $this->_set_property_fields('value', $field, $value);
	}
	
	protected function _set_property_fields($property, $field, $value)
	{
		$is_found = FALSE;
		foreach ($this->fields as $_index=>$_fields)
		{
			if ($_fields['field'] == $field)
			{
				$this->fields[$_index][$property] = $value;
				$is_found = TRUE;
				break;
			}
		}
		
		return $is_found;
	}

	protected function insert()
	{
		$insert_result = $this->db->insert($this->table_name);
		return $insert_result;
	}
	
	protected function update($id)
	{
		$this->db->where($this->field_id, $id);
		$update_result = $this->db->update($this->table_name);
		
		return $update_result;
	}

	protected function delete($id)
	{
		/**
		 *  Cek Field Flag is delete
		 */
		if(!empty($this->flag_delete_field))
		{
			$this->db->set($this->flag_delete_field, 1);
			$this->db->where($this->field_id, $id);
			$delete_result = $this->db->update($this->table_name);

			return $delete_result;
		}
		else
		{
			/**
			 *  Delete permanen
			 */
			$this->db->where($this->field_id, $id);
			return $this->db->delete($this->table_name);
		}
	}

	protected function get_data($id)
	{
		$this->db->from($this->table_name);
		$this->db->where($this->field_id, $id);
		$result = $this->db->get();
		return $result->result();
	}
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */