<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	protected $app_version = '1.0.0';
	protected $app_config_file = 'site_app_settings';

	protected $controller_name;
	protected $model_name;
	protected $uri_name = '';
	protected $title = '';
	protected $subtitle = '';
	
	/**
	 *  id_menu a string
	 *  
	 *  variable global untuk menampung id menu yang digunakan untuk acl.
	 */
	protected $id_menu;
	
		/**
	 *  type_permission a integer
	 *  
	 *  variable global untuk menampung jenis permision untuk hak akses pada sebuah halaman (id_menu).
	 *  
	 *  Nilai yang ditampung yaitu: 1 (data utama), 2(parameter), 3(sesuai hak akses aja)
	 *  
	 */	
	protected $type_permission = 1;

	/**
	 *  message as string
	 *  
	 *  variable global untuk menampung pesan yang akan dimunculkan pada setiap halaman. pesan ini di session dengan metode flashdata. 
	 *  artinya ketika redirect page pesan tersebut akan diakses untuk ditampilkan dan akan otomatis hilang dari session.
	 */
	public $message;
	
	/**
	 *  message_type as string 
	 *  
	 *  variable global untuk menampung jenis pesan. isinya adalah error_message atau success_message. Default value error_message
	 *  
	 *  Jika error_message maka pesan yang akan ditampilkan berwarna merah, sedangkan jika success_message akan berwarna hijau.
	 */
	public $message_type;

	/**
	 *  _displayed_additional as array
	 *  
	 *  variable global untuk menampung data tambahan yang akan di parsing ke view pada function index, form, detail, view_delete.
	 *  
	 *  untuk set value variable ini biasanya dilakukan overriding pada controlller yang merupakan class turunan MY_Controller.
	 *  
	 *  berikut ini contoh penggunaannya : 
	 *  	
	 *  	public function index($type='', $value=''){
	 *  
	 *  		$this->_displayed_additional['data_parsing_tambahan'] = $data_parsing_tambahan;
	 *
	 *  		parent::index($type, $value);
	 *  
	 *  	}
	 **/
	protected $_displayed_additional = array();

	/**
	 *  selects as array
	 *  
	 *  variable global untuk menampung field yang diselect pada query list yang akan di tampilkan pada table list. 
	 *  variable ini diset biasanya pada fungsi _get_list, custom_search dengan cara overriding pada file controller. 
	 *  
	 *  e.g:
	 *  
	 *  	$this->selects = array(
	 *  
	 *  		$this->table_name.'.id_user',
	 *  
	 *  		$this->table_name.'.nama',
	 *  
	 *  		$this->table_name.'.jabatan',
	 *  
	 *  		'SUM(bpd.point) pts'
	 *  
	 *  	);  
	 */
	protected $selects = array();
	
	/**
	 *  joins as array
	 *  
	 *  table	=> nama table yang akan di join
	 *  
	 *  joined	=> field yang di join. implementasi di query adalah ON (field1 = field2)
	 *  
	 *  type	=> jenis join yang digunkan, isinya left, inner, right
	 *  
	 */
	protected $joins = array();
	
	/**
	 *  group_by as array
	 *  
	 *  variable global untuk menampung field group_by pada query list. 
	 *  variable ini diset biasanya pada fungsi _get_list, custom_search dengan cara overriding pada file controller. 
	 *  
	 *  e.g:
	 *  
	 *  	$this->group_by[] = "p.id_user";
	 *  	$this->group_by[] = "i.status";
	 */
	protected $group_by = array();
	
	/**
	 *  having as array
	 *  
	 *  variable global untuk menampung field having pada query list. 
	 *  variable ini diset biasanya pada fungsi _get_list, custom_search dengan cara overriding pada file controller. 
	 *  
	 *  e.g:
	 *  	$this->having[] = 'COUNT(user.status) > 1';
	 */
	protected $having = array();
	

	public function __construct()
	{
		parent::__construct();

		// sebatsdulu version
		if ( ! defined('MY_VERSION') )
			define('MY_VERSION', $this->app_version);

		// Get current controller name
		// $this->controller = get_class($this);
		
		// Load CI native helper
		$this->load->helper('url');

		// Load application file settings
		$this->_load_app_settings();

		// Load MY libraries
		$this->load->library('template');
		$this->load->library('tag');

		// Load MY helpers
		$this->load->helper('array');
		$this->load->helper('tag');
		$this->load->helper('template');
		$this->load->helper('message');
		$this->load->helper('utility');
	}

	public function index()
	{
		/**
		 *  check permision read:   
		 *  check_permissions(@ref $id_menu, 'R', @ref $type_permission) in utility_helper.php
		 */
		check_permissions($this->id_menu, 'R', $this->type_permission);
		
		$data['title'] = $this->title;
		$data['controller_name'] = $this->uri_name;
		/**
		 *  Generate breadcrumbs
		 */
		$breadcrumbs = array(array('url' => $this->uri_name, 'title' => $this->title, 'active' => true));
		$data['breadcrumbs'] = breadcrumbs($breadcrumbs, '&raquo;');

		$this->template->view($this->uri_name.'/list', $data);
	}

	public function view_pdf($path='', $title='')
	{
		$parameters = array(
			'file' => $path,
			'download' => 'false',
			'print' => 'false',
			'openfile' => 'false'
		  );

		if(!is_array($parameters)) $params = split('&', $parameters);
		  
		$pairs = array();
		foreach($parameters as $key=>$val)
		{
    		if(null === $val || '' === $val) continue;
    		$pairs[] = urlencode($key).'='.urlencode($val);
		}
		
		$build_view_pdf = base_url().'pdfviewer?'.implode('&', $pairs);
		
		$args['title'] = $title;
		$args['path_view'] = $build_view_pdf;
		$this->template->render('pdfviewer/pdf_frame', $args, FALSE);
	}

	protected function table_list()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;
		$result = $this->$model->get_datatable();

		$this->template->render_json($result);
	}

	protected function form($id='')
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;
		$submit_action = 'insert';
		$result = NULL;
		if (!empty($id))
		{
			$result = $this->_get_detail($id);
			$submit_action = 'update/id/'.$id;
			if(count($result) == 0)
			{
                $this->session->set_flashdata($this->uri_name, "Ops! The one you selected was not found.");
				$this->session->set_flashdata($this->uri_name.'_type', 'alert-danger');
				// redirect($this->uri_name);

				$str = "<script language='javascript' type='text/javascript'>";
				$str .=	"$('#ModalAjaxID').modal('hide');";
				$str .= "window.location = '".$this->uri_name."';</script>";

				echo $str;
				// $data['controller_name'] = $this->uri_name;
				// $this->template->render($this->uri_name.'/form', $data, FALSE);
            }
		}
		$data['result']          = $result;
		$data['controller_name'] = $this->uri_name;
		$data['action']          = $submit_action;

		if(!empty($this->_displayed_additional)){
			foreach($this->_displayed_additional as $index=>$value)
			{
				$data[$index]=$value;
			}
		}

		$this->template->render($this->uri_name.'/form', $data, FALSE);
	}

	protected function delete()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;

		$id = $this->input->post('id');
		$this->$model->delete($id);
	}

	protected function _get_detail($criteria)
	{
		$model = $this->model_name;
		
		/**
		 *  Check custom select field
		 */
		foreach($this->selects as $select){
			$this->db->select($select,FALSE);
		}
		$this->db->from($this->$model->table_name);
		
		/**
		 *  Check custom join table
		 */
		foreach($this->joins as $join)
		{
			$this->db->join($join['table'], $join['joined'], $join['type'], FALSE);
		}
		
		/** 
		 *  Restructure criteria
		 */
        if (is_array($criteria))
		{
            foreach ($criteria as $field=>$value)
			{
                $this->db->where($field, $value);
            }
        }
		else
		{
            $this->db->where($this->$model->table_name.'.'.$this->$model->field_id, $criteria);
        }
		
		/**
		 *  Check custom group_by
		 */
		foreach($this->group_by as $group_by){
			$this->db->group_by($group_by);
		}
		
		/** 
		 *  Check custom having
		 */
		foreach($this->having as $having){
			$this->db->having($having);
		}
		
		$result = array();
		$results = $this->db->get();
		if($results->num_rows() > 0)
		{
			//seharusnya $results->row(); karena cuma 1 data yg diambil, karena bug php 7.2 count terjadi error kalo obj.
			$result = $results->row_array(); 
		}
		return $result;
	}

	/**
	 * Loads the app settings file. For each array $config index a constant by same
	 * name is defined.
	 *
	 * @return void
	 */
	private function _load_app_settings()
	{
		// Load app_settings file
		$this->config->load( $this->app_config_file, true );
		// Set configs will be readed
		$config = $this->config->config [ $this->app_config_file ];
		
		foreach($config as $key => $val)
		{
			if ( ! is_array($val) )
				if ( ! defined($key) )
					define($key, $val);

			$this->{$key} = $val;
		}
	}


}
