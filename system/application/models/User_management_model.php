<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_management_model extends MY_Model 
{
	private $_batchImport;
	protected $table_join1;
	protected $table_join2;

	function __construct()
	{
		parent::__construct();
		$this->table_name     = 'bsm_user';
		$this->table_join1     = 'bsm_jabatan';
		$this->table_join2     = 'bsm_apps_level';
		$this->field_id		  = 'id';
		$this->flag_delete_field = 'is_deleted';
		$this->set_sort       = 'DESC';
		$this->set_sort_field = 'RecordID';
		$this->set_order      = array(
				$this->table_name.'.name' => 'ID', 
				$this->table_name.'.nip' => 'NIP', 
				$this->table_name.'.email' => 'Email', 
				$this->table_name.'.created' => 'Date', 
				$this->table_name.'.status' => 'Status', 
				$this->table_name.'.poin' => 'Point',
				$this->table_join1.'.jabatan_name' => 'Position',
				$this->table_join2.'.nama' => 'LevelAccess'
				
			);

		$this->fields = array(
			array('field' => 'id', 'label' => 'ID', 'rules' => 'trim'),
			array('field' => 'id_jabatan', 'label' => 'ID Jabatan', 'rules' => 'trim'),
			array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),
			array('field' => 'name', 'label' => 'Name', 'rules' => 'trim|required'),
			// array('field' => 'jabatan', 'label' => 'Position', 'rules' => 'trim|required'),
            array('field' => 'nip', 'label' => 'Nip', 'rules' => 'trim|required'),
            array('field' => 'poin', 'label' => 'Point', 'rules' => 'trim'),
			array('field' => 'status', 'label' => 'Status', 'rules' => 'trim'),
			array('field' => 'created_by', 'label' => 'Created By', 'rules' => 'trim'),
			array('field' => 'created', 'label' => 'Created', 'rules' => 'trim'),
			array('field' => 'updated_by', 'label' => 'Updated By', 'rules' => 'trim'),
			array('field' => 'updated', 'label' => 'Updated', 'rules' => 'trim')
		);

	}
	
	public function get_datatable()
	{
		// Table initialize
		$this->load->library('datatables_lib');
		$ctable = $this->datatables_lib;
		$ctable->init($this->set_sort, $this->set_sort_field);
		
		// filter by status
		if(!empty($ctable->table['query']['fillStatus']) && $ctable->table['query']['fillStatus'] != "all") {
			$this->db->where($this->table_name.'.status', ($ctable->table['query']['fillStatus'] == "active" ? 1 : 0) );
		}
		
		// filter by level
		if(!empty($this->datatables_lib->table['query']['fillLvl']) && $this->datatables_lib->table['query']['fillLvl'] != "all") {
			$this->db->where($this->table_join1.'.id_level', $this->datatables_lib->table['query']['fillLvl']);
		}
		
		// filter by jabatan
		if(!empty($this->datatables_lib->table['query']['fillJabatan']) && $this->datatables_lib->table['query']['fillJabatan'] != "all") {
			$this->db->where($this->table_name.'.id_jabatan', $this->datatables_lib->table['query']['fillJabatan']);
		}
		

		// filter generalSearch
		if(!empty($ctable->table['query']['generalSearch'])) {
			$this->db->group_start();
				$match_keyword = trim($ctable->table['query']['generalSearch']);
				$array = array(
					$this->table_name.'.email'   => $match_keyword,
					$this->table_name.'.name'    => $match_keyword,
					$this->table_name.'.jabatan' => $match_keyword,
					$this->table_name.'.nip'     => $match_keyword
				);
				$this->db->or_like($array);
			$this->db->group_end();
		}

		$this->db->select($this->table_name.'.*,'.$this->table_join1.'.jabatan_name as groups_name,'.$this->table_join2.'.nama as level_access,');
		$this->db->from($this->table_name);
		$this->db->join($this->table_join1, $this->table_name.'.id_jabatan ='.$this->table_join1.'.id_jabatan', 'left');
		$this->db->join($this->table_join2, $this->table_join1.'.id_level ='.$this->table_join2.'.id_level', 'left');
		$this->db->where($this->table_name.'.is_deleted', 0);
		// Add additional data.
		// $this->db->where($this->table_name.'.is_publish', 1);

		// Table build data.
		$results = $ctable->build_data($ctable->field, $this->set_order, $ctable->sort, $ctable->perpage, $ctable->page);

		$data = array();
		// ----- opt select
		// $groups_opt = array('' => '');
		// $sql = "SELECT id_level, nama AS options_name FROM bsm_apps_level";
		// $list_lvl_records = $this->db->query($sql)->result();
		// foreach ($list_lvl_records as $list_lvl_records_idx=>$list_lvl_records)
		// {
			// $groups_opt[$list_lvl_records->id_level] = $list_lvl_records->options_name;
		// }


		foreach($results['data'] as $key => $row) 
		{
			// $opt_kategori  = form_dropdown('id_level', $groups_opt, set_value('id_level', (isset($row['id_level']) ? $row['id_level'] : '')), 'id="id_level" value="'.$row['id'].'"');
			
			$data[$key]['RecordID'] = (int)$row['id'];
			$data[$key]['Name']     = $row['name'];
			$data[$key]['LevelAccess'] = $row['level_access'];
			$data[$key]['NIP']      = $row['nip'];
			$data[$key]['Email']    = $row['email'];
			$data[$key]['Position'] = $row['groups_name'];
			$data[$key]['Point']    = $row['poin'];
			$data[$key]['Date']     = convert_date('Y-m-d H:i:s', $row['created'], 'M d, Y, H:i a');
			$data[$key]['Status']   = '<button type="button" class="btn m-btn--square btn-sm m-btn m-btn--custom btnPublish '.($row['status'] != 1 ? 'pubNo btn-danger' : 'pubYes btn-success').'" id="'.$row['id'].'"><i class="la la-'.($row['status'] != 1 ? 'close' : 'check').'"></i> '.($row['status'] != 1 ? 'Inactive' : 'Active').'</button>';
		}

		// Restructure array for generate json.
		return $ctable->build_datatable($results['total'], $data, $ctable->sort, $ctable->field, $ctable->page, $results['pages'], $ctable->perpage);
	}

	public function insert()
	{
		return parent::insert();
	}

	public function update2($data)
	{
		$config = array (
			'id',
			'caption',
			'thumbnail',
			'image',
			'is_publish'
		);

		$new_data = $this->set_data($config, $data);
		$new_data['updated'] = date('YmdHis');
		$this->db->where('id', $data['id']);
		$this->db->update($this->table_names, $new_data);
	}

	public function update($id)
	{
        return parent::update($id);
    }

	public function delete($id)
	{
        return parent::delete($id);
    }

	public function get_fields() 
	{
		return parent::get_fields();
	}

	public function modify_value_fields($field, $value)
	{
		return parent::modify_value_fields($field, $value);
	}

	## SECTION IMPORT DATA
	public function setBatchImport($batchImport) {
        $this->_batchImport = $batchImport;
    }

    public function importData() {
        $data = $this->_batchImport;
        $this->db->insert_batch($this->table_name, $data);
	}

}

/* End of file User_management_model.php */
/* Location: ./application/models/User_management_model.php */