<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_groups_model extends MY_Model 
{
	protected $table_join1;
	function __construct()
	{
		parent::__construct();
		$this->table_name     = 'part_level';
		$this->table_join1     = 'part_users';
		$this->field_id		  = $this->table_name.'id_level';
		$this->set_sort       = 'ASC';
		$this->set_sort_field = 'Name';
		$this->set_order      = array($this->table_name.'.nama' => 'Name');

		$this->fields = array(
			array('field' => 'id_level', 'label' => 'ID', 'rules' => 'trim'),
			array('field' => 'nama', 'label' => 'Groups Name', 'rules' => 'trim|required')
		);

	}
	
	public function get_datatable()
	{
		// Table initialize
		$this->load->library('datatables_lib');
		$ctable = $this->datatables_lib;
		$ctable->init($this->set_sort, $this->set_sort_field);
		
		// filter generalSearch
		if(!empty($ctable->table['query']['generalSearch'])) {
			$this->db->group_start();
				$match_keyword = trim($ctable->table['query']['generalSearch']);
				$array = array(
					$this->table_name.'.nama'   => $match_keyword
				);
				$this->db->or_like($array);
			$this->db->group_end();
		}

		$this->db->select($this->table_name.'.*, COUNT('.$this->table_join1.'.id_level) as total_pengguna');
		$this->db->from($this->table_name);
		$this->db->join($this->table_join1, $this->table_name.'.id_level ='.$this->table_join1.'.id_level', 'left');
		$this->db->group_by($this->table_name.'.id_level, '.$this->table_name.'.nama');
		// Add additional data.
		// $this->db->where($this->table_name.'.is_publish', 1);

		// Table build data.
		$results = $ctable->build_data($ctable->field, $this->set_order, $ctable->sort, $ctable->perpage, $ctable->page);
		$data = array();
		foreach($results['data'] as $key => $row) 
		{
			$data[$key]['RecordID'] = (int)$row['id_level'];
			$data[$key]['Name']     = $row['nama'];
			$data[$key]['UserGroups'] = $row['total_pengguna'];

		}

		// Restructure array for generate json.
		return $ctable->build_datatable($results['total'], $data, $ctable->sort, $ctable->field, $ctable->page, $results['pages'], $ctable->perpage);
	}

	public function insert()
	{
		return parent::insert();
	}

	public function update2($data)
	{
		$config = array (
			'id',
			'caption',
			'thumbnail',
			'image',
			'is_publish'
		);

		$new_data = $this->set_data($config, $data);
		$new_data['updated'] = date('YmdHis');
		$this->db->where('id', $data['id']);
		$this->db->update($this->table_names, $new_data);
	}

	public function update($id)
	{
        return parent::update($id);
    }

	public function delete($id)
	{
        return parent::delete($id);
    }

	public function get_fields() 
	{
		return parent::get_fields();
	}

	public function modify_value_fields($field, $value)
	{
		return parent::modify_value_fields($field, $value);
	}

}

/* End of file Setting_groups_model.php */
/* Location: ./application/models/Setting_groups_model.php */