<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Book_model extends MY_Model 
{
	private $table_join1;
	private $_batchImport;

	function __construct()
	{
		parent::__construct();
		$this->table_name     = 'book';
		$this->table_join1    = 'kategori';
		$this->field_id		  = 'id';
		$this->set_sort       = 'DESC';
		$this->set_sort_field = 'Date';
		$this->set_order      = array(
					$this->table_name.'.judul'        => 'Title', 
					$this->table_name.'.created_at'      => 'Date',
					$this->table_name.'.tahun_terbit' => 'ThnTerbit',
					$this->table_name.'.status' => 'Status',
					$this->table_join1.'.kategori' => 'kategori'
			);

		$this->fields = array(
			array('field' => 'id', 'label' => 'ID', 'rules' => 'trim'),
			array('field' => 'kategori', 'label' => 'Kategori', 'rules' => 'trim|required'),
			array('field' => 'judul', 'label' => 'Judul', 'rules' => 'trim|required'),
			array('field' => 'penulis', 'label' => 'Penulis', 'rules' => 'trim|required'),
			array('field' => 'penerbit', 'label' => 'Penerbit', 'rules' => 'trim|required'),
			array('field' => 'tahun_terbit', 'label' => 'Tahun terbit', 'rules' => 'trim|required'),
            array('field' => 'filename', 'label' => 'Filename', 'rules' => 'trim'),
            array('field' => 'cover', 'label' => 'Cover', 'rules' => 'trim'),
			array('field' => 'status', 'label' => 'Status', 'rules' => 'trim'),
		);
	}

	public function get_datatable()
	{
		// Table initialize
		$this->load->library('datatables_lib');
		$this->datatables_lib->init($this->set_sort, $this->set_sort_field);
		
		// filter by kategori
		if(!empty($this->datatables_lib->table['query']['f_kategori']) && $this->datatables_lib->table['query']['f_kategori'] != "all") {
			$this->db->where($this->table_name.'.id_kategori', $this->datatables_lib->table['query']['f_kategori']);
		}
		

		// filter generalSearch
		if(!empty($this->datatables_lib->table['query']['generalSearch'])) {
			$this->db->group_start();
				$match_keyword = trim($this->datatables_lib->table['query']['generalSearch']);
				$array = array(
					$this->table_name.'.judul' => $match_keyword,
					$this->table_name.'.tahun_terbit' => $match_keyword
				);
				$this->db->or_like($array);
			$this->db->group_end();
		}

		$this->db->select(
				$this->table_name.'.*,'.
				$this->table_join1.'.kategori,');
		$this->db->from($this->table_name);
		$this->db->join($this->table_join1, $this->table_name.'.kategori = '.$this->table_join1.'.id', 'LEFT');

		// Add additional data.
		// $this->db->where($this->table_name.'.status', 1);

		// Table build data.
		$results = $this->datatables_lib->build_data($this->datatables_lib->field, $this->set_order, $this->datatables_lib->sort, $this->datatables_lib->perpage, $this->datatables_lib->page);
		$data = array();
		foreach($results['data'] as $key => $row) 
		{
			$data[$key]['RecordID']  = (int)$row['id'];
			$data[$key]['Title']     = $row['judul'];
			$data[$key]['ThnTerbit'] = $row['tahun_terbit'];
			$data[$key]['CatID']     = $row['id'];
			$data[$key]['CatName']   = $row['kategori'];
			// $data[$key]['Document']  = (!empty($row['filename']) || !is_null($row['filename']) ? '<a class="m-link m-link--state m-link--success" href="'.base_url('regulasi/view_pdf/'.$row['keypdf']).'" target="_blank" title="View PDF: '.$row['filename'].'">'.$row['filename'].'</a>' : '');
			$data[$key]['Date']      = convert_date('Y-m-d H:i:s', $row['created_at'], 'M d, Y, H:i a');
			$data[$key]['Status']   = '<button type="button" class="btn m-btn--square btn-sm m-btn m-btn--custom btnPublish '.($row['status'] != 1 ? 'pubNo btn-danger' : 'pubYes btn-success').'" id="'.$row['id'].'" id_kat="'.$row['kategori'].'"><i class="la la-'.($row['status'] != 1 ? 'close' : 'check').'"></i> '.($row['status'] != 1 ? 'No' : 'Yes').'</button>';
			
		}

		// Restructure array for generate json.
		return $this->datatables_lib->build_datatable($results['total'], $data, $this->datatables_lib->sort, $this->datatables_lib->field, $this->datatables_lib->page, $results['pages'], $this->datatables_lib->perpage);
	}

	public function add($data)
	{
		$config = array (
			'judul',
			'penulis',
			'penerbit',
			'tahun_terbit',
			'kategori',
            'bahasa',
            'cover',
            'filename',
            'sinopsis'
		);
	
		$new_data = $this->set_data($config, $data);
		$new_data['created_at'] = date('YmdHis');
		$new_data['updated_at'] = date('YmdHis');
		$new_data['status'] = 1;
		$this->db->insert($this->table_name,$new_data);
		
		$insert_id = $this->db->insert_id();
		
		// store_last_id($data['id_kategori'], $insert_id);
		
		return $insert_id;
	}

	public function update($data)
	{
		$config = array (
			'judul',
			'penulis',
			'penerbit',
			'tahun_terbit',
			'kategori',
            'bahasa',
            'cover',
            'filename',
            'sinopsis'
		);

		$new_data = $this->set_data($config, $data);
		$new_data['updated_at'] = date('YmdHis');
		$this->db->where('id', $data['id']);
		$this->db->update($this->table_name, $new_data);
	}

	public function delete($id)
	{
		$this->db->select('attachment');
		$this->db->where('id', $id);
		$getpath = $this->db->get($this->table_name);
		if($getpath->num_rows() > 0){
			$result = $getpath->row();

			//remove file
			unlink($result->attachment);
		}
        parent::delete($id);
		
		return true;
    }

	public function get_path_pdf($keypdf)
	{
		$this->db->select('title, attachment')
				->from($this->table_name)
				->where('keypdf', $keypdf);

		$result = $this->db->get();

		return $result;
	}

	protected function set_data($nama_coloumn, $sumber)
	{
		$hasil = array();
			foreach($nama_coloumn as $row)
			{
				if(isset($sumber[$row]))
				{
					$hasil[$row] = $sumber[$row];
				}
			}
		
		return $hasil;
	}

	## SECTION IMPORT DATA
	public function setBatchImport($batchImport) {
        $this->_batchImport = $batchImport;
    }

    public function importData() {
        $data = $this->_batchImport;
        $this->db->insert_batch($this->table_name, $data);
	}
}

/* End of file Regulasi_model.php */
/* Location: ./application/models/Regulasi_model.php */