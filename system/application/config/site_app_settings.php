<?php defined('BASEPATH') OR exit('No direct script access allowed');

// APP CONFIG
$config['APP_NAME'] 	  = 'Elearning';
$config['EMAIL_FROM'] 	  = 'bsmcompashelp@gmail.com';


// URLS
$config['URL_ROOT'] = rtrim(base_url(), '/');
$config['URL_UPLOAD'] = $config['URL_ROOT'] . '/application/uploads';
$config['URL_ASSETS'] = $config['URL_ROOT'] . '/assets';
$config['URL_CSS'] = $config['URL_ASSETS'] . '/css';
$config['URL_JS'] =  $config['URL_ASSETS'] . '/js';
$config['URL_IMG'] = $config['URL_ASSETS'] . '/img';
$config['URL_PLUGINS'] = $config['URL_ASSETS'] . '/plugins';

// PATHS
$config['PATH_TEMP'] = 'application/temp';
$config['PATH_UPLOAD'] = 'application/uploads';
$config['PATH_ASSETS'] = 'assets';
$config['PATH_CSS'] = $config['PATH_ASSETS'] . '/css';
$config['PATH_JS'] = $config['PATH_ASSETS'] . '/js';
$config['PATH_IMG'] = $config['PATH_ASSETS'] . '/img';