<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config['profiler'] 	  = FALSE;
$config['lang_helper'] 	= FALSE;

$config['login_url']  		 = 'login';
$config['logout_url'] 		 = 'login/signout';

//Name of the sessions to convert to template variables
$config['sessions'] = array(
		'language' 	=> 'lang',
		'user' 			=> 'full_name',
		'group' 		=> 'groupname'
	);
//Name of labels you can use lang helper
$config['labels'] = [
	'logout'  => 'Logout',// You can use lang('lang_key')
	'profile' => 'Profile',// You can use lang('lang_key')
	'search'  => 'Search'
];

// Set theme name
$config['theme'] 		= 'metronic';
$config['body_class'] 	= 'm-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';
// $config['body_class'] 	= 'm-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default';
$config['title'] 		= 'Elearning – Admin';
$config['favicon'] 	    = 'public/assets/favicon.ico';
$config['company_name'] = 'Midnightminer';
$config['app_name'] 	= 'Budi Mulia Ebook';
// $config['app_version']  = CI_VERSION;
$config['app_version']  = '1.0.0';
$config['app_year']     = date("Y");

$config['meta']  	= array(
						'viewport'		=> 'width=device-width, initial-scale=1.0',
						'author'		=> 'Midnightminer',
						'description'	=> 'Midnightminer',
						'resource-type'	=> 'document'
					);

$config['css_files'] = array(
						'public/themes/css/vendors.min.css?v1&current='.date('Ym'),
						'public/themes/css/app.min.css?v1&current'.date('Ym'),
						'public/themes/css/custom.css?v1&current'.date('Ym')
					);

$config['js_files']  = array(
						'public/themes/js/vendors.min.js?v1&current='.date('Ym'),
						'public/themes/js/app.min.js?v1&current='.date('Ym'),
						'public/themes/js/custom.js?v1&current='.date('Ym'),
					);

$config['directories'] = array(
						'asset_path' 	=> 'public/themes/',
						'css_path' 		=> 'public/themes/css/',
						'js_path' 		=> 'public/themes/js/',
						'less_path' 	=> 'public/themes/less/',
						'plugins_path' 	=> 'public/themes/plugins/',
						'img_path' 		=> 'public/themes/img/',
						'upload_path' 	=> 'resources/uploads/',
						'download_path' => 'resources/download/'
					);

$config['multilingual'] = array(
							'default'		=> 'english',
							'available'		=> array(
								'en' => array(
									'label'	=> 'English',
									'value'	=> 'english',
								)
							),
							'autoload'		=> array('site')
							);








