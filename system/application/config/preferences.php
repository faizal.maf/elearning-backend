<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* Setup Profiler */
$config['enable_profiling'] = FALSE;

// APP CONFIG
$config['APP_NAME'] 	  = 'GRC Compas';
$config['EMAIL_FROM'] 	  = 'info@compas.id';

## CUSTOM MESSAGE
$config['message_error_submit'] = '<strong>Ups</strong>, Harap periksa kembali data yang anda masukan!';

/* End of file preferences.php */
/* Location: ./appication/config/preferences.php */