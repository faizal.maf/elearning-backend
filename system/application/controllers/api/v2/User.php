<?php
use Restserver\Libraries\Rest\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/rest/REST_Controller.php';
require APPPATH . 'libraries/rest/Format.php';

class User extends REST_Controller {
	private $_token; //var global tampung token
	protected $mail;
	protected $mail_template;

    function __construct() {
        parent::__construct();
        $this->load->library('template');
        $this->load->helper(array('url','file','array','utility'));
		$this->load->library('mail_lib');
		$this->load->library('mail_template_lib');

		$this->mail = $this->mail_lib;
		$this->mail_template = $this->mail_template_lib;

        if(
            $this->uri->segment(4) == "create_auth" ||
            $this->uri->segment(4) =="view_pdf" ||
            $this->uri->segment(4) =="app_init"||
            $this->uri->segment(4) =="test_notif" ||
            $this->uri->segment(4) =="reset_password" ||
            $this->uri->segment(4) =="sender_mail" ||
            $this->uri->segment(4) =="test"||
            $this->uri->segment(4) =="last_content"
            ){
        }else{
            $this->check_token();
        }

    }

    public function last_content_get(){
        $this->benchmark->mark('code_start');
        $resut = array();
        $sql = "SELECT * FROM bsm_last_content";
        $results = $this->db->query($sql);
        foreach ($results->result_array() as $key => $list)
        {
            $result['content'][$key]['field']           = $list['field'];
            $result['content'][$key]['last_id']        = $list['last_content_id'];
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function test_get(){
        $this->benchmark->mark('code_start');
        $result['user'] = $this->get_user_access();
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function user_access_get(){

        $this->benchmark->mark('code_start');
        $result['user'] = $this->get_user_access();
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function app_init_get(){
		$this->benchmark->mark('code_start');

                //check version application
                $version = $this->input->get_request_header('version', TRUE);
                if($version == NULL)
                        $this->response($this->_show_response_505(), REST_Controller::HTTP_VERSION_NOT_SUPPORTED);

                $result = array();
        $query = "SELECT * FROM bsm_config";
        $results = $this->db->query($query);
        $ajp = 0;
        foreach ($results->result_array() as $key => $list)
        {
            if($list['groups']=="Hubungi Kami"){
                $result['hubungi'][$list['deskripsi']]   = $list['catatan'];
                if($list['deskripsi'] == "socmed"){
                    $result['hubungi'][$list['deskripsi']] = json_decode($list['catatan']);
                }
            }
            if($list['groups']=="Default Poin"){
                $result['poin'][$list['deskripsi']]   = $list['catatan'];
            }
            if($list['groups']=="Ajukan Pertanyaan"){
                $result['subject'][$ajp]['subject'] = $list['deskripsi'];
                $ajp++;
            }
            if($list['groups']== "Info Tukar Poin"){
                $result['poininfo'] = $list['catatan'];
            }
            if($list['groups']== "Logo" && $list['deskripsi'] == "Logo Splashscreen"){
                $result['logo_image'] = base_url().$list['catatan'];
            }
        }  
        
        // $socmed = array();
        // $socmed[0] = array('socmed'=>'Instagram', 'image'=>'http://dev.dagor.in/25bsm_app/public/assets/instagram.png','title'=>'@bsmid','url'=>'');
        // $socmed[1] = array('socmed'=>'Youtube', 'image'=>'http://dev.dagor.in/25bsm_app/public/assets/youtube.png','title'=>'Bank Syariah Mandiri','url'=>'');
        // $socmed[2] = array('socmed'=>'Linkedin', 'image'=>'http://dev.dagor.in/25bsm_app/public/assets/linkedin.png','title'=>'PT Bank Syariah Mandiri','url'=>'');
        // $result['hubungi']['socmed'] = json_decode($list['']);
        $query = "SELECT * FROM bsm_setting_slider where status = 1 order by order_slider asc";
        $results = $this->db->query($query);
        foreach ($results->result_array() as $key => $list)
        {
            $result['slider'][$key]['id']           = $list['id'];
            $result['slider'][$key]['title']        = $list['title'];
            $result['slider'][$key]['url']          = base_url().$list['image'];
            $result['slider'][$key]['description']  = $list['description'];
            $result['slider'][$key]['category']     = $list['content_category'];
            $result['slider'][$key]['content_id']   = $list['content_id'];
            $category = $list['content_category'];
            if(($category == "regulasi" || $category == "ketentuan_internal" || $category == "erm") || $category == "apuppt" &&  $list['content_id'] != ""){
                $result['slider'][$key]['content_url']  = base_url('api/v1/User/view_pdf?path=').$list['content_url']."&kat=$category&id=".$list['content_id'];
            }else if($category == "infografis"){
                $sql = "SELECT image from bsm_infografis WHERE id = ".$list['content_id'];
                $image = $this->db->query($sql)->row();
                $result['slider'][$key]['content_url']   = base_url().$image->image;
            }else{
                $result['slider'][$key]['content_url']  = $list['content_url'];
            }
            $result['slider'][$key]['image']        = base_url().$list['image'];
            $result['slider'][$key]['date']         = $list['created'];
        }

        $query = "SELECT * FROM bsm_config WHERE groups = 'Whitelist Domain'";
        $results = $this->db->query($query);
        foreach ($results->result_array() as $key => $list)
        {
            $result['whitelist'][$key]['domain']    = $list['catatan'];
        }

        $sql = "SELECT * FROM bsm_last_content";
        $results = $this->db->query($sql);
        foreach ($results->result_array() as $key => $list)
        {
            $result['last_content'][$list['field']]           = $list['last_content_id'];
        }
        
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }
    
    public function regulasi_last_get(){ 
        $this->benchmark->mark('code_start');
		$get_id_kategori = $this->_check_permission_last_content('REG00');
        $sql = "SELECT bsm_regulasi.*, bsm_config.deskripsi
            FROM
                bsm_regulasi
            LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_regulasi.id_kategori
            WHERE bsm_regulasi.status = 1 ";
		
		$sql .= " AND bsm_regulasi.id_kategori IN ('".$get_id_kategori."') ";
			
        $sql .= " ORDER BY tahun_terbit desc, id desc
            LIMIT 10";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0)
        {
            foreach ($results->result_array() as $key => $list)
            {
                $result[$key]['id']        = $list['id'];
                $result[$key]['kategori']  = $list['deskripsi'];
                $result[$key]['title']   = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
                $result[$key]['created']  = $this->reformat_date($list['created']);
                $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
            }
        }

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function regulasi_list_get(){
        $this->benchmark->mark('code_start');
		$groupcode = $this->get('kategori');
		$result = array();
		if($this->_check_permission($groupcode))
		{
			$keyword        = trim(urldecode($this->get('keyword')));
			$page = 1;
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}

			// set the number of items to display per page
			$items_per_page = 20;

			// build query
			$offset = ($page - 1) * $items_per_page;

            $sql = "SELECT bsm_regulasi.*, bsm_config.deskripsi
                FROM
                    bsm_regulasi
                LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_regulasi.id_kategori
                WHERE
                    status = 1
                    ";
            if($groupcode!="REG"){
                $sql .= " AND id_kategori = '".$groupcode."'";
            }
            if(!empty($keyword))
            {
                $sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
            }

            $sql .= " ORDER BY tahun_terbit desc
                        LIMIT $offset, $items_per_page";

            $results = $this->db->query($sql);
            if ($results->num_rows() > 0)
            {
                foreach ($results->result_array() as $key => $list)
                {
                    $result[$key]['id']        = $list['id'];
                    $result[$key]['kategori']  = $list['deskripsi'];
                    $result[$key]['title']   = $list['title'];
                    $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
                    $result[$key]['created']  = $this->reformat_date($list['created']);
                    $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
                }
			}
		}
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function regulasi_detail_get(){
        $this->benchmark->mark('code_start');
        $id = $this->get("id");
        $sql = "SELECT bsm_regulasi.*, bsm_config.deskripsi
            FROM
                bsm_regulasi
            LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_regulasi.id_kategori
            WHERE
                bsm_regulasi.id = $id";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0)
        {
            foreach ($results->result_array() as $key => $list)
            {
                $result[$key]['id']        = $list['id'];
                $result[$key]['kategori']  = $list['deskripsi'];
                $result[$key]['title']   = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
                $result[$key]['created']  = $this->reformat_date($list['created']);
                $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function apuppt_list_get(){
        $this->benchmark->mark('code_start');
		$groupcode = $this->get('kategori');
		$result = array();
		if($this->_check_permission($groupcode))
		{
			if(empty($groupcode)){
				$this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
			}
			$keyword        = trim(urldecode($this->get('keyword')));
			$page = 1;
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}
			$items_per_page = 20;
			$offset = ($page - 1) * $items_per_page;
			$sql = "SELECT bsm_apuppt.*, bsm_config.deskripsi
					FROM
						bsm_apuppt
					LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_apuppt.id_kategori
					WHERE status = 1 AND id_kategori = '".$groupcode."'";
			if(!empty($keyword))
			{
				$sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
			}

			$sql .= " ORDER BY id DESC
							LIMIT $offset, $items_per_page";
			
			$results = $this->db->query($sql);
			if($results->num_rows() > 0){
				foreach($results->result_array() as $key => $list){
					$result[$key]['id']        = $list['id'];
					$result[$key]['kategori']  = $list['deskripsi'];
					$result[$key]['title']   = $list['title'];
					$result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
					$result[$key]['created']  = $this->reformat_date($list['created']);
					// $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
				}
			}
		}
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function apuppt_detail_get(){

    }

	public function kinternal_last_get(){
        $this->benchmark->mark('code_start');
		$get_id_kategori = $this->_check_permission_last_content('KIN00');
        $sql = "SELECT bsm_kinternal.*, bsm_config.deskripsi 
            FROM
                bsm_kinternal
            LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_kinternal.id_kategori
            WHERE
                bsm_kinternal.status = 1 ";
		$sql .= " AND bsm_kinternal.id_kategori IN ('".$get_id_kategori."') ";
			
        $sql .= " ORDER BY tahun_terbit desc, id desc
            LIMIT 10";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0)
        {
            foreach ($results->result_array() as $key => $list)
            {
                $result[$key]['id']             = $list['id'];
                $result[$key]['kategori']       = $list['deskripsi'];
                $result[$key]['title']          = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=internal&id=".$list['id'];
                $result[$key]['created']        = $this->reformat_date($list['created']);
                $result[$key]['tahun_terbit']   = "Tahun terbit ".$list['tahun_terbit'];
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function kinternal_list_get(){
        $this->benchmark->mark('code_start');
		$groupcode = $this->get('kategori');
		$result = array();
		if($this->_check_permission($groupcode))
		{

			if(empty($groupcode)){
				$this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
			}
			
			$keyword        = trim(urldecode($this->get('keyword')));
			$page = 1;
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}
			// set the number of items to display per page
			$items_per_page = 20;
			// build query
			$offset = ($page - 1) * $items_per_page;

            $sql = "SELECT bsm_kinternal.*, bsm_config.deskripsi
                FROM
                    bsm_kinternal
                LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_kinternal.id_kategori
                WHERE
                    status = 1";
            if($groupcode!="KIN"){
                $sql .= " AND id_kategori = '".$groupcode."'";
            }
            if(!empty($keyword))
            {
              $sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
            }
            $sql .= " ORDER BY tahun_terbit DESC, id DESC
                        LIMIT $offset, $items_per_page";
            $results = $this->db->query($sql);
            if ($results->num_rows() > 0)
            {
                foreach ($results->result_array() as $key => $list)
                {
                    // $result[$key]['product_id']    = $list['ID'];
                    // $result[$key]['customer_id']   = $list['CustID'];
                    $result[$key]['id']        = $list['id'];
                    $result[$key]['kategori']  = $list['deskripsi'];
                    $result[$key]['title']   = $list['title'];
                    $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=internal&id=".$list['id'];
                    $result[$key]['created']  = $this->reformat_date($list['created']);
                    $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
                }
            }
		}
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function kinternal_detail_get(){
        $this->benchmark->mark('code_start');
        $id = $this->get("id");
        $sql = "SELECT bsm_kinternal.*, bsm_config.deskripsi
            FROM
            bsm_kinternal
            LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_kinternal.id_kategori
            WHERE
            bsm_kinternal.id = $id";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0)
        {
            foreach ($results->result_array() as $key => $list)
            {
                $result[$key]['id']        = $list['id'];
                $result[$key]['kategori']  = $list['deskripsi'];
                $result[$key]['title']     = $list['title'];
                $result[$key]['url']       = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
                $result[$key]['created']    = $this->reformat_date($list['created']);
                $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function erm_last_get(){
        $this->benchmark->mark('code_start');
                $get_id_kategori = $this->_check_permission_last_content('ERM00');
        $sql = "SELECT bsm_erm.*, bsm_config.deskripsi
            FROM
                bsm_erm
            LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_erm.id_kategori
            WHERE
                bsm_erm.status = 1 ";
		$sql .= " AND bsm_erm.id_kategori IN ('".$get_id_kategori."') ";
			
        $sql .= " ORDER BY tahun_terbit desc, id desc
            LIMIT 10";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0)
        {
            foreach ($results->result_array() as $key => $list)
            {
                $result[$key]['id']             = $list['id'];
                $result[$key]['kategori']       = $list['deskripsi'];
                $result[$key]['title']          = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=internal&id=".$list['id'];
                $result[$key]['created']        = $this->reformat_date($list['created']);
                $result[$key]['tahun_terbit']   = "Tahun terbit ".$list['tahun_terbit'];
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }
	
    public function erm_list_get(){
        $this->benchmark->mark('code_start');
		$groupcode = $this->get('kategori');
		$result = array();
		if($this->_check_permission($groupcode))
		{

			if(empty($groupcode)){
				$this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
			}
			$keyword        = trim(urldecode($this->get('keyword')));
			$page = 1;
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}
			// set the number of items to display per page
			$items_per_page = 20;
			// build query
			$offset = ($page - 1) * $items_per_page;
            $sql = "SELECT bsm_erm.*, bsm_config.deskripsi
                FROM
                    bsm_erm
                LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_erm.id_kategori
                WHERE
                    status = 1";
            if($groupcode!="ERM"){
                $sql .= " AND id_kategori = '".$groupcode."'";
            }
            if(!empty($keyword))
            {
              $sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
            }

            $sql .= " ORDER BY tahun_terbit DESC, id DESC
                        LIMIT $offset, $items_per_page";
            $results = $this->db->query($sql);
            if ($results->num_rows() > 0)
            {
                foreach ($results->result_array() as $key => $list)
                {
                    // $result[$key]['product_id']    = $list['ID'];
                    // $result[$key]['customer_id']   = $list['CustID'];
                    $result[$key]['id']        = $list['id'];
                    $result[$key]['kategori']  = $list['deskripsi'];
                    $result[$key]['title']   = $list['title'];
                    $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=internal&id=".$list['id'];
                    $result[$key]['created']  = $this->reformat_date($list['created']);
                    $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
                }
			}
		}
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    public function erm_detail_get(){
        $this->benchmark->mark('code_start');
        $id = $this->get("id");
        $sql = "SELECT bsm_erm.*, bsm_config.deskripsi
            FROM
            bsm_erm
            LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_erm.id_kategori
            WHERE
            bsm_erm.id = $id";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0)
        {
            foreach ($results->result_array() as $key => $list)
            {
                $result[$key]['id']        = $list['id'];
                $result[$key]['kategori']  = $list['deskripsi'];
                $result[$key]['title']     = $list['title'];
                $result[$key]['url']       = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
                $result[$key]['created']    = $this->reformat_date($list['created']);
                $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];

            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function document_list_get(){
        $this->benchmark->mark('code_start');
        $groupcode = $this->get('kategori');
        if(empty($groupcode)){
            $this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
        }
        // $kat = empty($groupcode) ? $groupcode : ;

        $keyword        = trim(urldecode($this->get('keyword')));

        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }

		// set the number of items to display per page
		$items_per_page = 20;

		// build query
        $offset = ($page - 1) * $items_per_page;

            $sql = "SELECT *
                FROM
                    bsm_document
                WHERE
                    status = 1
                AND
                    kategori = ".$groupcode."
                    ";

            if(!empty($keyword))
            {
              $sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
            }

            $sql .= " ORDER BY id desc
                        LIMIT $offset, $items_per_page";

            $results = $this->db->query($sql);
            $result = array();
            if ($results->num_rows() > 0)
            {
                foreach ($results->result_array() as $key => $list)
                {
                    // $result[$key]['product_id']    = $list['ID'];
                    // $result[$key]['customer_id']   = $list['CustID'];
                    $result[$key]['id']        = $list['id'];
                    $result[$key]['title']   = $list['title'];
                    $result[$key]['url'] = base_url('docs/pdf/').urlencode($list['filename']);
                    $result[$key]['created']  = $this->reformat_date($list['date_created']);
                    $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
                }
            }

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    //LOGIN
    public function create_auth_post(){
      $this->benchmark->mark('code_start');
      $email = $this->post('email');
      $password = $this->post('password'); //sudah di encode md5
      $imei = $this->post('imei');
      $playerid = $this->post('playerid');
      $result = array();
      
      
      $sql = "SELECT * FROM bsm_user WHERE email = '$email' and password = '$password'";
      $results = $this->db->query($sql);
      $list = $results->row();
      if(isset($list)){
          if($list->status == 1){
            $token = md5('x'.time());
            $result['status'] = 1;
            $result['reason'] = "Login Berhasil";
            $result['user']['id']       = $list->id;
            $result['user']['email']    = $list->email;
            $result['user']['name']     = $list->name;
            $result['user']['nip']      = $list->nip;
            $result['user']['jabatan']  = $list->jabatan;
            $result['user']['point']    = $list->poin;
            $result['user']['token']    = $token;
            $sql = "SELECT * from bsm_inbox where user_id = $list->id ORDER BY id desc";
            $results = $this->db->query($sql);
            if ($results->num_rows() > 0){
                foreach ($results->result_array() as $key => $ls){
                    $result['inbox'][$key]['id']             = $ls['id'];
                    $result['inbox'][$key]['title']          = $ls['title'];
                    $result['inbox'][$key]['highlight']      = $ls['highlight'];
                    $result['inbox'][$key]['description']    = $ls['body'];
                    $result['inbox'][$key]['date']           = $this->reformat_date($ls['created']);
                    $result['inbox'][$key]['isRead']         = $ls['is_read'];
                }
            }
            $this->insertlogged($list->id,$token,$imei,$playerid);
            
          }else{
            $result['status'] = 0;
            $result['reason'] = "Akun anda sudah tidak berlaku";
          }
      }else{
        $result['status'] = 0;
        $result['reason'] = "Username atau Password Salah";
      }

      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
	}
	
	public function reset_password_post() {
		$this->benchmark->mark('code_start');
		// Collect email and validation
		$email = $email = $this->post('email');
		// Try to find user data
		$user = $this->db->get_where('bsm_user', array('email' => $email))->row_array(0);

		// User doesnt exist
		if( count($user) <= 0 )
		{
			$response = array('status' => 0,
						'message' => 'Mohon maaf email yang Anda masukan salah / tidak terdaftar'
				);
		}
		else
		{
			$password = substr(uniqid(),0,8);
			$curl = curl_init();

				curl_setopt_array($curl, array(
				CURLOPT_URL => "http://dev.dagor.in/25bsm_app/api/v2/user/sender_mail?email=$email&password=$password",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				));

				$responses = curl_exec($curl);
				$err = curl_error($curl);

				// curl_close($curl);

				if ($err) {
					$response = array(
						'status' => 0,
						'message' => 'Ops! Terjadi masalah saat mengirim email, silahkan coba beberapa saat lagi.'
					);
				} 
				else 
				{
					$resp = json_decode($responses);
					if($resp->status == 1)
					{
						$this->db->query("UPDATE bsm_user SET password = '".md5($password)."' WHERE email = '$email'");
						$response = array(
							'status' => 1,
							'message' => 'Password baru berhasil dikirim, silahkan cek email Anda'
						);
					}
					else
					{
						$response = array(
							'status' => 0,
							'message' => 'Ops! It was not possible to send the email message. Check the email settings and try again.'
						);
					}
				}


			// $args['user'] = $user;
			// $args['email'] = get_value($user, 'email');
			// $args['key_access'] = md5(uniqid());
			
            // $this->db->query("UPDATE bsm_user SET password = '".md5($password)."' WHERE email = '$email'");
			// // sending verify email
			// if($this->mail->sender($email, $this->mail_template->template_mail_reset_password($email,$password)))
			// {
			// 	$response = array(
			// 		'status' => 1,
			// 		'message' => 'Password baru berhasil dikirim, silahkan cek email Anda'
			// 	);
			// }
			// else
			// {
			// 	$response = array(
			// 		'status' => 0,
			// 		'message' => 'Ops! It was not possible to send the email message. Check the email settings and try again.'
			// 	);
			// }
		}
		$this->benchmark->mark('code_end');
     	$this->response($this->_show_response_200($response), REST_Controller::HTTP_OK);
    }
    
    public function change_password_post() {
		$this->benchmark->mark('code_start');
		// Collect email and validation
		$oldPassword = $this->post('old_password');
		$newPassword = $this->post('new_password');
        $userid = $this->getUserDetail();
		// Try to find user data
		$user = $this->db->get_where('bsm_user', array('id' => $userid,'password' => $oldPassword))->row_array(0);
		// User doesnt exist
		if( count($user) <= 0 )
		{
			$response = array(
                'status' => 0,
				'message' => 'Password lama tidak cocok'
			);
		}
		else
		{
            $this->db->query("UPDATE bsm_user SET password = '$newPassword' WHERE id = $userid");
			$response = array(
				'status' => 1,
				'message' => 'Password berhasil diubah'
			);
			
		}
		$this->benchmark->mark('code_end');
     	$this->response($this->_show_response_200($response), REST_Controller::HTTP_OK);
	}

    public function slider_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        // for($i = 0; $i < 3;$i++){
        //     $a = $i+1;
        $i = 0;
            $result[$i]['id']       = $i;
            $result[$i]['title']    = "Slider 1";
            $result[$i]['url']    = "Slider 1 URLNYA KEMANA";
            $result[$i]['description']    = "aman aman";
            $result[$i]['category']    = "Info";
            $result[$i]['image']    = base_url('public/banner/')."homebanner.jpg";
            $result[$i]['date']     = "";
        // }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function faq_list_get(){
        $this->benchmark->mark('code_start');
		$result = array();
		// if($this->_check_permission('faq'))
		// {
			$keyword        = trim(urldecode($this->get('keyword')));
			$filter         = $this->get('filter');
			$page = 1;
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}

			// set the number of items to display per page
			$items_per_page = 10;

			// build query
			$offset = ($page - 1) * $items_per_page;

				$sql = "SELECT * FROM bsm_faq WHERE status = 1";
				if(!empty($keyword))
				{
				$sql .= " AND (question LIKE '%".$keyword."%') ";
				}

				if($filter != "Semua"){
					$sql .= " AND kategori = '$filter' ";
				}

				$sql .= " ORDER BY id desc
							LIMIT $offset, $items_per_page";
			$results = $this->db->query($sql);
			if ($results->num_rows() > 0){
				foreach ($results->result_array() as $key => $list){
					$result[$key]['id']         = $list['id'];
					$result[$key]['question']   = $list['question'];
					$result[$key]['answer']     = $list['answer'];
				}
			}
		// }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function faq_ajukan_pertanyaan_post(){
        $result = array();
        $id = $this->getUserDetail();
        $subject = $this->post('subject');
        $tanya = $this->post('pertanyaan');
        $data = array();
        $data['subject'] = $subject;
        $data['question'] = $tanya;
        $data['created_by'] = $id;
        $data['status'] = 0;
        $data['created'] = date("Y-m-d H:i:s");
        $this->db->insert("bsm_inbox_question",$data);
        $result['status'] = 1;
        $result['message'] = "Pesan terkirim";
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function cpg_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
		if($this->_check_permission('galeri'))
		{
			$userid = $this->getUserDetail();
			$page = 1;
			$keyword        = trim(urldecode($this->get('keyword')));
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}
			$items_per_page = 20;
			$offset = ($page - 1) * $items_per_page;
			$sql = "SELECT *, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_infocpg.id and category = 2) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_infocpg.id and category = 2 and user_id = $userid) as is_liked from bsm_infocpg where status = 1";
			if(!empty($keyword))
			{
				$sql .= " AND caption LIKE '%".$keyword."%' ";
			}
			$sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
			
			$results = $this->db->query($sql);
			if ($results->num_rows() > 0){
				foreach ($results->result_array() as $i => $list){  
					$result[$i]['id']       = $list['id'];
					$result[$i]['title']    = $list['caption'];
					$result[$i]['image']    = base_url().$list['image'];
					$result[$i]['date']     = $this->reformat_date($list['created']);
					$result[$i]['liked']    = $list['liked'];
					$result[$i]['is_liked'] = $list['is_liked'];
				}
			}
		}
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function video_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
		if($this->_check_permission('video'))
		{
			$userid = $this->getUserDetail();
			$keyword        = trim(urldecode($this->get('keyword')));
			$page = 1;
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}

			// set the number of items to display per page
			$items_per_page = 10;

			// build query
			$offset = ($page - 1) * $items_per_page;
			$sql = "SELECT *, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_video.id and category = 3) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_video.id and category = 3 and user_id = $userid) as is_liked  from bsm_video where status = 1";
			// $sql = "SELECT * from bsm_video where status = 1";
			if(!empty($keyword))
				{
				$sql .= " AND title LIKE '%".$keyword."%'";
				}
			$sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
			
			$results = $this->db->query($sql);
			if ($results->num_rows() > 0){
				foreach ($results->result_array() as $key => $list){
					$result[$key]['id']             = $list['id'];
					$result[$key]['title']          = $list['title'];
					$result[$key]['desc']           = $list['description'];
					$result[$key]['ytvideo_id']     = $list['ytvideo_id'];
					$result[$key]['source']         = "https://img.youtube.com/vi/".$list['ytvideo_id']."/mqdefault.jpg";
					$result[$key]['views']          = $list['views'];
					$result[$key]['likes']          = $list['liked'];
					$result[$key]['is_liked']       = $list['is_liked'];
					$result[$key]['date']           = $this->reformat_date($list['created']);
				}
			}
		}
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    
    }

    public function video_detail_get(){
        $this->benchmark->mark('code_start');
        $userid = $this->getUserDetail();
        $result = array();
        $videoid = $this->get("video_id");
        $sql = "SELECT *, (SELECT COUNT(id) as liked FROM bsm_like_content WHERE content_id = bsm_video.id and category = 3) as liked, (SELECT count(id) FROM bsm_like_content WHERE content_id = bsm_video.id and category = 3 and user_id = $userid) as is_liked  FROM bsm_video WHERE id = $videoid AND status = 1";
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']             = $list['id'];
                $result[$key]['title']          = $list['title'];
                $result[$key]['desc']           = $list['description'];
                $result[$key]['ytvideo_id']     = $list['ytvideo_id'];
                $result[$key]['source']         = "https://img.youtube.com/vi/".$list['ytvideo_id']."/mqdefault.jpg";
                $result[$key]['views']          = $list['views'];
                $result[$key]['likes']          = $list['liked'];
                $result[$key]['is_liked']       = $list['is_liked'];
                $result[$key]['date']           = $this->reformat_date($list['created']);
            }
        }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    
    }

    public function infografis_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
		if($this->_check_permission('infografis'))
		{
			$userid = $this->getUserDetail();
			$page = 1;
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}
			$items_per_page = 20;
			$offset = ($page - 1) * $items_per_page;
			
			$sql = "SELECT *, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_infografis.id and category = 4) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_infografis.id and category = 4 and user_id = $userid) as is_liked from bsm_infografis WHERE status = 1";

			$keyword        = trim(urldecode($this->get('keyword')));
			if(!empty($keyword))
			{
				$sql .= " AND title LIKE '%".$keyword."%'";
			}

			$sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
			$results = $this->db->query($sql);
			if ($results->num_rows() > 0){
				foreach ($results->result_array() as $key => $list){
					$result[$key]['id']         = $list['id'];
					$result[$key]['title']      = $list['title'];
					$result[$key]['desc']       = $list['description'];
					$result[$key]['source']     = base_url().$list['image'];
					$result[$key]['date']       = $this->reformat_date($list['created']);
					$result[$key]['likes']      = $list['liked'];
					$result[$key]['is_liked']   = $list['is_liked'];
				}
			}
		}
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function literasi_search_get(){
        $this->benchmark->mark('code_start');
        $keyword = trim(urldecode($this->get('keyword')));
        $result = array();
        $userid = $this->getUserDetail();
        //FETCH APUPPT

		if($this->_check_permission('apuppt'))
		{
        $sql = "SELECT bsm_apuppt.*, bsm_config.deskripsi
        FROM
            bsm_apuppt
        LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_apuppt.id_kategori
                
                WHERE status = 1 AND title LIKE '%".$keyword."%'";
        $sql .= " ORDER BY id DESC";
        
        $result =array();
        $results = $this->db->query($sql);
        if($results->num_rows() > 0){
            foreach($results->result_array() as $key => $list){
                $result['apuppt'][$key]['id']        = $list['id'];
                $result['apuppt'][$key]['kategori']  = $list['deskripsi'];
                $result['apuppt'][$key]['title']   = $list['title'];
                $result['apuppt'][$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=apuppt&id=".$list['id'];
                $result['apuppt'][$key]['created']  = $this->reformat_date($list['created']);
                // $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
            }
        }
    }
        //FETCH VIDEO

		if($this->_check_permission('video'))
		{
        $sql = "SELECT *, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_video.id and category = 3) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_video.id and category = 3 and user_id = $userid) as is_liked  from bsm_video where status = 1 AND title LIKE '%".$keyword."%'";
        $sql .= " ORDER BY id desc";
        $res_regulasi = $this->db->query($sql);
        if($res_regulasi->num_rows() > 0){
            foreach($res_regulasi->result_array() as $key => $list){
                $result['video'][$key]['id']             = $list['id'];
                $result['video'][$key]['title']          = $list['title'];
                $result['video'][$key]['desc']           = $list['description'];
                $result['video'][$key]['ytvideo_id']     = $list['ytvideo_id'];
                $result['video'][$key]['source']         = "https://img.youtube.com/vi/".$list['ytvideo_id']."/mqdefault.jpg";
                $result['video'][$key]['views']          = $list['views'];
                $result['video'][$key]['likes']          = $list['liked'];
                $result['video'][$key]['is_liked']       = $list['is_liked'];
                $result['video'][$key]['date']           = $this->reformat_date($list['created']);
            }
        }
    }

		if($this->_check_permission('infografis'))
		{
        $sql = "SELECT *, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_infografis.id and category = 4) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_infografis.id and category = 4 and user_id = $userid) as is_liked from bsm_infografis WHERE status = 1 AND title LIKE '%".$keyword."%'";
        $sql .= " ORDER BY id desc";
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result['infografis'][$key]['id']         = $list['id'];
                $result['infografis'][$key]['title']      = $list['title'];
                $result['infografis'][$key]['desc']       = $list['description'];
                $result['infografis'][$key]['source']     = base_url().$list['image'];
                $result['infografis'][$key]['date']       = $this->reformat_date($list['created']);
                $result['infografis'][$key]['likes']      = $list['liked'];
                $result['infografis'][$key]['is_liked']   = $list['is_liked'];
            }
        }
    }

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    public function literasi_last_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        $userid = $this->getUserDetail();
        //FETCH APUPPT
        
		if($this->_check_permission('apuppt'))
		{
        $sql = "SELECT bsm_apuppt.*, bsm_config.deskripsi
        FROM
            bsm_apuppt
        LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_apuppt.id_kategori
                
                WHERE status = 1";
        $sql .= " ORDER BY id DESC LIMIT 5";
        
        $result =array();
        $results = $this->db->query($sql);
        if($results->num_rows() > 0){
            foreach($results->result_array() as $key => $list){
                $result['apuppt'][$key]['id']        = $list['id'];
                $result['apuppt'][$key]['kategori']  = $list['deskripsi'];
                $result['apuppt'][$key]['title']   = $list['title'];
                $result['apuppt'][$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=apuppt&id=".$list['id'];
                $result['apuppt'][$key]['created']  = $this->reformat_date($list['created']);
                // $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
            }
        }
    }
        //FETCH VIDEO

		if($this->_check_permission('video'))
		{
        $sql = "SELECT *, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_video.id and category = 3) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_video.id and category = 3 and user_id = $userid) as is_liked  from bsm_video where status = 1";
        $sql .= " ORDER BY id desc LIMIT 5";
        $res_regulasi = $this->db->query($sql);
        if($res_regulasi->num_rows() > 0){
            foreach($res_regulasi->result_array() as $key => $list){
                $result['video'][$key]['id']             = $list['id'];
                $result['video'][$key]['title']          = $list['title'];
                $result['video'][$key]['desc']           = $list['description'];
                $result['video'][$key]['ytvideo_id']     = $list['ytvideo_id'];
                $result['video'][$key]['source']         = "https://img.youtube.com/vi/".$list['ytvideo_id']."/mqdefault.jpg";
                $result['video'][$key]['views']          = $list['views'];
                $result['video'][$key]['likes']          = $list['liked'];
                $result['video'][$key]['is_liked']       = $list['is_liked'];
                $result['video'][$key]['date']           = $this->reformat_date($list['created']);
            }
        }
    }

		if($this->_check_permission('infografis'))
		{
        $sql = "SELECT *, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_infografis.id and category = 4) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_infografis.id and category = 4 and user_id = $userid) as is_liked from bsm_infografis WHERE status = 1";
        $sql .= " ORDER BY id desc LIMIT 5";
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result['infografis'][$key]['id']         = $list['id'];
                $result['infografis'][$key]['title']      = $list['title'];
                $result['infografis'][$key]['desc']       = $list['description'];
                $result['infografis'][$key]['source']     = base_url().$list['image'];
                $result['infografis'][$key]['date']       = $this->reformat_date($list['created']);
                $result['infografis'][$key]['likes']      = $list['liked'];
                $result['infografis'][$key]['is_liked']   = $list['is_liked'];
            }
        }
    }

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    public function search_engine_get(){
        $this->benchmark->mark('code_start');
        $keyword = trim(urldecode($this->get('keyword')));
        $result = array();

        $key = 0;

		//FETCH REGULASI
		$kat_regulasi = $this->_check_permission_last_content('REG00');
        $sql_regulasi = "SELECT id, title, attachment FROM `bsm_regulasi` WHERE title LIKE '%$keyword%' AND id_kategori IN ('".$kat_regulasi."') AND status = 1 LIMIT 10";
        $res_regulasi = $this->db->query($sql_regulasi);
        if($res_regulasi->num_rows() > 0){
            foreach($res_regulasi->result_array() as $keys => $list){
                $result[$key]['id'] = $list['id'];
                $result[$key]['category'] = "Regulasi";
                $result[$key]['category_id'] = "1";
                $result[$key]['title'] = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
                $result[$key]['extra'] = "";
                $key++;
            }
        }

		//FETCH KETENTUAN INTERNAL
		$kat_kinternal = $this->_check_permission_last_content('KIN00');
        $sql_kinternal = "SELECT id, title, attachment  FROM `bsm_kinternal` WHERE title LIKE '%$keyword%' AND id_kategori IN ('".$kat_kinternal."') AND status = 1 LIMIT 10";
        $res_kinternal = $this->db->query($sql_kinternal);
        if($res_kinternal->num_rows() > 0){
            foreach($res_kinternal->result_array() as $keys => $list){
                $result[$key]['id'] = $list['id'];
                $result[$key]['category'] = "Ketentuan Internal";
                $result[$key]['category_id'] = "2";
                $result[$key]['title'] = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=internal&id=".$list['id'];
                $result[$key]['extra'] = "";
                $key++;
            }
        }

		//FETCH ERM
		$kat_erm = $this->_check_permission_last_content('ERM00');
        $sql_erm = "SELECT id, title, attachment FROM `bsm_erm` WHERE title LIKE '%$keyword%' AND id_kategori IN ('".$kat_erm."') AND status = 1 LIMIT 10";
        $res_erm = $this->db->query($sql_erm);
        if($res_erm->num_rows() > 0){
            foreach($res_erm->result_array() as $keys => $list){
                $result[$key]['id'] = $list['id'];
                $result[$key]['category'] = "ERM";
                $result[$key]['category_id'] = "1";
                $result[$key]['title'] = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=erm&id=".$list['id'];
                $result[$key]['extra'] = "";
                $key++;
            }
        }

		//FETCH APUPPT
		$kat_apuppt = $this->_check_permission_last_content('APUPPT');
        $sql_apuppt = "SELECT id, title, attachment FROM `bsm_apuppt` WHERE title LIKE '%$keyword%' AND id_kategori IN ('".$kat_apuppt."') AND status = 1 LIMIT 10";
        $res_apuppt = $this->db->query($sql_apuppt);
        if($res_apuppt->num_rows() > 0){
            foreach($res_apuppt->result_array() as $keys => $list){
                $result[$key]['id'] = $list['id'];
                $result[$key]['category'] = "APUPPT";
                $result[$key]['category_id'] = "1";
                $result[$key]['title'] = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=apuppt&id=".$list['id'];
                $result[$key]['extra'] = "";
                $key++;
            }
        }

		//FETCH ARTIKEL
		if($this->_check_permission('artikel'))
		{
			$sql_artikel = "SELECT id, title FROM `bsm_artikel` WHERE title LIKE '%$keyword%' AND status = 1 LIMIT 10";
			$res_artikel = $this->db->query($sql_artikel);
			if($res_artikel->num_rows() > 0){
				foreach($res_artikel->result_array() as $keys => $list){
					$result[$key]['id'] = $list['id'];
					$result[$key]['category'] = "Artikel";
					$result[$key]['category_id'] = "3";
					$result[$key]['title'] = $list['title'];
					$result[$key]['url'] = "";
					$result[$key]['extra'] = "";
					$key++;
				}
			}
		}

        //FETCH NEWS
        $sql_news = "SELECT id, title FROM `bsm_news` WHERE title LIKE '%$keyword%' AND status = 1 LIMIT 10";
        $res_news = $this->db->query($sql_news);
        if($res_news->num_rows() > 0){
            foreach($res_news->result_array() as $keys => $list){
                $result[$key]['id'] = $list['id'];
                $result[$key]['category'] = "News";
                $result[$key]['category_id'] = "5";
                $result[$key]['title'] = $list['title'];
                $result[$key]['url'] = "";
                $result[$key]['extra'] = "";
                $key++;
            }
        }


		//FETCH VIDEO
		if($this->_check_permission('video'))
		{
                        $sql_video = "SELECT id, title FROM `bsm_video` WHERE title LIKE '%$keyword%' AND status = 1 LIMIT 10";
                        $res_video = $this->db->query($sql_video);
                        if($res_video->num_rows() > 0){
                                foreach($res_video->result_array() as $keys => $list){
                                        $result[$key]['id'] = $list['id'];
                                        $result[$key]['category'] = "Video";
                                        $result[$key]['category_id'] = "4";
                                        $result[$key]['title'] = $list['title'];
                                        $result[$key]['url'] = "";
                                        $result[$key]['extra'] = "";
                                        $key++;
				}
			}
		}

        // FETCH FAQ
        $sql_faq = "SELECT * FROM `bsm_faq` WHERE question LIKE '%$keyword%' AND status = 1 LIMIT 10";
        $res_gallery = $this->db->query($sql_faq);
        if($res_gallery->num_rows() > 0){
            foreach($res_gallery->result_array() as $keys => $list){
                $result[$key]['id']             = $list['id'];
                $result[$key]['category']       = "FAQ";
                $result[$key]['category_id']    = "7";
                $result[$key]['title']          = $list['question'];
                $result[$key]['url']            = $list['answer'];
                $result[$key]['extra']          = "";
                $key++;
            }
        }

		//FETCH GALERY
		if($this->_check_permission('galeri'))
		{
			$sql_gallery = "SELECT * FROM `bsm_infocpg` WHERE caption LIKE '%$keyword%' AND status = 1 LIMIT 10";
			$res_gallery = $this->db->query($sql_gallery);
			if($res_gallery->num_rows() > 0){
				foreach($res_gallery->result_array() as $keys => $list){
					$result[$key]['id']             = $list['id'];
					$result[$key]['category']       = "GALLERY";
					$result[$key]['category_id']    = "6";
					$result[$key]['title']          = $list['caption'];
					$result[$key]['url']            = base_url().$list['image'];
					$result[$key]['extra']          = "";
					$key++;
				}
			}
		}

		//FETCH INFOGRAFIS
		if($this->_check_permission('infografis'))
		{
			$sql_infografis = "SELECT * FROM `bsm_infografis` WHERE title LIKE '%$keyword%' AND status = 1 LIMIT 10";
			$res_infografis = $this->db->query($sql_infografis);
			if($res_infografis->num_rows() > 0){
				foreach($res_infografis->result_array() as $keys => $list){
					$result[$key]['id']             = $list['id'];
					$result[$key]['category']       = "INFOGRAFIS";
					$result[$key]['category_id']    = "6";
					$result[$key]['title']          = $list['title'];
					$result[$key]['url']            = base_url().$list['image'];
					$result[$key]['extra']          = "";
					$key++;
				}
			}
		}

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    public function news_get(){
        $this->benchmark->mark('code_start');
        $userid = $this->getUserDetail();
        $result = array();
    
        $sql = "SELECT id, title, image, created, views,sumber, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_news.id and category = 5) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_news.id and category = 5 and user_id = $userid) as is_liked from bsm_news where status = 1 ORDER BY id DESC LIMIT 3";
    
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']         = $list['id'];
                $result[$key]['title']      = $list['title'];
                $result[$key]['image']      = base_url(). $list['image'];
                $result[$key]['sumber']     = $list['sumber'];
                $result[$key]['content']    = "";
                $result[$key]['views']      = $list['views'];
                $result[$key]['likes']      = $list['liked'];
                $result[$key]['is_liked']   = $list['is_liked'];
                $result[$key]['date']       = $this->reformat_date($list['created']);
            }
        }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function news_list_get(){
        $this->benchmark->mark('code_start');
        $userid = $this->getUserDetail();
        $result = array();
        $keyword        = trim(urldecode($this->get('keyword')));
        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }

		// set the number of items to display per page
		$items_per_page = 10;

		// build query
        $offset = ($page - 1) * $items_per_page;
        $sql = "SELECT id, title, image, created, views, sumber,(SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_news.id and category = 5) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_news.id and category = 5 and user_id = $userid) as is_liked from bsm_news where status = 1";
        // $sql = "SELECT id, title, image, created, from bsm_artikel where status = 1";
        if(!empty($keyword))
            {
              $sql .= " AND title LIKE '%".$keyword."%'";
            }
        $sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
        
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']         = $list['id'];
                $result[$key]['title']      = $list['title'];
                $result[$key]['image']      = base_url(). $list['image'];
                $result[$key]['sumber']     = $list['sumber'];
                $result[$key]['content']    = "";
                $result[$key]['views']      = $list['views'];
                $result[$key]['likes']      = $list['liked'];
                $result[$key]['is_liked']   = $list['is_liked'];
                $result[$key]['date']       = $this->reformat_date($list['created']);
            }
        }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    public function news_detail_get(){
        $this->benchmark->mark('code_start');
        $userid = $this->getUserDetail();
        $id = $this->get('news_id');
        if(empty($id)){
            $this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
        }
        $sql = "SELECT *,(SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_news.id and category = 5) as likes, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_news.id and category = 5 and user_id = $userid) as is_liked FROM bsm_news WHERE id = $id";
        $result = $this->db->query($sql);
        $row = $result->row();
        $res = array();
        $res['id'] = $row->id;
        $res['title'] = $row->title;
        $res['image'] = base_url().$row->image;
        $res['sumber'] = $row->sumber;
        $res['content'] = $row->content;
        $res['views']      = $row->views;
        $res['likes']      = $row->likes;
        $res['is_liked']   = $row->is_liked;
        $res['date'] = $this->reformat_date($row->created);

        $this->db->query("UPDATE bsm_news SET views = views+1 WHERE id = $id");

      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($res), REST_Controller::HTTP_OK);

    }

    public function artikel_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
		if($this->_check_permission('artikel'))
		{
			$userid = $this->getUserDetail();
			$keyword        = trim(urldecode($this->get('keyword')));
			$page = 1;
			if($this->get('page') != NULL)
			{
				$page = $this->get('page');
			}

			// set the number of items to display per page
			$items_per_page = 10;

			// build query
			$offset = ($page - 1) * $items_per_page;
			$sql = "SELECT id, title, image, created, views, (SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_artikel.id and category = 1) as liked, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_artikel.id and category = 1 and user_id = $userid) as is_liked from bsm_artikel where status = 1";
			// $sql = "SELECT id, title, image, created, from bsm_artikel where status = 1";
			if(!empty($keyword))
				{
				$sql .= " AND title LIKE '%".$keyword."%'";
				}
			$sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
			
			$results = $this->db->query($sql);
			if ($results->num_rows() > 0){
				foreach ($results->result_array() as $key => $list){
					$result[$key]['id']         = $list['id'];
					$result[$key]['title']      = $list['title'];
					$result[$key]['image']      = base_url(). $list['image'];
					$result[$key]['content']    = "";
					$result[$key]['views']      = $list['views'];
					$result[$key]['likes']      = $list['liked'];
					$result[$key]['is_liked']   = $list['is_liked'];
					$result[$key]['date']       = $this->reformat_date($list['created']);
				}
			}
		}
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    public function artikel_detail_get(){
        $this->benchmark->mark('code_start');
        $userid = $this->getUserDetail();
        $id = $this->get('artikel_id');
        if(empty($id)){
            $this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
        }
        $sql = "SELECT *,(SELECT COUNT(id) as liked from bsm_like_content WHERE content_id = bsm_artikel.id and category = 1) as likes, (SELECT count(id) from bsm_like_content WHERE content_id = bsm_artikel.id and category = 1 and user_id = $userid) as is_liked FROM bsm_artikel WHERE id = $id";
        $result = $this->db->query($sql);
        $row = $result->row();
        $res = array();
        $res['id'] = $row->id;
        $res['title'] = $row->title;
        $res['image'] = base_url().$row->image;
        $res['content'] = $row->content;
        $res['views']      = $row->views;
        $res['likes']      = $row->likes;
        $res['is_liked']   = $row->is_liked;
        $res['date'] = $this->reformat_date($row->created);

        $this->db->query("UPDATE bsm_artikel SET views = views+1 WHERE id = $id");

      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($res), REST_Controller::HTTP_OK);

    }

    public function video_update_viewer_get(){
        $this->benchmark->mark('code_start');
        $id     = $this->get('video_id');
        if(empty($id)){
            $this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
        }
        $this->db->query("UPDATE bsm_video SET views = views+1 WHERE id = $id");
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($res), REST_Controller::HTTP_OK);
    }

    public function lapor_gratifikasi_post(){
        $this->benchmark->mark('code_start');
        $userid    = $this->getUserDetail();
        $lokasi     = $this->post('lokasi');
        $jenis      = $this->post('jenis');
        $harga      = $this->post('harga');
        $peristiwa  = $this->post('peristiwa');
        $nama       = $this->post('nama');
        $posisi     = $this->post('posisi');
        $hubungan   = $this->post('hubungan');
        $alamat     = $this->post('alamat');
        $kronologi  = $this->post('kronologi');
        $image      = $this->post('image');

        $path_file_address = 'upload/gratifikasi/'.date('Y').'/'.date('m');
		if (!is_dir($path_file_address))
			mkdir($path_file_address, 0755, TRUE);
		$foto_address = $path_file_address.'/'.$userid.date('Ymdhis');

		$foto_images = $this->post('image');
		if (! empty($foto_images))
		{
			$foto_decode = base64_decode($foto_images);

			$file_address = $foto_address.'.jpg';
			// $file_address22 = $path_file_address.'/001201805150006-event.jpg'; //test
            write_file($file_address, $foto_decode);

            $data['lokasi']         = $lokasi;
            $data['jenis']          = $jenis;
            $data['harga']          = $harga;
            $data['peristiwa']      = $peristiwa;
            $data['nama']           = $nama;
            $data['posisi']         = $posisi;
            $data['hubungan']       = $hubungan;
            $data['alamat']         = $alamat;
            $data['kronologi']      = $kronologi;
            $data['image']          = $file_address;
            $data['status']     	= 0;
            $data['created_by']     = $userid;
            $data['created']     	= date('YmdHis');
            $data['updated_by']     = $userid;
            $this->db->insert('bsm_gratifikasi', $data);
            
            $result = array();

            $result['status'] = 1;
            $result['message'] = "Pesan terkirim";
            $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
        }
    }

    public function hubungi_get(){
        $this->benchmark->mark('code_start');
        $query = "SELECT * FROM bsm_config WHERE groups = 'Hubungi Kami'";
        $res = $this->db->query($query);
        $row = $res->result_array();

        $result = array();

        $result['alamata']   = date('Ymdhis');
        $result['alamat']   = $row[0]['catatan'];
        $result['email']   = $row[1]['catatan'];
        $result['phone']   = $row[2]['catatan'];
        $result['web']      = "https://www.syariahmandiri.co.id/";
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function inbox_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        $last = 0;
        $last = $this->get('lastid');
        $userid         = $this->getUserDetail();
        $sql = "SELECT * from bsm_inbox where user_id = $userid and id > ".$last;
        $sql .= " ORDER BY id desc";
        
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']             = $list['id'];
                $result[$key]['title']          = $list['title'];
                $result[$key]['highlight']      = $list['highlight'];
                $result[$key]['description']    = $list['body'];
                $result[$key]['date']           = $this->reformat_date($list['created']);
                $result[$key]['isRead']         = $list['is_read'];
            }
        }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function reward_list_get(){
        $this->benchmark->mark('code_start');
        $sql = "SELECT * from bsm_setting_reward WHERE status = 1 AND is_deleted = 0 ORDER BY id desc";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']       = $list['id'];
                $result[$key]['title']    = $list['description'];
                $result[$key]['image']    = base_url().$list['image'];
                $result[$key]['poin']     = $list['poin'];
                $result[$key]['description']    = $list['description'];
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function history_reward_list_get(){
        $this->benchmark->mark('code_start');
        $userid = $this->getUserDetail();
        $sql = "SELECT a.id_reward, a.status as tukar_status, b.* from bsm_inbox_tukarpoin as a INNER JOIN bsm_setting_reward as b ON b.id = a.id_reward WHERE id_user = $userid";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']             = $list['id'];
                $result[$key]['title']          = $list['description'];
                $result[$key]['image']          = base_url().$list['image'];
                $result[$key]['poin']           = $list['poin'];
                $result[$key]['description']    = $list['description'];
                $result[$key]['status']         = $list['tukar_status'];
                $status = $list['tukar_status'];
                if($status == 0){
                    $status_text = "Belum diproses";
                }else if($status == 1){
                    $status_text = "Sedang diproses";
                }else{
                    $status_text = "Berhasil diproses";
                }
                $result[$key]['status_text']    = $status_text;
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

# question
# gratifikasi
# rewardpoin
# quiz
    public function quiz_participation_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $data = array();
        $userid = $this->getUserDetail();
        $quizid = $this->post("id_quiz");
        $data['id_quiz'] = $quizid;
        $data['id_user'] = $userid;
        $data['created_by'] = $userid;
        $poin = $this->post("poin");
        $data['answer_user'] = $this->post("answer");
        $data['status'] = $this->post("status");
        $data['created'] = date("Y-m-d H:i:s");
        $this->db->insert("bsm_quiz_participation",$data);
        if($data['status'] == 0){
            $inbox['user_id']   = $userid;
            $inbox['ref_id']    = $this->post("id_quiz");
            $inbox['title']     = "Selamat anda mendapatkan ".$poin." poin";
            $inbox['body']      = "Anda mendapatkan poin dengan menjawab quiz";
            $inbox['type']      = "#quiz";
            $this->db->insert("bsm_inbox",$inbox);
            $this->post_mutation($userid, $quizid, 0, $poin, "Berhasil Menjawab Quiz #".$quizid, "QUIZ");
        }
        $this->benchmark->mark('code_end');
        $result['status'] = 1;
        $result['message'] = "Pesan terkirim";
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function like_content_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $content_id = $this->post("content_id");
        $category = $this->post('category');
        $userid = $this->getUserDetail();
        /*
        1. Artikel
        2. Galeri
        3. Video
        */
        $data['category'] = $category;
        $data['content_id'] = $content_id;
        $data['user_id'] = $userid;
        $data['date_created'] = date("Y-m-d H:i:s");
        $this->db->insert("bsm_like_content",$data);
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function tukar_poin_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $data = array();
        $userid         = $this->getUserDetail();
        $rewardid       = $this->post("reward_id");
        $poin           = $this->post("poin");
        $data['id_reward']  = $rewardid;
        $data['id_user']    = $userid;
        $data['status']     = 0;
        $data['created']    = date('YmdHis');
        $data['created_by'] = $userid;
        $data['updated']    = date('YmdHis');
        $this->db->insert("bsm_inbox_tukarpoin",$data);
        $this->post_mutation($userid, $rewardid, $poin, 0, "Tukar Poin Reward #".$rewardid, "REWARD");

        $this->benchmark->mark('code_end');
        $result['status'] = 1;
        $result['message'] = "Pesan terkirim";
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function set_read_inbox_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $userid         = $this->getUserDetail();
        $inbox_id       = $this->post("inbox_id");
        $this->db->query("UPDATE bsm_inbox SET is_read = 1 WHERE id = $inbox_id");
        $result['status'] = 1;
        $result['message'] = "Pesan terkirim";
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function delete_inbox_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $inbox_id       = $this->post("inbox_id");
        $this->db->query("DELETE FROM `bsm_inbox` WHERE `bsm_inbox`.`id` in ($inbox_id)");
        $result['status'] = 1;
        $result['message'] = "Pesan terhapus";
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function post_mutation($id, $ref_id, $debet, $kredit, $ket, $type) {
		$data = array(
            'user_id' => $id,
            'ref_id'  => $ref_id,
            'tgl'     => date('YmdHis'),
            'debet'   => $debet,
            'kredit'  => $kredit,
            'ket'     => $ket,
            'poin'    => $poin,
            'type'    => $type,
        );

        if($debet == 0){
            $poin = $kredit;
        }else{
            $poin = 0 - $debet;
        }

        $this->db->insert('bsm_poin_mutation', $data);

        $sql = "UPDATE bsm_user set poin = poin+$poin where id = '$id'";
        $this->db->query($sql);
	}

    public function sendToAll($type,$id,$url,$message){
    	$fields = array(          
                    'to' => 'dsmq9wML8lw:APA91bFDYBIq9L6XkQDAcL7s87MLxxWvGVbT398IaqTcp20-zK1L7c5DbRbA_JDZMDGhF-74O_KxLH__XblUPRh71fNRlRUuZ4DT9R5oxt1gsZvDTXsyLz0y50gjN6P04cZLhhZZYSdZ',
                    'data' => array(
                        'title'         => "BSM Compas",
                        'is_background' => false,
                        'message'       => $message,
                        'payload'       =>
                            array(
                                "type"      => "artikel",
                                "artikel_id" => "17"
                            ),
                        'timestamp'     => date('Y-m-d G:i:s')
                    ),
                    'notification' => array(
                        'title' => 'BSM Compas',
                        'text' => $message,
                        'click_action' => "OPEN_ACTIVITY_1"
                    )
                    // 'timestamp' => date('Y-m-d G:i:s')
            );

        return $this->sendPushNotification($fields);
    }

    public function test_notif_get(){
        $result = $this->sendToAll("test","1","asdihasd","Diskusi Syariah 07 September 2019");
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
        
    }
	
    function sendPushNotification($fields) {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = array(
            'Authorization: key=AAAAXwMG6FI:APA91bFiWRNr3rMe_tUJ57qXoalBRXkNwKxwg9Rn5XWbJlnRI7MDXKzG93Be3hrJelBxmGUAjAnLP4MFTvFZK6cv-7Q_6MnifHwmMLI0cV7-53887Onu6szIwF6HBomNO6eEmuP7FiYt',
			'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);

        return $result;
    }

    protected function getDocumentLocation($kategori, $id){
        $sql = "SELECT attachment FROM $kategori WHERE id = $id";
        $row = $this->db->query($sql)->rows();
        return $row->attachment;
    }

    protected function getPoin(){
        $token  = $this->_token;
		$res = "SELECT bsm_user_logged.user,bsm_user.poin FROM bsm_user_logged LEFT JOIN bsm_user on bsm_user.id = bsm_user_logged.user where token = '$token'";
		$res = $this->db->query($res);
		if($res->num_rows() > 0){
			$poin = $res->row()->poin;
		}else{
			$poin = 0;
		}

        return $poin;
    }
	
    protected function getUserDetail(){
        $token  = $this->_token;
        $res = "SELECT bsm_user_logged.user,bsm_user.poin FROM bsm_user_logged LEFT JOIN bsm_user on bsm_user.id = bsm_user_logged.user where token = '$token'";
        $row = $this->db->query($res)->row();
        return $row->user;
    }

	protected function check_token() {

		$token = $this->input->get_request_header('token', TRUE);

		if($token == NULL) {
			$this->response($this->_show_response_401(""), REST_Controller::HTTP_UNAUTHORIZED);
		}else{
			$res = $this->db->query("SELECT * from bsm_user_logged where token = '$token'");
			$list = $res->row();
			if(isset($list)){
				$this->_token = $token; //set token global variabel
				return true;
			}else{
				$this->response($this->_show_response_401($token), REST_Controller::HTTP_UNAUTHORIZED);
			}
		}
	}

    protected function _show_response_401($token){
        return [
            'status' => 401,
            'confirm' => 'forbidden',
            'elapsedtime' => $this->benchmark->elapsed_time('code_start', 'code_end'),
            'token' => $token
        ];
	}
	
    protected function _show_response_505(){
        return [
            'status' => 505,
            'confirm' => 'VERSION_NOT_SUPPORTED',
            'elapsedtime' => $this->benchmark->elapsed_time('code_start', 'code_end'),
            'message' => "Versi terbaru sudah tersedia, silahkan lalukan update aplikasi."
        ];
    }

    protected function _show_response_200($result = array()){
        return [
            'status' => 200,
            'confirm' => 'success',
            'elapsedtime' => $this->benchmark->elapsed_time('code_start', 'code_end'),
            'poin'     => $this->getPoin(),
            'results' => $result
        ];
    }

    public function insertlogged($user,$token,$imei,$playerid){
        $this->db->db_debug = FALSE;
        $query = "INSERT INTO bsm_user_logged(token,user,imei,playerID,login_time) VALUES('$token',$user,'$imei','$playerid',NOW())";
        if(!$this->db->query($query)){
            $query = "update bsm_user_logged SET user = $user, token = '$token', imei = '$imei', playerID = '$playerid', login_time = NOW() WHERE user = $user";
            $this->db->query($query);
        }
    }
    
	public function reformat_date($date){
        $dates = date_create($date);
        return date_format($dates,"d M Y");
    }

	public function view_pdf_get(){
        $path   = $this->get('path');
        $kat    = $this->get('kat');
        $id     = $this->get('id');

        if($kat=="internal"){
            $this->db->query("UPDATE bsm_kinternal SET views = views+1 WHERE id = $id");
        }else{
            $this->db->query("UPDATE bsm_regulasi SET views = views+1 WHERE id = $id");
        }

		$parameters = array(
			'file' => $path,
			'download' => 'false',
			'print' => 'false',
			'openfile' => 'false'
		  );

		if(!is_array($parameters)) $params = split('&', $parameters);
		  
		$pairs = array();
		foreach($parameters as $key=>$val)
		{
    		if(null === $val || '' === $val) continue;
    		$pairs[] = urlencode($key).'='.urlencode($val);
		}
		
		$build_view_pdf = base_url().'pdfviewer?'.implode('&', $pairs);
		
		$args['title'] = '';
		$args['path_view'] = $build_view_pdf;

		$this->template->render('pdfviewer/pdf_frame', $args, FALSE);
    }
	
	public function sender_mail_get(){

		$email   = $this->get('email');
		$password   = $this->get('password');
		if($this->mail->sender($email, $this->mail_template->template_mail_reset_password($email,$password)))
		{
			$response = array(
				'status' => 1,
				// 'message' => 'Password baru berhasil dikirim, silahkan cek email Anda'
			);
		}
		else
		{
			$response = array(
				'status' => 0,
				// 'message' => 'Ops! It was not possible to send the email message. Check the email settings and try again.'
			);
		}

		$this->response($response, REST_Controller::HTTP_OK);
	}

    public function get_user_access(){
        $token  = $this->_token;
        $sql = "SELECT bsm_user.id_level FROM bsm_user_logged LEFT JOIN bsm_user on bsm_user.id = bsm_user_logged.user where token = '$token'";
        $row = $this->db->query($sql)->row();
        $sql = "SELECT * FROM bsm_apps_level_akses WHERE id_level = $row->id_level";
        return $this->db->query($sql)->result();
	}
    
    public function test_permission_get(){
        $id_menu = $this->get('id_menu');
		$token  = $this->_token;
		$sql = "SELECT lvl.permission
				FROM bsm_user_logged ulog
				LEFT JOIN bsm_user AS bu ON ulog.user = bu.id
				LEFT JOIN bsm_jabatan AS bj ON bj.id_jabatan = bu.id_jabatan
				LEFT JOIN bsm_apps_level_akses AS lvl ON bj.id_level = lvl.id_level
				WHERE ulog.token = '".$token."' 
				AND lvl.id_menu = '".$id_menu."'";
	
		$row = $this->db->query($sql);
		if($row->num_rows() > 0)
		{
			$permission = $row->row();
			if($permission->permission == "W")
			{
				return TRUE;
			}
        }
		return FALSE;
    }
	
	private function _check_permission($id_menu='') {
	
		$token  = $this->_token;

		if($id_menu=="REG" || $id_menu=="KIN" || $id_menu =="ERM")
			return TRUE;

		$sql = "SELECT lvl.permission
				FROM bsm_user_logged ulog
				LEFT JOIN bsm_user AS bu ON ulog.user = bu.id
				LEFT JOIN bsm_jabatan AS bj ON bj.id_jabatan = bu.id_jabatan
				LEFT JOIN bsm_apps_level_akses AS lvl ON bj.id_level = lvl.id_level
				WHERE ulog.token = '".$token."' 
				AND lvl.id_menu = '".$id_menu."' ";

		// logit($sql);
	
		$row = $this->db->query($sql);
		
		if($row->num_rows() > 0)
		{
			$permission = $row->row();
			if($permission->permission == "W")
			{
				return TRUE;
			}
		}
	
		return FALSE;
	}

	private function _check_permission_last_content($parent_id='') {
		$token = $this->_token;
		$results = '';
		$sql = "SELECT lv.id_menu, menu.parent_id
				FROM bsm_user_logged logged
				LEFT JOIN bsm_user AS u ON logged.`user` = u.id
				LEFT JOIN bsm_jabatan AS jab ON u.id_jabatan = jab.id_jabatan
				LEFT JOIN bsm_apps_level_akses AS lv ON jab.id_level = lv.id_level
				LEFT JOIN bsm_apps_menu AS menu ON lv.id_menu = menu.id_menu
				WHERE logged.token = '".$token."'
				AND lv.permission = 'W'
				AND menu.parent_id = '".$parent_id."'
				";
		$row = $this->db->query($sql);
		if($row->num_rows() > 0)
		{
			$result = $row->result_array();
			foreach($result as $idx => $val)
			{
				$data[$idx] = $val['id_menu'];
			}
			
			$string = implode(',', $data);
			$array = explode(',', $string);
			$results = implode("','",$array);
		}
	
		return $results;
	}
}
