<?php
use Restserver\Libraries\Rest\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/rest/REST_Controller.php';
require APPPATH . 'libraries/rest/Format.php';

class Product extends REST_Controller {

    private $_userapi;
    private $mui;

    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        // Extend db halalmui_product
        $this->mui = new stdClass();
        $this->mui->db = $this->load->database('db_product', TRUE);

        $this->_remap_request();
        $this->_userapi = $this->_apiuser;

        // $this->_check_module_access();
    }

	public function groups_categories_get()
	{
        $this->benchmark->mark('code_start');

            $sql = "SELECT * 
                    FROM rest_module
                    WHERE groups = 'Product Groups Categories'
                ";
            $results = $this->db->query($sql)->result_array();

            foreach ($results as $key => $val)
            {
                $result[$key]['groups'] = $val['groups'];
                $result[$key]['groups_code'] = $val['value_'];
                $result[$key]['categories_name'] = $val['description'];
                $result[$key]['categories_list'] = base_url().'api/v1/product/categories_list?groupsCode='.$val['value_'];
            }

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }
    
    public function categories_list_get()
    {
        $this->benchmark->mark('code_start');

        $groupcode = $this->get('groupsCode');

        $keyword          = trim($this->get('keyword'));
        $product_name  = strtolower(trim($this->get('productName')));
        $certified_no  = strtolower(trim($this->get('certifiedNo')));
        $produsen_name = strtolower(trim($this->get('produsenName')));
        $search_all = strtolower(trim($this->get('searchAll')));

        $page = 1;
		if($this->get('page') != NULL)
		{	
			$page = $this->get('page');
		}
		// set the number of items to display per page
		$items_per_page = 20;

		// build query
        $offset = ($page - 1) * $items_per_page;
        
            $sql = "SELECT
                    tpg.GroupCode,
                    tpg.ProductGroup,
                    thp.CustID,
                    thp.ID,
                    thp.CertifiedNo,
                    thp.ValidDate,
                    thp.ProductName,
                    thc.CustomerName 
                FROM
                    tblHalal_Product AS thp
                    LEFT JOIN tblProductGroup AS tpg ON thp.GroupCode = tpg.GroupCode
                    LEFT JOIN tblHalal_Company AS thc ON thp.CustID = thc.CustID
                WHERE
                    thp.GroupCode = " . $groupcode . " ";
            
            if(!empty($keyword))
            {
                if(!empty($search_all) && $search_all == "true")
                {
                    $sql .= " AND (thp.ProductName LIKE '%".$keyword."%' OR thp.CertifiedNo LIKE '%".$keyword."%' OR thc.CustomerName LIKE '%".$keyword."%') ";
                }
                else
                {
                    if(!empty($product_name) && $product_name == "true")
                    {
                        $sql .= " AND thp.ProductName LIKE '%".$keyword."%' ";
                    }
                    if(!empty($certified_no) && $certified_no == "true")
                    {
                        $sql .= " AND thp.CertifiedNo LIKE '%".$keyword."%' ";
                    }
                    if(!empty($produsen_name) && $produsen_name == "true")
                    {
                        $sql .= " AND thc.CustomerName LIKE '%".$keyword."%' ";
                    }
                }
            }
            
            $sql .= " AND thp.ValidDate >= CURDATE() 
                        AND thc.CustomerName != 'null' ";
            $sql .= " ORDER BY ProductName ASC 
                        LIMIT $offset, $items_per_page";

            $results = $this->mui->db->query($sql);
            $result = array();
            if ($results->num_rows() > 0) 
            {
                foreach ($results->result_array() as $key => $list) 
                {
                    // $result[$key]['product_id']    = $list['ID'];
                    // $result[$key]['customer_id']   = $list['CustID'];
                    $result[$key]['groups']        = $list['ProductGroup'];
                    $result[$key]['groups_code']   = $list['GroupCode'];
                    $result[$key]['product_name']  = trim($list['ProductName']);
                    $result[$key]['customer_name'] = strtoupper($list['CustomerName']);
                    $result[$key]['certified_no']  = $list['CertifiedNo'];
                    $result[$key]['expired_date']  = $list['ValidDate'];
                }
            }

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

	}
	
	public function scan_barcode_get()
	{
		$this->benchmark->mark('code_start');
		
		$url = $this->get('barcodeID');
		if (!filter_var($url, FILTER_VALIDATE_URL) === false)
		{
			parse_str(parse_url($url, PHP_URL_QUERY),$parse);	
			$barcode_id = $parse['barcode'];
		}
		else 
		{
			$barcode_id = $this->get('barcodeID');
		}
		
		$result = 'Product not found.';
		if(!empty($barcode_id))
		{
			$sql = "SELECT CustomerName,Code, CertifiedNo, OutletName, ValidDate 
					FROM tblBarcodeResto 
					INNER JOIN tblHalal_Company ON tblBarcodeResto.CustID=tblHalal_Company.CustID 
					WHERE Code=".$barcode_id." AND ValidDate>=CURDATE()
				";
			$results = $this->mui->db->query($sql);

			if($results->num_rows() > 0)
			{
				$row = $results->row();
				$result = array(
						'barcode_id'    => $row->Code,
						'outlet_name'   => $row->OutletName,
						'customer_name' => $row->CustomerName,
						'certified_no'  => $row->CertifiedNo,
						'expired_date'  => $row->ValidDate
					);
			}
		}
		$this->benchmark->mark('code_end');
		$this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
	}

    
    protected function _show_response_200($result = array())
    {
        return [
            'status' => 200,
            'confirm' => 'success',
            'elapsedtime' => $this->benchmark->elapsed_time('code_start', 'code_end'),
            'results' => array('result' => $result),
            'token' => $this->_userapi->token
        ];
    }
}
