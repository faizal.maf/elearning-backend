<?php
use Restserver\Libraries\Rest\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/rest/REST_Controller.php';
require APPPATH . 'libraries/rest/Format.php';

class User extends REST_Controller {
    // private $_userapi;
	// private $mui;
	protected $mail;
	protected $mail_template;

    function __construct()
    {
        parent::__construct();
        $this->load->library('template');
		$this->load->helper(array('url','file','array'));
		$this->load->library('mail_lib');
		$this->load->library('mail_template_lib');

		$this->mail = $this->mail_lib;
		$this->mail_template = $this->mail_template_lib;

		if($this->uri->segment(4) == "create_auth" || $this->uri->segment(4) =="view_pdf" || 
			$this->uri->segment(4) =="app_init"|| $this->uri->segment(4) =="test_notif" ||
			$this->uri->segment(4) =="reset_password"){
        }else{
            $this->check_token();
        }

    }

    public function app_init_get(){
		$this->benchmark->mark('code_start');
		
		//check version application
		if(!array_key_exists("version", $this->input->request_headers()))
			$this->response($this->_show_response_505(), REST_Controller::HTTP_VERSION_NOT_SUPPORTED); 

		$result = array();
        $query = "SELECT * FROM bsm_config";
        $results = $this->db->query($query);
        $ajp = 0;
        foreach ($results->result_array() as $key => $list)
        {
            if($list['groups']=="Hubungi Kami"){
                $result['hubungi'][$list['deskripsi']]   = $list['catatan'];
            }
            if($list['groups']=="Default Poin"){
                $result['poin'][$list['deskripsi']]   = $list['catatan'];
            }
            if($list['groups']=="Ajukan Pertanyaan"){
                $result['subject'][$ajp]['subject'] = $list['deskripsi'];
                $ajp++;
            }
            if($list['groups']== "Info Tukar Poin"){
                $result['poininfo'] = $list['catatan'];
            }
        }  
        
        $query = "SELECT * FROM bsm_setting_slider where status = 1 order by order_slider asc";
        $results = $this->db->query($query);
        foreach ($results->result_array() as $key => $list)
        {
            $result['slider'][$key]['id']           = $list['id'];
            $result['slider'][$key]['title']        = $list['title'];
            $result['slider'][$key]['url']          = base_url().$list['image'];
            $result['slider'][$key]['description']  = $list['description'];
            $result['slider'][$key]['category']     = "Info";
            $result['slider'][$key]['image']        = base_url().$list['image'];
            $result['slider'][$key]['date']         = $list['created']; 
        }    
    
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);    
    }
    public function regulasi_last_get(){ 
        $this->benchmark->mark('code_start');
        $sql = "SELECT bsm_regulasi.*, bsm_config.deskripsi 
            FROM
                bsm_regulasi
            LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_regulasi.id_kategori
            WHERE
                bsm_regulasi.status = 1
            ORDER BY tahun_terbit desc, id desc
            LIMIT 10";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0)
        {
            foreach ($results->result_array() as $key => $list)
            {
                $result[$key]['id']        = $list['id'];
                $result[$key]['kategori']  = $list['deskripsi'];
                $result[$key]['title']   = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
                $result[$key]['created']  = $this->reformat_date($list['created']);
                $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];

            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function regulasi_list_get(){
        $this->benchmark->mark('code_start');
        $groupcode = $this->get('kategori');
        $keyword        = trim(urldecode($this->get('keyword')));
        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }

		// set the number of items to display per page
		$items_per_page = 20;

		// build query
        $offset = ($page - 1) * $items_per_page;

            $sql = "SELECT bsm_regulasi.*, bsm_config.deskripsi
                FROM
                    bsm_regulasi
                LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_regulasi.id_kategori
                WHERE
                    status = 1
                    ";
            if($groupcode!="REG"){
                $sql .= " AND id_kategori = '".$groupcode."'";
            }
            if(!empty($keyword))
            {
                $sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
            }

            $sql .= " ORDER BY tahun_terbit desc
                        LIMIT $offset, $items_per_page";

            $results = $this->db->query($sql);
            $result = array();
            if ($results->num_rows() > 0)
            {
                foreach ($results->result_array() as $key => $list)
                {
                    $result[$key]['id']        = $list['id'];
                    $result[$key]['kategori']  = $list['deskripsi'];
                    $result[$key]['title']   = $list['title'];
                    $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
                    $result[$key]['created']  = $this->reformat_date($list['created']);
                    $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
                }
            }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function kinternal_last_get(){
        $this->benchmark->mark('code_start');
        $sql = "SELECT bsm_kinternal.*, bsm_config.deskripsi 
            FROM
                bsm_kinternal
            LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_kinternal.id_kategori
            WHERE
                bsm_kinternal.status = 1
            ORDER BY tahun_terbit desc, id desc
            LIMIT 10";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0)
        {
            foreach ($results->result_array() as $key => $list)
            {
                $result[$key]['id']             = $list['id'];
                $result[$key]['kategori']       = $list['deskripsi'];
                $result[$key]['title']          = $list['title'];
                $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=internal&id=".$list['id'];
                $result[$key]['created']        = $this->reformat_date($list['created']);
                $result[$key]['tahun_terbit']   = "Tahun terbit ".$list['tahun_terbit'];
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function kinternal_list_get(){
        $this->benchmark->mark('code_start');
        $groupcode = $this->get('kategori');
        if(empty($groupcode)){
            $this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
        }
        $keyword        = trim($this->get('keyword'));
        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }
		// set the number of items to display per page
		$items_per_page = 20;

		// build query
        $offset = ($page - 1) * $items_per_page;

            $sql = "SELECT bsm_kinternal.*, bsm_config.deskripsi
                FROM
                    bsm_kinternal
                LEFT JOIN bsm_config on bsm_config.id_lookup = bsm_kinternal.id_kategori
                WHERE
                    status = 1";
            if($groupcode!="KIN"){
                $sql .= " AND id_kategori = '".$groupcode."'";
            }
            if(!empty($keyword))
            {
              $sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
            }

            $sql .= " ORDER BY tahun_terbit DESC, id DESC
                        LIMIT $offset, $items_per_page";

            $results = $this->db->query($sql);
            $result = array();
            if ($results->num_rows() > 0)
            {
                foreach ($results->result_array() as $key => $list)
                {
                    // $result[$key]['product_id']    = $list['ID'];
                    // $result[$key]['customer_id']   = $list['CustID'];
                    $result[$key]['id']        = $list['id'];
                    $result[$key]['kategori']  = $list['deskripsi'];
                    $result[$key]['title']   = $list['title'];
                    $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=internal&id=".$list['id'];
                    $result[$key]['created']  = $this->reformat_date($list['created']);
                    $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
                }
            }

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    public function document_list_get(){
        $this->benchmark->mark('code_start');
        $groupcode = $this->get('kategori');
        if(empty($groupcode)){
            $this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
        }
        // $kat = empty($groupcode) ? $groupcode : ;

        $keyword        = trim($this->get('keyword'));

        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }

		// set the number of items to display per page
		$items_per_page = 20;

		// build query
        $offset = ($page - 1) * $items_per_page;

            $sql = "SELECT *
                FROM
                    bsm_document
                WHERE
                    status = 1
                AND
                    kategori = ".$groupcode."
                    ";

            if(!empty($keyword))
            {
              $sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
            }

            $sql .= " ORDER BY id desc
                        LIMIT $offset, $items_per_page";

            $results = $this->db->query($sql);
            $result = array();
            if ($results->num_rows() > 0)
            {
                foreach ($results->result_array() as $key => $list)
                {
                    // $result[$key]['product_id']    = $list['ID'];
                    // $result[$key]['customer_id']   = $list['CustID'];
                    $result[$key]['id']        = $list['id'];
                    $result[$key]['title']   = $list['title'];
                    $result[$key]['url'] = base_url('docs/pdf/').urlencode($list['filename']);
                    $result[$key]['created']  = $this->reformat_date($list['date_created']);
                    $result[$key]['tahun_terbit']  = "Tahun terbit ".$list['tahun_terbit'];
                }
            }

        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    //LOGIN
    public function create_auth_post(){
      $this->benchmark->mark('code_start');
      $email = $this->post('email');
      $password = $this->post('password'); //sudah di encode md5
      $imei = $this->post('imei');
      $playerid = $this->post('playerid');
      $sql = "SELECT * FROM bsm_user WHERE email = '$email' and password = '$password'";
      $results = $this->db->query($sql);
      $result = array();

      $list = $results->row();
      if(isset($list)){
        
        $token = md5('x'.time());
        $result['user']['id']       = $list->id;
        $result['user']['email']    = $list->email;
        $result['user']['name']     = $list->name;
        $result['user']['nip']      = $list->nip;
        $result['user']['jabatan']  = $list->jabatan;
        $result['user']['point']    = $list->poin;
        $result['user']['token']    = $token;
        $sql = "SELECT * from bsm_inbox where user_id = $list->id ORDER BY id desc";
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $ls){
                $result['inbox'][$key]['id']             = $ls['id'];
                $result['inbox'][$key]['title']          = $ls['title'];
                $result['inbox'][$key]['highlight']      = $ls['highlight'];
                $result['inbox'][$key]['description']    = $ls['body'];
                $result['inbox'][$key]['date']           = $this->reformat_date($ls['created']);
                $result['inbox'][$key]['isRead']         = $ls['is_read'];
            }
        }
        $this->insertlogged($list->id,$token,$imei,$playerid);
        
      }else{
        $result['user']['id']       = "";
        $result['user']['email']    = "";
        $result['user']['name']     = "";
        $result['user']['nip']      = "";
        $result['user']['jabatan']  = "";
        $result['user']['point']     = "";
        $result['user']['token']    = "";
      }

      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
	}
	
	public function reset_password_post() {
		$this->benchmark->mark('code_start');
		// Collect email and validation
		$email = $email = $this->post('email');

		// Try to find user data
		$user = $this->db->get_where('bsm_user', array('email' => $email))->row_array(0);

		// User doesnt exist
		if( count($user) <= 0 )
		{
			$response = array('status' => 0,
						'message' => 'Mohon maaf email yang Anda masukan salah / tidak terdaftar'
				);
		}
		else
		{
			// $args['user'] = $user;
			// $args['email'] = get_value($user, 'email');
			// $args['key_access'] = md5(uniqid());
			$password = substr(uniqid(),0,8);
            $this->db->query("UPDATE bsm_user SET password = '".md5($password)."' WHERE email = '$email'");
			// sending verify email
			if($this->mail->sender($email, $this->mail_template->template_mail_reset_password($email,$password)))
			{
				$response = array(
					'status' => 1,
					'message' => 'Password baru berhasil dikirim, silahkan cek email Anda'
				);
			}
			else
			{
				$response = array(
					'status' => 0,
					'message' => 'Ops! It was not possible to send the email message. Check the email settings and try again.'
				);
			}
		}
		$this->benchmark->mark('code_end');
     	$this->response($this->_show_response_200($response), REST_Controller::HTTP_OK);
	}

    public function slider_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        // for($i = 0; $i < 3;$i++){
        //     $a = $i+1;
        $i = 0;
            $result[$i]['id']       = $i;
            $result[$i]['title']    = "Slider 1";
            $result[$i]['url']    = "Slider 1 URLNYA KEMANA";
            $result[$i]['description']    = "aman aman";
            $result[$i]['category']    = "Info";
            $result[$i]['image']    = base_url('public/banner/')."homebanner.jpg";
            $result[$i]['date']     = "";
        // }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function faq_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        $keyword        = trim($this->get('keyword'));

        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }

		// set the number of items to display per page
		$items_per_page = 10;

		// build query
        $offset = ($page - 1) * $items_per_page;

            $sql = "SELECT *
                FROM
                    bsm_faq
                WHERE
                    status = 1";

            if(!empty($keyword))
            {
              $sql .= " AND (question LIKE '%".$keyword."%') ";
            }

            $sql .= " ORDER BY id desc
                        LIMIT $offset, $items_per_page";
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']         = $list['id'];
                $result[$key]['question']   = $list['question'];
                $result[$key]['answer']     = $list['answer'];
            }
        }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function faq_ajukan_pertanyaan_post(){
        $result = array();
        $id = $this->getUserDetail();
        $subject = $this->post('subject');
        $tanya = $this->post('pertanyaan');
        $data = array();
        $data['subject'] = $subject;
        $data['question'] = $tanya;
        $data['created_by'] = $id;
        $data['status'] = 0;
        $data['created'] = date("Y-m-d H:i:s");
        $this->db->insert("bsm_inbox_question",$data);
        $result['status'] = 1;
        $result['message'] = "Pesan terkirim";
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function cpg_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }
		$items_per_page = 20;
        $offset = ($page - 1) * $items_per_page;

        $sql = "SELECT * from bsm_infocpg where status = 1";
        $sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
        
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $i => $list){  
                $result[$i]['id']       = $list['id'];
                $result[$i]['title']    = $list['caption'];
                $result[$i]['image']    = base_url().$list['image'];
                $result[$i]['date']     = $this->reformat_date($list['created']);
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function video_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        $keyword        = trim($this->get('keyword'));
        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }

		// set the number of items to display per page
		$items_per_page = 10;

		// build query
        $offset = ($page - 1) * $items_per_page;

        $sql = "SELECT * from bsm_video where status = 1";
        if(!empty($keyword))
            {
              $sql .= " AND title LIKE '%".$keyword."%'";
            }
        $sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
        
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']             = $list['id'];
                $result[$key]['title']          = $list['title'];
                $result[$key]['desc']           = $list['description'];
                $result[$key]['ytvideo_id']     = $list['ytvideo_id'];
                $result[$key]['source']         = "https://img.youtube.com/vi/".$list['ytvideo_id']."/mqdefault.jpg";
                $result[$key]['date']           = $this->reformat_date($list['created']);
            }
        }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    
    }

    public function infografis_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }
		$items_per_page = 20;
        $offset = ($page - 1) * $items_per_page;
        
        $sql = "SELECT * from bsm_infografis WHERE status = 1";

        $keyword        = trim($this->get('keyword'));
        if(!empty($keyword))
        {
            $sql .= " AND title LIKE '%".$keyword."%'";
        }

        $sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']         = $list['id'];
                $result[$key]['title']      = $list['title'];
                $result[$key]['desc']       = $list['description'];
                $result[$key]['source']     = base_url().$list['image'];
                $result[$key]['date']       = $this->reformat_date($list['created']);
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function artikel_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        $keyword        = trim($this->get('keyword'));
        $page = 1;
		if($this->get('page') != NULL)
		{
			$page = $this->get('page');
        }

		// set the number of items to display per page
		$items_per_page = 10;

		// build query
        $offset = ($page - 1) * $items_per_page;

        $sql = "SELECT id, title, image, created from bsm_artikel where status = 1";
        if(!empty($keyword))
            {
              $sql .= " AND title LIKE '%".$keyword."%'";
            }
        $sql .= " ORDER BY id desc LIMIT $offset, $items_per_page";
        
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']         = $list['id'];
                $result[$key]['title']   = $list['title'];
                $result[$key]['image']     = $list['image'];
                $result[$key]['content']     = "";
                $result[$key]['date']     = $this->reformat_date($list['created']);
            }
        }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);

    }

    public function artikel_detail_get(){
        $this->benchmark->mark('code_start');
        $id = $this->get('artikel_id');
        if(empty($id)){
            $this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
        }
        $sql = "SELECT * FROM bsm_artikel WHERE id = $id";
        $result = $this->db->query($sql);
        $row = $result->row();
        $res = array();
        $res['id'] = $row->id;
        $res['title'] = $row->title;
        $res['image'] = base_url().$row->image;
        $res['content'] = $row->content;
        $res['date'] = $this->reformat_date($row->created);

        $this->db->query("UPDATE bsm_artikel SET views = views+1 WHERE id = $id");

      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($res), REST_Controller::HTTP_OK);

    }

    public function video_update_viewer_get(){
        $this->benchmark->mark('code_start');
        $id     = $this->get('video_id');
        if(empty($id)){
            $this->response($this->_show_response_200(array()), REST_Controller::HTTP_OK);
        }
        $this->db->query("UPDATE bsm_video SET views = views+1 WHERE id = $id");
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($res), REST_Controller::HTTP_OK);
    }

    public function lapor_gratifikasi_post(){
        $this->benchmark->mark('code_start');
        $userid    = $this->getUserDetail();
        $lokasi     = $this->post('lokasi');
        $jenis      = $this->post('jenis');
        $harga      = $this->post('harga');
        $peristiwa  = $this->post('peristiwa');
        $nama       = $this->post('nama');
        $posisi     = $this->post('posisi');
        $hubungan   = $this->post('hubungan');
        $alamat     = $this->post('alamat');
        $kronologi  = $this->post('kronologi');
        $image      = $this->post('image');

        $path_file_address = 'upload/gratifikasi/'.date('Y').'/'.date('m');
		if (!is_dir($path_file_address))
			mkdir($path_file_address, 0755, TRUE);
		$foto_address = $path_file_address.'/'.$userid.date('Ymdhis');

		$foto_images = $this->post('image');
		if (! empty($foto_images))
		{
			$foto_decode = base64_decode($foto_images);

			$file_address = $foto_address.'.jpg';
			// $file_address22 = $path_file_address.'/001201805150006-event.jpg'; //test
            write_file($file_address, $foto_decode);

            $data['lokasi']         = $lokasi;
            $data['jenis']          = $jenis;
            $data['harga']          = $harga;
            $data['peristiwa']      = $peristiwa;
            $data['nama']           = $nama;
            $data['posisi']         = $posisi;
            $data['hubungan']       = $hubungan;
            $data['alamat']         = $alamat;
            $data['kronologi']      = $kronologi;
            $data['image']          = $file_address;
            $data['status']     	= 0;
            $data['created_by']     = $userid;
            $data['created']     	= date('YmdHis');
            $data['updated_by']     = $userid;
            $this->db->insert('bsm_gratifikasi', $data);
            
            $result = array();

            $result['status'] = 1;
            $result['message'] = "Pesan terkirim";
            $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
        }
    }

    public function hubungi_get(){
        $this->benchmark->mark('code_start');
        $query = "SELECT * FROM bsm_config WHERE groups = 'Hubungi Kami'";
        $res = $this->db->query($query);
        $row = $res->result_array();

        $result = array();

        $result['alamata']   = date('Ymdhis');
        $result['alamat']   = $row[0]['catatan'];
        $result['email']   = $row[1]['catatan'];
        $result['phone']   = $row[2]['catatan'];
        $result['web']      = "https://www.syariahmandiri.co.id/";   
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);
    }

    public function inbox_list_get(){
        $this->benchmark->mark('code_start');
        $result = array();
        $last = 0;
        $last = $this->get('lastid');
        $userid         = $this->getUserDetail();
        $sql = "SELECT * from bsm_inbox where user_id = $userid and id > ".$last;
        $sql .= " ORDER BY id desc";
        
        $results = $this->db->query($sql);
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']             = $list['id'];
                $result[$key]['title']          = $list['title'];
                $result[$key]['highlight']      = $list['highlight'];
                $result[$key]['description']    = $list['body'];
                $result[$key]['date']           = $this->reformat_date($list['created']);
                $result[$key]['isRead']         = $list['is_read'];
            }
        }
      $this->benchmark->mark('code_end');
      $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);      
    }

    public function reward_list_get(){
        $this->benchmark->mark('code_start');
        $sql = "SELECT * from bsm_setting_reward WHERE status = 1 ORDER BY id desc";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']       = $list['id'];
                $result[$key]['title']    = $list['description'];
                $result[$key]['image']    = base_url().$list['image'];
                $result[$key]['poin']     = $list['poin'];            
                $result[$key]['description']    = $list['description'];
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);        
    }

    public function history_reward_list_get(){
        $this->benchmark->mark('code_start');
        $userid = $this->getUserDetail();
        $sql = "SELECT a.id_reward, a.status as tukar_status, b.* from bsm_inbox_tukarpoin as a INNER JOIN bsm_setting_reward as b ON b.id = a.id_reward WHERE id_user = $userid";
        $results = $this->db->query($sql);
        $result = array();
        if ($results->num_rows() > 0){
            foreach ($results->result_array() as $key => $list){
                $result[$key]['id']             = $list['id'];
                $result[$key]['title']          = $list['description'];
                $result[$key]['image']          = base_url().$list['image'];
                $result[$key]['poin']           = $list['poin'];            
                $result[$key]['description']    = $list['description'];
                $result[$key]['status']         = $list['tukar_status'];
                $status = $list['tukar_status'];
                if($status == 0){
                    $status_text = "Belum diproses";
                }else if($status == 1){
                    $status_text = "Sedang diproses";
                }else{
                    $status_text = "Berhasil diproses";
                }
                $result[$key]['status_text']    = $status_text;
            }
        }
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);        
    }
# question
# gratifikasi
# rewardpoin
# quiz
    public function quiz_participation_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $data = array();
        $userid = $this->getUserDetail();
        $quizid = $this->post("id_quiz");
        $data['id_quiz'] = $quizid;
        $data['id_user'] = $userid;
        $data['created_by'] = $userid;
        $poin = $this->post("poin");
        $data['answer_user'] = $this->post("answer");
        $data['status'] = $this->post("status");
        $data['created'] = date("Y-m-d H:i:s");
        $this->db->insert("bsm_quiz_participation",$data);
        if($data['status'] == 0){
            $inbox['user_id']   = $userid;
            $inbox['ref_id']    = $this->post("id_quiz");
            $inbox['title']     = "Selamat anda mendapatkan ".$poin." poin";
            $inbox['body']      = "Anda mendapatkan poin dengan menjawab quiz";
            $inbox['type']      = "#quiz";
            $this->db->insert("bsm_inbox",$inbox);
            $this->post_mutation($userid, $quizid, 0, $poin, "Berhasil Menjawab Quiz #".$quizid, "QUIZ");
        }
        $this->benchmark->mark('code_end');
        $result['status'] = 1;
        $result['message'] = "Pesan terkirim";
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);        
    }

    public function tukar_poin_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $data = array();
        $userid         = $this->getUserDetail();
        $rewardid       = $this->post("reward_id");
        $poin           = $this->post("poin");
        $data['id_reward']  = $rewardid;
        $data['id_user']    = $userid;
        $data['status']     = 0;
        $data['created']    = date('YmdHis');
        $data['created_by'] = $userid;
        $data['updated']    = date('YmdHis');
        $this->db->insert("bsm_inbox_tukarpoin",$data);
        $this->post_mutation($userid, $rewardid, $poin, 0, "Tukar Poin Reward #".$rewardid, "REWARD");

        $this->benchmark->mark('code_end');
        $result['status'] = 1;
        $result['message'] = "Pesan terkirim";
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);          
    }


    public function set_read_inbox_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $userid         = $this->getUserDetail();
        $inbox_id       = $this->post("inbox_id");
        $this->db->query("UPDATE bsm_inbox SET is_read = 1 WHERE id = $inbox_id");
        $result['status'] = 1;
        $result['message'] = "Pesan terkirim";
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);          
    }

    public function delete_inbox_post(){
        $this->benchmark->mark('code_start');
        $result = array();
        $inbox_id       = $this->post("inbox_id");
        $this->db->query("DELETE FROM `bsm_inbox` WHERE `bsm_inbox`.`id` = $inbox_id");
        $result['status'] = 1;
        $result['message'] = "Pesan terhapus";
        $this->benchmark->mark('code_end');
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);          
    }

    public function post_mutation($id, $ref_id, $debet, $kredit, $ket, $type) {
		$data = array(
            'user_id' => $id,
            'ref_id'  => $ref_id,
            'tgl'     => date('YmdHis'),
            'debet'   => $debet,
            'kredit'  => $kredit,
            'ket'     => $ket,
            'poin'    => $poin,
            'type'    => $type,
        );

        if($debet == 0){
            $poin = $kredit;
        }else{
            $poin = 0 - $debet;
        }
        

        $this->db->insert('bsm_poin_mutation', $data);
        
        $sql = "UPDATE bsm_user set poin = poin+$poin where id = '$id'";
        $this->db->query($sql);
	}

    public function sendToAll($type,$id,$url,$message){
    	$fields = array(          
                    'to' => 'dP3xk1vLVp8:APA91bEVYNYJ495QAhIem8YrYOGpRbZrNrrAm1xSB2mxnQPO0QPOo_2pKaA63CISrW1qvzb3wuk_wGLQGy4oZne-pcccmgcZ_0W33NDvjS91eScC9OOLEoiDIk7x4sWF5Zr-s9gaEnkM',
                    'data' => array(
                        'title'         => "BSM Compas",
                        'is_background' => false,
                        'message'       => $message,
                        'payload'       => 
                            array(
                                "type"      => "quiz",
                                "quiz_id" => "12",
                                "pertanyaan" => "Sebuah kereta api listrik bergerak ke utara dengan kecepatan 100 mph. Ke arah mana asapnya berhembus?",
                                "jawaban1" => "Utara",
                                "jawaban2" => "Barat",
                                "jawaban3" => "Selatan",
                                "jawaban4" => "Tidak Ketiganya",
                                "jawaban"  => "1",
                            ),
                        'timestamp'     => date('Y-m-d G:i:s')
                    ),
                    'notification' => array(
                        'title' => 'BSM Compas',
                        'text' => $message,
                        'click_action' => "OPEN_ACTIVITY_1"
                    )
                    // 'timestamp' => date('Y-m-d G:i:s')
            );
                
        return $this->sendPushNotification($fields);
    }

    public function test_notif_get(){
        $result = $this->sendToAll("test","1","asdihasd","Wahwahwah");
        $this->response($this->_show_response_200($result), REST_Controller::HTTP_OK);  
        
    }
    function sendPushNotification($fields) {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
        $headers = array(
            'Authorization: key=AAAAXwMG6FI:APA91bFiWRNr3rMe_tUJ57qXoalBRXkNwKxwg9Rn5XWbJlnRI7MDXKzG93Be3hrJelBxmGUAjAnLP4MFTvFZK6cv-7Q_6MnifHwmMLI0cV7-53887Onu6szIwF6HBomNO6eEmuP7FiYt',
			'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
 
        return $result;
    }

    protected function getPoin(){
        $header = $this->input->request_headers();
        if(array_key_exists("token",$header)){
            $token  = $header['token'];
            if($token == ""){
                $poin = 0;
            }else{
                $res = "SELECT bsm_user_logged.user,bsm_user.poin FROM bsm_user_logged LEFT JOIN bsm_user on bsm_user.id = bsm_user_logged.user where token = '$token'";
                $res = $this->db->query($res);
                if($res->num_rows() > 0){
                    $poin = $res->row()->poin;
                }else{
                    $poin = 0;
                }
                
            }
        }else{
            $poin = 0;
        }
        return $poin; 
    }
    protected function getUserDetail(){
        $header = $this->input->request_headers();
        $token  = $header['token'];
        $res = "SELECT bsm_user_logged.user,bsm_user.poin FROM bsm_user_logged LEFT JOIN bsm_user on bsm_user.id = bsm_user_logged.user where token = '$token'";
        $row = $this->db->query($res)->row();
        return $row->user;
    }

    protected function check_token(){

        $header = $this->input->request_headers();
        if(!array_key_exists("token",$header)){
            $this->response($this->_show_response_401(""), REST_Controller::HTTP_UNAUTHORIZED);
		}else{
            $token = $header['token'];
            $res = $this->db->query("SELECT * from bsm_user_logged where token = '$token'");
            $list = $res->row();
            if(isset($list)){
                return true;
            }else{
                $this->response($this->_show_response_401($token), REST_Controller::HTTP_UNAUTHORIZED);
            }
        }
    }

    protected function _show_response_401($token){
        return [
            'status' => 401,
            'confirm' => 'forbidden',
            'elapsedtime' => $this->benchmark->elapsed_time('code_start', 'code_end'),
            'token' => $token
        ];
	}
	
    protected function _show_response_505(){
        return [
            'status' => 505,
            'confirm' => 'VERSION_NOT_SUPPORTED',
            'elapsedtime' => $this->benchmark->elapsed_time('code_start', 'code_end'),
            'message' => "Versi terbaru sudah tersedia, silahkan lalukan update aplikasi."
        ];
    }

    protected function _show_response_200($result = array()){
        return [
            'status' => 200,
            'confirm' => 'success',
            'elapsedtime' => $this->benchmark->elapsed_time('code_start', 'code_end'),
            'poin'     => $this->getPoin(),
            'results' => $result
        ];
    }

    public function insertlogged($user,$token,$imei,$playerid){
        $this->db->db_debug = FALSE;
        $query = "INSERT INTO bsm_user_logged(token,user,imei,playerID,login_time) VALUES('$token',$user,'$imei','$playerid',NOW())";
        if(!$this->db->query($query)){
            $query = "update bsm_user_logged SET user = $user, token = '$token', imei = '$imei', playerID = '$playerid', login_time = NOW() WHERE user = $user";
            $this->db->query($query);
        }
    }
    
	public function reformat_date($date){
        $dates = date_create($date);
        return date_format($dates,"d M Y");
    }

	public function view_pdf_get(){
        $path   = $this->get('path');
        $kat    = $this->get('kat');
        $id     = $this->get('id');

        if($kat=="internal"){
            $this->db->query("UPDATE bsm_kinternal SET views = views+1 WHERE id = $id");
        }else{
            $this->db->query("UPDATE bsm_regulasi SET views = views+1 WHERE id = $id");
        }

		$parameters = array(
			'file' => $path,
			'download' => 'false',
			'print' => 'false',
			'openfile' => 'false'
		  );

		if(!is_array($parameters)) $params = split('&', $parameters);
		  
		$pairs = array();
		foreach($parameters as $key=>$val)
		{
    		if(null === $val || '' === $val) continue;
    		$pairs[] = urlencode($key).'='.urlencode($val);
		}
		
		$build_view_pdf = base_url().'pdfviewer?'.implode('&', $pairs);
		
		$args['title'] = '';
		$args['path_view'] = $build_view_pdf;

		$this->template->render('pdfviewer/pdf_frame', $args, FALSE);
    }
    

}
