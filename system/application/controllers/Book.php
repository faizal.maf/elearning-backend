<?php
defined('BASEPATH') or exit('No direct script access allowed');
//PhpSpreadsheet
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Book extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
		validate_session();
		
		$this->uri_name = 'book';
		$this->title = 'Book';
		$this->model_name = 'book_model';							
		$this->load->model($this->model_name);

		$this->load->helper('file','html');
	}
	
	public function index()
	{
		$breadcrumbs = array(array('url' => $this->uri_name, 'title' => $this->title),
					array('url' => $this->uri_name, 'title' => 'List', 'active' => true));
		
		// FILTER STATUS PUBLISH
		$status      = array('all' => 'All', 'yes' => 'Yes', 'no' => 'No');
		$opt_status      = form_dropdown('fillStatus',$status,'','id="fillStatus" class="custom-select custom-select-label form-control m-input m-input--square"');
		
		// FILTER DOKUMEN TERSEDIA
		$opt_doc      = form_dropdown('fillDoc',$status,'','id="fillDoc" class="custom-select custom-select-label form-control m-input m-input--square"');
		
		// FILTER KATEGORI
		$kategori_opt = array('all' => 'All');
		$sql = "SELECT id, kategori FROM kategori ";
		$list_kategori_book = $this->db->query($sql)->result();
		foreach ($list_kategori_book as $list_regulasi_record_idx=>$list_book_record)
		{
			$kategori_opt[$list_book_record->id] = ''.$list_book_record->kategori;
		}
		$opt_kategori      = form_dropdown('f_kategori',$kategori_opt,'','id="f_kategori" class="custom-select custom-select-label form-control m-input m-input--square"');
	
		// SET DATA RESULT
		$data['title'] = $this->title;
		$data['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title, $this->uri_name, 'New '.$this->title);
		$data['controller_name'] = $this->uri_name;
		$data['url_tablelist'] = $this->uri_name.'/table_list';
		$data['status'] = $opt_status;
		$data['doc'] = $opt_doc;
		$data['kategori'] = $opt_kategori;

        $this->template->view($this->uri_name . '/list', $data);
	}

	public function table_list()
	{
		parent::table_list();
	}

	public function view_pdf($keypdf='', $title='')
	{
		$model = $this->model_name;
		$path ='';
		$result = $this->$model->get_path_pdf($keypdf);
		if($result->num_rows() > 0)
		{
			$result = $result->row();
			$title = $result->title;
			$path = $result->attachment;
		}

		parent::view_pdf($path, $title);
	}

	public function form($id='')
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		#-- Options regulasi
		$regulasi_option = array('' => 'Open this select category');
		$opt_sql = "SELECT id, kategori
					FROM kategori";
		$list_kategori_book = $this->db->query($opt_sql)->result();
		foreach ($list_kategori_book as $list_regulasi_record_idx=>$list_regulasi_record)
		{
			$regulasi_option[$list_regulasi_record->id] = ''.ucwords($list_regulasi_record->kategori);
		}
		
		$submit_action = 'add';
		$result = NULL;
		if (!empty($id))
		{
			$result = $this->_get_details($id);
			$submit_action = 'update/id/'.$id;
		}
		$args['result']          = $result;
		$args['controller_name'] = $this->uri_name;
		$args['action']          = $submit_action;
		$args['list_kategori']		 = $regulasi_option;
		$this->template->render($this->uri_name.'/form', $args, FALSE);
	}

	public function add()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_form_validation());
		$this->form_validation->set_message('required','%s Please fill in this field. !');
		if($this->form_validation->run() == FALSE)
		{
			$result = array(
				'status' => 0,
				'error' => $this->_array_remove_index(array(
							'title' => form_error('title', '<div class="form-control-feedback">', '</div>'),
							'tahun_terbit' => form_error('tahun_terbit', '<div class="form-control-feedback">', '</div>'),
							'id_kategori' => form_error('id_kategori', '<div class="form-control-feedback">', '</div>'),
							'attachment' => form_error('attachment', '<div class="form-control-feedback">', '</div>')
					)),
				'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> Please fill in the data correctly and accordingly before submit</div>"
			);

			$this->template->render_json($result);
		}
		else
		{
			if(isset($_POST['id']) and trim($_POST['id'])!=''){
				$this->$model->update($_POST);
				$data['result']['id'] = $_POST['id'];
			}else{
				$_POST['id'] = $this->$model->add($_POST);
				$data['result']['id'] = array();
			}

			/* Prosesing Upload Attachement */
			if(isset($_FILES['filename']) and !empty($_FILES)) {
				if (!file_exists('upload/books/')) 
					mkdir('upload/books/', 0777, true); //add true agar membuat file recursive
				else
					chmod('upload/books/', 0777);
					
				$ID_KATEGORI = $_POST['kategori'];
				if (!file_exists('upload/books/'.$ID_KATEGORI.'/'))
					mkdir('upload/books/'.$ID_KATEGORI.'/', 0777, true);

				$config['upload_path'] = 'upload/books/'.$ID_KATEGORI.'/';
				$config['allowed_types'] = 'zip|gif|jpg|jpeg|png|bmp';
				$config['overwrite'] = true;
				$config['file_name'] = strtolower($_FILES["filename"]['name']);
				
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('filename'))
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status']= 1;
					$upload_data = $this->upload->data();

					$_POST['filename'] = $upload_data['client_name'];
					$_POST['keypdf'] = md5($_POST['id']);
					$_POST['attachment'] = $config['upload_path'].$upload_data['file_name'];

                    ## Extract Zip
                    $zip = new ZipArchive;
                    $res = $zip->open($config['upload_path'].$upload_data['file_name']);
                    if ($res === TRUE) {
                        // Extract file
                        $file_dir = $config['upload_path'].$_POST['judul'].'/';
                        $zip->extractTo($file_dir);
                        $zip->close();
                        $files = glob($file_dir."*.html");
                        $_POST['filename'] = $files[0];
                        unlink($config['upload_path'].$upload_data['file_name']);
                        $this->session->set_flashdata('msg','Upload & Extract successfully.');
                      } else {
                        $this->session->set_flashdata('msg','Failed to extract.');
                      }

					$this->$model->update($_POST);
				}
				else
				{
					$data['upload_result'] = "<div class='alert alert-warning' role='alert'><strong>Failed!</strong> ".$this->upload->display_errors()."</div>";
					$data['upload_status']=0;
				}
			}

            /* Prosesing Upload Attachement */
			if(isset($_FILES['cover']) and !empty($_FILES)) {
				if (!file_exists('upload/cover/')) 
					mkdir('upload/cover/', 0777, true); //add true agar membuat file recursive
				else
					chmod('upload/cover/', 0777);
					
				$ID_KATEGORI = $_POST['kategori'];
				if (!file_exists('upload/cover/'.$ID_KATEGORI.'/'))
					mkdir('upload/cover/'.$ID_KATEGORI.'/', 0777, true);

                $name = $_FILES["cover"]["name"];
                $ext = explode(".", $name);
                $filename = strtolower($_POST['judul']).".".end($ext);

				$conf['upload_path'] = 'upload/cover/'.$ID_KATEGORI.'/';
				$conf['allowed_types'] = 'gif|jpg|jpeg|png|bmp|zip';
				$conf['overwrite'] = true;
				$conf['file_name'] = $filename;
				
                $this->upload->initialize($conf);

				if ($this->upload->do_upload('cover'))
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status']= 1;
					$upload_data = $this->upload->data();
					$_POST['cover'] = $conf['upload_path'].str_replace(" ","_",$filename);
					$this->$model->update($_POST);
				}
				else
				{
					$data['upload_result'] = "<div class='alert alert-warning' role='alert'><strong>Failed!</strong> Process upload file failed, please check file upload & resubmit.".$this->upload->display_errors()."</div>";
					$data['upload_status']=0;
				}
			}

			$result = array(
				'status' => 1,
				'error' => $data,
				'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Has been saved.</div>"
			);

			$this->template->render_json($result);
		}
	}
	
	public function update($id='')
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_form_validation());
		$this->form_validation->set_message('required','%s Please fill in this field. !');
		if($this->form_validation->run() == FALSE)
		{
			$result = array(
				'status' => 0,
				'error' => $this->_array_remove_index(array(
							'title' => form_error('title', '<div class="form-control-feedback">', '</div>'),
							'tahun_terbit' => form_error('tahun_terbit', '<div class="form-control-feedback">', '</div>'),
							'id_kategori' => form_error('id_kategori', '<div class="form-control-feedback">', '</div>'),
							'attachment' => form_error('attachment', '<div class="form-control-feedback">', '</div>')
					)),
				'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> ".$this->upload->display_errors()."</div>"
			);

			$this->template->render_json($result);
		}
		else
		{
			$this->$model->update($_POST);
			$data['result']['id'] = $_POST['id'];
			$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
			$data['upload_status']= 1;
			/* Prosesing Upload Attachement */
			if(isset($_FILES['attachment']['name']) and !empty($_FILES['attachment']['name'])){
				
				//chmod('upload/', 0777);
				/* Create folder attachment if doesn't exist */
				if (!file_exists('upload/regulasi/')) 
					mkdir('upload/regulasi/', 0777, true); //add true agar membuat file recursive
				//else
				//	chmod('upload/attachment/', 0777);
					
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/regulasi/'.date("Y").'/'))
					mkdir('upload/regulasi/'.date("Y").'/', 0777, true);
				//else
				//	chmod('upload/attachment/'.date("Y").'/', 0777);
				
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/regulasi/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/regulasi/'.date("Y").'/'.date("m").'/', 0777, true);
				//else
				//	chmod('upload/attachment/'.date("Y").'/'.date("m").'/', 0777);
				
				$ID_KATEGORI = $_POST['id_kategori'];
				if (!file_exists('upload/regulasi/'.date("Y").'/'.date("m").'/'.$ID_KATEGORI.'/'))
					mkdir('upload/regulasi/'.date("Y").'/'.date("m").'/'.$ID_KATEGORI.'/', 0777, true);
				//else
				//	chmod('upload/attachment/'.date("Y").'/'.date("m").'/'.$ID_KATEGORI.'/', 0777);
					
				$config['upload_path'] = 'upload/regulasi/'.date("Y").'/'.date("m").'/'.$ID_KATEGORI.'/';
				$config['allowed_types'] = 'pdf';
				$config['overwrite'] = true;
				// $config['max_size']	= '20480'; //4mb
				$config['file_name'] = strtolower($_FILES["attachment"]['name']);
				
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('attachment'))
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status']= 1;
					$upload_data = $this->upload->data();
					
					$_POST['filename'] = $upload_data['client_name'];
					$_POST['keypdf'] = md5($_POST['id']);
					$_POST['attachment'] = $config['upload_path'].$upload_data['file_name'];

					$this->$model->update($_POST);
				}else{
					$data['upload_result'] = "<div class='alert alert-warning' role='alert'><strong>Failed!</strong> Process upload failed, please check file upload & resubmit.</div>";
					$data['upload_status']=0;
				}
			}

			$result = array(
				'status' => 1,
				'error' => $data,
				'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Has been updated.</div>"
			);

			$this->template->render_json($result);
		}
	}

	public function delete()
	{
		parent::delete();
	}

	public function set_status()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		$model = $this->model_name;
		$table_name = $this->$model->table_name;
		$id = $_POST['id'];
		$id_kategori = $_POST['id_kat'];

		$this->$model->update($_POST);

		// update_last_id_after_set_status($table_name, $id, $id_kategori);
	}

	private function _get_details($id)
	{
		$sql = "SELECT br.*, bc.deskripsi
				FROM bsm_regulasi br
				LEFT JOIN bsm_config bc ON br.id_kategori = bc.id_lookup
				WHERE br.id = $id
				";
		$results = $this->db->query($sql)->row_array();
		return $results;
	}

	private function _form_validation()
	{
		$config = array(
			array(
					'field' => 'id',
					'label' => 'ID',
					'rules' => ''
			),
			array(
					'field' => 'judul',
					'label' => 'Judul',
					'rules' => 'trim|required'
			),
			array(
					'field' => 'tahun_terbit',
					'label' => 'Tahun terbit',
					'rules' => 'trim|required'
			),
			array(
					'field' => 'kategori',
					'label' => 'Kategori',
					'rules' => 'required'
			)
		);

		return $config;
	}

	private function _array_remove_index($myarray = array())
	{
		foreach($myarray as $key=>$value)
		{
			if(is_null($value) || $value == '')
				unset($myarray[$key]);
		}

		return $myarray;
	}


	// checkFileValidation
	public function checkFileValidation($string) 
	{
		$file_mimes = array('text/x-comma-separated-values', 
				'text/comma-separated-values', 
				'application/octet-stream', 
				'application/vnd.ms-excel', 
				'application/x-csv', 
				'text/x-csv', 
				'text/csv', 
				'application/csv', 
				'application/excel', 
				'application/vnd.msexcel', 
				'text/plain', 
				'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		);

		if(isset($_FILES['fileURL']['name'])) {
			$arr_file = explode('.', $_FILES['fileURL']['name']);
			$extension = end($arr_file);
			if(($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)){
				return true;
			}else{
				$this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
				return false;
			}
		}else{
			$this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
			return false;
		}
    }

}
