<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends MY_Controller {

	public function __construct()
    {
		parent::__construct();
		validate_session();

		$this->uri_name = 'setting';
		$this->title = 'Setting';
		$this->load->helper('html');
	}
	
	public function index()
	{
		$breadcrumbs = array(array('url' => $this->uri_name, 'title' => $this->title, 'active' => true));
		$data['breadcrumbs'] = breadcrumbs($breadcrumbs, '&raquo;');
		
		$this->template->view($this->uri_name.'/list', $data);
	}

	public function configuration() {
		$flashmessage = $this->session->flashdata('configuration');
		$flashmessage_type = $this->session->flashdata('configuration_type');
		if (!empty($flashmessage))
			$this->message = $flashmessage;
			$this->message_type = $flashmessage_type;
				
		$this->template->view($this->uri_name.'/form_configuration');
	}

	## menu:: HUBUNGI KAMI
	public function hubungikami($id='') {
		
		$this->subtitle = 'Hubungi Kami';
		if(empty($id) && !isset($_POST['id']))
		{
			if(!$this->input->is_ajax_request()) show_404(uri_string());
			
			$result = $this->db->get_where('bsm_config', array('groups' => 'Hubungi Kami'))->result_array();
			$submit_action = 'hubungikami/id/'.$result[0]['id_lookup'];

			$args['controller_name'] = $this->uri_name;
			$args['action'] = $submit_action;
			$args['result'] = $result;

			$this->template->render($this->uri_name.'/form_hubungikami', $args, FALSE);
		}
		else
		{
			$this->db->set('catatan', $this->input->post('alamat'));
			$this->db->where('id_lookup', $this->input->post('id_alamat'));
			$this->db->update('bsm_config');
			
			$this->db->set('catatan', $this->input->post('email'));
			$this->db->where('id_lookup', $this->input->post('id_email'));
			$this->db->update('bsm_config');

			$this->db->set('catatan', $this->input->post('tlpn'));
			$this->db->where('id_lookup', $this->input->post('id_tlpn'));
			$this->db->update('bsm_config');
			
			$socmeds = '[
				{
					"socmed": "Instagram",
					"image": "'.base_url('public/assets/instagram.png').'",
					"title": "'.$this->input->post('socmed1').'",
					"url": "'.$this->input->post('socmed11').'"
				},
				{
					"socmed": "Youtube",
					"image": "'.base_url('public/assets/youtube.png').'",
					"title": "'.$this->input->post('socmed2').'",
					"url": "'.$this->input->post('socmed22').'"
				},
				{
					"socmed": "Linkedin",
					"image": "'.base_url('public/assets/linkedin.png').'",
					"title": "'.$this->input->post('socmed3').'",
					"url": "'.$this->input->post('socmed33').'"
				}
			]';
			
			
			$this->db->set('catatan', $socmeds);
			$this->db->where('id_lookup', $this->input->post('id_sosmed'));
			$this->db->update('bsm_config');

			$this->session->set_flashdata('configuration', "<i class='fa fa-check'></i> ".$this->subtitle." - Berhasil diupdate.");
			$this->session->set_flashdata('configuration_type', 'alert-success');
			redirect($this->uri_name.'/configuration');
		}
	}

	## menu:: INFO TUKAR POIN
	public function infotukarpoin($id='') {
		
		$this->subtitle = 'Info Tukar Poin';
		if(empty($id) && !isset($_POST['id']))
		{
			if(!$this->input->is_ajax_request()) show_404(uri_string());
			
			$result = $this->db->get_where('bsm_config', array('groups' => 'Info Tukar Poin'))->row_array();
			$submit_action = 'infotukarpoin/id/'.$result['id_lookup'];

			$args['controller_name'] = $this->uri_name;
			$args['action'] = $submit_action;
			$args['result'] = $result;

			$this->template->render($this->uri_name.'/form_infotukarpoin', $args, FALSE);
		}
		else
		{
			$this->db->set('catatan', $this->input->post('info'));
			$this->db->where('id_lookup', $this->input->post('id'));
			$this->db->update('bsm_config');

			$this->session->set_flashdata('configuration', "<i class='fa fa-check'></i> ".$this->subtitle." - Berhasil diupdate.");
			$this->session->set_flashdata('configuration_type', 'alert-success');
			redirect($this->uri_name.'/configuration');
		}
	}

	## menu:: POIN
	public function poin($id='') {
		
		$this->subtitle = 'Poin';
		if(empty($id) && !isset($_POST['id']))
		{
			if(!$this->input->is_ajax_request()) show_404(uri_string());

			$result = $this->db->get_where('bsm_config', array('groups' => 'Default Poin'))->result_array();
			$submit_action = 'poin/id/'.$result[0]['id_lookup'];

			$args['controller_name'] = $this->uri_name;
			$args['action'] = $submit_action;
			$args['result'] = $result;

			$this->template->render($this->uri_name.'/form_poin', $args, FALSE);
		}
		else
		{
			$this->db->set('catatan', $this->input->post('poin_quiz'));
			$this->db->where('id_lookup', $this->input->post('id_poin_quiz'));
			$this->db->update('bsm_config');
			
			$this->db->set('catatan', $this->input->post('poin_pertanyaan'));
			$this->db->where('id_lookup', $this->input->post('id_poin_pertanyaan'));
			$this->db->update('bsm_config');

			$this->db->set('catatan', $this->input->post('poin_gratifikasi'));
			$this->db->where('id_lookup', $this->input->post('id_poin_gratifikasi'));
			$this->db->update('bsm_config');

			$this->session->set_flashdata('configuration', "<i class='fa fa-check'></i> ".$this->subtitle." - Berhasil diupdate.");
			$this->session->set_flashdata('configuration_type', 'alert-success');
			redirect($this->uri_name.'/configuration');
		}
	}
	
	public function reset_poin() {
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		
		$this->db->set('poin', 0);
		$result = $this->db->update('bsm_user');
		
		db_log('Reset Poin', 'Reset Poin', '', array());

		if ( ! $result)
		{
			 $response = array(
					 'message' => "Failed Reset Poin",
					'status' => false
					 );

		}else{

			 // Function ran ok - do whatever
				 if($this->db->affected_rows() == true)
				{
					$response = array(
					 'message' => "User edited successfully",
					'status' => true
					 );
				} else {
				  $response = array(
				  'message' => "There is nothing to update",
				   'status' => false
				  );
			 }

		}
		
		$this->template->render_json($result);
		// UPDATE `25bsm_app`.`bsm_user` SET `poin` = 0
	}
	
	## menu:: WHITELIST DOMAIN
	public function whitelist_domain($id='') {
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		$result = $this->db->get_where('bsm_config', array('groups' => 'Whitelist Domain'))->result_array();
		
		$args['list_domain'] = $result;
		$args['controller_name'] = $this->uri_name;
		$this->template->render($this->uri_name.'/list_domain', $args, FALSE);
	}
	
	public function form_whitelist_domain($id='') {
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$submit_action = 'add_whitelist_domain';
		$result = NULL;
		if (!empty($id))
		{
			$this->db->select('*')
					->from('bsm_config')
					->where('id_lookup', $id);
			$result = $this->db->get()->row_array();
			$submit_action = 'update_whitelist_domain/id/'.$id;
		}
		$args['result']          = $result;
		$args['controller_name'] = $this->uri_name;
		$args['action']          = $submit_action;
		$this->template->render($this->uri_name.'/form_whitelist', $args, FALSE);
	}
	
	public function add_whitelist_domain() {
		if(!$this->input->is_ajax_request()) show_404(uri_string());

			$data = array(
				'id_lookup' => $this->randomString(),
				'groups' => 'Whitelist Domain',
				'deskripsi' => 'whitelistdomain',
				'catatan' => trim($this->input->post('domain'))
			);
			$this->db->insert('bsm_config',$data);
			
			$result = array(
				'status' => 1,
				'error' => array(),
				'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Has been saved.</div>"
			);

			$this->template->render_json($result);
	}

	public function update_whitelist_domain($id='') {
		if(!$this->input->is_ajax_request()) show_404(uri_string());
			
			$data['id_lookup'] = $this->input->post('id'); 
			$data['catatan'] = $this->input->post('domain'); 
			
			$config = array (
				'id_lookup',
				'catatan',
			);

			$datas = $this->set_data($config, $data);
			$this->db->where('id_lookup', $data['id_lookup']);
			$this->db->update('bsm_config', $datas);
			
			$result = array(
				'status' => 1,
				'error' => array(),
				'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Has been updated.</div>"
			);

			$this->template->render_json($result);
	}
	
	public function form_whitelist_domain2() {
		$this->subtitle = 'Whitelist Domain';
		if(empty($id) && !isset($_POST['id']))
		{
			if(!$this->input->is_ajax_request()) show_404(uri_string());
			
			$result = $this->db->get_where('bsm_config', array('groups' => 'Whitelist Domain'))->result_array();
			$submit_action = 'whitelist_domain/id/'.$result[0]['id_lookup'];

			$args['controller_name'] = $this->uri_name;
			$args['action'] = $submit_action;
			$args['result'] = $result;

			$this->template->render($this->uri_name.'/form_whitelist', $args, FALSE);
		}
		else
		{
			$this->db->set('catatan', $this->input->post('domain'));
			$this->db->where('id_lookup', $this->input->post('id_wlist'));
			$this->db->update('bsm_config');

			$this->session->set_flashdata('configuration', "<i class='fa fa-check'></i> ".$this->subtitle." - Berhasil diupdate.");
			$this->session->set_flashdata('configuration_type', 'alert-success');
			redirect($this->uri_name.'/configuration');
		}
	}
	
	public function delete_whitelist_domain() {
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		$id = $this->input->post('id');
		
		$this->db->where('id_lookup', $id);
		$this->db->delete('bsm_config');
	}
	
	## menu:: LOGO
	public function logo($id='') {
		
		$this->subtitle = 'Logo';
		if(empty($id) && !isset($_POST['id']))
		{
			if(!$this->input->is_ajax_request()) show_404(uri_string());

			$result = $this->db->get_where('bsm_config', array('groups' => 'Logo'))->result_array();
			$submit_action = 'logo/id/'.$result[0]['id_lookup'];
			
			$args['controller_name'] = $this->uri_name;
			$args['action'] = $submit_action;
			$args['result'] = $result;

			$this->template->render($this->uri_name.'/form_logo', $args, FALSE);
		}
		else
		{
			/* Create folder image if doesn't exist */
			if (!file_exists('public/assets/')) 
				mkdir('public/assets/', 0777, true); //add true agar membuat file recursive
			$this->load->library('image_lib');
			
			$config['upload_path'] = 'public/assets/';
			$config['allowed_types'] = 'png';
			// $config['overwrite'] = TRUE;
			$config['encrypt_name'] = TRUE;
			// $config['file_name'] = 'logo';
			
			for ($i=1; $i <=3 ; $i++) { 
			$this->load->library('upload', $config);
				if(!empty($_FILES['logo'.$i]['name'])){
					if($this->upload->do_upload('logo'.$i))
						$upload_data = $this->upload->data();
						$logo = $config['upload_path'].$upload_data['file_name'];
						
						$this->db->set('catatan', $logo);
						$this->db->where('id_lookup', $this->input->post('id_logo'.$i));
						$this->db->update('bsm_config');
					}
			}
			
			header("Cache-Control: no-cache, must-revalidate");
			header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header("Content-Type: application/xml; charset=utf-8");
			
			$this->session->set_flashdata('configuration', "<i class='fa fa-check'></i> ".$this->subtitle." - Berhasil diupdate.");
			$this->session->set_flashdata('configuration_type', 'alert-success');
			redirect($this->uri_name.'/configuration');
		}
	}



	## menu:: SLIDER
	public function slider($id='') {
		$this->subtitle = 'Slider';
		$breadcrumbs = array(
			array('url' => '', 'title' => $this->title),
			array('url' => $this->uri_name.'/slider', 'title' => $this->subtitle),
			array('url' => $this->uri_name, 'title' => 'list', 'active' => true)
		);

		$status      = array('all' => 'All', 'yes' => 'Yes', 'no' => 'No');
		$opt_status      = form_dropdown('fillStatus',$status,'','id="fillStatus" class="custom-select custom-select-label form-control m-input m-input--square"');

		$args['title'] = $this->subtitle;
		$args['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->subtitle);
		$args['controller_name'] = $this->uri_name;
		$args['url_tablelist'] = $this->uri_name.'/table_list_slider';
		$args['status'] = $opt_status;

		$this->template->view($this->uri_name.'/list_slider', $args);
	}

	public function table_list_slider() {
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$this->load->model('setting_slider_model');
		$result = $this->setting_slider_model->get_datatable();

		$this->template->render_json($result);
	}

	public function form_slider($id='') {
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$submit_action = 'add_slider';
		$result = NULL;
		$hyperlink_title = NULL;
		if (!empty($id))
		{
			$sql = "SELECT * FROM bsm_setting_slider WHERE id = $id";
			$result = $this->db->query($sql)->row_array();
			if($result['is_hyperlink'] == 1 && $result['content_category'] != "other_link") {
				
				$result['detail'] = $this->get_detail_content($result['content_id'], $result['content_category']);
				
			}
				

			$submit_action = 'update_slider/id/'.$id;
		}

		$regulasi_option = array(
			'regulasi' => 'Regulasi', 
			'ketentuan_internal' => 'Ketentuan Internal', 
			'erm' => 'ERM', 
			'apuppt' => 'APUPPT', 
			'video' => 'Video',
			'infografis' => 'Infografis',
			'artikel' => 'Artikel', 
			'news' => 'News', 
			'other_link' => 'Other Link'
		);

		$args['result']          = $result;
		$args['hyperlink_title']   = $hyperlink_title;
		$args['controller_name'] = $this->uri_name;
		$args['action']          = $submit_action;
		$args['list_kategori']	 = $regulasi_option;

		$this->template->render($this->uri_name.'/form_slider', $args, FALSE);
	}

	public function get_detail_content($id='', $category="") {
		
		$results = null;
		if($category=="regulasi"){
			$table_name = 'bsm_regulasi';
		}elseif($category=="ketentuan_internal"){
			$table_name = 'bsm_kinternal';
		}elseif($category=="erm"){
			$table_name = 'bsm_erm';
		}elseif($category=="apuppt"){
			$table_name = 'bsm_apuppt';
		}elseif($category=="video"){
			$table_name = 'bsm_video';
		}elseif($category=="infografis"){
			$table_name = 'bsm_infografis';
		}elseif($category=="artikel"){
			$table_name = 'bsm_artikel';
		}elseif($category=="news"){
			$table_name = 'bsm_news';
		}
		
		$results = $this->db->select('*')
					->from($table_name)
					->where('id', $id)
					->get()
					->row();
					
		return $results;
	}
	
	public function add_slider() {
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$this->load->model('setting_slider_model');
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_form_validation());
		$this->form_validation->set_message('required','%s Please fill in this field. !');
		if($this->form_validation->run() == FALSE)
		{
			$result = array(
				'status' => 0,
				'error' => $this->_array_remove_index(array(
							'title' => form_error('title', '<div class="form-control-feedback">', '</div>')
					)),
				'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> Please fill in the data correctly and accordingly before submit</div>"
			);

			$this->template->render_json($result);
		}
		else
		{
			if(!isset($_POST['is_hyperlink'])) {
				$_POST['is_hyperlink'] = 0;
				$_POST['content_category'] = "";
				$_POST['content_id'] = "";
				$_POST['content_url'] = "";
			}
			
			if(isset($_POST['id']) and trim($_POST['id'])!=''){
				$this->setting_slider_model->update($_POST);
				$data['result']['id'] = $_POST['id'];
			}else{
				$count_current_row = $this->db->count_all($this->setting_slider_model->table_name);
				$_POST['order_slider'] = $count_current_row+1;

				$data['result']['id'] = $this->setting_slider_model->add($_POST);
				$_POST['id'] = $data['result']['id'] ;
			}

			/* Prosesing Upload Attachement */
			if(isset($_FILES) and !empty($_FILES)){
				
				/* Create folder image if doesn't exist */
				if (!file_exists('upload/slider/')) 
					mkdir('upload/slider/', 0777, true); //add true agar membuat file recursive
					
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/slider/'.date("Y").'/'))
					mkdir('upload/slider/'.date("Y").'/', 0777, true);
				
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/slider/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/slider/'.date("Y").'/'.date("m").'/', 0777, true);
				
				if (!file_exists('upload/slider/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/slider/'.date("Y").'/'.date("m").'/', 0777, true);
					
				$config['upload_path'] = 'upload/slider/'.date("Y").'/'.date("m").'/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
				$config['overwrite'] = true;
				// $config['max_size']	= '2048'; //2mb
				$config['file_name'] = strtolower(date('YmdHis').'_'.$_FILES["image"]['name']);
				
				$this->load->library('upload', $config);
				$this->load->library('image_lib');
				if ($this->upload->do_upload('image'))
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status']= 1;
					$upload_data = $this->upload->data();
					$_POST['image'] = $config['upload_path'].$upload_data['file_name'];
					
					//Rezise image thumbnail
					/* Create folder image if doesn't exist */
					if (!file_exists('upload/slider/thumbnail/')) 
					mkdir('upload/slider/thumbnail/', 0777, true); //add true agar membuat file recursive

					$thumbnail_name = $upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];
					$source_path = $upload_data['full_path'];
					$target_path = 'upload/slider/thumbnail/';
					$config_manip = array(
						'image_library' => 'gd2',
						'source_image' => $source_path,
						'new_image' => $target_path,
						'maintain_ratio' => TRUE,
						'create_thumb' => TRUE,
          				'thumb_marker' => '_thumb',
						'width' => 150,
						'height' => 150
					);
					$this->image_lib->initialize($config_manip);
					$this->image_lib->resize();
					// echo $this->image_lib->display_errors();
					$this->image_lib->clear();

					$_POST['thumbnail'] = $config_manip['new_image'].$thumbnail_name;
					$this->setting_slider_model->update($_POST);
				}
				else
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status'] =1;
					$path_no_image = 'public/assets/img/default-small.jpg';
					$_POST['thumbnail'] = $path_no_image;
					$_POST['image'] = $path_no_image;

					$this->setting_slider_model->update($_POST);
				}
			}
			
			$result = array(
				'status' => 1,
				'error' => $data,
				'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Has been saved.</div>"
			);

			$this->template->render_json($result);
		}
	}
	
	public function update_slider($id='') {
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$this->load->model('setting_slider_model');
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_form_validation());
		$this->form_validation->set_message('required','%s Please fill in this field. !');
		if($this->form_validation->run() == FALSE)
		{
			$result = array(
				'status' => 0,
				'error' => $this->_array_remove_index(array(
							'title' => form_error('title', '<div class="form-control-feedback">', '</div>')
					)),
				'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> Please fill in the data correctly and accordingly before submit</div>"
			);

			$this->template->render_json($result);
		}
		else
		{
			if(!isset($_POST['is_hyperlink'])) {
				$_POST['is_hyperlink'] = 0;
				$_POST['content_category'] = "";
				$_POST['content_id'] = "";
				$_POST['content_url'] = "";
			}
			
			$this->setting_slider_model->update($_POST);
			$data['result']['id'] = $_POST['id'];
			$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
			$data['upload_status']= 1;
			/* Prosesing Upload Attachement */
			if(isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])){
				
				/* Create folder image if doesn't exist */
				if (!file_exists('upload/slider/')) 
					mkdir('upload/slider/', 0777, true); //add true agar membuat file recursive
					
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/slider/'.date("Y").'/'))
					mkdir('upload/slider/'.date("Y").'/', 0777, true);
				
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/slider/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/slider/'.date("Y").'/'.date("m").'/', 0777, true);
				
				if (!file_exists('upload/slider/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/slider/'.date("Y").'/'.date("m").'/', 0777, true);

					
				$config['upload_path'] = 'upload/slider/'.date("Y").'/'.date("m").'/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
				$config['overwrite'] = true;
				// $config['max_size']	= '2048'; //2mb
				$config['file_name'] = strtolower($_FILES["image"]['name']);
				
				$this->load->library('upload', $config);
				$this->load->library('image_lib');
				if ($this->upload->do_upload('image'))
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status']= 1;
					$upload_data = $this->upload->data();
					
					$_POST['image'] = $config['upload_path'].$upload_data['file_name'];

					//Rezise image thumbnail

					/* Create folder image if doesn't exist */
					if (!file_exists('upload/slider/thumbnail/')) 
					mkdir('upload/slider/thumbnail/', 0777, true); //add true agar membuat file recursive
					
					$thumbnail_name = $upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];
					$source_path = $upload_data['full_path'];
					$target_path = 'upload/slider/thumbnail/';
					$config_manip = array(
						'image_library' => 'gd2',
						'source_image' => $source_path,
						'new_image' => $target_path,
						'maintain_ratio' => TRUE,
						'create_thumb' => TRUE,
          				'thumb_marker' => '_thumb',
						'width' => 150,
						'height' => 150
					);
					$this->image_lib->initialize($config_manip);
					$this->image_lib->resize();
					// echo $this->image_lib->display_errors();
					$this->image_lib->clear();

					$_POST['thumbnail'] = $config_manip['new_image'].$thumbnail_name;

					$this->setting_slider_model->update($_POST);
				}
			}

			$result = array(
				'status' => 1,
				'error' => $data,
				'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Has been updated.</div>"
			);

			$this->template->render_json($result);
		}
	}

	public function delete() {
		// parent::delete();
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		
		$this->model_name = 'setting_slider_model';							
		$this->load->model($this->model_name);
		$model = $this->model_name;

		$id = $this->input->post('id');
		$this->$model->delete($id);
	}

	public function set_status() {
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		$this->load->model('setting_slider_model');

		$this->setting_slider_model->update($_POST);
	}

	public function update_order_slider() {
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$this->load->model('setting_slider_model');
		$_POST['id'] = substr($this->input->post('id'),6);
		$this->setting_slider_model->update($_POST);
		echo "1";
	}

	public function hyperlinkSearchContent() {
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		// $results = $this->_show_respone();
		$this->benchmark->mark('code_start');
		$keyword = trim(urldecode($this->input->get('q')));
		$cat     = $this->input->get('cat');
        $page = 1;
		if($this->input->get('page') != NULL)
		{
			$page = $this->input->get('page');
        }

		// set the number of items to display per page
		$items_per_page = 30;

		// build query
		$offset = ($page - 1) * $items_per_page;
		
		if($cat=="regulasi" || $cat=="ketentuan_internal" || $cat=="erm" || $cat=="apuppt") {
			$results = $this->get_list_categoty($keyword, $offset, $items_per_page, $this->input->get('cat'));
		}elseif($cat=="video" || $cat=="infografis" || $cat=="artikel" || $cat=="news"){
			$results = $this->get_list_categoty_other($keyword, $offset, $items_per_page, $this->input->get('cat'));
		}

		$this->benchmark->mark('code_end');
		$this->template->render_json($results);
	}

	protected function get_list_categoty($keyword='', $offset='', $per_page='', $category) {

		if($category=="regulasi"){
			$table_name = 'bsm_regulasi';
		}elseif($category=="ketentuan_internal"){
			$table_name = 'bsm_kinternal';
		}elseif($category=="erm"){
			$table_name = 'bsm_erm';
		}elseif($category=="apuppt"){
			$table_name = 'bsm_apuppt';
		}
		
		$sql = "SELECT A.*, bsm_config.deskripsi
				FROM $table_name AS A
				LEFT JOIN bsm_config on bsm_config.id_lookup = A.id_kategori
				WHERE status = 1 
			";
		
		$tempdb = clone $this->db;
		$total  = $tempdb->count_all_results();

		if(!empty($keyword))
		{
			$sql .= " AND (title LIKE '%".$keyword."%' OR filename LIKE '%".$keyword."%') ";
		}

		if($category !=="apuppt")
			$sql .= " ORDER BY tahun_terbit desc ";
		
		$sql .= " LIMIT $offset, $per_page";

		$results = $this->db->query($sql);

		$result = array();
		if ($results->num_rows() > 0)
		{
			foreach ($results->result_array() as $key => $list)
			{
				$result[$key]['id']        = $list['id'];
				$result[$key]['kategori']  = $list['deskripsi'];
				$result[$key]['title']   = $list['title'];
				$result[$key]['content_url'] = $list['attachment'];
				$result[$key]['created']  = convert_date('Y-m-d H:i:s', $list['created'], 'M d, Y, H:i a');
				$result[$key]['tahun_terbit']  = ($category !=="apuppt" ? $list['tahun_terbit'] : '-');
			}
		}

		return $this->_show_respone($result, $total);
	}
	
	protected function get_list_categoty_other($keyword='', $offset='', $per_page='', $category) {

		if($category=="video"){
			$table_name = 'bsm_video';
		}elseif($category=="infografis"){
			$table_name = 'bsm_infografis';
		}elseif($category=="artikel"){
			$table_name = 'bsm_artikel';
		}elseif($category=="news"){
			$table_name = 'bsm_news';
		}
		
		$sql = "SELECT *
				FROM $table_name
				WHERE status = 1 
			";
		
		$tempdb = clone $this->db;
		$total  = $tempdb->count_all_results();

		if(!empty($keyword))
		{
			$sql .= " AND title LIKE '%".$keyword."%' ";
		}
		
		$sql .= " LIMIT $offset, $per_page";

		$results = $this->db->query($sql);

		$result = array();
		if ($results->num_rows() > 0)
		{
			foreach ($results->result_array() as $key => $list)
			{
				if($category=="video"){
					$table_name = 'bsm_video';
				}elseif($category=="infografis"){
					$result[$key]['content_url']  = $list['image'];
				}elseif($category=="artikel"){
					$table_name = 'bsm_artikel';
				}elseif($category=="news"){
					$table_name = 'bsm_news';
				}
				
				$result[$key]['id']        = $list['id'];
				$result[$key]['kategori']  = $category;
				$result[$key]['title']   = $list['title'];
				
				// $result[$key]['url'] = base_url('api/v1/User/view_pdf?path=').$list['attachment']."&kat=regulasi&id=".$list['id'];
				$result[$key]['created']  = convert_date('Y-m-d H:i:s', $list['created'], 'M d, Y, H:i a');
			}
		}

		return $this->_show_respone($result, $total);
	}

	protected function _show_respone($results = array(), $total=0) {
        return [
            'status' => 200,
            'total_count' => $total,
            'elapsedtime' => $this->benchmark->elapsed_time('code_start', 'code_end'),
            'items' => $results
        ];
    }

	protected function randomString()
	{
		$sql = 'SELECT SUBSTRING(MD5(RAND()) FROM 1 FOR 15) AS id_lookup';
		$query = $this->db->query($sql);
		$row = $query->row_array();
		return $row['id_lookup'];
	}
	
	protected function set_data($nama_coloumn, $sumber)
	{
		$hasil = array();
			foreach($nama_coloumn as $row)
			{
				if(isset($sumber[$row]))
				{
					$hasil[$row] = $sumber[$row];
				}
			}
		
		return $hasil;
	}
	
	## Validation config
	private function _form_validation() {
		$config = array(
			array(
					'field' => 'id',
					'label' => 'ID',
					'rules' => ''
			),
			array(
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required'
			)
		);

		return $config;
	}

	private function _array_remove_index($myarray = array()) {
		foreach($myarray as $key=>$value)
		{
			if(is_null($value) || $value == '')
				unset($myarray[$key]);
		}

		return $myarray;
	}
}
