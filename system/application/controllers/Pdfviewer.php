<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdfviewer extends MY_Controller {

	public function __construct()
  	{
		parent::__construct();

		$this->uri_name = 'pdfviewer';
		$this->title = 'PDF Viewer';
	}

	public function index()
	{
		$this->template->render($this->uri_name.'/pdf_viewer', array(), FALSE);
	}
}
