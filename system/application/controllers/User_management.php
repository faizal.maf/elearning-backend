<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
//PhpSpreadsheet
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class User_management extends MY_Controller {

	public function __construct()
  	{
		parent::__construct();
		validate_session();

		$this->uri_name = 'user_management';
		$this->title = 'User Management';
		$this->model_name = 'user_management_model';							
		$this->load->model($this->model_name);
		$this->load->helper('html');
	}

	public function index()
	{
		$flashmessage = $this->session->flashdata($this->uri_name);
		$flashmessage_type = $this->session->flashdata($this->uri_name.'_type');
		if (!empty($flashmessage))
			$this->message = $flashmessage;
			$this->message_type = $flashmessage_type;

		$breadcrumbs = array(
			array('url' => $this->uri_name, 'title' => $this->title),
			array('url' => $this->uri_name, 'title' => 'List', 'active' => true)
		);

		// FILTER STATUS
		$status     = array('all' => 'All', 'active' => 'Active', 'inactive' => 'Inactive');
		$opt_status = form_dropdown('fillStatus',$status,'','id="fillStatus" class="custom-select custom-select-label form-control m-input m-input--square"');

		// FILTER LEVEL
		$lvl_opt = array('all' => 'All');
		$sql = "SELECT * FROM bsm_apps_level ORDER BY nama ASC";
		$list_records = $this->db->query($sql)->result();
		foreach ($list_records as $list_record_idx=>$list_records) {
			$lvl_opt[$list_records->id_level] = ''.$list_records->nama;
		}
		$opt_lvl = form_dropdown('fillLvl',$lvl_opt,'','id="fillLvl" class="custom-select custom-select-label form-control m-input m-input--square"');

		// FILTER JABATAN
		$jabatan_opt = array('all' => 'All');
		$opt_sql = "SELECT id_jabatan, jabatan_name AS options_name
					FROM bsm_jabatan
					ORDER BY jabatan_name ASC
					";
		$list_lvl_records = $this->db->query($opt_sql)->result();
		foreach ($list_lvl_records as $list_lvl_records_idx=>$list_lvl_records)
		{
			$jabatan_opt[$list_lvl_records->id_jabatan] = $list_lvl_records->options_name;
		}
		$opt_jabatan = form_dropdown('fillJabatan',$jabatan_opt,'','id="fillJabatan" class="custom-select custom-select-label form-control m-input m-input--square"');

		// SET DATA
		$data['title'] = $this->title;
		$data['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title, $this->uri_name, 'New '.$this->title);
		$data['controller_name'] = $this->uri_name;
		$data['url_tablelist'] = $this->uri_name.'/table_list';
		$data['status'] = $opt_status;
		$data['level'] = $opt_lvl;
		$data['jabatan'] = $opt_jabatan;

		$this->template->view($this->uri_name.'/list', $data);
	}

	public function table_list()
	{
		parent::table_list();
	}
	
	public function form($id='')
	{
		$flashmessage = $this->session->flashdata($this->uri_name);
		$flashmessage_type = $this->session->flashdata($this->uri_name.'_type');
		if (!empty($flashmessage))
			$this->message = $flashmessage;
			$this->message_type = $flashmessage_type;
			
		#-- Options Jabatan
		$groups_opt = array('' => 'Select Jabatan');
		$opt_sql = "SELECT id_jabatan, jabatan_name AS options_name
					FROM bsm_jabatan
					ORDER BY jabatan_name ASC
					";
		$list_lvl_records = $this->db->query($opt_sql)->result();
		foreach ($list_lvl_records as $list_lvl_records_idx=>$list_lvl_records)
		{
			$groups_opt[$list_lvl_records->id_jabatan] = $list_lvl_records->options_name;
		}

		$this->_displayed_additional = array(
						'groups_list' => $groups_opt
					);
			
		parent::form($id);
	}

	public function insert()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;
		$this->load->library(array('form_validation','func_validation'));
		$this->form_validation->set_rules($this->$model->get_fields());
		if($this->form_validation->run() == FALSE)
		{
			$result = $this->func_validation->set_form_error($this->$model->get_fields());
		}
		else
		{
			$data = array();
			foreach($this->$model->get_fields() as $properties)
				$data[$properties['field']] = set_value($properties['field']);
			
			$data = array_merge($data, ['poin' => 0, 'status' => 1]);
			
			foreach($this->$model->get_fields() as $properties)
				$this->$model->modify_value_fields($properties['field'], (isset($data[$properties['field']]) ? $data[$properties['field']] : ''));

			$this->_set_data("insert", $this->$model->get_fields(), $data);
			$respone['data']['result'] = $this->$model->insert();

			$result = $this->func_validation->set_form_success($respone, "Saved");
		}

		$this->template->render_json($result);
	}
	
	public function update($id='')
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		
		$uri_array = $this->uri->uri_to_assoc(3);
		$id = '';
		if (isset($uri_array['id']))
		{
			$id = $uri_array['id'];
			unset($uri_array['id']);
		}

		$model = $this->model_name;
		$this->load->library(array('form_validation','func_validation'));
		$this->form_validation->set_rules($this->$model->get_fields());
		if($this->form_validation->run() == FALSE)
		{
			$result = $this->func_validation->set_form_error($this->$model->get_fields());
		}
		else
		{
			$data = array();
			$this->$model->remove_fields($this->$model->field_id);
			foreach($this->$model->get_fields() as $properties)
			{
				if($properties['field'] != 'poin' && $properties['field'] != 'status' && $properties['field'] != 'created' && $properties['field'] != 'created_by')
					$data[$properties['field']] = set_value($properties['field']);
				else
					$this->$model->remove_fields($properties['field']);
			}

			foreach($this->$model->get_fields() as $properties)
				$this->$model->modify_value_fields($properties['field'], (isset($data[$properties['field']]) ? $data[$properties['field']] : ''));

			$this->_set_data("update", $this->$model->get_fields(), $data);
			$respone['data']['result'] = $this->$model->update($id);

			$result = $this->func_validation->set_form_success($respone, "Updated");
		}
		$this->template->render_json($result);
	}

	public function delete()
	{
		parent::delete();
	}

	public function set_status()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		$model = $this->model_name;

		$this->db->set('status', $this->input->post('status'));
		$this->$model->update($this->input->post('id'));
	}

	protected function _set_data($type ="insert", $fields, $data)
	{
		foreach($fields as $properties)
		{
			if($type=='insert')
			{
				//unset field ID, karena autoincreament
				if($properties['field'] == 'id')
					unset($data[$properties['field']]);

				if($properties['field'] == 'created')
					$data[$properties['field']] = date('YmdHis');
				if($properties['field'] == 'created_by')
					$data[$properties['field']] = $this->session->userdata('Username');
			}
			else
			{
				if($properties['field'] == 'created' || $properties['field'] == 'created_by' || $properties['field'] == 'poin' || $properties['field'] == 'status')
				{
					if(isset($data[$properties['field']]))
						unset($data[$properties['field']]);
				}
			}

			if($properties['field'] == 'updated')
				$data[$properties['field']] = date('YmdHis');
			if($properties['field'] == 'updated_by')
				$data[$properties['field']]= $this->session->userdata('Username');

			if(isset($data[$properties['field']]))
			{
				if($data[$properties['field']] !== '')
				{
					$this->db->set($properties['field'], $data[$properties['field']]);
				}
				else
				{
					$this->db->set($properties['field'], 'NULL', FALSE);
				}
			}
		}
	}

	## SECTION IMPORT DATA

	public function form_import()
	{
		$breadcrumbs = array(array('url' => $this->uri_name, 'title' => $this->title),
					array('url' => $this->uri_name, 'title' => 'Import Data', 'active' => true));

		$submit_action = 'import_data';
		$args['title'] = "Import data ".$this->title;
		$args['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title);
		$args['controller_name'] = $this->uri_name;
		$args['action'] = $submit_action;

		
		$flashmessage = $this->session->flashdata($this->uri_name);
		$flashmessage_type = $this->session->flashdata($this->uri_name.'_type');
		if (!empty($flashmessage))
			$this->message = $flashmessage;
			$this->message_type = $flashmessage_type;


		$this->template->view($this->uri_name.'/form_import', $args);
	}

	public function import_data()
	{
    	$data = array();
        $breadcrumbs = array(array('url' => $this->uri_name, 'title' => $this->title),
					array('url' => $this->uri_name, 'title' => 'List Data Import', 'active' => true));

		$data['title'] = "List data import ".$this->title;
		$data['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title);
		$data['controller_name'] = $this->uri_name;
		
		$model = $this->model_name;
		// Load form validation library
		$this->load->library('form_validation');
		$this->form_validation->set_rules('fileURL', 'Files', 'callback_checkFileValidation');
		if($this->form_validation->run() == false)
		{
			$this->form_import();
		} 
		else
		{
			// If file uploaded
			if(!empty($_FILES['fileURL']['name']))
			{ 
				// get file extension
				$extension = pathinfo($_FILES['fileURL']['name'], PATHINFO_EXTENSION);

				if($extension == 'csv'){
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
				} elseif($extension == 'xlsx') {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
				} else {
					$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
				}
				// file path
				$spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);
				$allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
			
				// array Count
				$arrayCount = count($allDataInSheet);
				$flag = 0;
				$createArray = array('NAMA', 'EMAIL', 'NIP', 'KODE_JABATAN');
				$makeArray = array('NAMA' => 'NAMA', 'EMAIL' => 'EMAIL', 'NIP' => 'NIP', 'KODE_JABATAN' => 'KODE_JABATAN');
				$SheetDataKey = array();
				foreach ($allDataInSheet as $dataInSheet) {
					foreach ($dataInSheet as $key => $value) {
						if (in_array(trim($value), $createArray)) {
							$value = preg_replace('/\s+/', '', $value);
							$SheetDataKey[trim($value)] = $key;
						} 
					}
				}
				$dataDiff = array_diff_key($makeArray, $SheetDataKey);
				if (empty($dataDiff)) {
					$flag = 1;
				}
				// match excel sheet column
				if ($flag == 1) {
					for ($i = 2; $i <= $arrayCount; $i++) {
						$addresses = array();
						$nama = $SheetDataKey['NAMA'];
						$email = $SheetDataKey['EMAIL'];
						$nip = $SheetDataKey['NIP'];
						$jabatan = $SheetDataKey['KODE_JABATAN'];

						$nama = filter_var(trim($allDataInSheet[$i][$nama]), FILTER_SANITIZE_STRING);
						$email = filter_var(trim($allDataInSheet[$i][$email]), FILTER_SANITIZE_EMAIL);
						$nip = filter_var(trim($allDataInSheet[$i][$nip]), FILTER_SANITIZE_STRING);
						$jabatan = filter_var(trim($allDataInSheet[$i][$jabatan]), FILTER_SANITIZE_STRING);
						$fetchData[] = array(
							'name' => $nama,
							'email' => $email,
							'nip' => $nip,
							'id_jabatan' => $jabatan,
							'status' => 1,
							'poin' => 0,
							'created_by' => 'bulk_data',
							'created' => date('YmdHis'),
							'updated' => date('YmdHis')
						);
					}   
					$data['dataInfo'] = $fetchData;
					$this->$model->setBatchImport($fetchData);
					$this->$model->importData();
				}
				else 
				{
					$this->session->set_flashdata($this->uri_name, "Please import correct file, did not match excel sheet column.");
					$this->session->set_flashdata($this->uri_name.'_type', 'alert-danger');

					// $this->form_import();
					redirect($this->uri_name.'/form_import');
				}

				$this->template->view($this->uri_name.'/display_data_import', $data);
			}
    	}
	}
 
	// checkFileValidation
	public function checkFileValidation($string) 
	{
		$file_mimes = array('text/x-comma-separated-values', 
				'text/comma-separated-values', 
				'application/octet-stream', 
				'application/vnd.ms-excel', 
				'application/x-csv', 
				'text/x-csv', 
				'text/csv', 
				'application/csv', 
				'application/excel', 
				'application/vnd.msexcel', 
				'text/plain', 
				'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		);

		if(isset($_FILES['fileURL']['name'])) {
			$arr_file = explode('.', $_FILES['fileURL']['name']);
			$extension = end($arr_file);
			if(($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)){
				return true;
			}else{
				$this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
				return false;
			}
		}else{
			$this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
			return false;
		}
    }
}
