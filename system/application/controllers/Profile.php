<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {

	public function __construct()
    {
		parent::__construct();
		validate_session();

		$this->uri_name = 'profile';
		$this->title = 'Profile';
	}
	
	public function index()
	{
		
	}

	public function form($id='')
	{
		if(empty($id) && !isset($_POST['id']))
		{
			$breadcrumbs = array(array('url' => $this->uri_name, 'title' => $this->title, 'active' => true));

			$result = $this->db->get_where('part_users', array('id_user' => $this->session->userdata('UserID')))->row_array();
			$submit_action = 'form/id/'.$result['id_user'];

			$args['title'] = $this->title;
			$args['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title);
			$args['controller_name'] = $this->uri_name;
			$args['action'] = $submit_action;
			$args['result'] = $result;

			
			$flashmessage = $this->session->flashdata($this->uri_name);
			$flashmessage_type = $this->session->flashdata($this->uri_name.'_type');
			if (!empty($flashmessage))
				$this->message = $flashmessage;
				$this->message_type = $flashmessage_type;

			$this->template->view($this->uri_name.'/form', $args);
		}
		else
		{
			$args['password_error'] = false;
			// Update user password
			$old_pass = md5($this->input->post('oldpass'));
			$new_pass = md5($this->input->post('newpass'));
			$cnf_pass = md5($this->input->post('confirmpass'));

			// Try to find user by old password and email
			$user = $this->db->get_where('part_users', array('password' => $old_pass, 'id_user' => $this->session->userdata('UserID')))->result_array(0);

			// Basic password check
			if($new_pass != $cnf_pass || count($user) <= 0)
				$args['password_error'] = true;

			if($args['password_error'])
			{
				$this->session->set_flashdata($this->uri_name, "Failed change password!");
				$this->session->set_flashdata($this->uri_name.'_type', 'alert-danger');

				redirect($this->uri_name.'/form');
			}else{

				// Update pass on dtabase
				$upd_user['password'] = $new_pass;

				$this->db->update('part_users', $upd_user, array('id_user' => $this->session->userdata('UserID')));

				$this->session->set_flashdata($this->uri_name, "Success change password.");
				$this->session->set_flashdata($this->uri_name.'_type', 'alert-success');
				redirect($this->uri_name.'/form');
			}
		}
	}

}
