<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_management_lvl extends MY_Controller {

	public function __construct()
    {
		parent::__construct();
		validate_session();

		$this->uri_name = 'user_management_lvl';
		$this->title = 'Level';
		$this->model_name = 'user_management_lvl_model';
		$this->load->model($this->model_name);
		$this->load->helper('html');

		$this->load->library(array('func_validation'));
	}
	
	public function index()
	{
		$breadcrumbs = array(
			array('url' => 'user_management', 'title' => 'User Management'),
			array('url' => $this->uri_name, 'title' => $this->title),
			array('url' => $this->uri_name, 'title' => 'list', 'active' => true)
		);

		$flashmessage = $this->session->flashdata($this->uri_name);
		$flashmessage_type = $this->session->flashdata($this->uri_name.'_type');
		if (!empty($flashmessage))
			$this->message = $flashmessage;
			$this->message_type = $flashmessage_type;
			

		$args['title'] = $this->title;
		$args['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title);
		$args['controller_name'] = $this->uri_name;
		$args['url_tablelist'] = $this->uri_name.'/table_list';

		$this->template->view($this->uri_name.'/list', $args);
	}

	public function table_list()
	{
		parent::table_list();
	}

	public function form($id='')
	{
		$breadcrumbs = array(
			array('url' => 'user_management', 'title' => 'User Management'),
			array('url' => $this->uri_name, 'title' => $this->title),
			array('url' => $this->uri_name, 'title' => 'Form', 'active' => true)
		);

		$submit_action = 'insert';
		$result = NULL;

		if (!empty($id))
		{
			$args['ID_ORI'] = $id;
			$result = $this->_getRow($id);
			$row_count = $result->num_rows();
			$result = $result->row_array();
			$submit_action = 'update/id/'.$id;
			if($row_count == 0)
			{
				$this->session->set_flashdata($this->uri_name, "Ops! Data yang dipilih tidak ditemukan!");
				$this->session->set_flashdata($this->uri_name.'_type', 'alert-danger');
				redirect($this->uri_name);
			}
		}

		$args['title'] = ($result != null ? "Update ".$this->title : "Input ".$this->title);
		$args['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title);
		$args['controller_name'] = $this->uri_name;
		$args['action'] = $submit_action;
		$args['result'] = $result;

		
		$flashmessage = $this->session->flashdata($this->uri_name);
		$flashmessage_type = $this->session->flashdata($this->uri_name.'_type');
		if (!empty($flashmessage))
			$this->message = $flashmessage;
			$this->message_type = $flashmessage_type;


		$this->template->view($this->uri_name.'/form', $args);
	}

	public function insert() 
	{
		if($this->input->post() === FALSE)
			redirect($this->uri_name.'/form');

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama','Level Name','trim|required');
		$this->form_validation->set_message('required','%s Wajib diisi. !');
		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata($this->uri_name, validation_errors());
			$this->session->set_flashdata($this->uri_name.'_type', 'has-danger');
			$this->form();
		}
		else
		{
			$level = array();
			$level['nama'] = $this->input->post('nama');
			$this->db->insert('bsm_apps_level', $level);

			$this->db->order_by('id_level', 'desc');
			$query = $this->db->get('bsm_apps_level', 1);
			$level['id'] = $query->row()->id_level;

			$id_menu = $this->input->post('id_menu');
			$access_desc = $this->input->post('access_desc');
			
			for($i=0;$i<count($id_menu);$i++){
				if(isset($access_desc[$id_menu[$i]])){
					srand($this->make_seed());
					$data = array(
						'id_permission' => $this->randomString(),
						'id_menu' => $id_menu[$i],
						'id_level' => $level['id'],
						'permission' => $access_desc[$id_menu[$i]]
					);
					$this->db->insert('bsm_apps_level_akses',$data);
				}
			}

			$this->session->set_flashdata($this->uri_name, "<i class='fa fa-check'></i> Data $this->title - <strong>".$level['nama']."</strong> Berhasil disimpan.");
			$this->session->set_flashdata($this->uri_name.'_type', 'alert-success');
			redirect($this->uri_name);
		}
	}

	public function update($id='')
	{
		if($this->input->post() === FALSE)
			redirect($this->uri_name.'/form');

		$uri_array = $this->uri->uri_to_assoc(3);
		$id = '';
		if (isset($uri_array['id']))
		{
			$id = $uri_array['id'];
			unset($uri_array['id']);
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama','Groups Name','trim|required');
		$this->form_validation->set_message('required','%s Wajib diisi. !');
		if($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata($this->uri_name, validation_errors());
			$this->session->set_flashdata($this->uri_name.'_type', 'has-danger');
			$this->form($id);
		}
		else
		{
			$level= array();
			$level['id_level'] =$_POST['id'];
			$level['nama'] =$_POST['nama'];
			$this->db->where('id_level',$_POST['id']);
			$this->db->update('bsm_apps_level', $level);

			$id_menu = $this->input->post('id_menu');
			$access_desc = $this->input->post('access_desc');
			
			/* Delete menu before create new */
			$this->db->where('id_level', $_POST['id']);
			$this->db->delete('bsm_apps_level_akses');

			for($i=0;$i<count($id_menu);$i++){
				if(isset($access_desc[$id_menu[$i]])){
					srand($this->make_seed());
					$data = array(
						'id_permission' => $this->randomString(),
						'id_menu' => $id_menu[$i],
						'id_level' => $_POST['id'],
						'permission' => $access_desc[$id_menu[$i]]
					);
					$this->db->insert('bsm_apps_level_akses',$data);
				}
			}

			$this->session->set_flashdata($this->uri_name, "<i class='fa fa-check'></i> Data $this->title - <strong>".$level['nama']."</strong> Berhasil diupdate.");
			$this->session->set_flashdata($this->uri_name.'_type', 'alert-success');
			redirect($this->uri_name);
		}
	}

	function make_seed()
	{
	  list($usec, $sec) = explode(' ', microtime());
	  return (float) $sec + ((float) $usec * 100000);
	}
	
	function randomString()
	{
		$sql = 'SELECT SUBSTRING(MD5(RAND()) FROM 1 FOR 15) AS id_permission';
		$query = $this->db->query($sql);
		$row = $query->row_array();
		return $row['id_permission'];
	}

	public function delete($id='')
	{
		if(!empty($id))
		{
			$result = $this->_getRow($id);
			if($result->num_rows() > 0)
			{
				$result = $result->row();
				$groups_name = $result->nama;

				if(isset($id) && trim($id) == "1" || trim($id) == "99")
				{
					$addmessage = 'Mohon maaf anda tidak dapat menghapus level ini, karena digunakan oleh user lain.';
					$this->func_validation->alert_redirect($addmessage, $this->uri_name);	
					die();
				}

				//sebelum hapus cek apakah terkait ke table users
				$sql = $this->db->get_where('part_users', array('id_level' => $id));
				if($sql->num_rows() > 0)
				{
					$addmessage = 'Mohon maaf anda tidak dapat menghapus level ini, karena digunakan oleh user lain.';
					$this->func_validation->alert_redirect($addmessage, $this->uri_name);	
					die();
				}
				else
				{
					//delete row table terkait
					$this->db->where('id_level', $id);
					$this->db->delete('bsm_apps_level_akses');

					//delete row table level
					$this->db->where('id_level', $id);
					$this->db->delete('bsm_apps_level');

					$this->session->set_flashdata($this->uri_name, "<i class='fa fa-trash'></i> Data $this->title - <strong>$groups_name</strong> Berhasil di hapus.");
					$this->session->set_flashdata($this->uri_name.'_type', 'alert-success');
					redirect($this->uri_name);
				}
			}
			else
			{
				$this->session->set_flashdata($this->uri_name, "Ops! Data yang dipilih tidak ditemukan!");
				$this->session->set_flashdata($this->uri_name.'_type', 'alert-danger');
				redirect($this->uri_name);
			}
		}
		else
		{
			$this->session->set_flashdata($this->uri_name, "Ops! Data yang dipilih tidak ditemukan!");
			$this->session->set_flashdata($this->uri_name.'_type', 'alert-danger');
			redirect($this->uri_name);
		}
		
	}

	private function _getRow($id='')
	{
		$this->db->select('*');
		$this->db->from('bsm_apps_level');
		$this->db->where('id_level',$id);
		$result = $this->db->get();

		return $result;
	}


}
