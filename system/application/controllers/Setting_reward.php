<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_reward extends MY_Controller {

	public function __construct()
  	{
		parent::__construct();
		validate_session();

		$this->uri_name = 'setting_reward';
		$this->title = 'Rewards';
		$this->model_name = 'setting_reward_model';							
		$this->load->model($this->model_name);
		$this->load->helper('html');
	}

	public function index()
	{
		$breadcrumbs = array(
			array('url' => 'setting', 'title' => 'Setting'),
			array('url' => $this->uri_name, 'title' => $this->title),
			array('url' => $this->uri_name, 'title' => 'list', 'active' => true)
		);

		$status      = array('all' => 'All', 'yes' => 'Yes', 'no' => 'No');
		$opt_status      = form_dropdown('fillStatus',$status,'','id="fillStatus" class="custom-select custom-select-label form-control m-input m-input--square"');

		$data['title'] = $this->title;
		$data['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title, $this->uri_name, 'Tambah '.$this->title);
		$data['controller_name'] = $this->uri_name;
		$data['url_tablelist'] = $this->uri_name.'/table_list';
		$data['status'] = $opt_status;

		$this->template->view($this->uri_name.'/list', $data);
	}

	public function table_list()
	{
		parent::table_list();
	}
	
	public function form($id='')
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$submit_action = 'add';
		$result = NULL;
		if (!empty($id))
		{
			$result = $this->_get_details($id);
			$submit_action = 'update/id/'.$id;
		}
		$args['result']          = $result;
		$args['controller_name'] = $this->uri_name;
		$args['action']          = $submit_action;
		$this->template->render($this->uri_name.'/form', $args, FALSE);
	}

	public function add()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_form_validation());
		$this->form_validation->set_message('required','%s Please fill in this field. !');
		if($this->form_validation->run() == FALSE)
		{
			$result = array(
				'status' => 0,
				'error' => $this->_array_remove_index(array(
							'poin' => form_error('poin', '<div class="form-control-feedback">', '</div>'),
							'description' => form_error('description', '<div class="form-control-feedback">', '</div>')
					)),
				'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> Please fill in the data correctly and accordingly before submit</div>"
			);

			$this->template->render_json($result);
		}
		else
		{
			if(isset($_POST['id']) and trim($_POST['id'])!=''){
				$this->$model->update($_POST);
				$data['reward']['id'] = $_POST['id'];
			}else{
				$data['reward']['id'] = $this->$model->add($_POST);
				$_POST['id'] = $data['reward']['id'] ;
			}

			/* Prosesing Upload Attachement */
			if(isset($_FILES) and !empty($_FILES)){
				
				/* Create folder image if doesn't exist */
				if (!file_exists('upload/reward/')) 
					mkdir('upload/reward/', 0777, true); //add true agar membuat file recursive
					
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/reward/'.date("Y").'/'))
					mkdir('upload/reward/'.date("Y").'/', 0777, true);
				
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/reward/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/reward/'.date("Y").'/'.date("m").'/', 0777, true);
				
				if (!file_exists('upload/reward/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/reward/'.date("Y").'/'.date("m").'/', 0777, true);
					
				$config['upload_path'] = 'upload/reward/'.date("Y").'/'.date("m").'/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
				$config['overwrite'] = true;
				$config['max_size']	= '2048'; //2mb
				$config['file_name'] = strtolower(date('YmdHis').'_'.$_FILES["image"]['name']);
				
				$this->load->library('upload', $config);
				$this->load->library('image_lib');
				if ($this->upload->do_upload('image'))
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status']= 1;
					$upload_data = $this->upload->data();
					$_POST['image'] = $config['upload_path'].$upload_data['file_name'];
					
					//Rezise image thumbnail
					/* Create folder image if doesn't exist */
					if (!file_exists('upload/reward/thumbnail/')) 
					mkdir('upload/reward/thumbnail/', 0777, true); //add true agar membuat file recursive

					$thumbnail_name = $upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];
					$source_path = $upload_data['full_path'];
					$target_path = 'upload/reward/thumbnail/';
					$config_manip = array(
						'image_library' => 'gd2',
						'source_image' => $source_path,
						'new_image' => $target_path,
						'maintain_ratio' => TRUE,
						'create_thumb' => TRUE,
          				'thumb_marker' => '_thumb',
						'width' => 150,
						'height' => 150
					);
					$this->image_lib->initialize($config_manip);
					$this->image_lib->resize();
					// echo $this->image_lib->display_errors();
					$this->image_lib->clear();

					$_POST['thumbnail'] = $config_manip['new_image'].$thumbnail_name;
					$this->$model->update($_POST);
				}
				else
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status'] =1;
					$no_image_sm = 'public/assets/img/default-small.jpg';
					$no_image_big = 'public/assets/img/default-big.jpg';
					$_POST['thumbnail'] = $no_image_sm;
					$_POST['image'] = $no_image_big;

					$this->$model->update($_POST);
				}
			}
			
			$result = array(
				'status' => 1,
				'error' => $data,
				'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Has been saved.</div>"
			);

			$this->template->render_json($result);
		}
	}
	
	public function update($id='')
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;
		$this->load->library('form_validation');
		$this->form_validation->set_rules($this->_form_validation());
		$this->form_validation->set_message('required','%s Please fill in this field. !');
		if($this->form_validation->run() == FALSE)
		{
			$result = array(
				'status' => 0,
				'error' => $this->_array_remove_index(array(
					'poin' => form_error('poin', '<div class="form-control-feedback">', '</div>'),
					'description' => form_error('description', '<div class="form-control-feedback">', '</div>')
				)),
				'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> Please fill in the data correctly and accordingly before submit</div>"
			);

			$this->template->render_json($result);
		}
		else
		{
			$this->$model->update($_POST);
			$data['reward']['id'] = $_POST['id'];
			$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
			$data['upload_status']= 1;
			/* Prosesing Upload Attachement */
			if(isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])){
				
				/* Create folder image if doesn't exist */
				if (!file_exists('upload/reward/')) 
					mkdir('upload/reward/', 0777, true); //add true agar membuat file recursive
					
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/reward/'.date("Y").'/'))
					mkdir('upload/reward/'.date("Y").'/', 0777, true);
				
				/* Create folder year today if doesn't exist */
				if (!file_exists('upload/reward/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/reward/'.date("Y").'/'.date("m").'/', 0777, true);
				
				if (!file_exists('upload/reward/'.date("Y").'/'.date("m").'/'))
					mkdir('upload/reward/'.date("Y").'/'.date("m").'/', 0777, true);

					
				$config['upload_path'] = 'upload/reward/'.date("Y").'/'.date("m").'/';
				$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
				$config['overwrite'] = true;
				$config['max_size']	= '2048'; //2mb
				$config['file_name'] = strtolower($_FILES["image"]['name']);
				
				$this->load->library('upload', $config);
				$this->load->library('image_lib');
				if ($this->upload->do_upload('image'))
				{
					$data['upload_result'] = "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Process upload success.</div>";
					$data['upload_status']= 1;
					$upload_data = $this->upload->data();
					
					$_POST['image'] = $config['upload_path'].$upload_data['file_name'];

					//Rezise image thumbnail

					/* Create folder image if doesn't exist */
					if (!file_exists('upload/reward/thumbnail/')) 
					mkdir('upload/reward/thumbnail/', 0777, true); //add true agar membuat file recursive
					
					$thumbnail_name = $upload_data['raw_name'].'_thumb'.$upload_data['file_ext'];
					$source_path = $upload_data['full_path'];
					$target_path = 'upload/reward/thumbnail/';
					$config_manip = array(
						'image_library' => 'gd2',
						'source_image' => $source_path,
						'new_image' => $target_path,
						'maintain_ratio' => TRUE,
						'create_thumb' => TRUE,
          				'thumb_marker' => '_thumb',
						'width' => 150,
						'height' => 150
					);
					$this->image_lib->initialize($config_manip);
					$this->image_lib->resize();
					// echo $this->image_lib->display_errors();
					$this->image_lib->clear();

					$_POST['thumbnail'] = $config_manip['new_image'].$thumbnail_name;

					$this->$model->update($_POST);
				}
			}

			$result = array(
				'status' => 1,
				'error' => $data,
				'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Has been updated.</div>"
			);

			$this->template->render_json($result);
		}
	}

	public function delete()
	{
		parent::delete();
	}

	public function set_status()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		$model = $this->model_name;

		$this->$model->update($_POST);
	}

	private function _get_details($id)
	{
		$sql = "SELECT * FROM bsm_setting_reward WHERE id = $id";
		$results = $this->db->query($sql)->row_array();
		return $results;
	}

	private function _form_validation()
	{
		$config = array(
			array(
					'field' => 'id',
					'label' => 'ID',
					'rules' => ''
			),
			array(
					'field' => 'poin',
					'label' => 'Poin',
					'rules' => 'trim|required'
			),
			array(
					'field' => 'description',
					'label' => 'Description',
					'rules' => 'trim|required'
			)
		);

		return $config;
	}

	private function _array_remove_index($myarray = array())
	{
		foreach($myarray as $key=>$value)
		{
			if(is_null($value) || $value == '')
				unset($myarray[$key]);
		}

		return $myarray;
	}

}
