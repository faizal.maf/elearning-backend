<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function __construct()
  	{
		parent::__construct();
		validate_session();

		$this->uri_name = 'welcome';
		$this->title = 'Welcome';
	}

	public function index()
	{
		$this->dashboard();
	}

	public function dashboard()
	{
		// QUERY REGULASI
		$data_regulasi = $this->db
					->select(array('bsm_config.deskripsi as label', 'count(bsm_config.id_lookup) as value'))
					->from('bsm_regulasi')
					->join('bsm_config','bsm_regulasi.id_kategori = bsm_config.id_lookup','left')
					->group_by('bsm_regulasi.id_kategori')
					->get()
					->result_array();
		
		// QUERY KETENTUAN INTERNAL
		$data_kinternal = $this->db
					->select(array('bsm_config.deskripsi as label', 'count(bsm_config.id_lookup) as value'))
					->from('bsm_kinternal')
					->join('bsm_config','bsm_kinternal.id_kategori = bsm_config.id_lookup','left')
					->group_by('bsm_kinternal.id_kategori')
					->get()
					->result_array();
					
		
		

		// QUERY LITERASI
		$artikel    	= $this->db->count_all('bsm_artikel');
		$video      	= $this->db->count_all('bsm_video');
		$infografis 	= $this->db->count_all('bsm_infografis');
		$faq        	= $this->db->count_all('bsm_faq');
		$galeri      	= $this->db->count_all('bsm_infocpg');
		$gratifikasi    = $this->db->count_all('bsm_gratifikasi');
		$tukarpoin     = $this->db->count_all('bsm_inbox_tukarpoin');

		$data_literasi = array(
			array('label' => 'Artikel', 'value' => $artikel, 'style' => 'danger', 'url' => base_url('artikel'), 'icon' => 'flaticon-notes'),
			array('label' => 'Video', 'value' => $video, 'style' => 'info', 'url' => base_url('video'), 'icon' => 'flaticon-computer'),
			array('label' => 'Infografis', 'value' => $infografis, 'style' => 'warning', 'url' => base_url('infografis'), 'icon' => 'flaticon-graph'),
			array('label' => 'FAQ', 'value' => $faq, 'style' => 'success', 'url' => base_url('faq'), 'icon' => 'flaticon-exclamation'),
			array('label' => 'Gratifikasi', 'value' => $gratifikasi, 'style' => 'accent', 'url' => base_url('gratifikasi'), 'icon' => 'flaticon-interface-7'),
			array('label' => 'Galeri', 'value' => $galeri, 'style' => 'focus', 'url' => base_url('infocpg'), 'icon' => 'flaticon-photo-camera'),
			array('label' => 'Tukar Poin', 'value' => $tukarpoin, 'style' => 'brand', 'url' => base_url('inbox_tukarpoin'), 'icon' => 'flaticon-trophy')
		);
		
		// QUERY INBOX QUESTION
		$data_question = $this->db
					->select(array('subject as label', 'count(subject) as value'))
					->from('bsm_inbox_question')
					->group_by('subject')
					->get()
					->result_array();
					
		// QUERY USERS APPS
		$data_users = $this->db
					->select(array("CASE WHEN status = 0 THEN 'Inactive' ELSE 'Active' END AS label", "count(status) as value"))
					->from('bsm_user')
					->where('is_deleted', 0)
					->group_by('status')
					->get()
					->result_array();
		  

		//FOR CHART TEXT
		$args['result_regulasi']  = $data_regulasi;
		$args['result_kinternal'] = $data_kinternal;
		$args['result_literasi']  = $data_literasi;
		// $args['result_erm']  	  = $data_erm;
		// $args['result_apuppt']    = $data_apuppt;

		//FOR CHART GRAFIK
		$args['data_regulasi']  = json_encode($data_regulasi);
		$args['data_kinternal'] = json_encode($data_kinternal);
		$args['data_literasi']  = json_encode($data_literasi);
		// $args['data_erm']	    = json_encode($data_erm);
		// $args['data_apuppt']	= json_encode($data_apuppt);
		$args['data_question']	= json_encode($data_question);
		$args['data_users']	    = json_encode($data_users);

		// Load view
		$this->template->view($this->uri_name.'/list', $args);
	}

	public function getTopContent($type)
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		
		$result_html = '';
		$table_name = '';
		
		if($type==1){
			$table_name = 'bsm_regulasi';
		}elseif($type==2){
			$table_name = 'bsm_kinternal';
		}elseif($type==3){
			$table_name = 'bsm_erm';
		}elseif($type==4){
			$table_name = 'bsm_news';
		}elseif($type==5){
			$table_name = 'bsm_video';
		}elseif($type==6){
			$table_name = 'bsm_artikel';
		}elseif($type==7){
			$table_name = 'bsm_apuppt';
		}
			
		
		if(!empty($table_name))
		{
			$results = $this->db->select($table_name.'.*')
					->from($table_name)
					->order_by($table_name.'.views', 'desc')
					->limit(10)
					->get()
					->result();
			
			$counter = 1;
			foreach ($results as $result) {
				$result_html .= '<tr>
									<th>'.$counter++.'.</th>
									<td>'.$result->title.'</td>
									<td>'.$result->views.'</td>
								</tr>';
			}
		}
	
		 echo json_encode($result_html);
	}
	
	public function getTopLike($type)
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		
		$result_html = '';
		$table_name = '';
		
		if($type==1){
			$table_name = 'bsm_artikel';
		}elseif($type==2){
			$table_name = 'bsm_infocpg';
		}elseif($type==3){
			$table_name = 'bsm_video';
		}elseif($type==4){
			$table_name = 'bsm_infografis';
		}elseif($type==5){
			$table_name = 'bsm_news';
		}
			
		
		if(!empty($table_name))
		{
			if($type==2){
				$sql = "SELECT id, caption AS title, (SELECT COUNT( id ) AS liked FROM bsm_like_content WHERE content_id = $table_name.id AND category = $type ) AS liked ";
			}else{
				$sql = "SELECT id, title, (SELECT COUNT( id ) AS liked FROM bsm_like_content WHERE content_id = $table_name.id AND category = $type ) AS liked ";
			}
			
			$sql .= " FROM $table_name
				ORDER BY liked DESC
				LIMIT 0, 10";
			$results = $this->db->query($sql)->result();
			
			$counter = 1;
			foreach ($results as $result) {
				$result_html .= '<tr>
									<th>'.$counter++.'.</th>
									<td>'.$result->title.'</td>
									<td>'.$result->liked.'</td>
								</tr>';
			}
		}
	
		 echo json_encode($result_html);
	}
	
	public function getTopUsers($type)
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		
		$result_html = '';

		if($type==1)
		{
			$results = $this->db->select('*')->from('bsm_user')->order_by('poin', 'desc')->limit(10)->get()->result();
			
			$counter = 1;
			foreach ($results as $result) {
				$result_html .= '<tr>
									<th>'.$counter++.'.</th>
									<td><b> '.$result->name.' - '.$result->nip.' </b><br>
										Jabatan : '.$result->jabatan.'<br>
										E-mail : '.$result->email.'
									</td>
									<td>'.$result->poin.'</td>
								</tr>';
			}
		}
		elseif($type==2)
		{
			$results = $this->db->select('B.name, B.jabatan, B.nip, B.email, count(A.id_user) as total_participan')
				->from('bsm_quiz_participation AS A')
				->join('bsm_user AS B', 'A.id_user = B.id', 'LEFT')
				->order_by('COUNT(*)', 'desc')
				->group_by('B.name')
				->group_by('B.jabatan')
				->group_by('B.nip')
				->group_by('B.email')
				->limit(10)
				->get()
				->result();
				
			$counter = 1;
			foreach ($results as $result) {
				$result_html .= '<tr>
									<th>'.$counter++.'.</th>
									<td><b> '.$result->name.' - '.$result->nip.' </b><br>
										Jabatan : '.$result->jabatan.'<br>
										E-mail : '.$result->email.'
									</td>
									<td>'.$result->total_participan.'</td>
								</tr>';
			}
		}elseif($type==3)
		{
			$results = $this->db->select('B.name, B.jabatan, B.nip, B.email, count(A.created_by) as total_bertanya')
				->from('bsm_inbox_question AS A')
				->join('bsm_user AS B', 'A.created_by = B.id', 'LEFT')
				->order_by('COUNT(*)', 'desc')
				->group_by('B.name')
				->group_by('B.jabatan')
				->group_by('B.nip')
				->group_by('B.email')
				->limit(10)
				->get()
				->result();
				
			$counter = 1;
			foreach ($results as $result) {
				
				$result_html .= '<tr>
									<th>'.$counter++.'.</th>
									<td><b> '.$result->name.' - '.$result->nip.' </b><br>
										Jabatan : '.$result->jabatan.'<br>
										E-mail : '.$result->email.'
									</td>
									<td>'.$result->total_bertanya.'</td>
								</tr>';  
			}
		}
	
		 echo json_encode($result_html);
	}

	//Upload image summernote
    public function upload_image() {
        $this->common_lib->upload_image();
    }
 
    //Delete image summernote
    public function delete_image(){
       $this->common_lib->delete_image();
    }

}
