<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_userwebadmin extends MY_Controller {

	public function __construct()
    {
		parent::__construct();
		validate_session();

		$this->uri_name = 'setting_userwebadmin';
		$this->title = 'User Web Admin';
		$this->model_name = 'setting_userwebadmin_model';
		$this->load->model($this->model_name);
		$this->load->helper('html');

		$this->load->library(array('func_validation'));
	}
	
	public function index()
	{
		$breadcrumbs = array(
			array('url' => 'setting', 'title' => 'Setting'),
			array('url' => $this->uri_name, 'title' => $this->title),
			array('url' => $this->uri_name, 'title' => 'list', 'active' => true)
		);

		$flashmessage = $this->session->flashdata($this->uri_name);
		$flashmessage_type = $this->session->flashdata($this->uri_name.'_type');
		if (!empty($flashmessage))
			$this->message = $flashmessage;
			$this->message_type = $flashmessage_type;
		
		$status     = array('all' => 'All', 'active' => 'Active', 'inactive' => 'Inactive');
		$opt_status = form_dropdown('fillStatus',$status,'','id="fillStatus" class="custom-select custom-select-label form-control m-input m-input--square"');
		
		$args['title'] = $this->title;
		$args['breadcrumbs'] = breadcrumbs($breadcrumbs, $this->title, $this->uri_name, 'New '.$this->title);
		$args['controller_name'] = $this->uri_name;
		$args['url_tablelist'] = $this->uri_name.'/table_list';
		$args['status'] = $opt_status;

		$this->template->view($this->uri_name.'/list', $args);
	}

	public function table_list()
	{
		parent::table_list();
	}

	public function form($id='')
	{
		$flashmessage = $this->session->flashdata($this->uri_name);
		$flashmessage_type = $this->session->flashdata($this->uri_name.'_type');
		if (!empty($flashmessage))
			$this->message = $flashmessage;
			$this->message_type = $flashmessage_type;

		#-- Options regulasi
		$groups_opt = array('' => 'Select groups access');
		$opt_sql = "SELECT id_level, nama AS options_name
					FROM part_level
					WHERE id_level NOT IN ('99')";
		$list_lvl_records = $this->db->query($opt_sql)->result();
		foreach ($list_lvl_records as $list_lvl_records_idx=>$list_lvl_records)
		{
			$groups_opt[$list_lvl_records->id_level] = $list_lvl_records->options_name;
		}

		$this->_displayed_additional = array(
						'groups_list' => $groups_opt
					);
			
		parent::form($id);
	}

	public function insert()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());

		$model = $this->model_name;
		$this->load->library(array('form_validation','func_validation'));
		$this->form_validation->set_rules($this->$model->get_fields());
		if($this->form_validation->run() == FALSE)
		{
			$result = $this->func_validation->set_form_error($this->$model->get_fields());
		}
		else
		{
			$isExistUsername = $this->func_validation->isExistUsername(trim($this->input->post('username')));
			$isExistEmail = $this->func_validation->isExistEmail(trim($this->input->post('email')));

			if(!$isExistUsername && !$isExistEmail)
			{
				$data = array();
				foreach($this->$model->get_fields() as $properties)
					$data[$properties['field']] = set_value($properties['field']);
				
				$data = array_merge($data, ['password' => md5(trim($this->input->post('username'))), 'status' => 1]);
				
				foreach($this->$model->get_fields() as $properties)
					$this->$model->modify_value_fields($properties['field'], (isset($data[$properties['field']]) ? $data[$properties['field']] : ''));

				$this->_set_data("insert", $this->$model->get_fields(), $data);
				$respone['data']['result'] = $this->$model->insert();

				$result = $this->func_validation->set_form_success($respone, "Saved");
			}
			else
			{
				$checkExistUsername = array();
				if($isExistUsername)
					$checkExistUsername = array('username' => '<div class="form-control-feedback">Username sudah digunakan.</div>');

				$checkExistEmail = array();
				if($isExistEmail)
					$checkExistEmail = array('email' => '<div class="form-control-feedback">Email sudah digunakan.</div>');

				$result = array(
					'status' => 0,
					'error' => array_merge($checkExistUsername, $checkExistEmail),
					'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> Mohon periksa kembali data yang anda masukan.</div>"
				);
			}
		}

		$this->template->render_json($result);
	}

	public function update($id='')
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		
		$uri_array = $this->uri->uri_to_assoc(3);
		$id = '';
		if (isset($uri_array['id']))
		{
			$id = $uri_array['id'];
			unset($uri_array['id']);
		}

		$model = $this->model_name;
		$this->load->library(array('form_validation','func_validation'));
		$this->form_validation->set_rules($this->$model->get_fields());
		if($this->form_validation->run() == FALSE)
		{
			$result = $this->func_validation->set_form_error($this->$model->get_fields());
		}
		else
		{
			$isExistEmail = $this->func_validation->isExistEmail(trim($this->input->post('email')), $id);

			if(!$isExistEmail)
			{
				$data = array();
				$this->$model->remove_fields($this->$model->field_id);
				foreach($this->$model->get_fields() as $properties)
				{
					if($properties['field'] != 'username' && $properties['field'] != 'status' && $properties['field'] != 'is_deleted' && $properties['field'] != 'created' && $properties['field'] != 'created_by')
						$data[$properties['field']] = set_value($properties['field']);
					else
						$this->$model->remove_fields($properties['field']);
				}

				foreach($this->$model->get_fields() as $properties)
					$this->$model->modify_value_fields($properties['field'], (isset($data[$properties['field']]) ? $data[$properties['field']] : ''));

				$this->_set_data("update", $this->$model->get_fields(), $data);
				$respone['data']['result'] = $this->$model->update($id);

				$result = $this->func_validation->set_form_success($respone, "Updated");
			}
			else
			{
				$checkExistEmail = array();
				if($isExistEmail)
					$checkExistEmail = array('email' => '<div class="form-control-feedback">Email sudah digunakan.</div>');

				$result = array(
					'status' => 0,
					'error' => $checkExistEmail,
					'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> Mohon periksa kembali data yang anda masukan.</div>"
				);
			}
		}
		$this->template->render_json($result);
	}

	public function delete()
	{
		parent::delete();
	}

	public function set_status()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		$model = $this->model_name;

		$this->db->set('status', $this->input->post('status'));
		$this->db->set('updated', date('YmdHis'));
		$this->db->set('updated_by', $this->session->userdata('Username'));
		$this->$model->update($this->input->post('id'));
	}

	protected function _set_data($type ="insert", $fields, $data)
	{
		foreach($fields as $properties)
		{
			if($type=='insert')
			{
				//unset field ID, karena autoincreament
				if($properties['field'] == 'id_user')
					unset($data[$properties['field']]);

				if($properties['field'] == 'created')
					$data[$properties['field']] = date('YmdHis');
				if($properties['field'] == 'created_by')
					$data[$properties['field']] = $this->session->userdata('Username');
			}
			else
			{
				if($properties['field'] == 'created' || $properties['field'] == 'created_by' || $properties['field'] == 'status')
				{
					if(isset($data[$properties['field']]))
						unset($data[$properties['field']]);
				}
			}

			/**
			 *  Set default value 0 for is_deleted
			 */
			if($properties['field'] == 'is_deleted')
			{
				if(!isset($data[$properties['field']]) || (isset($data[$properties['field']]) && $data[$properties['field']] === '')) 
					$data[$properties['field']] = 0;
			}

			if($properties['field'] == 'updated')
				$data[$properties['field']] = date('YmdHis');
			if($properties['field'] == 'updated_by')
				$data[$properties['field']]= $this->session->userdata('Username');

			if(isset($data[$properties['field']]))
			{
				if($data[$properties['field']] !== '')
				{
					$this->db->set($properties['field'], $data[$properties['field']]);
				}
				else
				{
					$this->db->set($properties['field'], 'NULL', FALSE);
				}
			}
		}
	}
}
