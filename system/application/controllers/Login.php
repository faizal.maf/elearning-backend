<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	protected $external = true;

	public function __construct()
	{
		parent::__construct();

		$this->uri_name = 'login';
	}

	public function index()
	{
		if($this->session->userdata('LoginAccess') === TRUE)
		{
			redirect('welcome');
		}
		
		$this->login();
	}

	public function login()
	{
		$args['title'] = 'Login';
		// Get email for errors
		$args['email_user'] = $this->session->userdata('email_user');
		$this->session->unset_userdata('email_user');

		// Get email error message
		$args['email_msg_error'] = $this->session->userdata('email_msg_error');
		$this->session->unset_userdata('email_msg_error');

		// Boolean defining if error is in email input
		$args['bool_email_error'] = $this->session->userdata('bool_email_error');
		$this->session->unset_userdata('bool_email_error');

		$args['logo'] = $this->db->get_where('bsm_config', array('groups' => 'Logo'))->result();

		// Load view
		$this->template->render($this->uri_name.'/login', $args, FALSE);
	}

	public function login_in()
	{
		if(!$this->input->is_ajax_request()) show_404(uri_string());
		
		if(!empty($this->input->post('username')))
		{
			$username = $this->input->post('username');
		}
		$pass = $this->input->post('password');

		// Try to get the user by username and pass
		$user = $this->auth->validate_login($username, $pass);

		// Case user doesnt exist, redirect to login page again
		if( ! $user ) {
			
			// redirect('app-login');
			$response = array('status' => false,
							'message' => 'Username/Email or password anda salah, silahkan coba kembali.',
							'redirect_to' => ''
					);
			echo json_encode($response);

		}
		else
		{

			if($user['status'] == '0')
			{
				$response = array('status' => false,
							'message' => 'Akun anda tidak aktif.',
							'redirect_to' => ''
					);
				// echo json_encode($response);
			}
			else
			{
				// Check url default for user, if not exist try to redirect to default dashboard
				$url_welcome = (get_value($user, 'url_welcome') != '') ? $this->tag->tag_replace(get_value($user, 'url_welcome')) : URL_ROOT . '/welcome/';

				// Put some user data in session
				$session['Username'] = get_value($user, 'Username');
				$session['UserID'] = get_value($user, 'id_user');
				$session['LevelID'] = get_value($user, 'id_level');
				$session['Email'] = get_value($user, 'email');
				$session['FirstName'] = get_value($user, 'first_name');
				$session['LastName'] = get_value($user, 'last_name');
				$session['LevelName'] = get_value($user, 'nama');
				$session['url_welcome'] = $url_welcome;
				$session['LoginAccess'] = TRUE;

				// Set the data above
				$this->session->set_userdata($session);

				// Log the login
				$this->auth->db_log('login', 'login');

				$response = array('status' => true,
								'message' => 'Sign success, Redirecting Please Wait...',
								'redirect_to' => $url_welcome
						);
			}
			echo json_encode($response);
		}
	}

	/**
	 * Application exit. Clear all session data and redirect to login.
	 *
	 * @return void
	 */
	public function signout()
	{
		$this->session->sess_destroy();
		redirect('login', 'auto');
	}

	/**
	 * For 404 pages.
	 *
	 * @return void
	 */
	public function not_found()
	{
		$this->error->show_404();
	}

	/**
	 * Requirement new pass form for users.
	 *
	 * @param boolean process
	 * @return void
	 */
	public function forgot_password($process = false)
	{
		if( ! $process)
			$this->template->load_view($this->uri_name . '/forgot-password', array(), false, false);
		else 
		{
			// Collect email and validation
			$email = $this->input->post('email');
			$validate = $this->input->post('validate_human');

			// Check consistency
			if($email == '' || $validate == '')
				redirect($this->uri_name . '/forgot-password');

			// Try to find user data
			$user = $this->db->get_where('hsapp_user', array('email' => $email))->row_array(0);

			// User doesnt exist
			if( count($user) <= 0 )
			{
				// $this->template->load_view($this->uri_name . '/forgot-password', array('error' => lang('Ops! There is no users registered with this email address')), false, false);
				
				$response = array('status' => false,
							'message' => 'Ops! There is no users registered with this email address.',
							'type_message' => 'danger'
					);
				// echo json_encode($response);

			}
			else
			{

				$args['user'] = $user;
				$args['id_user'] = get_value($user, 'id_user');
				$args['key_access'] = md5(uniqid());

				// Collect email body msg
				$body_msg = $this->template->load_view('App_User/email-reset-password', $args, true, false);

				// Now try to send email
				$this->load->library('email');
				$this->email->clear();
			    $this->email->to(get_value($user, 'email'));
			    $this->email->from(EMAIL_FROM, APP_NAME);
			    $this->email->subject(lang('Reset password'));
			    $this->email->message($body_msg);

			    if( ! @$this->email->send() ) {
			    	// print_r($this->email->print_debugger());

			    	// die;
					// $this->template->load_view($this->uri_name . '/forgot-password', array('error' => lang('Ops! It has been not possible to send the email message. Please try it again later.')), false, false);
					
					$response = array('status' => false,
							'message' => 'Ops! It has been not possible to send the email message. Please try it again later.',
							'type_message' => 'danger'
					);
					// echo json_encode($response);



				} else {

					// Log asking for reset pass
					$data['id_user'] = $args['id_user'];
					$data['key_access'] = $args['key_access'];

					$this->auth->db_log(lang('Reset password request'), 'reset_password', '', $data);

					// Load page success!
					// $this->template->load_view($this->uri_name . '/forgot-password', array('success' => lang('OK! One message containing all steps to reset your password was forwarded to ') . $email), false, false);
					$response = array('status' => true,
							'message' => 'OK! Password recovery instruction has been sent to "'.$email.'"',
							'type_message' => 'success'
					);
					// echo json_encode($response);
				}
			}
			
			echo json_encode($response);
		}
	}

	/**
	 * Form to reset user password. The parameters (iduser, key_access) must be valid in
	 * order to get access to this form. The second parameter says to process this same form.
	 *
	 * @param int id_user
	 * @param string key_access
	 * @param boolean process
	 * @return void
	 */
	public function reset_password($id_user = 0, $key_access = '', $process = false)
	{
		if($id_user == '' || $key_access == '')
			redirect($this->uri_name);

		// Var to check if exist any valid log
		$valid = false;
		$id_log = 0;

		// Collect all reset_passwords logs
		$data = $this->db->get_where('hsapp_log', array('action' => 'reset_password'))->result_array();

		// Loop all logs, trying to find a valid reset password log
		foreach ($data as $log) {

			$reset = json_decode(get_value($log, 'additional_data'), true);

			if(get_value($reset, 'id_user') == $id_user && get_value($reset, 'key_access') == $key_access) {
				$valid = true;
				$id_log = get_value($log, 'id_log');
				break;
			}
		}

		// Well, this is not a valid token or this reset already used
		if( ! $valid)
			redirect( $this->uri_name );

		// This is not a process action
		if( ! $process)
			$this->template->load_view($this->uri_name . '/reset-password', array('id_user' => $id_user, 'key_access' => $key_access), false, false);
		else {

			// Get user's new pass
			$password = $this->input->post('password');
			$p_repeat = $this->input->post('password_repeat');

			// Bad passwords
			if($password == '' || $p_repeat == '' || $password != $p_repeat)
				redirect($this->uri_name);

			// Update user data
			$this->db->update('hsapp_user', array('password' => md5($password)), array('id_user' => $id_user));

			// Delete log
			$this->db->delete('hsapp_log', array('id_log' => $id_log));

			// Load view (update ok)
			$this->template->load_view($this->uri_name . '/reset-password', array('id_user' => $id_user, 'key_access' => $key_access, 'success' => lang('OK! Password successfully changed')), false, false);
		}
	}

	/**
	 * Change current language on session.
	 *
	 * @param string language 	// en_US, pt_BR, es_ES ...
	 * @return string json
	 */
	public function change_language($language = '')
	{
		$this->session->set_userdata('language', $language);
		echo json_encode(array('return' => true));
	}
}
