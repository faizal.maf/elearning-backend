<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
    {
		parent::__construct();
		validate_session();
		$this->uri_name = 'dashboard';
		$this->title = 'Dashboard';
	}
	
	public function index()
	{
		$breadcrumbs = array(array('url' => $this->uri_name, 'title' => $this->title, 'active' => true));
		$data['breadcrumbs'] = breadcrumbs($breadcrumbs, '&raquo;');
		// $this->load->view('welcome_message');
		$this->template->view($this->uri_name.'/list', $data);
	}
}
