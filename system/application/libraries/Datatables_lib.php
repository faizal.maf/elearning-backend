<?php

class Datatables_lib
{
	protected $CI;
	public $table;
	public $sort;
	public $field;
	public $page;
	public $perpage;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

	public function init($set_sort, $set_sort_field)
	{
		$this->table   = array_merge(['pagination' => [], 'sort' => [], 'query' => []], $_REQUEST);
		$this->sort    = ! empty($this->table['sort']['sort']) ? $this->table['sort']['sort'] : $set_sort;
		$this->field   = ! empty($this->table['sort']['field']) ? $this->table['sort']['field'] : $set_sort_field;
		$this->page    = ! empty($this->table['pagination']['page']) ? (int)$this->table['pagination']['page'] : 1;
		$this->perpage = ! empty($this->table['pagination']['perpage']) ? (int)$this->table['pagination']['perpage'] : -1;
	}

	public function build_data($field, $set_order, $sort, $perpage, $page)
	{
		$total = $this->_get_total();
		$this->_get_order($field, $set_order, $sort);

		$pages   = $this->_get_pages($total, $perpage);
		$datas = $this->_get_data($pages, $perpage, $page);

		$result['total'] = $total;
		$result['pages'] = $pages;
		$result['data'] = $datas;

		return $result;
	}

	public function build_datatable($total, $data=array(), $sort, $field, $page, $pages, $perpage)
	{
		$meta = [
			'page'    => (int)$page,
			'pages'   => (int)$pages,
			'perpage' => (int)$perpage,
			'total'   => (int)$total,
		];

		$result = [
			'meta' => $meta + [
					'sort'  => $sort,
					'field' => $field,
				],
			'data' => $data
		];

		return $result;
	}

	private function _get_total()
	{
		$tempdb = clone $this->CI->db;
		$total  = $tempdb->count_all_results();

		return $total;
	}

	private function _get_order($field, $set_order, $sort)
	{
		$this->CI->db->order_by(array_search($field, $set_order), $sort);
	}

	private function _get_pages($total, $perpage)
	{
		return ceil($total / $perpage); // calculate total pages
	}

	private function _get_data($pages, $perpage, $page)
	{
		$result = array();
		if($perpage > 0)
		{
			$page   = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
			$page   = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
			$offset = ($page - 1) * $perpage;
			if ($offset < 0) {
				$offset = 0;
			}
			
			$this->CI->db->limit($perpage, $offset);
			
			$result = $this->CI->db->get()->result_array();
		}

		return $result;
	}


}
