<?php defined('BASEPATH') or exit('No direct script access allowed');

class Func_validation {

	protected $CI;
	public function __construct () 
	{
		$this->CI =& get_instance();
	}

	public function set_field($field, $label=null, $rules=" ")
	{
		if($label== null) {
			$label = $field;
		}

		return array(
				'field'   => $field,
				'label'   => $label,
				'rules'   => $rules
			);
	}

	public function set_form_success($data= array(), $text=" ")
	{
		$result = array(
			'status' => 1,
			'error' => $data,
			'msg' => "<div class='alert alert-success m-alert m-alert--square m-alert--air' role='alert'><strong>Success!</strong> Data has been $text.</div>"
		);

		return $result;
	}

	public function set_form_error($field= array())
	{
		foreach($field as $properties)
				$formError[$properties['field']] = form_error($properties['field'], '<div class="form-control-feedback">', '</div>');

		$result = array(
			'status' => 0,
			'error' => array_remove_index($formError),
			'msg' => "<div class='alert alert-danger m-alert m-alert--square m-alert--air' role='alert'><strong>Ups!</strong> Please fill in the data correctly and accordingly before submit</div>"
		);

		return $result;
	}

	public function set_data($nama_coloumn, $sumber)
	{
		$hasil = array();
			foreach($nama_coloumn as $row)
			{
				if(isset($sumber[$row]))
				{
					$hasil[$row] = $sumber[$row];
				}
			}
		
		return $hasil;
	}
	






	function alert_redirect($pesan='pesan',$url)
    {
        echo "<script type=\"text/javascript\">alert('".$pesan."');window.location ='".site_url()."".$url."';</script>";
	}
	
	function isExistUsername($id)
	{
		$this->CI->db->select('*');
		$this->CI->db->from('part_users');
		$this->CI->db->where('username',$id);

		$hasil = $this->CI->db->get();

		if($hasil->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}

	}

	function isExistEmail($email, $id='')
	{
		$this->CI->db->select('*');
		$this->CI->db->from('part_users');
		$this->CI->db->where('email',$email);
		if(!empty($id))
		{
			$this->CI->db->where_not_in('id_user', $id);
		}

		$hasil = $this->CI->db->get();

		if($hasil->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
