<?php defined('BASEPATH') or exit('No direct script access allowed');

class Mail_lib {

	/**
	 * CI controller instance.
	 * @var object
	 */
	public $CI = null;

	/**
	 * Class constructor.
	 */
	public function __construct () 
	{
		$this->CI =& get_instance();
	}
	
	public function sender($to = '', $data = array())
	{
		$result = TRUE;
		$app = $this->CI->config->item('APP_NAME');
		$email_from = $this->CI->config->item('EMAIL_FROM');
		// Collect email body msg

		// Now try to send email
		$this->CI->load->library('email');
		$this->CI->email->clear();
	    $this->CI->email->to($to);
	    $this->CI->email->from($email_from, $app);
	    $this->CI->email->subject($data['subject']);
		$this->CI->email->message($data['body']);

		if( ! @$this->CI->email->send() ) 
		{
			echo $this->CI->email->print_debugger();
			$result = FALSE;
		}

		return $result;
	}
}
