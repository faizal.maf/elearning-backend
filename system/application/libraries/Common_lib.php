<?php defined('BASEPATH') or exit('No direct script access allowed');

class Common_lib 
{
    var $obj;
    public function __construct() {
		$this->obj =& get_instance();
    }

	public function post_inbox($id, $ref_id, $title, $highlight, $body, $type) {
		$data = array(
            'user_id'    => $id,
            'ref_id'    => $ref_id,
            'title'      => $title,
            'highlight'  => $highlight,
            'body'       => $body,
            'type'       => $type,
            'created_by' => $this->obj->session->userdata('Username'),
            'created'    => date('YmdHis'),
            'updated'    => date('YmdHis')
        );

        $this->obj->db->insert('bsm_inbox', $data);
	}

	public function post_mutation($id, $ref_id, $debet, $kredit, $ket, $poin, $type) {
		$data = array(
            'user_id' => $id,
            'ref_id' => $ref_id,
            'tgl'     => date('YmdHis'),
            'debet'   => $debet,
            'kredit'  => $kredit,
            'ket'     => $ket,
            'poin'    => $poin,
            'type'    => $type,
        );

        $this->obj->db->insert('bsm_poin_mutation', $data);
	}

	public function get_const_default_poin_quiz() {
		$field = array('id_lookup' => 'POIN1', 'deskripsi' => 'quiz');
		$temp = $this->obj->db->get_where('bsm_config', $field);
		$temp = $temp->result_array();
		if(isset($temp[0]))
		{
			return $temp[0]['catatan'];
		}
		return "";
	}

	public function get_const_default_poin_pertanyaan() {
		$field = array('id_lookup' => 'POIN2', 'deskripsi' => 'pertanyaan');
		$temp = $this->obj->db->get_where('bsm_config', $field);
		$temp = $temp->result_array();
		if(isset($temp[0]))
		{
			return $temp[0]['catatan'];
		}
		return "";
	}

	public function get_const_default_poin_gratifikasi() {
		$field = array('id_lookup' => 'POIN3', 'deskripsi' => 'gratifikasi');
		$temp = $this->obj->db->get_where('bsm_config', $field);
		$temp = $temp->result_array();
		if(isset($temp[0]))
		{
			return $temp[0]['catatan'];
		}
		return "";
	}
	
	public function store_last_id($field, $last_id) {
	
		$this->obj->db->set('last_content_id', $last_id);
		$this->obj->db->where('field', $field);
		$this->obj->db->update('bsm_last_content');
	}
	
	public function update_last_id_after_delete($table_name, $id, $id_kategori)
	{
		$this->obj->db->select('*');
		$this->obj->db->from($table_name);
		$this->obj->db->where('id', $id);
		$this->obj->db->limit(1);
		$query = $this->obj->db->get()->row();
		
		$this->obj->session->set_userdata('update_last_id_table_name', $table_name);
		$this->obj->session->set_userdata('update_last_id_id', $query->id);
		if(!empty($id_kategori)) {
			$this->obj->session->set_userdata('update_last_id_id_kategori', $id_kategori);
		}else{
			$this->obj->session->set_userdata('update_last_id_id_kategori', $query->id_kategori);
		}
		
	}
	
	public function get_last_id_after_delete()
	{
		$table_name = $_SESSION['update_last_id_table_name'];
		$id = $_SESSION['update_last_id_id'];
		$id_kategori = $_SESSION['update_last_id_id_kategori'];
		
		
		$this->obj->db->select('*');
		$this->obj->db->from($table_name);

		if($id_kategori == "galeri" || $id_kategori == "infografis" || $id_kategori == "apuppt" || $id_kategori == "video" || $id_kategori == "artikel" || $id_kategori == "faq")
		{}
		else{
			$this->obj->db->where('id_kategori', $id_kategori);
		}
		$this->obj->db->limit(1);
		$this->obj->db->where('status', 1);
		$this->obj->db->order_by('id', 'DESC');
		$query = $this->obj->db->get();
		
		if($query->num_rows() > 0)
		{
			$query = $query->row();
			$this->store_last_id($id_kategori, $query->id);
		}
		else
		{
			$this->store_last_id($id_kategori, 0);
		}
		
		unset(
			$_SESSION['update_last_id_table_name'],
			$_SESSION['update_last_id_id'],
			$_SESSION['update_last_id_id_kategori']
		);
	}

	public function update_last_id_after_set_status($table_name, $id, $id_kategori)
	{
		$this->obj->db->select('*');
		$this->obj->db->from('bsm_last_content');
		$this->obj->db->where('field', $id_kategori);
		$this->obj->db->where('last_content_id', $id);
		$result = $this->obj->db->get();

		if($result->num_rows() > 0) {

			$this->obj->db->select('*');
			$this->obj->db->from($table_name);

			if($id_kategori == "galeri" || $id_kategori == "infografis" || $id_kategori == "apuppt" || $id_kategori == "video" || $id_kategori == "artikel" || $id_kategori == "faq")
			{}
			else{
				$this->obj->db->where('id_kategori', $id_kategori);
			}
			$this->obj->db->where('status', 1);
			$this->obj->db->limit(1);
			$this->obj->db->order_by('id', 'DESC');
			$query = $this->obj->db->get();
			if($query->num_rows() > 0)
			{
				$query = $query->row();
				$this->store_last_id($id_kategori, $query->id);
			}
			else
			{
				$this->store_last_id($id_kategori, 0);
			}
		}
		else
		{
			$this->obj->db->select('*');
			$this->obj->db->from($table_name);

			if($id_kategori == "galeri" || $id_kategori == "infografis" || $id_kategori == "apuppt" || $id_kategori == "video" || $id_kategori == "artikel" || $id_kategori == "faq")
			{}
			else{
				$this->obj->db->where('id_kategori', $id_kategori);
			}
			$this->obj->db->where('status', 1);
			$this->obj->db->limit(1);
			$this->obj->db->order_by('id', 'DESC');
			$query = $this->obj->db->get();
			if($query->num_rows() > 0)
			{
				$query = $query->row();
				$this->store_last_id($id_kategori, $query->id);
			}
			else
			{
				$this->store_last_id($id_kategori, 0);
			}
		}

	}

	//Upload image summernote
    public function upload_image() {
        if(isset($_FILES["image"]["name"])){
            /* Create folder image if doesn't exist */
			if (!file_exists('upload/imgs/')) 
				mkdir('upload/imgs/', 0777, true); //add true agar membuat file recursive
				
			/* Create folder year today if doesn't exist */
			if (!file_exists('upload/imgs/'.date("Y").'/'))
				mkdir('upload/imgs/'.date("Y").'/', 0777, true);
			
			/* Create folder year today if doesn't exist */
			if (!file_exists('upload/imgs/'.date("Y").'/'.date("m").'/'))
				mkdir('upload/imgs/'.date("Y").'/'.date("m").'/', 0777, true);
			
			if (!file_exists('upload/imgs/'.date("Y").'/'.date("m").'/'))
				mkdir('upload/imgs/'.date("Y").'/'.date("m").'/', 0777, true);
					
			$this->obj->load->library('upload');
			$config['upload_path'] = 'upload/imgs/'.date("Y").'/'.date("m").'/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->obj->upload->initialize($config);
            if(!$this->obj->upload->do_upload('image')){
                $this->obj->upload->display_errors();
                return FALSE;
            }else{
                $data = $this->obj->upload->data();
                //Compress Image
                $config['image_library']= 'gd2';
                $config['source_image']= 'upload/imgs/'.date("Y").'/'.date("m").'/'.$data['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= TRUE;
                $config['quality']= '60%';
                $config['width']= 800;
                $config['height']= 800;
                $config['new_image']= 'upload/imgs/'.date("Y").'/'.date("m").'/'.$data['file_name'];
                $this->obj->load->library('image_lib', $config);
                $this->obj->image_lib->resize();
                echo base_url().'upload/imgs/'.date("Y").'/'.date("m").'/'.$data['file_name'];
            }
        }
    }
	
	//Delete image summernote
    public function delete_image() {
        $src = $this->obj->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if(unlink($file_name))
        {
            echo 'File Delete Successfully';
        }
    }
}
