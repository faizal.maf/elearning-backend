<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth {

	public $CI = null;

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function validate_login($username = '', $pass = '')
	{
		// Basic return
		$user = array();

		// Only check if given username and pass are valid
		if(isset($username) && isset($pass) && $username != '' && $pass != '')
		{
			$user = array();
			$sql = "SELECT u.id_user,
						u.id_level,
						u.username,
						u.email,
						u.first_name,
						u.last_name,
						u.status,
						lvl.nama
						
					FROM part_users u
					LEFT JOIN part_level lvl ON (lvl.id_level = u.id_level)
						WHERE (u.email = ? OR u.username = ?)
							AND u.password = md5(?)";
							
			// Gets user
			$q = $this->CI->db->query($sql, array($username, $username, $pass));
			if($q->num_rows() > 0)
			{
				$user = $q->row_array(0);
			}
		}

		// Find user
		return count($user) > 0 ? $user : false;
	}

	public function validate_session()
	{
		if($this->check_session() === FALSE)
		{
			redirect('login');
			return FALSE;
		}

		return TRUE;
	}

	public function check_session()
	{
		if($this->CI->session->userdata('LoginAccess') == '' || $this->CI->session->userdata('UserID') == '')
		{
			return FALSE;
		}
		return TRUE;
	}

	// Logger For login
	public function db_log($text_log = '', $action = '', $table = '', $additional_data = array())
	{
		$this->CI =& get_instance();
		$this->CI->load->library('user_agent');

		// Maybe user dont exist
		if( $this->CI->session->userdata('UserID') != '' )
		{
			// update table user last activity
			$this->CI->db->where('id_user',$this->CI->session->userdata('UserID'));
			$log_activity['last_activity'] = date('YmdHis');
			$this->CI->db->update('part_users', $log_activity);

			$log['id_user'] = $this->CI->session->userdata('UserID');
		}

		// Log data
		$log['table_name'] = $table;
		$log['action'] = $action;
		$log['log_description'] = $text_log;
		$log['additional_data'] = json_encode($additional_data);
		$log['user_agent'] = $this->CI->agent->agent_string();
		$log['browser_name'] = $this->CI->agent->browser();
		$log['browser_version'] = $this->CI->agent->version();
		$log['device_name'] = $this->CI->agent->is_mobile()	? $this->CI->agent->mobile() : 'PC';
		$log['platform'] = $this->CI->agent->platform();
		$log['ip_address'] = $this->CI->input->ip_address();

		// Insert record
		$this->CI->db->insert('part_log_users', $log);
	}

	public function log_error($error_type = '', $header = '', $message = '', $status_code = '')
	{
		$this->CI =& get_instance();
		$this->CI->load->library('user_agent');

		// Log data
		$log['error_type'] = $error_type;
		$log['header'] = $header;
		$log['status_code'] = $status_code;
		$log['user_agent'] = $this->CI->agent->agent_string();
		$log['browser_name'] = $this->CI->agent->browser();
		$log['browser_version'] = $this->CI->agent->version();
		$log['device_name'] = $this->CI->agent->is_mobile()	? $this->CI->agent->mobile() : 'PC';
		$log['platform'] = $this->CI->agent->platform();
		$log['ip_address'] = $this->CI->input->ip_address();
		$log['message'] = $message;

		// Adjusts column
		if($this->CI->session->userdata('id_user') != '' && $this->CI->session->userdata('id_user') != 0)
			$log['id_user'] = $this->CI->session->userdata('id_user');

		if(is_array($message)) {
			$vars = json_encode($message, true);
			$log['message'] = $vars;
			$log['additional_data'] = $vars;
		}

		// Insert record
		$this->CI->db->insert('part_log_error', $log);
	}
}
