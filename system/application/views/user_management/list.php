<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-xl-12">
            <!--begin::Portlet-->
            <div class="m-portlet " id="m_portlet">
				<div class="m-portlet__head" style="height: 3rem;">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">Daftar <?php echo $title; ?></h3>
						</div>
					</div>
					<div class="m-portlet__head-tools right">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
							</li>
						</ul>
					</div>
				</div>
                <div class="m-portlet__body">
					<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
                    <div class="m-form m-form--label-align-right">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-4">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="m-input-icon m-input-icon--left">
												<input type="text" class="form-control m-input m-input--square" id="generalSearch" placeholder="Pencarian..." autocomplete="off">
												<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
												<label for="fillStatus">Status</label>
													<?php echo (isset($status) ? $status : ''); ?>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
												<label for="fillLvl">Level</label>
													<?php echo (isset($level) ? $level : ''); ?>
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
												<label for="fillJabatan">Jabatan</label>
													<?php echo (isset($jabatan) ? $jabatan : ''); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
								<a href="<?php echo base_url().$controller_name.'/form_import' ?>" class="btn btn-secondary m-btn--label-default m-btn--bolder btn-sm m-btn--custom m-btn--icon">
									<span>
										<i class="fa fa-cloud-upload-alt"></i>
										<span>Import data</span>
									</span>
								</a>
								<div class="m-separator m-separator--dashed d-sm-none"></div>
							</div>
						</div>
					</div>
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
						<div class="row align-items-center">
							<div class="col-xl-12">
								<div class="m-form__group m-form__group--inline">
									<div class="m-form__label m-form__label-no-wrap">
										<label class="m--font-bold m--font-danger-">Selected
											<span id="m_datatable_selected_number">0</span> records:</label>
									</div>
									<div class="m-form__control">
										<div class="btn-toolbar">
											<div class="dropdown">
												<button type="button" class="btn btn-accent btn-sm dropdown-toggle" data-toggle="dropdown">
													Update status
												</button>
												<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
													<a class="dropdown-item" href="#">Pending</a>
													<a class="dropdown-item" href="#">Delivered</a>
													<a class="dropdown-item" href="#">Canceled</a>
												</div>
											</div>
											&nbsp;&nbsp;&nbsp;
											<button class="btn btn-sm btn-danger" type="button" id="m_datatable_delete_all">Delete All</button>
											&nbsp;&nbsp;&nbsp;
											<button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#m_modal_fetch_id">Fetch Selected Records</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <!---begin: list -->
                    <div id="list"></div>
					
					<!--begin::Modal-->
								<div class="modal fade" id="m_modal_fetch_id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-height="200">
													<ul class="m_datatable_selected_ids"></ul>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>

								<!--end::Modal-->
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
</div>
<script type="text/javascript">
var table = {
	init: function () {
		var t;
		t = $("#list").mDatatable({
			<?php echo init_table_list('', $url_tablelist); ?>
			columns: [
			// {
				// field: 'RecordID',
				// title: '#',
				// sortable: false,
				// width: 40,
				// textAlign: 'center',
				// selector: {class: 'm-checkbox--solid m-checkbox--brand'},
			// }, 
			{
				field: "ID",
				title: "Nama",
				filterable: !1,
				width: 200,
				template: function (t) {
					var a = mUtil.getRandomInt(0, 7);
					output = '<div class="m-card-user m-card-user--sm">';
					output += '<div class="m-card-user__pic">';
					output += '<div style="width:30px;height:30px;" class="m-card-user__no-photo m--bg-fill-' + ["success", "brand", "danger", "accent", "warning", "metal", "primary", "info"][a] + '">';
					output += '<span style="font-size: 1rem;">' + t.Name.substring(0, 1) + '</span>';
					output += '</div></div>';
					output += '<div class="m-card-user__details">';
					output += '<span class="m-card-user__name">' + t.Name + '</span>';
					output += '</div>';
					output += '</div>';
					return output
				}
			}, 
			{
				field: "LevelAccess",
				title: "Level",
				width: 80
			},
			{
				field: "NIP",
				title: "NIP",
				width: 100
			}, {
				field: "Email",
				title: "Email",
				template: function (t) {
					return '<a title="' + t.Email + '" href="mailto:' + t.Email + '">' + t.Email + "</a>"
				}
			}, {
				field: "Position",
				title: "Jabatan",
				width: 150
			}, {
				field: "Point",
				title: "Point",
				width: 75,
				textAlign: "center"
			}, {
				field: "Date",
				title: "Tanggal Input",
				sortable: "desc",
				width: 140
			}, {
				field: "Status",
				title: "Status",
				width: 75,
				textAlign: "center"
			}, {
				field: "Actions",
				title: "",
				sortable: !1,
				width: 70,
				textAlign: "center",
				overflow: "visible",
				template: function (t, e, a) {
					return '\t\t\t\t\t<a data-toggle="mainmodal" data-modal-type="modal-md" title="Edit <?php echo $title; ?>" id="frmEdit" href="{base_url}<?php echo $controller_name; ?>/form/'+t.RecordID+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\t\t\t\t\t\t\t<i class="la la-pencil-square"></i>\t\t\t\t\t\t</a>\t\t\t\t\t<a id="frmDelete" value="' + t.RecordID + '" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\t\t\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t'
				}
			}]
		}), 
		$("#fillStatus").on("change", function () {
			t.search($(this).val(), "fillStatus")
		});
		$("#fillLvl").on("change", function () {
			t.search($(this).val(), "fillLvl")
		});
		$("#fillJabatan").on("change", function () {
			t.search($(this).val(), "fillJabatan")
		});
		
		
		$('#m_form_status').on('change', function() {
			t.search($(this).val().toLowerCase(), 'Status');
		});

		$('#m_form_type').on('change', function() {
			t.search($(this).val().toLowerCase(), 'Type');
		});

		$('#m_form_status,#m_form_type').selectpicker();

		t.on('m-datatable--on-check m-datatable--on-uncheck m-datatable--on-layout-updated', function(e) {
			var checkedNodes = t.rows('.m-datatable__row--active').nodes();
			var count = checkedNodes.length;
			$('#m_datatable_selected_number').html(count);
			if (count > 0) {
				$('#m_datatable_group_action_form').collapse('show');
			} else {
				$('#m_datatable_group_action_form').collapse('hide');
			}
		});

		$('#m_modal_fetch_id').on('show.bs.modal', function(e) {
			var ids = t.rows('.m-datatable__row--active').
				nodes().
				find('.m-checkbox--single > [type="checkbox"]').
				map(function(i, chk) {
					return $(chk).val();
				});
			var c = document.createDocumentFragment();
			for (var i = 0; i < ids.length; i++) {
				var li = document.createElement('li');
				li.setAttribute('data-id', ids[i]);
				li.innerHTML = 'Selected record ID: ' + ids[i];
				c.appendChild(li);
			}
			$(e.target).find('.m_datatable_selected_ids').append(c);
		}).on('hide.bs.modal', function(e) {
			$(e.target).find('.m_datatable_selected_ids').empty();
		});

	}
}

$(function () {
	table.init();
	$('form').submit(!1)
});

$(document).on('click', '.btnPublish', function (e) {
	e.preventDefault();
	var id = $(this).attr('id');
	var status;
	if ($(this).hasClass('pubYes')) {
		$(this).addClass('pubNo btn-danger').removeClass('pubYes btn-success').html('<i class="la la-close"></i>Inactive');
		status = 0;
	} else if ($(this).hasClass('pubNo')) {
		$(this).removeClass('pubNo btn-danger').addClass('pubYes btn-success').html('<i class="la la-check"></i>Active');
		status = 1;
	}
	$.post("<?php echo base_url().$controller_name ?>/set_status", {id: id, status: status}, function (theResponse) {
		make_toast('success', 'Data has been success change.')
	})
});

$(document).on('click', '#frmDelete', function (e) {
	e.preventDefault();
	var id = $(this).attr('value');
	var trID = $(this).closest('tr').attr("data-row");

	funcDelete("<?php echo base_url().$controller_name ?>/delete", id, trID);
});

$(document).on('change', '#id_level', function(e) {
	e.preventDefault();
	var val2 = $(this).val();
	var id = $(this).attr('value');
	
	console.log("VAL : " + val2);
	console.log("ID : " + id);
  // Does some stuff and logs the event to the console
});
</script>