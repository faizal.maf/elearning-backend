<?php $result = (object)$result;
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--state myForm', 'id' => 'frmUPSERTDATA', 'novalidate'=>'novalidate'));
	echo form_hidden('id', (isset($result->id) ? $result->id : ''));
?>
<div class="m-portlet__body">
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group">
			<input type="text" id="name" name="name" maxlength="40" class="form-control m-input m-input--square" value="<?php echo set_value('name', (isset($result->name) ? $result->name : ''), FALSE) ?>" placeholder="Nama" autocomplete="off">
			<label for="name">Nama</label>
		</div>
	</div>
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group">
			<input type="text" id="email" name="email" maxlength="40" class="form-control m-input m-input--square" value="<?php echo set_value('email', (isset($result->email) ? $result->email : ''), FALSE) ?>" placeholder="Email" autocomplete="off">
			<label for="email">Email</label>
		</div>
	</div>
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group">
			<input type="text" id="nip" name="nip" maxlength="30" class="form-control m-input m-input--square" value="<?php echo set_value('nip', (isset($result->nip) ? $result->nip : ''), FALSE) ?>" placeholder="Nip" autocomplete="off">
			<label for="nip">Nip</label>
		</div>
	</div>
	<!-- div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group">
			<input type="text" id="jabatan" name="jabatan" maxlength="150" class="form-control m-input m-input--square" value="< ?php echo set_value('jabatan', (isset($result->jabatan) ? $result->jabatan : ''), FALSE) ?>" placeholder="Jabatan" autocomplete="off">
			<label for="jabatan">Jabatan</label>
		</div>
	</div-->
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group form-label-group-select">
		<label for="Jabatan">Jabatan</label>
			<?php echo form_dropdown('id_jabatan', $groups_list, set_value('id_jabatan', (isset($result->id_jabatan) ? $result->id_jabatan : '')), 'class="custom-select custom-select-label form-control m-input m-input--square" id="id_jabatan"'); ?>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script>
var JSValidation = function() {
	var e;
	return {
		init: function() {
			e = $("#frmUPSERTDATA").validate({
				rules: {
					name: {required: !0},
					email: {required: !0,email: !0},
					nip: {required: !0},
					id_jabatan: {required: !0}
				},
				messages: {
					name: {required: "Wajib diisi!"},
					email: {required: "Wajib diisi!", email: "Email tidak sah!"},
					nip: {required: "Wajib diisi!"},
					id_jabatan: {required: "Wajib diisi!"}
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

var MAXLength = {
    init: function() {
		$("#name, #email, #nip").maxlength({
			alwaysShow: !0,
			warningClass: "m-badge m-badge--metal m-badge--rounded m-badge--wide",
			limitReachedClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
			appendToParent: !0
		})
	}
};

$(document).ready(function() {
	onDemandScript("<?php echo base_url('public/themes/js/custom.js?v1&current='.date('Ym'))?>");
	JSValidation.init();
	MAXLength.init();
	
	var Btn = "<button type='button' class='btn btn-sm btn-success m-btn--square pull-left' id='btnSubmit'>Submit</button>";
		Btn += "<button type='button' class='btn btn-sm btn-metal m-btn--square' data-dismiss='modal'>Cancel</button>";
	$('#ModalFooterID').html(Btn);

	$("#frmUPSERTDATA").find('input[type=text]').filter(':visible:first').focus();
	
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		$('#ResponseInput, #ResponseUpload').removeClass('fade show');
		$('#ResponseInput, #ResponseUpload').html('');

		var form = $("#frmUPSERTDATA").valid();
		if(form === true)
			upsert_data();
	});
});

function upsert_data() {
	var formData = new FormData($("#frmUPSERTDATA").get(0));
	$.ajax({
		url: $('#frmUPSERTDATA').attr('action'),
		type: "POST",
		cache: false,
		processData: false,
		contentType: false,
		data: formData,
		dataType:'json',
		beforeSend: function() {
			progressModal('start');
		},
		error: function() {
			alert('Ajax Error');
			progressModal('stop');
		},
		success: function(response) {
			if(response.status == 1)
			{
				$('#ResponseInput').html(response.msg).show('slow');
					setTimeout(function() {
						$('#ResponseInput').html('').hide();
						$('#ModalAjaxID').modal('hide');
						$('#list').mDatatable('reload');
					}, 1e3);
			}
			else {
				$('#ResponseInput').html(response.msg).show('slow');
				$.each(response.error, function(key, value) {
                    $('#'+key).addClass('is-invalid');
                    $('#'+key).after(value);
                    $('#'+key).parents('.form-group').addClass('has-danger');
                });
			}
		},
		complete : function() {
			progressModal('stop');
		}
	});
}
</script>