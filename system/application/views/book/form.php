<?php
	$result = (object)$result;
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--state myForm', 'id' => 'frmUPSERTDATA', 'novalidate'=>'novalidate'));
	echo form_hidden('id', (isset($result->id) ? $result->id : ''));
?>
	<div class="m-portlet__body">
		<div class="form-group m-form__group pt-2 pb-2">
			<div class="form-label-group">
				<?php 
					echo form_input(
						array('id' => 'judul',
						'name' => 'judul',
						'value' => set_value('judul', (isset($result->judul) ? $result->judul : ''), FALSE),
						'class' => 'form-control m-input m-input--square',
						'placeholder' => 'Judul',
						'maxlength' => '200',
						'autocomplete' => 'off'
						)
					);
				?>
					<label for="title">Judul</label>
			</div>
		</div>
        <div class="form-group m-form__group pt-2 pb-2">
			<div class="form-label-group">
				<?php 
					echo form_input(
						array('id' => 'penulis',
						'name' => 'penulis',
						'value' => set_value('penulis', (isset($result->penulis) ? $result->penulis : ''), FALSE),
						'class' => 'form-control m-input m-input--square',
						'placeholder' => 'Penulis',
						'maxlength' => '200',
						'autocomplete' => 'off'
						)
					);
				?>
					<label for="title">Penulis</label>
			</div>
		</div>
		<div class="form-group m-form__group pt-2 pb-2">
			<div class="form-label-group">
				<?php 
					echo form_input(
						array('id' => 'tahun_terbit',
						'name' => 'tahun_terbit',
						'value' => set_value('tahun_terbit', (isset($result->tahun_terbit) ? $result->tahun_terbit : ''), FALSE),
						'class' => 'form-control m-input m-input--square',
						'placeholder' => 'Tahun terbit',
						'maxlength' => '10',
						'autocomplete' => 'off'
						)
					);
				?>
				<label for="tahun_terbit">Tahun terbit</label>
			</div>
		</div>
		<div class="form-group m-form__group pt-2 pb-2">
			<div class="form-label-group form-label-group-select">
			<label for="kategori">Kategori</label>
				<?php echo form_dropdown('kategori', $list_kategori, set_value('kategori', (isset($result->kategori) ? $result->kategori : '')), 'class="custom-select custom-select-label form-control m-input m-input--square" id="kategori"'); ?>
			</div>
		</div>
        
        
		<div class="form-group m-form__group row pt-0 pb-2">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<?php 
					echo form_textarea(
						array('id' => 'sinopsis',
							'name' => 'sinopsis',
							'value' => set_value('sinopsis', (isset($result->sinopsis) ? $result->sinopsis : ''), FALSE),
							'type' => 'textarea',
							'class' => 'summernote form-control form-control-sm m-input m-input--square'
						)
					);
				?>
                
				<label for="sinopsis">Sinopsis</label>
			</div>
		</div>
        <div class="form-group m-form__group row pt-2 pb-2">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<input type="hidden" id="chkimage" value="<?php echo set_value('cover', (isset($result->cover) ? $result->cover : ''), FALSE); ?>">
				<div class="custom-file">
					<?php 
						echo form_input(
							array('id' => 'cover',
								'name' => 'cover',
								'value' => set_value('image', (isset($result->cover) ? $result->cover : ''), FALSE),
								'type' => 'file',
								'class' => 'form-control m-input custom-file-input',
								'accept'=> 'image/*',
								'style' => 'padding: 2px;'
							)
						);
					?>
					<label class="custom-file-label" for="customFile">Pilih file...</label>
				</div>
				<span class="m-form__help">* Upload <b>JPG,JPEG,PNG</b>,Ukuran file maksimal <b>5MB</b>.</span>
				<div class="pt-2" style="display:none;" id="divPreviewImage">
					<span class="m--font-boldest">Preview : </span>
					<br>
						<?php 
							$image_properties = array(
								'src'   => set_value('image', (isset($result->cover) ? $result->cover : ''), FALSE),
								'id'	=> 'previewImg',
								'class' => 'pt-2',
								'width' => '100px'
							);
							// echo img($image_properties);
						?>
				</div>
			</div>
		</div>	
		<div class="form-group m-form__group row pt-2 pb-2">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<div class="custom-file">
					<?php 
						echo form_input(
							array('id' => 'filename',
								'name' => 'filename',
								'type' => 'file',
								'class' => 'form-control m-input custom-file-input',
								'style' => 'padding: 2px;'
							)
						);
					?>
					<label class="custom-file-label" for="customFile">Pilih file...</label>
				</div>
				<span class="m-form__help">* Upload <b>Ebook</b>, Ukuran file maksimal <b>50MB</b>.</span>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<?php if($action !== 'add'){ ?>
					<?php $attachmentpath = 'File PDF: <a class="m-link m-link--state m-link--info" href="'.base_url('regulasi/view_pdf/'.$result->keypdf).' " target="_blank" title="Lihat file">'.$result->filename.'</a>'; ?>
					<span class="form-control-plaintext m-font-bolder"><?php echo set_value('filename', (isset($result->filename) ? $attachmentpath : ''), FALSE); ?></span>
				<?php } ?>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>

<script>
var JSValidation = function() {
var e;
	return {
		init: function() {
			e = $("#frmUPSERTDATA").validate({
				rules: {
					title: {required: !0},
					tahun_terbit: {required: !0, digits: !0},
					id_kategori: {required: !0}
				},
				messages: {
					title: {required: "Wajib diisi!"},
					tahun_terbit: {required: "Wajib diisi!", digits: "Harus angka!"},
					id_kategori: {required: "Wajib diisi!"}
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

var MAXLength = {
    init: function() {
		$("#title, #tahun_terbit").maxlength({
			alwaysShow: !0,
			warningClass: "m-badge m-badge--metal m-badge--rounded m-badge--wide",
			limitReachedClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
			appendToParent: !0
		})
	}
};

$(document).ready(function() {
	JSValidation.init();
	MAXLength.init();
	
	$(".custom-file-input").on("change", function() {
  		var fileName = $(this).val().split("\\").pop();
			$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});

	$('#frmUPSERTDATA input[type=file]').on('change', function () {
		const b = 'Pilih file...';
		// console.log($(this).val());
		if( !$(this).val() ) 
		{
			$(this).next(".custom-file-label").removeClass("selected").html(b).val('');
			// $(this).addClass('is-invalid').attr('aria-invalid', 'true');
			$(this).parents('.form-group').addClass('has-danger');
			$(this).next("div").remove();
		}
		else
		{
			var a=(this.files[0].size); //maxfile 20mb
			if(a > 20971520) {
				alert('Ukuran file lebih dari 20MB!');
					$(this).next(".custom-file-label").removeClass("selected").html(b).val('');
					// $(this).addClass('is-invalid').attr('aria-invalid', 'false');
					$(this).parents('.form-group').addClass('has-danger');
					$(this).next("div").remove();
			}
			else
			{
				// $(this).removeClass('is-invalid').addClass('is-valid').attr('aria-invalid', 'false');
				$(this).parents('.form-group').removeClass('has-danger');
				$(this).next("div").remove();
			}
		}
    });

	var Btn = "<button type='button' class='btn btn-sm btn-success m-btn--square pull-left' id='btnSubmit'>Submit</button>";
		Btn += "<button type='button' class='btn btn-sm btn-metal m-btn--square' data-dismiss='modal'>Cancel</button>";
	$('#ModalFooterID').html(Btn);

	$("#frmUPSERTDATA").find('input[type=text],select').filter(':visible:first').focus();
	
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		$('#ResponseInput, #ResponseUpload').hide().html('');

		var form = $("#frmUPSERTDATA").valid();
		if(form === true)
		upsertContrainAttachment();
	});
});

function upsertContrainAttachment()
{
	var formData = new FormData($("#frmUPSERTDATA").get(0));
	$.ajax({
		url: $('#frmUPSERTDATA').attr('action'),
		type: "POST",
		cache: false,
		processData: false,
		contentType: false,
		data: formData,
		dataType:'json',
        beforeSend: function() {
			progressModal('start');
		},
		error: function() {
			alert('Ajax Error');
			progressModal('stop');
		},
		success: function(response) {
			if(response.status == 1) {
				if(response.error.upload_status == 1) {
					$('#ResponseInput').html(response.msg).show('slow');
					setTimeout(function() {
						$('#ResponseInput').html('');
						$('#ModalAjaxID').modal('hide');
						$('#list').mDatatable('reload');
					}, 1e3);
				}
				else
				{
					$("input[name^='id']").val(response.error.result.id);
					$('#ResponseInput, #ResponseUpload').show('slow');
					$('#ResponseUpload').html(response.error.upload_result);
					$('#list').mDatatable('reload');
				}
			}
			else
			{
				$('#ResponseInput').html(response.msg).show('slow');
				$.each(response.error, function(key, value) {
					$('#'+key).addClass('is-invalid');
					$('#'+key).after(value);
					$('#'+key).parents('.form-group').addClass('has-danger');
				});
			}
		},
		complete : function() {
			progressModal('stop');
		}
	});
}
</script>