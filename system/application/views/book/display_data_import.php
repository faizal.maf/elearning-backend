<style>
.table th, .table td {
    border-top: 1px solid #f4f5f8;
    border-bottom: 1px solid #f4f5f8;
}
table > thead > tr > th, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > td, table > tbody > tr > td, table > tfoot > tr > td {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd !important;
}
.table-list td.force-border, .table-list th.force-border {
    border-top: 1px solid #eee  !important;
}
.table-list thead th {
    /* padding: 10px 20px  !important; */
    vertical-align: middle  !important;
    border-left: 1px solid #eee  !important;
    border-right: 1px solid #eee  !important;
    font-weight: 400  !important;
    background: #f7f7f9  !important;
}
.table-list td {
    font-size: 13px  !important;
	border-left: 1px solid #eee  !important;
	border-right: 1px solid #eee  !important;
}

.contents__box {
	padding: 0.2rem 2.2rem;
}
</style>
<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								<?php echo $title; ?>
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="<?php echo base_url().$controller_name;?>" class="btn btn-secondary m-btn m-btn--icon m-btn--wide btn-sm m-btn--pill">
									<span>
										<i class="la la-arrow-left"></i>
										<span>Kembali ke list regulasi</span>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="alert m-alert m-alert--default" role="alert">
						Jumlah data : <b><?php echo count($dataInfo); ?></b>
					</div>
					<div class="m-section__content">
						<div class="table-responsive">
							<table class="table-list table table-sm table-borderless table-bordered m-table table-hover">
								<thead>
									<tr>
										<th style="text-align: center;width: 30px">No.</th>
										<th style="width: 100px">Kategori ID</th>
										<th>Title</th>
										<th>Tahun Terbit</th>
									</tr>
								</thead>
								<tbody>
									<?php
										$no=1; 
										foreach($dataInfo as $key=>$element) { ?>
									<tr>
										<td><?php echo $no;?></td>
										<td><?php echo $element['id_kategori'];?></td>
										<td><?php echo $element['title'];?></td>
										<td><?php echo $element['tahun_terbit'];?></td>
									</tr>
									<?php $no++; } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>