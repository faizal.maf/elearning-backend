<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
	<div class="row">
		<div class="col-xl-12">
			<!--begin::Portlet-->
			<div class="m-portlet " id="m_portlet">
				<div class="m-portlet__head" style="height: 3rem;">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">Daftar <?php echo $title; ?></h3>
						</div>
					</div>
					<div class="m-portlet__head-tools right">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="m-form m-form--label-align-right">
						<div class="row align-items-center">
							<div class="col-xl-10 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-4">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="m-input-icon m-input-icon--left">
												<input type="text" class="form-control m-input m-input--square" id="generalSearch" placeholder="Pencarian..." autocomplete="off">
												<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
											<label for="f_kategori">Kategori</label>
												<?php echo (isset($kategori) ? $kategori : ''); ?>
											</div>
										</div>
									</div>
									<!-- <div class="col-md-2">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
												<label for="fillStatus">Status Publish</label>
													<?php echo (isset($status) ? $status : ''); ?>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
												<label for="fillTahun">Tahun Terbit</label>
													<?php echo (isset($tahun_terbit) ? $tahun_terbit : ''); ?>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
												<label for="fillDoc">Dokumen</label>
													<?php echo (isset($doc) ? $doc : ''); ?>
											</div>
										</div>
									</div> -->
								</div>
							</div>
						</div>
					</div>
					<!---begin: list -->
					<div id="list"></div>
				</div>
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>

<script type="text/javascript">
var table = {
	init: function() {
		var t;
		t = $("#list").mDatatable({
			<?php echo init_table_list('', $url_tablelist); ?>
			columns: [
				{
					field: "Title",
					title: "Title",
					width: 300
				},
				{
					field: "CatName",
					title: "Kategori",
					width: 120
				}, 
				{
					field: "ThnTerbit",
					title: "Tahun Terbit",
					width: 100
				},
				{
					field: "Date",
					title: "Tanggal Input",
					sortable: "desc",
					attr: {
						nowrap: "nowrap"
					},
					width: 140
				},{
					field: "Status",
					title: "Status Publish",
					width: 70,
					textAlign: "center",
				}, {
					field: "Actions",
					title: "",
					width: 70,
					sortable: !1,
					textAlign: "center",
					overflow: "visible",
					template: function(t, e, a) {
						return '\t\t\t\t\t<a data-toggle="mainmodal" data-modal-type="modal-md" title="Edit <?php echo $title; ?>" id="frmEdit" href="{base_url}<?php echo $controller_name; ?>/form/' + t.RecordID + '" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\t\t\t\t\t\t\t<i class="la la-pencil-square"></i>\t\t\t\t\t\t</a>\t\t\t\t\t<a id="frmDelete" value="' + t.RecordID + '" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\t\t\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t'
					}
				}
			]}),
			$("#f_kategori").on("change", function() {
				t.search($(this).val(), "f_kategori")
			}),
			$("#fillStatus").on("change", function() {
				t.search($(this).val(), "fillStatus")
			}),
			$("#fillTahun").on("change", function() {
				t.search($(this).val(), "fillTahun")
			}),
			$("#fillDoc").on("change", function() {
				t.search($(this).val(), "fillDoc")
			})
	}
};

$(function() {
	table.init();
	$('form').submit(false);
});

$(document).on('click', '.btnPublish', function (e) {
	e.preventDefault();
	var id = $(this).attr('id');
	var id_kat = $(this).attr('id_kat');
	var status;

	if ($(this).hasClass('pubYes')) {
		$(this).addClass('pubNo btn-danger').removeClass('pubYes btn-success').html('<i class="la la-close"></i>No');
		status = 0;
	} else if ($(this).hasClass('pubNo')) {
		$(this).removeClass('pubNo btn-danger').addClass('pubYes btn-success').html('<i class="la la-check"></i>Yes');
		status = 1;
	}
	$.post("<?php echo base_url().$controller_name ?>/set_status", {id: id, id_kat: id_kat, status: status}, function (theResponse) {
		make_toast('success', 'Data has been success change.')
	})
});

$(document).on('click', '#frmDelete', function (e) {
	e.preventDefault();
	var id = $(this).attr('value');
	var trID = $(this).closest('tr').attr("data-row");

	funcDelete("<?php echo base_url().$controller_name ?>/delete", id, trID);
});
</script>