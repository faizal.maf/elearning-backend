<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air alert alert-dismissible m--margin-bottom-30" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation m--font-warning"></i>
		</div>
		<div class="m-alert__text">
		Download sample files : 
          <a href="<?php echo base_url('docs/sample/regulasi/regulasi-sample-xlsx.xlsx'); ?>" class="btn btn-info btn-sm m-btn--wide"><i class="fa fa-file-excel"></i> Sample .XLSX</a>
          <a href="<?php echo base_url('docs/sample/regulasi/regulasi-sample-xls.xls'); ?>" class="btn btn-info btn-sm m-btn--wide"><i class="fa fa-file-excel"></i> Sample .XLS</a>
          <a href="<?php echo base_url('docs/sample/regulasi/regulasi-sample-csv.csv'); ?>" class="btn btn-info btn-sm m-btn--wide" target="_blank"><i class="fa fa-file-csv"></i> Sample .CSV</a>
		<ul class="m--font-accent pt-3">
			<li>Pastikan semua kolom terisi (Mandatory Data).</li>
			<li>Pastikan Kategori ID tidak salah input.</li>
		</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<div class="m-portlet m-portlet--tab">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon m--hide">
								<i class="la la-gear"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Kategori ID Regulasi
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body pt-0">
						<div class="m-section__content pb-0">
							<div class="table-responsive">
								<table class="table table-hover">
									<thead>
										<tr>
											<th style="width: 150px">Kode</th>
											<th>Keterangan</th>
										</tr>
									</thead>
									<tbody>
									<?php foreach($list_regulasi as $data){ ?>
										<tr>
											<td><?php echo $data['id_lookup'] ?></td>
											<td><?php echo $data['options_name'] ?></td>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-8">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--mobile m-portlet--head-md" m-portlet="true" id="m_portlet_tools_form">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								<?php echo $title; ?>
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="<?php echo base_url().$controller_name;?>" class="btn btn-secondary m-btn m-btn--icon m-btn--wide btn-sm m-btn--pill">
									<span>
										<i class="la la-arrow-left"></i>
										<span>Kembali</span>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			<?php 
				echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed myForm', 'id' => 'frmImport', 'autocomplete' =>'off', 'novalidate'=>'novalidate'));
			?>
				<div class="m-portlet__body">
					<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
					<?php echo alert_message('m_error_submit_'.$controller_name, 'alert-danger', $this->config->item('message_error_submit'),'2');?>

					<div class="form-group m-form__group row mb-5 <?php echo form_error('fileURL') ? $this->message_type : '' ?>">
						<label class="col-lg-1 col-form-label">Files:</label>
						<div class="col-lg-11">
							<div class="custom-file">
								<input type="file" id="fileURL" name="fileURL" class="custom-file-input form-control" id="customFile">
								<label class="custom-file-label" for="customFile">Pilih file...</label>
							</div>
							<?php echo form_error('fileURL', '<div class="form-control-feedback">', '</div>'); ?>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-6">
								<button type="submit" class="btn btn-success btn-sm m-btn--wide">Import</button>
								<span class="m--margin-left-10">- <a href="<?php echo base_url().$controller_name; ?>" class="m-link m-link--danger m--font-bold">Batal</a></span>
							</div>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>

<script languange="javascript">
var JSValidateForm = function() {
var e;
	return {
		init: function() {
			e = $("#frmImport").validate({
				rules: {
					fileURL: {required: !0, extension: "xls|xlsx|csv"}
				},
				messages: {
					fileURL: {required: "Wajib diisi!", extension: "Harus .xls, .xlsx, .csv !"}
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

$(function () {
    JSValidateForm.init();
});
</script>