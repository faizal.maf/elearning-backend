<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
	<div class="row">
		<div class="col-xl-12">
			<!--begin::Portlet-->
			<div class="m-portlet " id="m_portlet">
				<div class="m-portlet__body">
				<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
					<div class="m-form m-form--label-align-right">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">
									<div class="col-md-4">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="m-input-icon m-input-icon--left">
												<input type="text" class="form-control form-control-sm m-input m-input--square" id="generalSearch" placeholder="Search on enter.." autocomplete="off">
												<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xl-4 order-1 order-xl-2 m--align-right">
								<a href="<?php echo base_url().$controller_name.'/form' ?>" id="frmAdd" class="btn btn-success btn-sm m-btn m-btn--custom m-btn--icon btnAddNew">
									<span>
										<i class="fa fa-plus"></i>
										<span>Tambah Baru</span>
									</span>
								</a>
								<div class="m-separator m-separator--dashed d-sm-none"></div>
							</div>
						</div>
					</div>
					<hr>

					<!---begin: list -->
					<div id="list"></div>
				</div>
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>
<script type="text/javascript">
var table = {
	init: function () {
		var t;
		t = $("#list").mDatatable({
			<?php echo init_table_list('',$url_tablelist);?>
			columns: [{
				field: "RecordID",
				title: "Level Name",
				sortable: !1,
				filterable: !1,
				template: function (t) {
					var a = mUtil.getRandomInt(0, 7);
					output = '<div class="m-card-user m-card-user--sm">';
					output += '<div class="m-card-user__pic">';
					output += '<div style="width:30px;height:30px;" class="m-card-user__no-photo m--bg-fill-' + ["success", "brand", "danger", "accent", "warning", "metal", "primary", "info"][a] + '">';
					output += '<span style="font-size: 1rem;">' + t.Name.substring(0, 1) + '</span>';
					output += '</div></div>';
					output += '<div class="m-card-user__details">';
					output += '<span class="m-card-user__name">' + t.Name + '</span>';
					output += '</div>';
					output += '</div>';
					return output
				}
			}, {
				field: "UserGroups",
				title: "Total Jabatan",
				sortable: !1,
				textAlign: "center"
			}, {
				field: "Actions",
				title: "Actions",
				sortable: !1,
				width: 70,
				textAlign: "center",
				overflow: "visible",
				template: function (t, e, a) {
					return '\t\t\t\t\t<a id="frmEdit" href="{base_url}<?php echo $controller_name; ?>/form/'+t.RecordID+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\t\t\t\t\t\t\t<i class="la la-pencil-square"></i>\t\t\t\t\t\t</a>\t\t\t\t\t<a id="frmDelete" value="' + t.RecordID + '" href="{base_url}<?php echo $controller_name; ?>/delete/'+t.RecordID+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\t\t\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t'
				}
			}]
		}), $("#fillStatus").on("change", function () {
			t.search($(this).val(), "fillStatus")
		})
	}
}

$(function () {
	table.init();
	$('form').submit(!1)
});
</script>