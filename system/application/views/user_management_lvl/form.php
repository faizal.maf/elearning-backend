<style>
.table th, .table td {
    border-top: 1px solid #f4f5f8;
    border-bottom: 1px solid #f4f5f8;
}
table > thead > tr > th, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > td, table > tbody > tr > td, table > tfoot > tr > td {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd !important;
}
.table-list td.force-border, .table-list th.force-border {
    border-top: 1px solid #eee  !important;
}
.table-list thead th {
    /* padding: 10px 20px  !important; */
    vertical-align: middle  !important;
    border-left: 1px solid #eee  !important;
    border-right: 1px solid #eee  !important;
    font-weight: 400  !important;
    background: #f7f7f9  !important;
}
.table-list td {
    font-size: 13px  !important;
	border-left: 1px solid #eee  !important;
	border-right: 1px solid #eee  !important;
}

.contents__box {
	padding: 0.2rem 2.2rem;
}
</style>

<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--mobile m-portlet--head-md" m-portlet="true" id="m_portlet_tools_form">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon">
								<i class="flaticon-settings"></i>
							</span>
							<h4 class="m-portlet__head-text">
								Settings Level <small>choose menu for grant access</small>
							</h4>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="<?php echo base_url().$controller_name;?>" class="btn btn-secondary m-btn m-btn--icon m-btn--wide btn-sm m-btn--pill">
									<span>
										<i class="la la-arrow-left"></i>
										<span>Back</span>
									</span>
								</a>
							</li>
						</ul>
					</div>
				</div>
			<?php 
				echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--fit m-form--label-align-right', 'id' => 'frmUpload', 'autocomplete' =>'off', 'novalidate'=>'novalidate'));
				echo form_hidden('id', ($result ? $result['id_level'] : ''));
			?>
				<div class="m-portlet__body">
					<!-- < ?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?> -->
					<!-- < ?php echo alert_message('m_error_submit_'.$controller_name, 'alert-danger', $this->config->item('message_error_submit'),'2');?> -->
					<div class="form-group m-form__group row <?php echo form_error('nama') ? $this->message_type : '' ?>">
						<label class="col-lg-2 col-form-label">* Level Name:</label>
						<div class="col-lg-6">
							<?php
								echo form_input(
									array('id' => 'nama',
										'name' => 'nama',
										'value' => set_value('nama', ($result ? $result['nama'] : ''), FALSE),
										'class' => 'form-control m-input m-input--solid',
										'maxlength' => '100',
										'placeholder' => 'Enter Level name',
										($result ? ($result['id_level']=='1' || $result['id_level']=='99') ? 'readonly' : '' : '') => ''
									)
								);
								echo form_error('nama', '<div class="form-control-feedback">', '</div>');
							?>
						</div>
					</div>


					<div class="contents__box contents__box--right">
							<!-- contents__box -->
							<div class="section">
							<?php
								function get_collection_module($parent = '', $id_user='')
								{
									$CI =& get_instance();
									$where = '';
									if(isset($parent) && trim($parent)=='')
									{
										$where = ' AND (T2.parent_id = "0" OR T2.parent_id IS NULL)';
									}
									else
									{
										$where = ' AND T2.parent_id = "'.$parent.'"';
									}
										$select_join = '';
										if(isset($id_user) && trim($id_user)!='')
											$select_join = 'lvlperm.id_level, lvlperm.permission as deskripsi,';
											
										$left_join = '';
										if(isset($id_user) && trim($id_user)!='')
											$left_join = 'LEFT JOIN (SELECT * FROM bsm_apps_level_akses WHERE id_level = "'.$id_user.'") lvlperm on T1.id_menu = lvlperm.id_menu';
										
										$group_join = '';
										if(isset($id_user) && trim($id_user)!='')
											$group_join = 'lvlperm.id_level, lvlperm.permission,';
										
										$sql = "select
												".$select_join." T1.id_menu,T1.deskripsi as akses,T2.deskripsi as Jmenu,
												T2.parent_id 
												from bsm_apps_menu as T1 	
												LEFT JOIN bsm_apps_menu as T2 on T1.id_menu=T2.id_menu
												".$left_join."
												where T2.is_deleted = '0' ".$where." 
												group by ".$group_join." T1.id_menu, T1.deskripsi, T2.deskripsi, T2.parent_id
												order by T2.order_ asc";
									
									$query = $CI->db->query($sql);
									return $query->result_array();
								}
								
								function draw_table_module($rows, $i = '', $level = 0, $totalRows = '')
								{
									$CI =& get_instance();
									$html = '';
									$class = '';
									$first = '';
										
										$html_use_list = '';$last = '';
										if($i == ($totalRows - 1))
											$last = ' last';
										
										$html .= '<tr class="level-'.$level.$first.$last.'">';
										
										$child_uid = $rows['id_menu'];
										$access_desc_W = '';$access_desc_N = '';
										$child = get_collection_module($child_uid);
										if(isset($rows['id_level'])){
											switch($rows['deskripsi'])
											{
												case "W":$access_desc_W = 'checked="checked"';break;
												case "N":$access_desc_N = 'checked="checked"';break;
											}
											$child = get_collection_module($child_uid, $rows['id_level']);
										}
										
										$hasChildren = $child && count($child);
										$separator = '';
										for($s=0;$s<$level;$s++)
											$separator .= '--------';
										
										$module_name = $separator.' '.$rows['Jmenu'];
										// $endpoint = ($level !="0" ? $rows['controller_name'].'/'.$rows['method_name']:'');
										$html .= ($hasChildren)?'<td '.($level =="0" ? 'colspan="3"':'').' ><strong>'.$module_name.'</strong></td>' : '<td>'.$module_name.'</td>';

										// $html .= ($hasChildren)?'<td><strong>'.$module_name.'</strong></td>':'<td>'.$module_name.'</td>';	
											// if($level != "0")
											// {
												// $html .= '<td>'."\n";
												// $html .= $endpoint;
												// $html .= '</td>'."\n";	
												$html .= '<td style="text-align: center; '.($level =="0" && $hasChildren == TRUE  ? 'display:none;' : '').'">'."\n";
												$html .= '<input type="hidden" name="id_menu[]" value="'.$rows['id_menu'].'" />';
												$html .= '<label class="m-radio m-radio--state-success"><input type="radio" name="access_desc['.$rows['id_menu'].']" value="W" id="access_desc_W_'.$rows['id_menu'].'" class="access_desc_W" '.$access_desc_W.'/><span></span></label>';
												$html .= '</td>'."\n";
												$html .= '<td style="text-align: center; '.($level =="0" && $hasChildren == TRUE  ? 'display:none;' : '').'">'."\n";	
												$html .= '<label class="m-radio m-radio--state-danger"><input type="radio" name="access_desc['.$rows['id_menu'].']" value="N" id="access_desc_N_'.$rows['id_menu'].'" class="access_desc_N" '.$access_desc_N.'/><span></span></label>';		
												$html .= '</td>'."\n";
											// }

										$html .= '</tr>'."\n";
										
										
										if ($hasChildren) {
											$htmlChildren = '';
											for($j=0;$j<count($child);$j++){
												$htmlChildren .= draw_table_module($child[$j], $j, $level+1, count($child));
											} 
											if (!empty($htmlChildren)) {
											$html.= $htmlChildren;
											}
										}
										
									return $html;
								}
								$collection = get_collection_module('', (isset($ID_ORI) && trim($ID_ORI) != '') ? $ID_ORI : '');
								$html = '';
							?>
								<div class="table-responsive">
									<table class="table-list table table-sm table-borderless table-bordered m-table table-hover">
										<thead>
											<tr>
												<th class="force-border" style="text-align: center;width: 80%;">LIST MENU APPS</th>
												<th class="force-border" style="text-align: center;"><label class="m-checkbox m-checkbox--state-success"><input type="checkbox" name="access_desc_W" id="access_desc_W" checked=""/> Allow<span></span></label></th>
												<th class="force-border" style="text-align: center;"><label class="m-checkbox m-checkbox--state-danger"><input type="checkbox" name="access_desc_N" id="access_desc_N" checked=""/> Not Allow<span></span></label></th>
											</tr>
										</thead>
										<tbody>
											<?php 
												for($i=0;$i<count($collection);$i++){
													$html .= draw_table_module($collection[$i], $i, 0, count($collection)); 
												}
												echo $html; 
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
				</div>
				<div class="clearfix"></div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions--solid">
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-6">
								<button type="submit" class="btn btn-danger">Submit</button>
								<span class="m--margin-left-10">or <a href="<?php echo base_url().$controller_name; ?>" class="m-link m-link--default m--font-bold">Cancel</a></span>
							</div>
						</div>
					</div>
				</div>
			<?php echo form_close(); ?>
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>

<script languange="javascript">
var JSValidateForm = function() {
var e;
	return {
		init: function() {
			e = $("#frmUpload").validate({
				rules: {
					nama: {required: !0}
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

var MAXLength = {
    init: function() {
		$("#nama").maxlength({
			alwaysShow: !0,
			warningClass: "m-badge m-badge--metal m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide"
		})
	}
};

$(function () {
    JSValidateForm.init();
	MAXLength.init();

	$("#access_desc_W").prop('checked', false);
	$("#access_desc_N").prop('checked', false);
		
	$("#access_desc_W").change(function() {
		if($("#access_desc_W").prop('checked') == true)
		{
			$(".access_desc_W").prop('checked', true);
			$(".access_desc_N").prop('checked', false);
			$("#access_desc_W").prop('checked', true);
			$("#access_desc_N").prop('checked', false);
		}
		else
		{
			$(".access_desc_W").prop('checked', false);
		}
	});

	$("#access_desc_N").change(function() {
		if($("#access_desc_N").prop('checked') == true)
		{
			$(".access_desc_W").prop('checked', false);
			$(".access_desc_N").prop('checked', true);
			$("#access_desc_W").prop('checked', false);
			$("#access_desc_N").prop('checked', true);
		}
		else
		{
			$(".access_desc_N").prop('checked', false);
		}
	});
});
</script>