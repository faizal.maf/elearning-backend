<?php $result = (object)$result;
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--state myForm', 'id' => 'frmUserAdmin', 'novalidate'=>'novalidate'));
	echo form_hidden('id', (isset($result->id_user) ? $result->id_user : ''));
	echo form_hidden('status', (isset($result->status) ? $result->status : ''));
	echo form_hidden('username', (isset($result->username) ? $result->username : ''));
?>
<div class="m-portlet__body">
	<?php if($action == 'insert'){ ?>
	<div class="m-alert m-alert--outline-2x m-alert--square alert alert-info fade show" role="alert">
		<i class="fa fa-info-circle"></i> Untuk akun baru password default sama dengan username.
	</div>
	<hr>
	<?php } ?>
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group">
			<input type="text" id="username" name="username" class="form-control m-input m-input--square" value="<?php echo set_value('username', (isset($result->username) ? $result->username : ''), FALSE) ?>" placeholder="username" autocomplete="off" <?php echo ($action=='insert' ? '' : 'readonly disabled style="cursor:not-allowed"') ?> >
			<label for="username">Username</label>
		</div>
	</div>
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group">
			<input type="text" id="first_name" name="first_name" class="form-control m-input m-input--square" value="<?php echo set_value('first_name', (isset($result->first_name) ? $result->first_name : ''), FALSE) ?>" placeholder="first name" autocomplete="off">
			<label for="first name">First Name</label>
		</div>
	</div>
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group">
			<input type="text" id="last_name" name="last_name" class="form-control m-input m-input--square" value="<?php echo set_value('last_name', (isset($result->last_name) ? $result->last_name : ''), FALSE) ?>" placeholder="last name" autocomplete="off">
			<label for="last name">Last Name</label>
		</div>
	</div>
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group">
			<input type="text" id="email" name="email" class="form-control m-input m-input--square" value="<?php echo set_value('email', (isset($result->email) ? $result->email : ''), FALSE) ?>" placeholder="Email" autocomplete="off">
			<label for="email">Email</label>
		</div>
	</div>
	<div class="form-group m-form__group pt-2 pb-2">
		<div class="form-label-group form-label-group-select">
		<label for="kategori">Groups</label>
			<?php echo form_dropdown('id_level', $groups_list, set_value('id_level', (isset($result->id_level) ? $result->id_level : '')), 'class="custom-select custom-select-label form-control m-input m-input--square" id="id_level"'); ?>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script>
var JSValidation = function() {
	var e;
	return {
		init: function() {
			e = $("#frmUserAdmin").validate({
				ignore: ":hidden",
				rules: {
					username: {required: !0},
					first_name: {required: !0},
					email: {required: !0,email: !0},
					id_level: {required: !0},
				},
				messages: {
					username: {required: "Wajib diisi!"},
					first_name: {required: "Wajib diisi!"},
					email: {required: "Wajib diisi!", email: "Email tidak valid"},
					id_level: {required: "Wajib diisi!"},
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

$(document).ready(function() {
	onDemandScript("<?php echo base_url('public/themes/js/custom.js?v1&current='.date('Ym'))?>");
	JSValidation.init();

	var Btn = "<button type='button' class='btn btn-sm btn-success m-btn--square pull-left' id='btnSubmit'>Submit</button>";
		Btn += "<button type='button' class='btn btn-sm btn-metal m-btn--square' data-dismiss='modal'>Cancel</button>";
	$('#ModalFooterID').html(Btn);

	$("#frmUserAdmin").find('input[type=text]').filter(':visible:first').focus();
	
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		$('#ResponseInput, #ResponseUpload').removeClass('fade show');
		$('#ResponseInput, #ResponseUpload').html('');

		var form = $("#frmUserAdmin").valid();
		if(form === true)
			upsert_data();
	});
});

function upsert_data() {
	var formData = new FormData($("#frmUserAdmin").get(0));
	$.ajax({
		url: $('#frmUserAdmin').attr('action'),
		type: "POST",
		cache: false,
		processData: false,
		contentType: false,
		data: formData,
		dataType:'json',
		success: function(json){
			if(json.status == 1)
			{
				progressModal('start'),
				setTimeout(function() {
					progressModal('stop');
					$('#ResponseInput').html(json.msg).show('slow');
						setTimeout(function() {
							$('#ResponseInput').html('').hide();
							$('#ModalAjaxID').modal('hide');
							$('#list').mDatatable('reload');
						}, 2000);
				}, 1e3);
			}
			else {
				$('#ResponseInput').html(json.msg).show('slow');
				$.each(json.error, function(key, value) {
                    $('#'+key).addClass('is-invalid');
                    $('#'+key).after(value);
                    $('#'+key).parents('.form-group').addClass('has-danger');
                });
			}
		}
	});
}
</script>