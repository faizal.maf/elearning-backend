<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
	<div class="row">
		<div class="col-xl-12">
			<!--begin::Portlet-->
			<div class="m-portlet " id="m_portlet">
				<div class="m-portlet__body">
				<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
					<div class="m-form m-form--label-align-right">
						<div class="row align-items-center">
							<div class="col-xl-4">
								<div class="form-group m-form__group pt-2 pb-2">
									<div class="m-input-icon m-input-icon--left">
										<input type="text" class="form-control m-input m-input--square" id="generalSearch" placeholder="Search on enter.." autocomplete="off">
										<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
									</div>
								</div>
							</div>
							<div class="col-xl-8">
								<div class="form-group m-form__group row align-items-center pt-0 pb-0">
									<div class="col-md-3">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
												<label for="fillStatus">Status Publish</label>
													<?php echo (isset($status) ? $status : ''); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>

					<!---begin: list -->
					<div id="list"></div>
				</div>
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>
<script type="text/javascript">
var table = {
	init: function () {
		var t;
		t = $("#list").mDatatable({
			<?php echo init_table_list('',$url_tablelist);?>
			columns: [{
				field: "RecordID",
				title: "Name",
				sortable: !1,
				filterable: !1,
				width: 200,
				template: function (t) {
					var a = mUtil.getRandomInt(0, 7);
					output = '<div class="m-card-user m-card-user--sm">';
					output += '<div class="m-card-user__pic">';
					output += '<div style="width:30px;height:30px;" class="m-card-user__no-photo m--bg-fill-' + ["success", "brand", "danger", "accent", "warning", "metal", "primary", "info"][a] + '">';
					output += '<span style="font-size: 1rem;">' + t.Name.substring(0, 1) + '</span>';
					output += '</div></div>';
					output += '<div class="m-card-user__details">';
					output += '<span class="m-card-user__name">' + t.Name + '</span>';
					output += '</div>';
					output += '</div>';
					return output
				}
			}, {
				field: "Email",
				title: "Email",
				sortable: !1,
				// width: 95
				template: function(t) {
					return '<a class="m-link m-link--state m-link--info" href="mailto:' + t.Email + '">' + t.Email + "</a>"
				}
			}, {
				field: "UserGroups",
				title: "Groups Name",
				sortable: !1,
				textAlign: "center",
				template: function(t) {
					return '<a class="m-link m-link--state" href="{base_url}setting_groups">' + t.UserGroups + "</a>"
				}
			}, {
				field: "LastActivity",
				title: "Last Login",
				sortable: !1,
				textAlign: "center"
			}, {
				field: "Date",
				title: "Created",
				sortable: "desc",
				width: 140
			}, {
				field: "Status",
				title: "Status",
				sortable: !0,
				width: 75,
				textAlign: "center"
			}, {
				field: "Actions",
				title: "Actions",
				sortable: !1,
				width: 70,
				textAlign: "center",
				overflow: "visible",
				template: function (t, e, a) {
					return '\t\t\t\t\t<a data-toggle="mainmodal" data-modal-type="modal-md" title="Edit <?php echo $title; ?>" id="frmEdit" href="{base_url}<?php echo $controller_name; ?>/form/'+t.RecordID+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\t\t\t\t\t\t\t<i class="la la-pencil-square"></i>\t\t\t\t\t\t</a>\t\t\t\t\t<a id="frmDelete" value="' + t.RecordID + '" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\t\t\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t'
				}
			}]
		}), $("#fillStatus").on("change", function () {
			t.search($(this).val(), "fillStatus")
		})
	}
}

$(function () {
	table.init();
	$('form').submit(!1)
});

$(document).on('click', '.btnPublish', function (e) {
	e.preventDefault();
	var id = $(this).attr('id');
	var status;
	if ($(this).hasClass('pubYes')) {
		$(this).addClass('pubNo btn-danger').removeClass('pubYes btn-success').html('<i class="la la-close"></i>Inactive');
		status = 0;
	} else if ($(this).hasClass('pubNo')) {
		$(this).removeClass('pubNo btn-danger').addClass('pubYes btn-success').html('<i class="la la-check"></i>Active');
		status = 1;
	}
	$.post("<?php echo base_url().$controller_name ?>/set_status", {id: id, status: status}, function (theResponse) {
		make_toast('success', 'Data has been success change.')
	})
});

$(document).on('click', '#frmDelete', function (e) {
	e.preventDefault();
	var id = $(this).attr('value');
	var trID = $(this).closest('tr').attr("data-row");

	funcDelete("<?php echo base_url().$controller_name ?>/delete", id, trID);
});
</script>