<?php 
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--fit m-form--label-align-right', 'id' => 'frmhubungikami', 'autocomplete' =>'off', 'novalidate'=>'novalidate'));
	echo form_hidden('id_alamat', ($result ? $result[0]['id_lookup'] : ''));
	echo form_hidden('id_email', ($result ? $result[1]['id_lookup'] : ''));
	echo form_hidden('id_tlpn', ($result ? $result[2]['id_lookup'] : ''));
	echo form_hidden('id_sosmed', ($result ? $result[3]['id_lookup'] : ''));
?>
		<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
		<?php echo alert_message('m_error_submit_'.$controller_name, 'alert-danger', $this->config->item('message_error_submit'),'2');?>
		<div class="form-group m-form__group row <?php echo form_error('alamat') ? $this->message_type : '' ?>">
			<label class="col-lg-2 col-form-label">* Alamat:</label>
			<div class="col-lg-6">
				<?php 
					echo form_textarea(
						array('id' => 'alamat',
							'name' => 'alamat',
							'value' => set_value('alamat', ($result ? $result[0]['catatan'] : ''), FALSE),
							'class' => 'form-control m-input m-input--solid',
							'maxlength' => '200',
							'style' => 'height:100px',
							'placeholder' => 'Enter alamat'
						)
					);
					echo form_error('alamat', '<div class="form-control-feedback">', '</div>');
				?>
			</div>
		</div>
		<div class="form-group m-form__group row <?php echo form_error('email') ? $this->message_type : '' ?>">
			<label class="col-lg-2 col-form-label">* Email:</label>
			<div class="col-lg-6">
				<?php 
					echo form_input(
						array('id' => 'email',
							'name' => 'email',
							'value' => set_value('email', ($result ? $result[1]['catatan'] : ''), FALSE),
							'class' => 'form-control m-input m-input--solid',
							'maxlength' => '25',
							'placeholder' => 'Enter email'
						)
					);
					echo form_error('email', '<div class="form-control-feedback">', '</div>');
				?>
			</div>
		</div>
		<div class="form-group m-form__group row <?php echo form_error('tlpn') ? $this->message_type : '' ?>">
			<label class="col-lg-2 col-form-label">* Telepon:</label>
			<div class="col-lg-6">
				<?php 
					echo form_input(
						array('id' => 'tlpn',
							'name' => 'tlpn',
							'value' => set_value('tlpn', ($result ? $result[2]['catatan'] : ''), FALSE),
							'class' => 'form-control m-input m-input--solid',
							'maxlength' => '25',
							'placeholder' => 'Enter tlpn'
						)
					);
					echo form_error('tlpn', '<div class="form-control-feedback">', '</div>');
				?>
			</div>
		</div>
		<div class="form-group m-form__group row <?php echo form_error('alamat') ? $this->message_type : '' ?>">
			<label class="col-lg-2 col-form-label">* Social Media:</label>
			<div class="col-lg-6">
			<?php $socmed = json_decode($result[3]['catatan']); 
			?>
			<table>
				  <tr>
					<th style="padding: 20px;">Instagram</th>
					<td><?php 
							echo form_input(
								array('id' => 'socmed1',
									'name' => 'socmed1',
									'value' => set_value('socmed1', ($socmed[0]->title ? $socmed[0]->title : ''), FALSE),
									'class' => 'form-control m-input m-input--solid',
									'maxlength' => '25',
									'placeholder' => 'Enter title socmed'
								)
							);
							echo form_error('socmed1', '<div class="form-control-feedback">', '</div>');
						?>
					</td>
					<td><?php 
							echo form_input(
								array('id' => 'socmed11',
									'name' => 'socmed11',
									'value' => set_value('socmed11', ($socmed[0]->url ? $socmed[0]->url : ''), FALSE),
									'class' => 'form-control m-input m-input--solid',
									'maxlength' => '25',
									'placeholder' => 'Enter URL'
								)
							);
							echo form_error('socmed11', '<div class="form-control-feedback">', '</div>');
						?>
					</td>
				  </tr>
				  <tr>
					<th style="padding: 20px;">Youtube</th>
					<td><?php 
							echo form_input(
								array('id' => 'socmed2',
									'name' => 'socmed2',
									'value' => set_value('socmed2', ($socmed[1]->title ? $socmed[1]->title : ''), FALSE),
									'class' => 'form-control m-input m-input--solid',
									'maxlength' => '25',
									'placeholder' => 'Enter Enter title socmed'
								)
							);
							echo form_error('socmed2', '<div class="form-control-feedback">', '</div>');
						?>
					</td>
					<td><?php 
							echo form_input(
								array('id' => 'socmed22',
									'name' => 'socmed22',
									'value' => set_value('socmed22', ($socmed[1]->url ? $socmed[1]->url : ''), FALSE),
									'class' => 'form-control m-input m-input--solid',
									'maxlength' => '25',
									'placeholder' => 'Enter URL'
								)
							);
							echo form_error('socmed22', '<div class="form-control-feedback">', '</div>');
						?>
					</td>
				  </tr>
				  <tr>
					<th style="padding: 20px;">Linkedin</th>
					<td><?php 
							echo form_input(
								array('id' => 'socmed3',
									'name' => 'socmed3',
									'value' => set_value('socmed3', ($socmed[2]->title ? $socmed[2]->title : ''), FALSE),
									'class' => 'form-control m-input m-input--solid',
									'maxlength' => '25',
									'placeholder' => 'Enter Enter title socmed'
								)
							);
							echo form_error('socmed3', '<div class="form-control-feedback">', '</div>');
						?>
					</td>
					<td><?php 
							echo form_input(
								array('id' => 'socmed33',
									'name' => 'socmed33',
									'value' => set_value('socmed33', ($socmed[2]->url ? $socmed[2]->url : ''), FALSE),
									'class' => 'form-control m-input m-input--solid',
									'maxlength' => '25',
									'placeholder' => 'Enter URL'
								)
							);
							echo form_error('socmed33', '<div class="form-control-feedback">', '</div>');
						?>
					</td>
				  </tr>
				</table>
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-lg-2 col-form-label"></label>
			<div class="col-lg-6">
				<button type="submit" class="btn btn-success m-btn m-btn--sm">Update</button>
			</div>
		</div>
		<div class="clearfix"></div>
<?php echo form_close(); ?>

<script languange="javascript">
var JSValidateForm = function() {
var e;
	return {
		init: function() {
			e = $("#frmhubungikami").validate({
				rules: {
					alamat: {required: !0},
					email: {required: !0},
					tlpn: {required: !0}
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

var MAXLength = {
    init: function() {
		$("#alamat, #email, #tlpn, #sosmed").maxlength({
			alwaysShow: !0,
			warningClass: "m-badge m-badge--metal m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide"
		})
	}
};

$(function () {
    JSValidateForm.init();
	MAXLength.init();
});
</script>
