<?php
	$result = (object)$result;
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--state myForm', 'id' => 'frmUPSERTDATA', 'novalidate'=>'novalidate'));
	echo form_hidden('id', (isset($result->id_lookup) ? $result->id_lookup : ''));
?>
	<div class="m-portlet__body">
		<div class="form-group m-form__group pt-2 pb-2">
			<div class="form-label-group">
				<?php 
					echo form_input(
						array('id' => 'domain',
						'name' => 'domain',
						'value' => set_value('domain', (isset($result->catatan) ? $result->catatan : ''), FALSE),
						'class' => 'form-control m-input m-input--square',
						'placeholder' => 'Domain',
						'maxlength' => '50',
						'autocomplete' => 'off'
						)
					);
				?>
					<label for="domain">Domain</label>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>
<div id="ResponseInput" class="pt-4" style="margin-bottom: -2em;"></div>

<script>
var JSValidation = function() {
var e;
	return {
		init: function() {
			e = $("#frmUPSERTDATA").validate({
				rules: {
					domain: {required: !0}
				},
				messages: {
					domain: {required: "Wajib diisi!"}
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

var MAXLength = {
    init: function() {
		$("#domain").maxlength({
			alwaysShow: !0,
			warningClass: "m-badge m-badge--metal m-badge--rounded m-badge--wide",
			limitReachedClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
			appendToParent: !0
		})
	}
};

$(document).ready(function() {
	JSValidation.init();
	MAXLength.init();

	var Btn = "<button type='button' class='btn btn-sm btn-success m-btn--square pull-left' id='btnSubmit'>Submit</button>";
		Btn += "<button type='button' class='btn btn-sm btn-metal m-btn--square' data-dismiss='modal'>Cancel</button>";
	$('#ModalFooterID').html(Btn);

	$("#frmUPSERTDATA").find('input[type=text],select').filter(':visible:first').focus();
	
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		$('#ResponseInput').hide().html('');

		var form = $("#frmUPSERTDATA").valid();
		if(form === true)
		upsertContrainAttachment();
	});
});

function upsertContrainAttachment()
{
	var formData = new FormData($("#frmUPSERTDATA").get(0));
	$.ajax({
		url: $('#frmUPSERTDATA').attr('action'),
		type: "POST",
		cache: false,
		processData: false,
		contentType: false,
		data: formData,
		dataType:'json',
        beforeSend: function() {
			progressModal('start');
		},
		error: function() {
			alert('Ajax Error');
			progressModal('stop');
		},
		success: function(response) {
			if(response.status == 1) {
				$('#ResponseInput').html(response.msg).show('slow');
				setTimeout(function() {
					$('#ResponseInput').html('');
					$('#ModalAjaxID').modal('hide');
					$('#list').mDatatable('reload');
					location.reload(true);
				}, 1e3);
			}
			else
			{
				$("input[name^='id_lookup']").val(response.error.result.id);
				$('#ResponseInput').show('slow');
				$('#list').mDatatable('reload');
			}
		},
		complete : function() {
			progressModal('stop');
		}
	});
}
</script>