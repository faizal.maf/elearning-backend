<?php 
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--fit m-form--label-align-right', 'id' => 'frmtukarpoin', 'autocomplete' =>'off', 'novalidate'=>'novalidate'));
	echo form_hidden('id', ($result ? $result['id_lookup'] : ''));
?>
		<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
		<?php echo alert_message('m_error_submit_'.$controller_name, 'alert-danger', $this->config->item('message_error_submit'),'2');?>
		<div class="form-group m-form__group row <?php echo form_error('alamat') ? $this->message_type : '' ?>">
			<label class="col-form-label col-lg-2 col-sm-12">* Content Info:</label>
			<div class="col-lg-10 col-md-10 col-sm-12">
				<?php 
					echo form_textarea(
						array('id' => 'info',
							'name' => 'info',
							'value' => set_value('info', ($result ? $result['catatan'] : ''), FALSE),
							'class' => 'form-control m-input m-input--solid summernote',
							'placeholder' => 'Enter info tukar poin'
						)
					);
					echo form_error('info', '<div class="form-control-feedback">', '</div>');
				?>
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-lg-2 col-form-label"></label>
			<div class="col-lg-6">
				<button type="submit" class="btn btn-success m-btn m-btn--sm">Update</button>
			</div>
		</div>
		<div class="clearfix"></div>
<?php echo form_close(); ?>

<script languange="javascript">
var infoBody = {init:function() {
	$("#info").summernote({
		height: 500,
		placeholder: 'Write Content Here ...',
		dialogsInBody: !0,
		callbacks: {
			onImageUpload: function(image) {
				uploadImage("<?php echo base_url(); ?>",image[0]);
			},
			onMediaDelete : function(target) {
				deleteImage("<?php echo base_url(); ?>",target[0].src);
			}
		},
	})
}};

var JSValidateForm = function() {
var e;
	return {
		init: function() {
			e = $("#frmtukarpoin").validate({
				rules: {
					info: {required: !0}
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

$(function () {
    JSValidateForm.init();
    infoBody.init();
});
</script>
