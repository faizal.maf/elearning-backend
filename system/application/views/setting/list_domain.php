<a data-toggle="mainmodal" data-modal-type="modal-md" title="New Whitelist Domain" href="<?php echo base_url('setting/form_whitelist_domain') ?>" id="frmAdd" class="btn btn-success btn-sm m-btn m-btn--custom m-btn--icon btnAddNew">
	<span>
		<i class="fa fa-plus"></i>
		<span>Tambah Baru</span>
	</span>
</a>
<div class="m-separator m-separator--space m-separator--dashed"></div>
<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>Domain</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$i = 1;
			$trows = 0;
			foreach ($list_domain as $key => $val) { ?>
			
		<tr data-row="<?php echo $trows++ ?>">
			<th scope="row"><?php echo $i++ ?></th>
			<td><?php echo $val['catatan'] ?></td>
			<td>
				<a data-toggle="mainmodal" data-modal-type="modal-md" title="Edit Whitelist Domain" id="frmEdit" href="<?php echo base_url().'setting/form_whitelist_domain/'.$val['id_lookup'] ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill"><i class="la la-pencil-square"></i></a>
				<a id="frmDelete" value="<?php echo $val['id_lookup'] ?>" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"><i class="la la-trash"></i></a>
				</td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<script type="text/javascript">

	// ------- deleted.
$(document).on('click', '#frmDelete', function (e) {
	e.preventDefault();
	var id = $(this).attr('value');
	var trID = $(this).closest('tr').attr("data-row");

	funcDelete("<?php echo base_url().$controller_name ?>/delete_whitelist_domain", id, trID);
});
</script>