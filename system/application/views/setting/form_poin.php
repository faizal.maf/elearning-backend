<?php 
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--fit m-form--label-align-right', 'id' => 'frmpoin', 'autocomplete' =>'off', 'novalidate'=>'novalidate'));
	echo form_hidden('id_poin_quiz', ($result ? $result[0]['id_lookup'] : ''));
	echo form_hidden('id_poin_pertanyaan', ($result ? $result[1]['id_lookup'] : ''));
	echo form_hidden('id_poin_gratifikasi', ($result ? $result[2]['id_lookup'] : ''));
?>
	<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
	<?php echo alert_message('m_error_submit_'.$controller_name, 'alert-danger', $this->config->item('message_error_submit'),'2');?>
	<div class="m-form__heading">
		<h3 class="m-form__heading-title">Default Poin :</h3>
	</div>
	<div class="form-group m-form__group row <?php echo form_error('poin_quiz') ? $this->message_type : '' ?>">
		<label class="col-lg-2 col-form-label">* Poin Quiz:</label>
		<div class="col-lg-6">
			<?php 
				echo form_input(
					array('id' => 'poin_quiz',
						'name' => 'poin_quiz',
						'value' => set_value('poin_quiz', ($result ? $result[0]['catatan'] : ''), FALSE),
						'class' => 'form-control m-input m-input--solid',
						'maxlength' => '5',
						'placeholder' => 'Enter poin quiz'
					)
				);
				echo form_error('poin_quiz', '<div class="form-control-feedback">', '</div>');
			?>
		</div>
	</div>
	<div class="form-group m-form__group row <?php echo form_error('poin_pertanyaan') ? $this->message_type : '' ?>">
		<label class="col-lg-2 col-form-label">* Poin Pertanyaan:</label>
		<div class="col-lg-6">
			<?php 
				echo form_input(
					array('id' => 'poin_pertanyaan',
						'name' => 'poin_pertanyaan',
						'value' => set_value('poin_pertanyaan', ($result ? $result[1]['catatan'] : ''), FALSE),
						'class' => 'form-control m-input m-input--solid',
						'maxlength' => '5',
						'placeholder' => 'Enter poin pertanyaan'
					)
				);
				echo form_error('poin_pertanyaan', '<div class="form-control-feedback">', '</div>');
			?>
		</div>
	</div>
	<div class="form-group m-form__group row <?php echo form_error('poin_gratifikasi') ? $this->message_type : '' ?>">
		<label class="col-lg-2 col-form-label">* Poin Gratifikasi:</label>
		<div class="col-lg-6">
			<?php 
				echo form_input(
					array('id' => 'poin_gratifikasi',
						'name' => 'poin_gratifikasi',
						'value' => set_value('poin_gratifikasi', ($result ? $result[2]['catatan'] : ''), FALSE),
						'class' => 'form-control m-input m-input--solid',
						'maxlength' => '5',
						'placeholder' => 'Enter poin gratifikasi'
					)
				);
				echo form_error('poin_gratifikasi', '<div class="form-control-feedback">', '</div>');
			?>
		</div>
	</div>
	<div class="form-group m-form__group row">
		<label class="col-lg-2 col-form-label"></label>
		<div class="col-lg-6">
			<button type="submit" class="btn btn-success m-btn--square m-btn m-btn--sm" style="width: 100%;font-weight: 600;font-size: large;">Update</button>
		</div>
	</div>
	
	<div class="m-form__seperator m-form__seperator--dashed"></div>
	<br></br>
	
	<div class="m-alert m-alert--icon m-alert--air alert-black m-alert--outline alert alert-dismissible m--margin-bottom-30" role="alert">
		<div class="m-alert__icon">
			<i class="flaticon-exclamation m--font-danger"></i>
		</div>
		<div class="m-alert__text">
		RESET POIN SEMUA USER : 
          <a id="frmReset" href="#" class="btn btn-danger btn-sm m-btn--wide"><i class="fa fa-sync"></i> Reset Poin</a>
		<ul class="m--font-danger pt-3">
			<li>Fitur reset poin digunakan untuk mereset kembali poin semua user.</li>
			<li>Fitur reset poin digunakan ketika mengawali awal tahun.</li>
		</ul>
		</div>
	</div>
	
	<div class="clearfix"></div>
<?php echo form_close(); ?>

<script languange="javascript">
var JSValidateForm = function() {
var e;
	return {
		init: function() {
			e = $("#frmpoin").validate({
				rules: {
					poin_quiz: {required: !0, number: !0},
					poin_pertanyaan: {required: !0, number: !0},
					poin_gratifikasi: {required: !0, number: !0}
				},
				messages: {
					poin_quiz: {
						required: "Wajib diisi!",
						number: "Harus diisi angka!",
					},      
					poin_pertanyaan: {
						required: "Wajib diisi!",
						number: "Harus diisi angka!",
					},     
					poin_gratifikasi: {
						required: "Wajib diisi!",
						number: "Harus diisi angka!",
					},
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

var MAXLength = {
    init: function() {
		$("#poin_quiz, #poin_pertanyaan, #poin_gratifikasi").maxlength({
			alwaysShow: !0,
			warningClass: "m-badge m-badge--metal m-badge--rounded m-badge--wide",
            limitReachedClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide"
		})
	}
};

$(function () {
    JSValidateForm.init();
	MAXLength.init();
});

$(document).on('click', '#frmReset', function (e) {
	e.preventDefault();

	funcReset("<?php echo base_url().$controller_name ?>/reset_poin");
});

function funcReset(url, id, trID) {
	swal({
		title: "RESET SEMUA POIN ?",
		text: "Semua poin akan direset kembali menjadi 0 !",
		type: "warning",
		showCancelButton: !0,
		confirmButtonClass: "btn m-btn--square btn-danger",
		confirmButtonText: "Ya, Reset!",
		cancelButtonText: "Batal"
	}).then(function (e) {
		e.value ? swal({
			title: "Berhasil direset!",
			// text: "Your data has been deleted.",
			type: "success",
			timer: 1e3,
			showConfirmButton: !1
		}).then(function (e) {
			$.post(url, function (theResponse) {});
			"timer" === e.dismiss && console.log("I was closed by the timer");
		}) : "cancel" === e.dismiss && swalCancelReset()
	})
}
//------- set message cancel fow swal
function swalCancelReset(){
	swal("Batal Reset", "", "error")
}
</script>
