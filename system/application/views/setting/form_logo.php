<?php 
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--fit m-form--label-align-right', 'id' => 'frmActLogo', 'autocomplete' =>'off', 'novalidate'=>'novalidate'));
	echo form_hidden('id_logo1', ($result ? $result[0]['id_lookup'] : ''));
	echo form_hidden('id_logo2', ($result ? $result[1]['id_lookup'] : ''));
	echo form_hidden('id_logo3', ($result ? $result[2]['id_lookup'] : ''));
?>
	<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
	<?php echo alert_message('m_error_submit_'.$controller_name, 'alert-danger', $this->config->item('message_error_submit'),'2');?>
	
		<div class="m-form__heading">
			<h3 class="m-form__heading-title">Web Admin</h3>
		</div>
		<div class="form-group m-form__group row <?php echo form_error('logo1') ? $this->message_type : '' ?>">
			<label class="col-lg-2 col-form-label">Logo Login</label>
			<div class="col-lg-6">
				<input type="hidden" id="chkimage" value="<?php echo set_value('logo1', ($result ? $result[0]['catatan'] : ''), FALSE); ?>">
				<div class="custom-file">
					<?php 
						echo form_input(
							array('id' => 'logo1',
								'name' => 'logo1',
								'value' => set_value('logo1', ($result ? $result[0]['catatan'] : ''), FALSE),
								'type' => 'file',
								'class' => 'form-control m-input custom-file-input logo1-file-input',
								'accept'=> 'image/png',
								'style' => 'padding: 2px;'
							)
						);
					?>
					<label class="custom-file-label logo1-file-label" for="customFile">Pilih file...</label>
				</div>
				<span class="m-form__help">* Upload <b>PNG</b>,Size <b>(258x127).</b>.</span>
				<div class="pt-2 preview-logo1" style="display:none;width: 60%;" id="divPreviewImage">
					<span class="m--font-boldest">Preview : </span>
					<br>
						<?php 
							$image_properties = array(
								'src'   => set_value('logo1', ($result ? $result[0]['catatan'] : ''), FALSE),
								'id'	=> 'previewImg',
								'class' => 'pt-2',
								'width' => '100%'
							);
							echo img($image_properties);
						?>
				</div>
			</div>
		</div>
		<div class="form-group m-form__group row <?php echo form_error('logo3') ? $this->message_type : '' ?>">
			<label class="col-lg-2 col-form-label">Logo Dashboad</label>
			<div class="col-lg-6">
				<input type="hidden" id="chkimage3" value="<?php echo set_value('logo3', ($result ? $result[2]['catatan'] : ''), FALSE); ?>">
				<div class="custom-file">
					<?php 
						echo form_input(
							array('id' => 'logo3',
								'name' => 'logo3',
								'value' => set_value('logo3', ($result ? $result[2]['catatan'] : ''), FALSE),
								'type' => 'file',
								'class' => 'form-control m-input custom-file-input logo3-file-input',
								'accept'=> 'image/png',
								'style' => 'padding: 2px;'
							)
						);
					?>
					<label class="custom-file-label logo3-file-label" for="customFile">Pilih file...</label>
				</div>
				<span class="m-form__help">* Upload <b>PNG</b>,Size <b>(112x15).</b>.</span>
				<div class="pt-2 preview-logo3" style="display:none;" id="divPreviewImage">
					<span class="m--font-boldest">Preview : </span>
					<br>
						<?php 
							$image_properties = array(
								'src'   => set_value('logo3', ($result ? $result[2]['catatan'] : ''), FALSE),
								'id'	=> 'previewImg3',
								'class' => 'pt-2',
								'width' => '100%'
							);
							echo img($image_properties);
						?>
				</div>
			</div>
		</div>
		<div class="m-form__heading">
			<h3 class="m-form__heading-title">Apps</h3>
		</div>
		<div class="form-group m-form__group row <?php echo form_error('logo2') ? $this->message_type : '' ?>">
			<label class="col-lg-2 col-form-label">Splashscreen</label>
			<div class="col-lg-6">
				<input type="hidden" id="chkimage2" value="<?php echo set_value('logo2', ($result ? $result[1]['catatan'] : ''), FALSE); ?>">
				<div class="custom-file">
					<?php 
						echo form_input(
							array('id' => 'logo2',
								'name' => 'logo2',
								'value' => set_value('logo2', ($result ? $result[1]['catatan'] : ''), FALSE),
								'type' => 'file',
								'class' => 'form-control m-input custom-file-input logo2-file-input',
								'accept'=> 'image/png',
								'style' => 'padding: 2px;'
							)
						);
					?>
					<label class="custom-file-label logo2-file-label" for="customFile">Pilih file...</label>
				</div>
				<span class="m-form__help">* Upload <b>PNG</b>,Ukuran file maksimal <b>5MB</b>.</span>
				<div class="pt-2 preview-logo2" style="display:none;" id="divPreviewImage">
					<span class="m--font-boldest">Preview : </span>
					<br>
						<?php 
							$image_properties = array(
								'src'   => set_value('logo2', ($result ? $result[1]['catatan'] : ''), FALSE),
								'id'	=> 'previewImg2',
								'class' => 'pt-2',
								'width' => '100%'
							);
							echo img($image_properties);
						?>
				</div>
			</div>
		</div>
		<div class="form-group m-form__group row">
			<label class="col-lg-2 col-form-label"></label>
			<div class="col-lg-6">
				<button type="submit" class="btn btn-success m-btn m-btn--sm">Update</button>
			</div>
		</div>
		<div class="clearfix"></div>
<?php echo form_close(); ?>

<script languange="javascript">
var JSValidateForm = function() {
var e;
	return {
		init: function() {
			e = $("#frmActLogo").validate({
				rules: {},
				messages: {
					logo1: "Harus PNG",
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

$(function () {
	onDemandScript("<?php echo base_url('public/themes/js/custom.js?v1&current='.date('Ym'))?>");
    JSValidateForm.init();
	
	//Section logo1
	$(".logo1-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
			$(this).siblings(".logo1-file-input").addClass("selected").html(fileName);
	});

	if($("#chkimage").val() != "") {
		$('.preview-logo1').show("slide");
	};

	$('#frmActLogo input[name=logo1]').on('change', function () {
		const b = 'Pilih file...';
		if( !$(this).val() )
		{
			$(this).next(".logo1-file-input").removeClass("selected").html(b).val('');
			$(this).parents('.form-group').addClass('has-danger');
			$(this).next("div").remove();
			$('.preview-logo1').hide("slide");
			$('#previewImg').attr('src', '#');
		}
		else
		{
			var a=(this.files[0].size); //maxfile 5mb
			if(a > 5242880) {
				alert('Ukuran file lebih dari 5MB!');
					$(this).next(".logo1-file-input").removeClass("selected").html(b).val('');
					$(this).parents('.form-group').addClass('has-danger');
					$(this).next("div").remove();
					$('.preview-logo1').hide("slide");
					$('#previewImg').attr('src', '#');
			}
			else
			{
				readURL(this);
				$(this).parents('.form-group').removeClass('has-danger');
				$(this).next("div").remove();
			}
		}
	});
	
	//Section logo2
	$(".logo2-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
			$(this).siblings(".logo2-file-input").addClass("selected").html(fileName);
	});

	if($("#chkimage2").val() != "") {
		$('.preview-logo2').show("slide");
	};

	$('#frmActLogo input[name=logo2]').on('change', function () {
		const b = 'Pilih file...';
		if( !$(this).val() )
		{
			$(this).next(".logo2-file-input").removeClass("selected").html(b).val('');
			$(this).parents('.form-group').addClass('has-danger');
			$(this).next("div").remove();
			$('.preview-logo2').hide("slide");
			$('#previewImg2').attr('src', '#');
		}
		else
		{
			var a=(this.files[0].size); //maxfile 5mb
			if(a > 5242880) {
				alert('Ukuran file lebih dari 5MB!');
					$(this).next(".logo2-file-input").removeClass("selected").html(b).val('');
					$(this).parents('.form-group').addClass('has-danger');
					$(this).next("div").remove();
					$('.preview-logo2').hide("slide");
					$('#previewImg2').attr('src', '#');
			}
			else
			{
				readURL2(this);
				$(this).parents('.form-group').removeClass('has-danger');
				$(this).next("div").remove();
			}
		}
	});
	
	//Section logo3
	$(".logo3-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
			$(this).siblings(".logo3-file-input").addClass("selected").html(fileName);
	});

	if($("#chkimage3").val() != "") {
		$('.preview-logo3').show("slide");
	};

	$('#frmActLogo input[name=logo3]').on('change', function () {
		const b = 'Pilih file...';
		if( !$(this).val() )
		{
			$(this).next(".logo3-file-input").removeClass("selected").html(b).val('');
			$(this).parents('.form-group').addClass('has-danger');
			$(this).next("div").remove();
			$('.preview-logo3').hide("slide");
			$('#previewImg3').attr('src', '#');
		}
		else
		{
			var a=(this.files[0].size); //maxfile 5mb
			if(a > 5242880) {
				alert('Ukuran file lebih dari 5MB!');
					$(this).next(".logo3-file-input").removeClass("selected").html(b).val('');
					$(this).parents('.form-group').addClass('has-danger');
					$(this).next("div").remove();
					$('.preview-logo3').hide("slide");
					$('#previewImg3').attr('src', '#');
			}
			else
			{
				readURL3(this);
				$(this).parents('.form-group').removeClass('has-danger');
				$(this).next("div").remove();
			}
		}
	});
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('.preview-logo1').show("slide");
			$('#previewImg').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

function readURL2(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('.preview-logo2').show("slide");
			$('#previewImg2').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

function readURL3(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('.preview-logo3').show("slide");
			$('#previewImg3').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}
</script>
