<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">Dashboard</h3>
		</div>
		<div>
			<span class="m-subheader__daterange">
				<span class="m-subheader__daterange-label">
					<span class="m-subheader__daterange-title">Today:</span>
					<span class="m-subheader__daterange-date m--font-brand"><?php echo substr(date("F"),0,3).' '.date("j") ?></span>
				</span>
				<a href="javascript:void(0);" class="btn btn-sm btn-success m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
					<i class="la la-calendar"></i>
				</a>
			</span>
		</div>
	</div>
</div>
<!-- <div class="m-content">
	<div class="row">
			<div class="col-sm-4">
				<div class="card">
				<div class="card-body">
					<h5 class="card-title">Special title treatment</h5>
					<div id="m_chart_kinternal" class="m-widget14__chart1" style="height: 180px">
					</div>
				</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="card">
				<div class="card-body">
					<h5 class="card-title">Special title treatment</h5>
					<p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
					<a href="#" class="btn btn-primary">Go somewhere</a>
				</div>
				</div>
			</div>
	</div>
</div> -->
<div class="m-content">
	<!--Begin::Section-->
	<div class="m-portlet">
		<div class="m-portlet__body  m-portlet__body--no-padding">
			<div class="row m-row--no-padding m-row--col-separator-xl">
				<div class="col-xl-4">
					<!--begin:: Widgets/Regulasi-->
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								Regulasi
							</h3>
							<span class="m-widget14__desc">
								Total by category
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px; margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_regulasi" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
							<div class="col" style="padding: unset;">
								<div class="m-widget14__legends">
									<?php 
									$class = array('Fatwa DSN' => 'success', 'UU' => 'warning', 'BI' => 'primary', 'OJK' => 'danger');								
									foreach($result_regulasi as $result) : ?>
									<div class="m-widget14__legend">
										<span class="m-widget14__legend-bullet m--bg-<?php echo $class[$result['label']];?> "></span>
										<span class="m-widget14__legend-text"><?php echo $result['label']; ?></span>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
					<!--end:: Widgets/Regulasi-->
				</div>
				<div class="col-xl-4">
					<!--begin:: Widgets/Ketentuan Internal-->
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								Ketentuan Internal
							</h3>
							<span class="m-widget14__desc">
								Total by category
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px; margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_kinternal" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
							<div class="col" style="padding: unset;">
								<div class="m-widget14__legends">
									<?php 
									$class = array('Kebijakan' => 'warning', 'Prosedur' => 'info', 'Petunjuk Teknis' => 'primary', 'Opini DPS' => 'success');								
									foreach($result_kinternal as $result) : ?>
									<div class="m-widget14__legend">
										<span class="m-widget14__legend-bullet m--bg-<?php echo $class[$result['label']];?>"></span>
										<span class="m-widget14__legend-text"><?php echo $result['label']; ?></span>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
					<!--end:: Widgets/Regulasi-->
				</div>
				<div class="col-xl-4">
					<!--begin:: Widgets/Literasi-->
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								Literasi
							</h3>
							<span class="m-widget14__desc">
								Total by category
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px; margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_literasi" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
							<div class="col" style="padding: unset;">
								<div class="m-widget14__legends">
									<?php foreach($result_literasi as $result) : ?>
									<div class="m-widget14__legend">
										<span class="m-widget14__legend-bullet m--bg-brand"></span>
										<span class="m-widget14__legend-text"><?php echo $result['label']; ?></span>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
					<!--end:: Widgets/Regulasi-->
				</div>
			</div>
		</div>
	</div>
	<!--End::Section-->
</div>

<script languange="javascript" type="text/javascript">
var Dashboard = {
	init: function() {
		0 != $("#m_chart_regulasi").length && Morris.Donut({
			element: "m_chart_regulasi",
			data: <?php echo $data_regulasi; ?>,
			colors: [mApp.getColor("success"), mApp.getColor("warning"), mApp.getColor("primary"), mApp.getColor("danger")]
		}),
		0 != $("#m_chart_kinternal").length && Morris.Donut({
			element: "m_chart_kinternal",
			data: <?php echo $data_kinternal; ?>,
			colors: [mApp.getColor("warning"), mApp.getColor("accent"), mApp.getColor("primary"), mApp.getColor("success")]
		}),
		0 != $("#m_chart_literasi").length && Morris.Donut({
			element: "m_chart_literasi",
			data: <?php echo $data_literasi; ?>,
			// colors: [mApp.getColor("warning"), mApp.getColor("accent"), mApp.getColor("primary"), mApp.getColor("success")]
		})
	}
};
jQuery(document).ready(function() {
    Dashboard.init()
});
</script>