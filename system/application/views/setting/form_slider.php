<?php 
	$result = (object)$result;
	$detail = (isset($result->detail) ? (object)$result->detail : '' );
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--state myForm', 'id' => 'frmUPSERTDATA', 'novalidate'=>'novalidate'));
	echo form_hidden('id', (isset($result->id) ? $result->id : ''));
?>
	<div class="m-portlet__body">
		<div class="form-group m-form__group pt-2 pb-2">
			<div class="form-label-group">
				<?php 
					echo form_input(
						array('id' => 'title',
						'name' => 'title',
						'value' => set_value('title', (isset($result->title) ? $result->title : ''), FALSE),
						'class' => 'form-control m-input m-input--square',
						'placeholder' => 'Title',
						'maxlength' => '150',
						'autocomplete' => 'off'
						)
					);
				?>
					<label for="title">Title</label>
			</div>
		</div>
		<div class="form-group m-form__group row pt-2 pb-2">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="form-label-group">
					<?php 
						echo form_textarea(
							array('id' => 'description',
								'name' => 'description',
								'value' => set_value('description', (isset($result->description) ? $result->description : ''), FALSE),
								'type' => 'textarea',
								'rows' => '2',
								'maxlength' => '250',
								'placeholder' => 'description',
								'class' => 'form-control m-input m-input--square'
							)
						);
					?>
					<label for="description">Keterangan</label>
				</div>
			</div>
		</div>
		<div class="form-group m-form__group pt-2 pb-2">
			<div class="m-checkbox-inline">
				<label class="m-checkbox">
					<?php 
						echo form_checkbox(
							array(
								'name'          => 'is_hyperlink',
								'id'            => 'is_hyperlink',
								'value'         => set_value('is_hyperlink', (isset($result->is_hyperlink) ? $result->is_hyperlink : 0), FALSE),
								'checked'       => (isset($result->is_hyperlink) && $result->is_hyperlink == 1 ? TRUE : FALSE)
							)
						);
					?>
					Hyperlink
					<span></span>
				</label>
			</div>
		</div>
		<div class="zxc" style="background: white;border: 4px solid #f7f7fa;padding: 0px 4px 0px 4px;display:none;">
			<div class="form-group m-form__group pt-2 pb-2">
				<div class="form-label-group form-label-group-select">
				<label for="kategori">Category</label>
					<?php echo form_dropdown('content_category', $list_kategori, set_value('content_category', (isset($result->content_category) ? $result->content_category : '')), 'class="custom-select custom-select-label form-control m-input m-input--square" id="content_category"'); ?>
				</div>
			</div>
			<div class="form-group m-form__group row pt-2 pb-2 autosuggest" style="display:none;">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<select style="width: 100%" class="form-control m-select2" id="content_id" name="content_id">
						<option value="<?php echo (isset($result->content_id) ? $result->content_id : ''); ?>" title="<?php echo (isset($detail->title) ? $detail->title : ''); ?>" selected><?php echo (isset($detail->title) ? $detail->title : ''); ?></option>
					</select>
				</div>
			</div>
			<div class="form-group m-form__group pt-2 pb-2 otherlink" style="display:none;">
				<div class="form-label-group">
					<?php 
						echo form_input(
							array('id' => 'content_url',
							'name' => 'content_url',
							'value' => set_value('content_url', (isset($result->content_url) ? $result->content_url : ''), FALSE),
							'class' => 'form-control m-input m-input--square',
							'placeholder' => 'Other link',
							'maxlength' => '150',
							'autocomplete' => 'off'
							)
						);
					?>
						<label for="content_url">URL</label>
				</div>
			</div>
		</div>
		
		<div class="form-group m-form__group row pt-2 pb-2">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<input type="hidden" id="chkimage" value="<?php echo set_value('image', (isset($result->image) ? $result->image : ''), FALSE); ?>">
				<div class="custom-file">
					<?php 
						echo form_input(
							array('id' => 'image',
								'name' => 'image',
								'value' => set_value('image', (isset($result->image) ? $result->image : ''), FALSE),
								'type' => 'file',
								'class' => 'form-control m-input custom-file-input',
								'accept'=> 'image/*',
								'style' => 'padding: 2px;'
							)
						);
					?>
					<label class="custom-file-label" for="customFile">Pilih file...</label>
				</div>
				<span class="m-form__help">* Upload <b>JPG,JPEG,PNG</b>,Ukuran file maksimal <b>5MB</b>.</span>
				<div class="pt-2" style="display:none;" id="divPreviewImage">
					<span class="m--font-boldest">Preview : </span>
					<br>
						<?php 
							$image_properties = array(
								'src'   => set_value('image', (isset($result->image) ? $result->image : ''), FALSE),
								'id'	=> 'previewImg',
								'class' => 'pt-2',
								'width' => '100px'
							);
							echo img($image_properties);
						?>
				</div>
			</div>
		</div>
		<hr>
		<b class="m--font-danger m--regular-font-size-sm1">Format size slider/banner :</b>
		<ul class="m--font-danger m--regular-font-size-sm7">
			<li>1600x900 (Width x Height)</li>
		</ul>
	</div>
<?php echo form_close(); ?>
<div id="ResponseInput" class="pt-4" style="margin-bottom: -2em;"></div>
<div id="ResponseUpload" class="pt-4" style="margin-bottom: -2em;"></div>

<script type="text/javascript">
	var MAXLength = {
		init: function() {
			$("#title").maxlength({
				alwaysShow: !0,
				warningClass: "m-badge m-badge--metal m-badge--rounded m-badge--wide",
				limitReachedClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
				appendToParent: !0
			})
		}
	};
	
	var JSValidation = function() {
		var e;
		return {
			init: function() {
				e = $("#frmUPSERTDATA").validate({
					rules: {
						title: {
							required: !0
						}
					},
					messages: {
						title: {required: "Wajib diisi!"},
						image: "Harus JPG,JPEG,PNG",
					},
					invalidHandler: function(e, r) {
						$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
						mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
					},
					submitHandler: function(e) {
						return !0
					}
				})
			}
		}
	}();

	$(document).ready(function() {
		onDemandScript("<?php echo base_url('public/themes/js/custom.js?v1&current='.date('Ym'))?>");
		JSValidation.init();
		MAXLength.init();

		var catValue = $('#content_category').val();
		if(catValue == "regulasi" || catValue == "ketentuan_internal" || catValue == "erm" || catValue == "apuppt" || catValue == "infografis" || catValue == "video" || catValue == "artikel" || catValue == "news" ) {
			$('.autosuggest').show(500);
			$('.otherlink').hide(500);
		} else if(catValue == "other_link") {
			$('.autosuggest').hide(500);
			$('.otherlink').show(500);
		} else {
			$('.autosuggest').hide(500);
			$('.otherlink').hide(500);
		}

		if ($("#is_hyperlink").is(':checked')){
			$(".zxc").show(200);
		}
		
		// if($('#is_hyperlink').val() == "1")
			// $('.zxc').show(500);

		$('#is_hyperlink').click(function() {
			if(this.checked) {
				$('.zxc').show(500);
				$('#is_hyperlink').val(1);
			}else{
				$('.zxc').hide(500);
				$('#is_hyperlink').val(0);
			}
		});
		
		$("#content_id").select2({
			dropdownParent: $("#ModalAjaxID"),
			placeholder: "Search for content here",
				allowClear: !0,
				debug: !0,
				ajax: {
					url: "<?php echo base_url('setting/hyperlinkSearchContent') ?>",
					dataType: "json",
					delay: 250,
					data: function(params) {
						return {
							q: params.term,
							cat: $('#content_category').val(),
							page: params.page
						}
					},
					processResults: function(data, params) {
						return params.page = params.page || 1,
						{
							results: data.items,
							pagination: {
								more: 30 * params.page < data.total_count
							}
						}
					},
					cache: !0
				},
				escapeMarkup: function(markup) {
					// console.log("MARKUP :" + markup);
					return markup
				},
				minimumInputLength: 3,
				templateResult: formatRepo,
				templateSelection: formatRepoSelection
			});
			// .on("change", function(e) {
				// console.log($('#content_category').val());
			//	// console.log("SELECTED ID: "+ e.id));
			//	// $('#content_id').val(e.id);
			//	// $('#content_id_updated').val(e.id);
			// });

		$(".custom-file-input").on("change", function() {
			var fileName = $(this).val().split("\\").pop();
				$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});

		if($("#chkimage").val() != "") {
			$('#divPreviewImage').show("slide");
		};

		$('#frmUPSERTDATA input[type=file]').on('change', function () {
			const b = 'Pilih file...';
			if( !$(this).val() )
			{
				$(this).next(".custom-file-label").removeClass("selected").html(b).val('');
				$(this).parents('.form-group').addClass('has-danger');
				$(this).next("div").remove();
				$('#divPreviewImage').hide("slide");
				$('#previewImg').attr('src', '#');
			}
			else
			{
				var a=(this.files[0].size); //maxfile 5mb
				if(a > 5242880) {
					alert('Ukuran file lebih dari 5MB!');
						$(this).next(".custom-file-label").removeClass("selected").html(b).val('');
						$(this).parents('.form-group').addClass('has-danger');
						$(this).next("div").remove();
						$('#divPreviewImage').hide("slide");
						$('#previewImg').attr('src', '#');
				}
				else
				{
					readURL(this);
					$(this).parents('.form-group').removeClass('has-danger');
					$(this).next("div").remove();
				}
			}
		});

		var Btn = "<button type='button' class='btn btn-sm btn-success m-btn--square pull-left' id='btnSubmit'>Submit</button>";
			Btn += "<button type='button' class='btn btn-sm btn-metal m-btn--square' data-dismiss='modal'>Cancel</button>";
		$('#ModalFooterID').html(Btn);

		$("#frmUPSERTDATA").find('input[type=text],textarea, input[type=file]').filter(':visible:first').focus();
		
		$('#btnSubmit').click(function(e){
			e.preventDefault();
			$('#ResponseInput, #ResponseUpload').removeClass('fade show');
			$('#ResponseInput, #ResponseUpload').html('');

			var form = $("#frmUPSERTDATA").valid();
			if(form === true)
				upsert_data();
		});
	});

	$('#content_category').change(function() {
		if($(this).val() == "regulasi" || $(this).val() == "ketentuan_internal" || $(this).val() == "erm" || 
			$(this).val() == "apuppt" || $(this).val() == "video" || $(this).val() == "infografis" || $(this).val() == "artikel" || $(this).val() == "news" ) {
			$('.autosuggest').show(500);
			$('.otherlink').hide(500);
			$('#content_id').val(null).trigger('change');
			$('#content_url').val('').trigger('change');
		} else if($(this).val() == "other_link") {
			$('.autosuggest').hide(500);
			$('.otherlink').show(500);
			$('#content_id').val(null).trigger('change');
		} else {
			$('#content_id').val(null).trigger('change');
			$('#content_url').val('').trigger('change');
			
			$('.autosuggest').hide(500);
			$('.otherlink').hide(500);
		}
	});

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#divPreviewImage').show("slide");
				$('#previewImg').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	function upsert_data() {
		var formData = new FormData($("#frmUPSERTDATA").get(0));
		$.ajax({
			url: $('#frmUPSERTDATA').attr('action'),
			type: "POST",
			cache: false,
			processData: false,
			contentType: false,
			data: formData,
			dataType:'json',
			beforeSend: function() {
				progressModal('start');
			},
			error: function() {
				alert('Ajax Error');
				progressModal('stop');
			},
			success: function(response) {
				if(response.status == 1) {
					if(response.error.upload_status == 1) {
						$('#ResponseInput').html(response.msg).show('slow');
						setTimeout(function() {
							$('#ResponseInput').html('');
							$('#ModalAjaxID').modal('hide');
							$('#list').mDatatable('reload');
						}, 1e3);
					}
					else
					{
						$("input[name^='id']").val(response.error.result.id);
						$('#ResponseInput, #ResponseUpload').show('slow');
						$('#ResponseUpload').html(response.error.upload_result);
						$('#list').mDatatable('reload');
					}
				}
				else
				{
					$('#ResponseInput').html(response.msg).show('slow');
					$.each(response.error, function(key, value) {
						$('#'+key).addClass('is-invalid');
						$('#'+key).after(value);
						$('#'+key).parents('.form-group').addClass('has-danger');
					});
				}
			},
			complete : function() {
				progressModal('stop');
			}
		});
	}
	
	function formatRepo(repo) {
		// console.log(repo);
		var catValue = $('#content_category').val();
		if (repo.loading) return repo.text;
		var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__meta'>" +
			"<div class='select2-result-repository__title'>Title : " + repo.title + "</div>";
			
		if(catValue == "regulasi" || catValue == "ketentuan_internal" || catValue == "erm") {
			
			return repo.kategori_hyperlink && (markup += "<div class='select2-result-repository__description'>Kategori : <b>" + repo.kategori_hyperlink + " - " + repo.kategori + "</b></div>"),
				markup += "<div class='select2-result-repository__description'>Tahun terbit : <b>" + repo.tahun_terbit + "</b></div>",
				markup += "<div class='select2-result-repository__description'>Tanggal input : <b>" + repo.created + "</b></div>";
		}else{
			
			return repo.kategori_hyperlink && (markup += "<div class='select2-result-repository__description'>Kategori : <b>" + repo.kategori_hyperlink + " - " + repo.kategori + "</b></div>"),
				markup += "<div class='select2-result-repository__description'>Tanggal input : <b>" + repo.created + "</b></div>";
		}
		
		return markup;
	}

	function formatRepoSelection(repo) {
		var catValue = $('#content_category').val();
		
		if(catValue == "regulasi" || catValue == "ketentuan_internal" || catValue == "erm" || catValue == "infografis" || catValue == "apuppt") {
			$('#content_url').val(repo.content_url);
		}
		return repo.title;
	}

</script>