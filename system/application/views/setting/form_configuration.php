<style type="text/css">
	.tab-content .tab-pane {    
    position: relative;
}
</style>
<!--<div class="m-subheader">
	< ?php echo $breadcrumbs; ?>
</div>-->
<div class="m-content">
	<div class="row">
		<div class="col-lg-12">
			<!--begin::Portlet-->
			<div class="m-portlet m-portlet--mobile m-portlet--head-md" m-portlet="true" id="m_portlet_tools_form">
				<div class="m-portlet__body">
					<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a href="<?php echo base_url('setting/hubungikami') ?>" class="nav-link active" data-toggle="tabajax" data-target="#hubungikami">Hubungi Kami</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('setting/infotukarpoin') ?>" class="nav-link" data-toggle="tabajax" data-target="#infotukarpoin">Info Tukar Poin</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('setting/poin') ?>" class="nav-link" data-toggle="tabajax" data-target="#poin">Poin</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('setting/whitelist_domain') ?>" class="nav-link" data-toggle="tabajax" data-target="#whitelist_domain">Whitelist Domain</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('setting/logo') ?>" class="nav-link" data-toggle="tabajax" data-target="#whitelist_domain">Logo</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="hubungikami" role="tabpanel"></div>
						<div class="tab-pane" id="infotukarpoin" role="tabpanel"></div>
						<div class="tab-pane" id="poin" role="tabpanel"></div>
						<div class="tab-pane" id="whitelist_domain" role="tabpanel"></div>
						<div class="tab-pane" id="logo" role="tabpanel"></div>
					</div>
				</div>	
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>
<script languange="javascript">
	$(document).ready(function(){
		var defaultTab = $('[data-toggle="tabajax"]');
		activaTab(defaultTab);
	});

	$('[data-toggle="tabajax"]').click(function(e) {
		var $this = $(this),
			loadurl = $this.attr('href'),
			targ = $this.attr('data-target');

		$.get(loadurl, function(data) {
			$(targ).html(data);
		});

		$this.tab('show');
		return false;
	});
	
	function activaTab(thisTab){
		loadurl = thisTab.attr('href'),
		targ = thisTab.attr('data-target');
			
		$.get(loadurl, function(data) {
			$(targ).html(data);
		});
		
		$('.nav-tabs a[href="#hubungikami"]').tab('show');

	};
</script>
