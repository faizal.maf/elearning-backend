<div class="m-stack__item m-brand m-brand--skin-light ">
    <div class="m-stack m-stack--ver m-stack--general">
        <div class="m-stack__item m-stack__item--middle m-brand__logo">
            <a href="?page=index" class="m-brand__logo-wrapper">
			<?php 
			$this->db->select('catatan')->from('bsm_config')->where('id_lookup', 'LOGO3');
			$query = $this->db->get()->row();
			?>
                <img alt=" " style="width: 110px;height: 50px;margin-left: 30px;" src="<?php echo $query->catatan; ?>"/>
            </a>
        </div>
        <div class="m-stack__item m-stack__item--middle m-brand__tools">
            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block"><span></span></a>
            <a href="javascript:;" id="m_aside_left_hide_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--hidden-tablet-and-mobile"><span></span></a>
            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block"><i class="flaticon-more"></i></a>
        </div>
    </div>
</div>