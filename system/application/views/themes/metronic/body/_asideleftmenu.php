<button class="m-aside-left-close m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item m-aside-left m-aside-left--skin-light ">
    <div id="m_ver_menu" class="m-aside-menu m-aside-menu--skin-light m-aside-menu--submenu-skin-light " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">		
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow " id="sidebarnav">
			<li class="m-menu__item" aria-haspopup="true" >
                <a  href="<?php echo base_url().'welcome' ?>" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-analytics"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">Dashboard</span>
                        </span>
                    </span>
                </a>
            </li>
			<li class="m-menu__section ">
                <h4 class="m-menu__section-text">NAVIGATION</h4>
                <i class="m-menu__section-icon flaticon-more-v2"></i>
            </li>
<?php
function build_menu ($menus = array()) 
{
	// Check if current level is parent
	$current_line = isset($menus[0]) ? $menus[0] : array();
	$root_level = (get_value($current_line, 'id_menu_parent') <= 0) ? true : false;
	if( count($menus) > 0 ) {
		foreach($menus as $menu) {
			// Counting children menu
			$count_menu_children = count(get_value($menu, 'children'));
			$parent_id = get_value($menu, 'id_menu_parent');

			// Build a link
			$link = tag_replace(get_value($menu, 'url'));
			$target = (get_value($menu, 'target') != '') ? ' target="' . tag_replace(get_value($menu, 'target')) . '" ' : '';
			$label = get_value($menu, 'deskripsi');
			$img = get_value($menu, 'icon');

			// Build a single line menu
			$class_span = $root_level ? 'hide-menu' : '';
			if($count_menu_children > 0) {
				$class = $root_level ? 'has-arrow' : 'has-arrow';
				$class_span = $root_level ? 'hide-menu' : '';
?>
				<li class="m-menu__item  m-menu__item--submenu " aria-haspopup="true"  m-menu-submenu-toggle="hover">
					<a  href="<?php echo $link ?>" class="m-menu__link m-menu__toggle">
						<i class="m-menu__link-icon <?php echo $img; ?> "></i>
						<span class="m-menu__link-text"><?php echo $label ?></span>
							<i class="m-menu__ver-arrow la la-angle-right"></i>
					</a>
					<div class="m-menu__submenu "><span class="m-menu__arrow"></span>
						<ul class="m-menu__subnav">
							<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
								<span class="m-menu__link"><span class="m-menu__link-text"><?php echo $label ?></span></span>
							</li>
							<?php build_menu(get_value($menu, 'children')) ?>
						</ul>
					</div>
				</li>
			<?php
			} elseif($count_menu_children == 0 && $parent_id == '0') {
			?>
				<li class="m-menu__item " aria-haspopup="true">
					<a href="<?php echo $link ?>" <?php echo $target ?> class="m-menu__link ">
						<i class="m-menu__link-icon <?php echo $img; ?>"></i>
						<span class="m-menu__link-title">
							<span class="m-menu__link-wrap">
								<span class="m-menu__link-text"><?php echo $label ?></span>
							</span>
						</span>
					</a>
				</li>
			<?php
			}else{
			?>
				<li class="m-menu__item " aria-haspopup="true" >
                    <a href="<?php echo $link ?>" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text"><?php echo $label ?></span>
                    </a>
                </li>
			<?php 
			}
		}
	}
}
build_menu($menus);
?>
        </ul>
    </div>
</div>