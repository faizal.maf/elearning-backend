<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo $app_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script type="text/javascript">WebFont.load({google:{"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},active:function(){sessionStorage.fonts=true}});</script>
	<base href="<?php echo $base_url; ?>">
	<link href="<?php echo $favicon; ?>" rel="icon" type="image/png">
	<?php echo $css_files; ?>
	<?php echo $js_files; ?>
</head>
<script type="text/javascript">
	// $.ajaxSetup({
	// 	cache: !1
	// }), modalfunc();
jQuery(document).ready(function() {

	// function modalfunc() {
		// $('[data-toggle="mainmodal"]').bind("click", function (e) {
		// 	e.preventDefault();
		// 	var t = $(this).attr("href");
		// 	var c = $(this).attr("title");
		// 	var m = $(this).attr("data-modal-type");
		// 	0 === t.indexOf("#") ? $("#ModalAjaxID").modal("show") : $.get(t, function (e) {
		// 		$('.modal-dialog').addClass(m), $('#ModalHeaderID').html(c), $('#ModalBodyID').html(e), $('#ModalAjaxID').modal('show')
		// 	})
		// }), 
		$(document).on("click", '[data-toggle="mainmodal"]', function (e) {
			e.preventDefault();
			var t = $(this).attr("href");
			var c = $(this).attr("title");
			var m = $(this).attr("data-modal-type");
			0 === t.indexOf("#") ? $("#ModalAjaxID").modal("show") : $.get(t, function (e) {
				$('.modal-dialog').addClass(m), $('#ModalHeaderID').html(c), $('#ModalBodyID').html(e), $('#ModalAjaxID').modal('show')
			})
		});
	// }
})
</script>
<body class="<?php echo $body_class; ?>">
	<?php echo $body; ?>

	<div class="modal fade" id="ModalAjaxID" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-labelledby="ajaxModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="ModalHeaderID"></h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body" id="ModalBodyID"></div>
				<div id="ResponseInput" style="display:none;padding: 0px 15px 0px 15px;"></div>
				<div id="ResponseUpload" style="display:none;padding: 0px 15px 0px 15px;"></div>
				<div class="modal-footer" id="ModalFooterID">
					<button type="button" class="btn btn-sm btn-success m-btn--square pull-left" id="btnSubmit">Submit</button>
					<button type="button" class="btn btn-sm btn-metal m-btn--square" data-dismiss="modal" id="btnCancel">Cancel</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('#ModalAjaxID').on('hide.bs.modal',function(){setTimeout(function(){$('#ModalHeaderID, #ModalBodyID, #ModalFooterID').html('')},500)});
	</script>

	<div id="m_scroll_top" class="m-scroll-top">
		<i class="la la-arrow-up"></i>
	</div>
</body>
<!--[if lt IE 9]> <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script> <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
</html>