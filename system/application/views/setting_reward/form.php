<?php $result = (object)$result;
	echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--state myForm', 'id' => 'frmUPSERTDATA', 'novalidate'=>'novalidate'));
	echo form_hidden('id', (isset($result->id) ? $result->id : ''));
?>
	<div class="m-portlet__body">
		<div class="form-group m-form__group pt-2 pb-2">
			<div class="form-label-group">
				<?php 
					echo form_input(
						array('id' => 'poin',
						'name' => 'poin',
						'value' => set_value('poin', (isset($result->poin) ? $result->poin : ''), FALSE),
						'class' => 'form-control m-input m-input--square',
						'placeholder' => 'poin',
						'maxlength' => '10',
						'autocomplete' => 'off'
						)
					);
				?>
				<label for="poin">poin</label>
			</div>
		</div>
		<div class="form-group m-form__group row pt-2 pb-2">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="form-label-group">
					<?php 
						echo form_textarea(
							array('id' => 'description',
								'name' => 'description',
								'value' => set_value('description', (isset($result->description) ? $result->description : ''), FALSE),
								'type' => 'textarea',
								'rows' => '2',
								'maxlength' => '250',
								'placeholder' => 'description',
								'class' => 'form-control m-input m-input--square'
							)
						);
					?>
					<label for="description">Keterangan</label>
				</div>
			</div>
		</div>
		<div class="form-group m-form__group row pt-2 pb-2">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<input type="hidden" id="chkimage" value="<?php echo set_value('image', (isset($result->image) ? $result->image : ''), FALSE); ?>">
				<div class="custom-file">
					<?php 
						echo form_input(
							array('id' => 'image',
								'name' => 'image',
								'value' => set_value('image', (isset($result->image) ? $result->image : ''), FALSE),
								'type' => 'file',
								'class' => 'form-control m-input custom-file-input',
								'accept'=> 'image/*',
								'style' => 'padding: 2px;'
							)
						);
					?>
					<label class="custom-file-label" for="customFile">Pilih file...</label>
				</div>
				<span class="m-form__help">* Upload <b>JPG,JPEG,PNG</b>,Ukuran file maksimal <b>5MB</b>.</span>
				<div class="pt-2" style="display:none;" id="divPreviewImage">
					<span class="m--font-boldest">Preview : </span>
					<br>
						<?php 
							$image_properties = array(
								'src'   => set_value('image', (isset($result->image) ? $result->image : ''), FALSE),
								'id'	=> 'previewImg',
								'class' => 'pt-2',
								'width' => '100px'
							);
							echo img($image_properties);
						?>
				</div>
			</div>
		</div>	
	</div>
<?php echo form_close(); ?>
<div id="ResponseInput" class="pt-4" style="margin-bottom: -2em;"></div>
<div id="ResponseUpload" class="pt-4" style="margin-bottom: -2em;"></div>

<script>
var JSValidation = function() {
	var e;
	return {
		init: function() {
			e = $("#frmUPSERTDATA").validate({
				rules: {
					poin: {required: !0, number: !0},
					description: {required: !0}
				},
				messages: {
					poin: {
						required: "Wajib diisi!", 
						number: "Harus diisi angka!"
					},
					description: {
						required: "Wajib diisi!",
					},
				},
				invalidHandler: function(e, r) {
					$("#m_error_submit_<?php echo $controller_name ?>").removeClass("m--hide").show(),
					mUtil.scrollTo("m_error_submit_<?php echo $controller_name; ?>", -200)
				},
				submitHandler: function(e) {
					return !0
				}
			})
		}
	}
}();

var MAXLength = {
    init: function() {
		$("#poin, #description").maxlength({
			alwaysShow: !0,
			warningClass: "m-badge m-badge--metal m-badge--rounded m-badge--wide",
			limitReachedClass: "m-badge m-badge--danger m-badge--rounded m-badge--wide",
			appendToParent: !0
		})
	}
};

$(document).ready(function() {
	JSValidation.init();
	MAXLength.init();

	$(".custom-file-input").on("change", function() {
  		var fileName = $(this).val().split("\\").pop();
			$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});

	if($("#chkimage").val() != "") {
        $('#divPreviewImage').show("slide");
	};

	$('#frmUPSERTDATA input[type=file]').on('change', function () {
		const b = 'Pilih file...';
		if( !$(this).val() )
		{
			$(this).next(".custom-file-label").removeClass("selected").html(b).val('');
			$(this).parents('.form-group').addClass('has-danger');
			$(this).next("div").remove();
			$('#divPreviewImage').hide("slide");
			$('#previewImg').attr('src', '#');
		}
		else
		{
        	var a=(this.files[0].size); //maxfile 5mb
			if(a > 5242880) {
				alert('Ukuran file lebih dari 5MB!');
					$(this).next(".custom-file-label").removeClass("selected").html(b).val('');
					$(this).parents('.form-group').addClass('has-danger');
					$(this).next("div").remove();
					$('#divPreviewImage').hide("slide");
					$('#previewImg').attr('src', '#');
			}
			else
			{
				readURL(this);
				$(this).parents('.form-group').removeClass('has-danger');
				$(this).next("div").remove();
			}
		}
	});

	var Btn = "<button type='button' class='btn btn-sm btn-success m-btn--square  pull-left' id='btnSubmit'>Submit</button>";
		Btn += "<button type='button' class='btn btn-sm btn-metal m-btn--square' data-dismiss='modal'>Cancel</button>";
	$('#ModalFooterID').html(Btn);

	$("#frmUPSERTDATA").find('input[type=text],textarea, input[type=file]').filter(':visible:first').focus();
	
	$('#btnSubmit').click(function(e){
		e.preventDefault();
		$('#ResponseInput, #ResponseUpload').removeClass('fade show');
		$('#ResponseInput, #ResponseUpload').html('');

		var form = $("#frmUPSERTDATA").valid();
		if(form === true)
			upsert_data();
	});
});

function readURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$('#divPreviewImage').show("slide");
			$('#previewImg').attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]);
	}
}

function upsert_data() {
	var formData = new FormData($("#frmUPSERTDATA").get(0));
	$.ajax({
		url: $('#frmUPSERTDATA').attr('action'),
		type: "POST",
		cache: false,
		processData: false,
		contentType: false,
		data: formData,
		dataType:'json',
		beforeSend: function() {
			progressModal('start');
		},
		error: function() {
			alert('Ajax Error');
			progressModal('stop');
		},
		success: function(response) {
			if(response.status == 1) {
				if(response.error.upload_status == 1) {
					$('#ResponseInput').html(response.msg).show('slow');
					setTimeout(function() {
						$('#ResponseInput').html('');
						$('#ModalAjaxID').modal('hide');
						$('#list').mDatatable('reload');
					}, 1e3);
				}
				else
				{
					$("input[name^='id']").val(response.error.result.id);
					$('#ResponseInput, #ResponseUpload').show('slow');
					$('#ResponseUpload').html(response.error.upload_result);
					$('#list').mDatatable('reload');
				}
			}
			else
			{
				$('#ResponseInput').html(response.msg).show('slow');
				$.each(response.error, function(key, value) {
					$('#'+key).addClass('is-invalid');
					$('#'+key).after(value);
					$('#'+key).parents('.form-group').addClass('has-danger');
				});
			}
		},
		complete : function() {
			progressModal('stop');
		}
	});
}
</script>