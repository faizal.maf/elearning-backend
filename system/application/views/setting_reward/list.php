<script src="<?php echo base_url('public/themes/js/magnificpopup.min.js?v1&current='.date('Ym'))?>"></script>
<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
	<div class="row">
		<div class="col-xl-12">
			<!--begin::Portlet-->
			<div class="m-portlet " id="m_portlet">
				<div class="m-portlet__body">
					<div class="m-form m-form--label-align-right">
						<div class="row align-items-center">
							<div class="col-xl-3">
								<div class="form-group m-form__group pt-2 pb-2">
									<div class="m-input-icon m-input-icon--left">
										<input type="text" class="form-control m-input m-input--square" id="generalSearch" placeholder="Search on enter.." autocomplete="off">
										<span class="m-input-icon__icon m-input-icon__icon--left"><span><i class="la la-search"></i></span></span>
									</div>
								</div>
							</div>
							<div class="col-xl-9">
								<div class="form-group m-form__group row align-items-center pt-0 pb-0">
									<div class="col-md-3">
										<div class="form-group m-form__group pt-2 pb-2">
											<div class="form-label-group form-label-group-select">
												<label for="fillStatus">Status Publish</label>
													<?php echo (isset($status) ? $status : ''); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>

					<!---begin: list -->
					<div id="list"></div>
				</div>
			</div>
			<!--end::Portlet-->
		</div>
	</div>
</div>

<script type="text/javascript">
	// ------ init table list.
	var table = {
		init: function() {
			var t;
			t = $("#list").mDatatable({
				<?php echo init_table_list('', $url_tablelist); ?>
				columns: [{
					field: "Thumbnail",
					title: "",
					sortable: !1,
					width: 50,
				}, {
					field: "PointsRewards",
					title: "Poin",
					sortable: !1,
					width: 80,
				}, {
					field: "Description",
					title: "Keterangan",
					sortable: !1,
					width: 300,
				}, {
					field: "Date",
					title: "Tgl Input",
					sortable: "desc",
					width: 140,
				}, {
					field: "Status",
					title: "Status Publish",
					sortable: !0,
					width: 110,
					textAlign: "center",
				}, {
					field: "Actions",
					title: "",
					width: 70,
					sortable: !1,
					textAlign: "center",
					overflow: "visible",
					template: function(t, e, a) {
						return '\t\t\t\t\t<a data-toggle="mainmodal" data-modal-type="modal-md" title="Edit <?php echo $title; ?>" id="frmEdit" href="{base_url}<?php echo $controller_name; ?>/form/'+t.RecordID+'" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Edit">\t\t\t\t\t\t\t<i class="la la-pencil-square"></i>\t\t\t\t\t\t</a>\t\t\t\t\t<a id="frmDelete" value="' + t.RecordID + '" href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\t\t\t\t\t\t\t<i class="la la-trash"></i>\t\t\t\t\t\t</a>\t\t\t\t\t'
					}
				}]
			}),
			$("#fillStatus").on("change", function () {
				t.search($(this).val(), "fillStatus")
			})

			//call popup images
			$("#list").on("m-datatable--on-layout-updated", function() {
				popupImagesFit();
            })
		}
	};

	// ------ doc ready.
	$(function() {
		table.init();
		$('form').submit(false);
	});

	// ------ set published.
	$(document).on('click', '.btnPublish', function (e) {
	e.preventDefault();
	var id = $(this).attr('id');
	var status;

	if ($(this).hasClass('pubYes')) {
		$(this).addClass('pubNo btn-danger').removeClass('pubYes btn-success').html('<i class="la la-close"></i>No');
		status = 0;
	} else if ($(this).hasClass('pubNo')) {
		$(this).removeClass('pubNo btn-danger').addClass('pubYes btn-success').html('<i class="la la-check"></i>Yes');
		status = 1;
	}
	$.post("<?php echo base_url().$controller_name ?>/set_status", {id: id, status: status}, function (theResponse) {
		make_toast('success', 'Data has been success change.')
	})
});

	// ------- deleted.
$(document).on('click', '#frmDelete', function (e) {
	e.preventDefault();
	var id = $(this).attr('value');
	var trID = $(this).closest('tr').attr("data-row");

	funcDelete("<?php echo base_url().$controller_name ?>/delete", id, trID);
});
</script>