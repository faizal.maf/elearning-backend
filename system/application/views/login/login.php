<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<title>BSM Compas – Admin</title>

	<link rel="shortcut icon" href="public/assets/favicon.ico" />
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>WebFont.load({google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]}, active: function() {sessionStorage.fonts = true;}});</script>

	<link href="<?php echo base_url(); ?>public/themes/css/vendors.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url(); ?>public/themes/css/app.min.css" rel="stylesheet" type="text/css" />
</head>
<!-- end::Head -->

    
    <!-- begin::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">      
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-login m-login--signin  m-login--5" id="m_login" style="background-image: url('public/assets/img/bg/bg-3.jpg');">
				<div class="m-login__wrapper-1 m-portlet-full-height">
					<div class="m-login__wrapper-1-1">
						<div class="m-login__contanier">
							<div class="m-login__content">
								<div class="m-login__logo">
									<a href="#">
										<img src="" width="0%">
									</a>
								</div>
								<div class="m-login__title">
									<!-- <h3>&nbsp;</h3> -->
								</div>
								<div class="m-login__desc m-login__logo">
										<img width="70%" src="<?php echo $logo[0]->catatan; ?>">
								</div>
								<!-- <div class="m-login__form-action">
									<button type="button" id="m_login_signup" class="btn btn-outline-info m-btn--pill">Get An Account</button>
								</div> -->
							</div>
						</div>
						<div class="m-login__border">
							<div></div>
						</div>
					</div>
				</div>
				<div class="m-login__wrapper-2 m-portlet-full-height">
					<div class="m-login__contanier">
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">Login To Your Account</h3>
							</div>
							<form class="m-login__form m-form" id="loginform" role="form" action="?" method="post">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Username or email" name="username" autocomplete="off" autufocus>
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="Password" placeholder="Password" name="password">
								</div>
								<div class="row m-login__form-sub">
									<div class="col m--align-left">
										<!-- <label class="m-checkbox m-checkbox--info">
											<input type="checkbox" name="remember"> Remember me
											<span></span>
										</label> -->
									</div>
									<div class="col m--align-right">
										<!-- <a href="javascript:;" id="m_login_forget_password" class="m-link">Forget Password ?</a> -->
									</div>
								</div>
								<div class="m-login__form-action">
									<button id="m_login_signin_submit" class="btn btn-warning m-btn m-btn--pill m-btn--custom m-btn--air">Sign In</button>
								</div>
							</form>
						</div>
						<div class="m-login__signup">
							<div class="m-login__head">
								<h3 class="m-login__title">Sign Up</h3>
								<div class="m-login__desc">Enter your details to create your account:</div>
							</div>
							<form class="m-login__form m-form" id="registerform" role="form" action="?" method="post" novalidate="novalidate">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="First Name" name="fname" autocomplete="off" autofocus required aria-required="true">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Last Name" name="lname" autocomplete="off" required aria-required="true">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" required aria-required="true">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Phone" name="phone" autocomplete="off" required aria-required="true">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="password" placeholder="Password" id='pwd' name="pwd" minlength="5" required aria-required="true">
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="rpwd" minlength="5" required aria-required="true">
								</div>
								<div class="m-login__form-sub">
									<label class="m-checkbox m-checkbox--info">
										<input type="checkbox" name="agree"> I Agree the <a href="#" class="m-link m-link--info">terms and conditions</a>.
										<span></span>
									</label>
									<span class="m-form__help"></span>
								</div>
								<div class="m-login__form-action">
									<button id="m_login_signup_submit" class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--air">Sign Up</button>
									<button id="m_login_signup_cancel" class="btn btn-outline-info m-btn m-btn--pill m-btn--custom">Cancel</button>
								</div>
							</form>
						</div>
						<div class="m-login__forget-password">
							<div class="m-login__head">
								<h3 class="m-login__title">Forgotten Password ?</h3>
								<div class="m-login__desc">Enter your email to reset your password:</div>
							</div>
							<form class="m-login__form m-form" action="">
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
								</div>
								<div class="m-login__form-action">
									<button id="m_login_forget_password_submit" class="btn btn-info m-btn m-btn--pill m-btn--custom m-btn--air">Request</button>
									<button id="m_login_forget_password_cancel" class="btn btn-outline-info m-btn m-btn--pill m-btn--custom ">Cancel</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- end:: Page -->


        <!--begin::Global Theme Bundle -->
                    <script src="<?php echo base_url(); ?>public/themes/js/vendors.min.js" type="text/javascript"></script>
                    <script src="<?php echo base_url(); ?>public/themes/js/app.min.js" type="text/javascript"></script>
                <!--end::Global Theme Bundle -->
        <!-- begin::Page Loader -->
        <script>
            //== Class Definition
            var SnippetLogin = function() {

            var login = $('#m_login');

            var showErrorMsg = function(form, type, msg) {
                var alert = $('<div class="m-alert m-alert--outline alert alert-' + type + ' alert-dismissible" role="alert">\
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>\
                    <span></span>\
                </div>');

                form.find('.alert').remove();
                alert.prependTo(form);
                //alert.animateClass('fadeIn animated');
                mUtil.animateClass(alert[0], 'fadeIn animated');
                alert.find('span').html(msg);
            }

            //== Private Functions

            var displaySignUpForm = function() {
                login.removeClass('m-login--forget-password');
                login.removeClass('m-login--signin');

                login.addClass('m-login--signup');
                mUtil.animateClass(login.find('.m-login__signup')[0], 'fadeIn animated');
            }

            var displaySignInForm = function() {
                login.removeClass('m-login--forget-password');
                login.removeClass('m-login--signup');

                login.addClass('m-login--signin');
                mUtil.animateClass(login.find('.m-login__signin')[0], 'fadeIn animated');
                //login.find('.m-login__signin').animateClass('flipInX animated');
            }

            var displayForgetPasswordForm = function() {
                login.removeClass('m-login--signin');
                login.removeClass('m-login--signup');

                login.addClass('m-login--forget-password');
                //login.find('.m-login__forget-password').animateClass('flipInX animated');
                mUtil.animateClass(login.find('.m-login__forget-password')[0], 'fadeIn animated');

            }

            var handleFormSwitch = function() {
                $('#m_login_forget_password').click(function(e) {
                    e.preventDefault();
                    displayForgetPasswordForm();
                });

                $('#m_login_forget_password_cancel').click(function(e) {
                    e.preventDefault();
                    displaySignInForm();
                });

                $('#m_login_signup').click(function(e) {
                    e.preventDefault();
                    displaySignUpForm();
                });

                $('#m_login_signup_cancel').click(function(e) {
                    e.preventDefault();
                    displaySignInForm();
                });
            }

            var handleSignInFormSubmit = function() {
                $('#m_login_signin_submit').click(function(e) {
                    e.preventDefault();
                    var btn = $(this);
                    var form = $(this).closest('form');

                    form.validate({
                        rules: {
                            username: {
                                required: true
                            },
                            password: {
                                required: true
                            }
                        }
                    });

                    if (!form.valid()) {
                        return;
                    }

                    btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    form.ajaxSubmit({
                        url: "<?php echo base_url().'login/login_in?currtime=' ?>"+Date.now(),
                        success: function(response, status, xhr, $form) {
                            var data = $.parseJSON(response);
                            setTimeout(function() {
                                if(!data.status)
                                {
                                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                                    showErrorMsg(form, 'danger', data.message);
                                }
                                else
                                {
                                    // btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                                    showErrorMsg(form, 'success', data.message);

                                    window.location.replace(data.redirect_to);
                                }
                            }, 1000);
                        }
                    });
                });
            }

            var handleSignUpFormSubmit = function() {
                $('#m_login_signup_submit').click(function(e) {
                    e.preventDefault();

                    var btn = $(this);
                    var form = $(this).closest('form');

                    form.validate({
                        rules: {
                            fname: {
                                required: true
                            },
                            lname: {
                                required: true
                            },
                            phone: {
                                required: true,
                                number: true
                            },
                            email: {
                                required: true,
                                email: true
                            },
                            pwd: {
                                required: true,
                                minlength: 5
                            },
                            rpwd: {
                                required: true,
                                minlength: 5,
                                equalTo: "#pwd"
                            },
                            agree: {
                                required: true
                            }
                        }
                    });

                    if (!form.valid()) {
                        return;
                    }

                    btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    form.ajaxSubmit({
                        url: "<?php echo base_url().'register/partner?currtime=' ?>"+Date.now() ,
                        success: function(response, status, xhr, $form) {
                            var data = $.parseJSON(response);
                            setTimeout(function() {
                                if(!data.status)
                                {
                                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                                    form.clearForm();
                                    form.validate().resetForm();

                                    // display signup form
                                    displaySignInForm();
                                    var signInForm = login.find('.m-login__signin form');
                                    signInForm.clearForm();
                                    signInForm.validate().resetForm();

                                    showErrorMsg(signInForm, 'danger', data.message);

                                       // a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(l, "danger", resp.message);
                                    }
                                    else
                                    {
                                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                                        form.clearForm();
                                        form.validate().resetForm();

                                        // display signup form
                                        displaySignInForm();
                                        var signInForm = login.find('.m-login__signin form');
                                        signInForm.clearForm();
                                        signInForm.validate().resetForm();

                                        showErrorMsg(signInForm, 'success', data.message);

                                        // a.removeClass("m-loader m-loader--right m-loader--light").attr("disabled", !1), i(l, "success", resp.message);
                                        // window.location.replace(resp.url);
                                    }
                            }, 1000);
                        }
                    });
                });
            }

            var handleForgetPasswordFormSubmit = function() {
                $('#m_login_forget_password_submit').click(function(e) {
                    e.preventDefault();

                    var btn = $(this);
                    var form = $(this).closest('form');

                    form.validate({
                        rules: {
                            email: {
                                required: true,
                                email: true
                            }
                        }
                    });

                    if (!form.valid()) {
                        return;
                    }

                    btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

                    form.ajaxSubmit({
                        url: '',
                        success: function(response, status, xhr, $form) { 
                            // similate 2s delay
                            setTimeout(function() {
                                btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false); // remove 
                                form.clearForm(); // clear form
                                form.validate().resetForm(); // reset validation states

                                // display signup form
                                displaySignInForm();
                                var signInForm = login.find('.m-login__signin form');
                                signInForm.clearForm();
                                signInForm.validate().resetForm();

                                showErrorMsg(signInForm, 'success', 'Cool! Password recovery instruction has been sent to your email.');
                            }, 2000);
                        }
                    });
                });
            }

            //== Public Functions
            return {
                // public functions
                init: function() {
                    handleFormSwitch();
                    handleSignInFormSubmit();
                    handleSignUpFormSubmit();
                    handleForgetPasswordFormSubmit();
                }
            };
            }();

            //== Class Initialization
            jQuery(document).ready(function() 
            {
                SnippetLogin.init();
            });
        </script>
    </body>
    <!-- end::Body -->
</html>