<div class="m-subheader">
	<?php echo $breadcrumbs; ?>
</div>
<div class="m-content">
	<div class="row">
		<div class="col-xl-4 col-lg-4">
			<div class="m-portlet m-portlet--full-height" m-portlet="true" id="m_portlet">
				<!-- <div class="m-portlet__head">
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
							</li>
						</ul>
					</div>
				</div> -->
				<div class="m-portlet__body">
					<div class="m-card-profile">
						<div class="m-card-profile__title m--hide">
							Your Profile
						</div>
						<div class="m-card-profile__pic">
							<div class="m-card-profile__pic-wrapper">
								<span class="m-type m-type--lg m--bg-success">
									<span class="m--font-light">
									<i class="fa fa-user m--icon-font-size-lg5"></i>
									</span>
								</span>
							</div>
						</div>
						<div class="m-card-profile__details">
							<span class="m-card-profile__name"><?php echo $info_username; ?></span>
							<a href="javascript:void(0)" class="m-card-profile__email m-link"><i class="fa fa-envelope"></i> <?php echo $info_useremail; ?></a>
						</div>
					</div>
					<div class="m-portlet__body-separator"></div>
					<div class="m-section__content">
						<span class="m-badge m-badge--secondary m-badge--wide m-badge--rounded mb-2" data-skin="dark" data-toggle="m-tooltip" data-placement="right" data-original-title="User group"><i class="fa fa-users"></i> <?php echo $info_level; ?></span>
						<br>
						<span class="m-badge m-badge--success m-badge--wide m-badge--rounded mb-2" data-skin="dark" data-toggle="m-tooltip" data-placement="right" data-original-title="User status"><i class="fa fa-check-circle"></i> Active</span>
						<br>
						<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded mb-2" data-skin="dark" data-toggle="m-tooltip" data-placement="right" data-original-title="Last login"><i class="fa fa-calendar-alt"></i> <?php echo $result['last_activity'] ?></span>
					
					</div>
				</div>
			</div>
		</div>
		<div class="col-xl-8 col-lg-8">
			<div class="m-portlet m-portlet--full-height">
				<div class="tab-content">
					<?php echo form_open_multipart($controller_name.'/'.$action, array('class' => 'm-form m-form--fit m-form--label-align-right myForm', 'id' => 'frmProfile', 'novalidate'=>'novalidate'));
						echo form_hidden('id', (isset($result['id_user']) ? $result['id_user'] : ''));
					?>
						<!-- <form class="m-form m-form--fit m-form--label-align-right"> -->
							<div class="m-portlet__body">
								<?php echo (isset($this->message)) ? alert_message('',$this->message_type, $this->message) : '' ?>
								<div class="form-group m-form__group row">
									<div class="col-10 ml-auto">
										<h3 class="m-form__section">Personal Details</h3>
									</div>
								</div>
								<div class="form-group m-form__group row">
									<label for="example-text-input" class="col-3 col-form-label">Username</label>
									<div class="col-7">
										<input type="text" id="username" name="username" class="form-control m-input m-input--square" value="<?php echo set_value('username', (isset($result['username']) ? $result['username'] : ''), FALSE) ?>" placeholder="username" autocomplete="off" <?php echo ($action=='insert' ? '' : 'readonly disabled style="cursor:not-allowed"') ?> >
									</div>
								</div>
								<div class="form-group m-form__group row">
									<label for="example-text-input" class="col-3 col-form-label">First Name</label>
									<div class="col-7">
										<input type="text" id="first_name" name="first_name" class="form-control m-input m-input--square" value="<?php echo set_value('first_name', (isset($result['first_name']) ? $result['first_name'] : ''), FALSE) ?>" placeholder="first name" autocomplete="off" <?php echo ($action=='insert' ? '' : 'readonly disabled style="cursor:not-allowed"') ?>>
									</div>
								</div>
								<div class="form-group m-form__group row">
									<label for="example-text-input" class="col-3 col-form-label">Last Name</label>
									<div class="col-7">
									<input type="text" id="last_name" name="last_name" class="form-control m-input m-input--square" value="<?php echo set_value('last_name', (isset($result['last_name']) ? $result['last_name'] : ''), FALSE) ?>" placeholder="last name" autocomplete="off" <?php echo ($action=='insert' ? '' : 'readonly disabled style="cursor:not-allowed"') ?>>
									</div>
								</div>
								<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
								<div class="form-group m-form__group row">
									<div class="col-10 ml-auto">
										<h3 class="m-form__section">Change Password</h3>
									</div>
								</div>
								<div class="form-group m-form__group row">
									<label for="example-text-input" class="col-3 col-form-label">Old Password</label>
									<div class="col-7">
										<input class="form-control m-input" type="password" name="oldpass" id="oldpass" autocomplete="off">
									</div>
								</div>
								<div class="form-group m-form__group row">
									<label for="example-text-input" class="col-3 col-form-label">New Password</label>
									<div class="col-7">
										<input class="form-control m-input" type="password" name="newpass" id="newpass" autocomplete="off">
									</div>
								</div>
								<div class="form-group m-form__group row">
									<label for="example-text-input" class="col-3 col-form-label">Confirm New Password</label>
									<div class="col-7">
										<input class="form-control m-input" type="password" name="confirmpass" id="confirmpass" autocomplete="off">
									</div>
								</div>
							</div>
							<div class="m-portlet__foot m-portlet__foot--fit">
								<div class="m-form__actions">
									<div class="row">
										<div class="col-5">
										</div>
										<div class="col-7">
											<button type="submit" class="btn btn-success m-btn btn-sm m-btn--custom">Update</button>
										</div>
									</div>
								</div>
							</div>
						<!-- </form> -->
						<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
