<style type="text/css">
	.m-widget14 {padding: 20px !important;}
	.col {flex-basis: unset !important;}
	.m-widget14__legends {padding: 20px;}
	.tablink_custom {padding: 5px 5px 5px 5px !important;border-radius: 0px !important;}
	.m-widget24__title {margin-top: 1rem !important;}
	.dropdown-menu>li>a.active, .dropdown-menu>.dropdown-item.active {
		background: #faa719 !important;
	}
</style>
<div class="m-content">
	<!-- DONUT CHART -->
	<div class="m-portlet">
		<div class="m-portlet__body  m-portlet__body--no-padding">
			<div class="row m-row--no-padding m-row--col-separator-xl">
				<!--begin:: Widgets/Regulasi-->
				<div class="col-xl-2">
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								<a href="<?php echo base_url('regulasi') ?>" class="m-nav__link">
									REGULASI
								</a>
							</h3>
							<span class="m-widget14__desc">
								Total by category
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px;margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_regulasi" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
							<!--<div class="col" style="padding: unset;">
								<div class="m-widget14__legends">
									<!--< ?php 
									$class = array('FATWA DSN' => 'success', 'UU Permen Perda' => 'warning', 'PBI SERBI' => 'primary', 'POJK SEOJK' => 'danger', 'EKSUM CPG' => 'info');								
									foreach($result_regulasi as $result) : ?>
									<div class="m-widget14__legend">
										<span class="m-widget14__legend-bullet m--bg-< ?php echo $class[$result['label']];?> "></span>
										<span class="m-widget14__legend-text">< ?php echo $result['label']; ?></span>
									</div>
									< ?php endforeach; ?>
								</div>
							</div>-->
						</div>
					</div>
				</div>
					<!--end:: Widgets/Regulasi-->
					<!--begin:: Widgets/Ketentuan Internal-->
				<div class="col-xl-2">
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								<a href="<?php echo base_url('kinternal') ?>" class="m-nav__link">
									KETENTUAN
								</a>
							</h3>
							<span class="m-widget14__desc">
								Total by category
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px;margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_kinternal" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
							<!--<div class="col" style="padding: unset;">
								<div class="m-widget14__legends">
									<!--< ?php 
									$class = array('Kebijakan' => 'warning', 'Standar Prosedur' => 'info', 'Petunjuk Teknis' => 'primary', 'Opini DPS' => 'success', 'Manual Produk' => 'danger');								
									foreach($result_kinternal as $result) : ?>
									<div class="m-widget14__legend">
										<span class="m-widget14__legend-bullet m--bg-< ?php echo $class[$result['label']];?>"></span>
										<span class="m-widget14__legend-text">< ?php echo $result['label']; ?></span>
									</div>
									< ?php endforeach; ?>
								</div>
							</div>-->
						</div>
					</div>
				</div>
					<!--end:: Widgets/Ketentuan Internal-->
					<!--begin:: Widgets/ERM-->
				<div class="col-xl-2">
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								<a href="<?php echo base_url('erm') ?>" class="m-nav__link">
									ERM
								</a>
							</h3>
							<span class="m-widget14__desc">
								Total by category
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px;margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_erm" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
							<!--<div class="col" style="padding: unset;">
								<div class="m-widget14__legends">
									<!--< ?php 
									$class = array('Makro Ekonomi' => 'success', 'Ekonomi Regional' => 'warning', 'Industry Update' => 'primary', 'Reviu Sektroral' => 'danger', 'Harga Komoditas' => 'info', 'Banking Industry Update' => 'focus');								
									foreach($result_erm as $result) : ?>
									<div class="m-widget14__legend">
										<span class="m-widget14__legend-bullet m--bg-< ?php echo $class[$result['label']];?> "></span>
										<span class="m-widget14__legend-text">< ?php echo $result['label']; ?></span>
									</div>
									< ?php endforeach; ?>
								</div>
							</div>-->
						</div>
					</div>
				</div>
					<!--end:: Widgets/ERM-->
					<!--begin:: Widgets/APUPPT-->
				<div class="col-xl-2">
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								<a href="<?php echo base_url('apuppt') ?>" class="m-nav__link">
									APUPPT
								</a>
							</h3>
							<span class="m-widget14__desc">
								Total by category
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px;margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_apuppt" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
							<!--<div class="col" style="padding: unset;">
								<div class="m-widget14__legends">
									<!--< ?php foreach($result_literasi as $result) : ?>
									<div class="m-widget14__legend">
										<span class="m-widget14__legend-bullet m--bg-brand"></span>
										<span class="m-widget14__legend-text">< ?php echo $result['label']; ?></span>
									</div>
									< ?php endforeach; ?>
								</div>
							</div>-->
						</div>
					</div>
				</div>
					<!--end:: Widgets/APUPPT-->
					<!--begin:: Widgets/Question-->
				<div class="col-xl-2">
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								<a href="<?php echo base_url('inbox_question') ?>" class="m-nav__link">
									QUESTIONS
								</a>
							</h3>
							<span class="m-widget14__desc">
								Total by Subject
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px;margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_question" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
							<!--<div class="col" style="padding: unset;">
								<div class="m-widget14__legends">
									<!--< ?php foreach($result_literasi as $result) : ?>
									<div class="m-widget14__legend">
										<span class="m-widget14__legend-bullet m--bg-brand"></span>
										<span class="m-widget14__legend-text">< ?php echo $result['label']; ?></span>
									</div>
									< ?php endforeach; ?>
								</div>
							</div>-->
						</div>
					</div>
				</div>
					<!--end:: Widgets/Questions-->
					
					<!--begin:: Widgets/User APPS-->
				<div class="col-xl-2">
					<div class="m-widget14">
						<div class="m-widget14__header">
							<h3 class="m-widget14__title">
								<a href="<?php echo base_url('user_management') ?>" class="m-nav__link">
									USERS APPS
								</a>
							</h3>
							<span class="m-widget14__desc">
								Total by Status
							</span>
						</div>
						<div class="row  align-items-center" style="margin-top: -20px;margin-bottom: -20px;">
							<div class="col">
								<div id="m_chart_user" class="m-widget14__chart1" style="height: 180px">
								</div>
							</div>
						</div>
					</div>
				</div>
					<!--end:: Widgets/User APPS-->
			</div>
		</div>
	</div>
	<!-- END DONUT CHART -->  
	
	<!-- LITERASI BAR -->
	<div class="m-portlet ">
		<div class="m-portlet__body  m-portlet__body--no-padding">
			<div class="row m-row--no-padding m-row--col-separator-xl">
				<?php foreach($result_literasi as $result) : ?>
				<div class="col-md-12 col-lg-6 col-xl-3">
					<div class="m-widget24">
						<div class="m-widget24__item">
							<h4 class="m-widget24__title">
								<i class="<?php echo $result['icon']; ?>"></i> Total <?php echo $result['label']; ?>
							</h4><br>
							<span class="m-widget24__desc">
								<a href="<?php echo $result['url']; ?>" class="m-link m-link--state m-link--primary">See all</a>
							</span>
							<span class="m-widget24__stats m--font-<?php echo $result['style']; ?>">
								<?php echo $result['value']; ?>
							</span>
							<div class="m--space-10"></div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<!-- END LITERASI BAR -->


	<!-- TOP TEN VIEWS & TOP TEN USERS -->
	<div class="row">
		<!-- TOP TEN CONTENT VIEWS -->
		<div class="col-xl-6">
			<div class="m-portlet " id="m_portlet_TopViews">
				<div class="m-portlet__head" style="height: 6.1rem;">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon"><i class="flaticon-eye"></i></span>
							<h3 class="m-portlet__head-text">TOP TEN VIEWS</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="nav nav-pills nav-pills--brand" role="tablist">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">List Top Views By</a>
								<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 32px, 0px);">
									<a href="<?php echo base_url('welcome/getTopContent/1') ?>" class="dropdown-item active show" data-toggle="tabajax" role="tab">Regulasi</a>
									<a href="<?php echo base_url('welcome/getTopContent/2') ?>" class="dropdown-item" data-toggle="tabajax" role="tab">Ketentuan Internal</a>
									<a href="<?php echo base_url('welcome/getTopContent/3') ?>" class="dropdown-item" data-toggle="tabajax" role="tab">ERM</a>
									<a href="<?php echo base_url('welcome/getTopContent/4') ?>" class="dropdown-item" data-toggle="tabajax" role="tab">News</a>
									<a href="<?php echo base_url('welcome/getTopContent/5') ?>" class="dropdown-item" data-toggle="tabajax" role="tab">Video</a>
									<a href="<?php echo base_url('welcome/getTopContent/6') ?>" class="dropdown-item" data-toggle="tabajax" role="tab">Artikel</a>
									<a href="<?php echo base_url('welcome/getTopContent/7') ?>" class="dropdown-item" data-toggle="tabajax" role="tab">APU/PPT</a>
									<div class="dropdown-divider"></div>
								</div>
							</li>
						</ul>
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<table id="topContent" class="table table-sm m-table" style="font-size: 11px;">
					<tbody>
					</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<!-- TOP TEN USERS -->
		<div class="col-xl-6">
			<div class="m-portlet " id="m_portlet_TopUser">
				<div class="m-portlet__head" style="height: 6.1rem;">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon"><i class="flaticon-medal"></i></span>
							<h3 class="m-portlet__head-text">TOP TEN USER</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="nav nav-pills nav-pills--brand" role="tablist">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
								<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 32px, 0px);">
									<a href="<?php echo base_url('welcome/getTopUsers/1') ?>" class="dropdown-item active show" data-toggle="tabajax2" role="tab">by Point</a>
									<a href="<?php echo base_url('welcome/getTopUsers/2') ?>" class="dropdown-item" data-toggle="tabajax2" role="tab">by Quiz Participant</a>
									<a href="<?php echo base_url('welcome/getTopUsers/3') ?>" class="dropdown-item" data-toggle="tabajax2" role="tab">by Questions</a>
									<div class="dropdown-divider"></div>
								</div>
							</li>
						</ul>
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
				<div class="m-scrollable m-scroller ps ps--active-y" data-scrollable="true" style="height: auto; overflow: hidden;">
					<table id="topUser" class="table table-sm m-table" style="font-size: 11px;">
						<tbody>
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>
	<!--End::Section-->
	
		<!-- TOP TEN LIKE -->
	<div class="row">
		<!-- TOP TEN CONTENT LIKES -->
		<div class="col-xl-6">
			<div class="m-portlet " id="m_portlet_TopLike">
				<div class="m-portlet__head" style="height: 6.1rem;">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon"><i class="flaticon-black"></i></span>
							<h3 class="m-portlet__head-text">TOP TEN LIKE</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="nav nav-pills nav-pills--brand" role="tablist">
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">List Top Likes By</a>
								<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 32px, 0px);">
									<a href="<?php echo base_url('welcome/getTopLike/1') ?>" class="dropdown-item active show" data-toggle="tabajax_like" role="tab">Artikel</a>
									<a href="<?php echo base_url('welcome/getTopLike/2') ?>" class="dropdown-item" data-toggle="tabajax_like" role="tab">Galeri</a>
									<a href="<?php echo base_url('welcome/getTopLike/3') ?>" class="dropdown-item" data-toggle="tabajax_like" role="tab">Video</a>
									<a href="<?php echo base_url('welcome/getTopLike/4') ?>" class="dropdown-item" data-toggle="tabajax_like" role="tab">Infografis</a>
									<a href="<?php echo base_url('welcome/getTopLike/5') ?>" class="dropdown-item" data-toggle="tabajax_like" role="tab">News</a>
									<div class="dropdown-divider"></div>
								</div>
							</li>
						</ul>
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="#" m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="m-portlet__body">
					<table id="topLike" class="table table-sm m-table" style="font-size: 11px;">
					<tbody>
					</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<!-- TOP TEN USERS -->
	</div>
	<!--End::Section-->
</div>

<script languange="javascript" type="text/javascript">
var Dashboard = {
	init: function() {
		0 != $("#m_chart_regulasi").length && Morris.Donut({
			element: "m_chart_regulasi",
			data: <?php echo $data_regulasi; ?>,
			colors: [mApp.getColor("success"), mApp.getColor("warning"), mApp.getColor("primary"), mApp.getColor("danger"), mApp.getColor("info")]
		}),
		0 != $("#m_chart_kinternal").length && Morris.Donut({
			element: "m_chart_kinternal",
			data: <?php echo $data_kinternal; ?>,
			colors: [mApp.getColor("warning"), mApp.getColor("accent"), mApp.getColor("primary"), mApp.getColor("success"), mApp.getColor("danger")]
		}),
		0 != $("#m_chart_erm").length && Morris.Donut({
			element: "m_chart_erm",
			data: <?php echo $data_erm; ?>,
			colors: [mApp.getColor("warning"), mApp.getColor("accent"), mApp.getColor("primary"), mApp.getColor("success"), mApp.getColor("danger"), mApp.getColor("focus")]
		}),
		0 != $("#m_chart_apuppt").length && Morris.Donut({
			element: "m_chart_apuppt",
			data: <?php echo $data_apuppt; ?>,
			colors: [mApp.getColor("brand"), mApp.getColor("danger")]
		}),
		0 != $("#m_chart_question").length && Morris.Donut({
			element: "m_chart_question",
			data: <?php echo $data_question; ?>,
			// colors: [mApp.getColor("warning"), mApp.getColor("accent"), mApp.getColor("primary"), mApp.getColor("success")]
		}),
		0 != $("#m_chart_user").length && Morris.Donut({
			element: "m_chart_user",
			data: <?php echo $data_users; ?>,
			colors: [mApp.getColor("danger"), mApp.getColor("success")]
		})
	}
};

var PortletTools = function () {
	var TopViewsPortlet = function() {
	
		var portlet = new mPortlet('m_portlet_TopViews');

        //== Toggle event handlers
        portlet.on('beforeCollapse', function(portlet) {
            setTimeout(function() {
            }, 100);
        });

        portlet.on('afterCollapse', function(portlet) {
            setTimeout(function() {
            }, 2000);            
        });
	}
	
    var TopUserPortlet = function() {
	
		var portlet = new mPortlet('m_portlet_TopUser');

        //== Toggle event handlers
        portlet.on('beforeCollapse', function(portlet) {
            setTimeout(function() {
            }, 100);
        });

        portlet.on('afterCollapse', function(portlet) {
            setTimeout(function() {
            }, 2000);            
        });
	}
	
	var TopLikePortlet = function() {
	
		var portlet = new mPortlet('m_portlet_TopLike');

        //== Toggle event handlers
        portlet.on('beforeCollapse', function(portlet) {
            setTimeout(function() {
            }, 100);
        });

        portlet.on('afterCollapse', function(portlet) {
            setTimeout(function() {
            }, 2000);            
        });
	}
	
	return {
        //main function to initiate the module
        init: function () {
            TopViewsPortlet();
            TopUserPortlet();
            TopLikePortlet();
        }
    };
}();

jQuery(document).ready(function() {
    Dashboard.init();
    PortletTools.init();
	
	var defaultTab = $('[data-toggle="tabajax"]');
	var defaultTab2 = $('[data-toggle="tabajax2"]');
	var default_tab_like = $('[data-toggle="tabajax_like"]');
	activaTab(defaultTab, '#topContent tbody');
	activaTab(defaultTab2, '#topUser tbody');
	activaTab(default_tab_like, '#topLike tbody');
		
	$('[data-toggle="tabajax"]').click(function(e) {
		var $this = $(this),
			loadurl = $this.attr('href');
			
		$.ajax({
			url: loadurl,
			type: 'POST',
			dataType: 'JSON',

			success:function (data) {
				$('#topContent tbody').html(data);
			}
		});

		$this.tab('show');
		return false;
	});
	
	$('[data-toggle="tabajax2"]').click(function(e) {
		var $this = $(this),
			loadurl = $this.attr('href');
			
		$.ajax({
			url: loadurl,
			type: 'POST',
			dataType: 'JSON',

			success:function (data) {
				$('#topUser tbody').html(data);
			}
		});

		$this.tab('show');
		return false;
	});
	
	$('[data-toggle="tabajax_like"]').click(function(e) {
		var $this = $(this),
			loadurl = $this.attr('href');
			
		$.ajax({
			url: loadurl,
			type: 'POST',
			dataType: 'JSON',

			success:function (data) {
				$('#topLike tbody').html(data);
			}
		});

		$this.tab('show');
		return false;
	});
});

function activaTab(thisTab, divNya){
	loadurl = thisTab.attr('href'),
	
	$.ajax({
		url: loadurl,
		type: 'POST',
		dataType: 'JSON',

		success:function (data) {
			$(divNya).html(data);
		}
	});
};
</script>