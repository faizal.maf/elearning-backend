<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('post_inbox'))
{
	function post_inbox($id='', $ref_id='', $title='', $highlight='', $body='', $type='')
	{
		$CI = get_instance();
		return $CI->common_lib->post_inbox($id, $ref_id, $title, $highlight, $body, $type);
	}
}

if (!function_exists('post_mutation'))
{
	function post_mutation($id='', $ref_id='', $debet='', $kredit='', $ket='', $poin='', $type='')
	{
		$CI = get_instance();
		return $CI->common_lib->post_mutation($id, $ref_id, $debet, $kredit, $ket, $poin, $type);
	}
}

if (!function_exists('store_last_id'))
{
	function store_last_id($field='', $last_id='')
	{
		$CI = get_instance();
		return $CI->common_lib->store_last_id($field, $last_id);
	}
}

if (!function_exists('update_last_id_after_delete'))
{
	function update_last_id_after_delete($table_name='', $id='', $id_kategori='')
	{
		$CI = get_instance();
		return $CI->common_lib->update_last_id_after_delete($table_name, $id, $id_kategori);
	}
}

if (!function_exists('get_last_id_after_delete'))
{
	function get_last_id_after_delete()
	{
		$CI = get_instance();
		return $CI->common_lib->get_last_id_after_delete();
	}
}

if (!function_exists('update_last_id_after_set_status'))
{
	function update_last_id_after_set_status($table_name='', $id='', $id_kategori='')
	{
		$CI = get_instance();
		return $CI->common_lib->update_last_id_after_set_status($table_name, $id, $id_kategori);
	}
}