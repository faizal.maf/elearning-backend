<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function image($url_img = '')
{
	$CI =& get_instance();
	return $CI->template->image($url_img);
}

if ( ! function_exists('css'))
{
    /**
     * Attach Cascading Style Sheet file to the current template.
     * @param string  $css_file CSS file route.
     * @return void
     */
  function css($styles)
  {
    $CI =& get_instance();

    return $CI->template->set_css($styles);
  }
}


if ( ! function_exists('js'))
{
    /**
     * Attach Javascript file to the current template.
     * @param string  $js_file JS file route.
     * @return void
     */
  function js($scripts)
  {
    $CI =& get_instance();

    return $CI->template->set_js($scripts);
  }
}


if ( ! function_exists('code'))
{
    /**
     * Output pretty print code for development
     * @param array  $code
     * @return void
     */
     function code($code, $exit = true)
     {
          print('<pre>');
          print_r($code);
          print('</pre>');
          if ($exit)
          {
            exit;
          }
     }
}


if ( ! function_exists('view'))
{
    /**
     * render view
     * @param array  $code
     * @return void
     */
     function view($view, $params, $return = FALSE)
     {
          $CI =& get_instance();

          return $CI->template->view($view, $params, $return);
     }
}

if ( ! function_exists('template'))
{
    /**
     * render template
     * @param array  $code
     * @return void
     */
     function template($view, $params, $return = FALSE)
     {
          $CI =& get_instance();

          return $CI->template->minor($view, $params, $return);
     }
}

if ( ! function_exists('json'))
{
    /**
     * output json string
     * @param array
     * @return string
     */
     function json($json, $status = 200)
     {
          $CI =& get_instance();

          return $CI->template->render_json($json, $status);

     }
}
if ( ! function_exists('html'))
{
    /**
     * output html string
     * @param string
     * @return string
     */
     function html($html, $status = 200)
     {
          $CI =& get_instance();

          return $CI->template->render_html($html, $status);

     }
}

if ( ! function_exists('success'))
{
    /**
     * output success string
     * @param array
     * @return string
     */
     function success($message, $url = '')
     {
          $CI =& get_instance();

          $array = ['response' => 'success', 'message'=> $message, 'success' => true, 'redirect' => $url];

          return $CI->template->render_json($array);

     }
}


if ( ! function_exists('error'))
{
    /**
     * output error string
     * @param array
     * @return string
     */
     function error($message, $url = '')
     {
          $CI =& get_instance();

          $array = ['response' => 'error', 'message'=> $message, 'success' => false, 'redirect' => $url];

          return $CI->template->render_json($array);

     }
}


if ( ! function_exists('response'))
{
    /**
     * output response string
     * @param array
     * @return string
     */
     function response($status = 200)
     {
          $CI =& get_instance();

          return $CI->template->render_json([], $status);

     }
}

function message($type = 'info', $title = '', $description = '', $close = false, $style = '')
{
	$CI =& get_instance();
	return $CI->template->message($type, $title, $description, $close, $style);
}

if (!function_exists('safe_string')) {
   /**
    * Output safe string.
    *
    * @param      string  $code   codigo a escapar
    * @return     string
    */

   function safe_string($code) {
       $chars = [
      '/\'/',
      '/\"/',
      '/\</',
      '/\>/',
      '/\&/',
      '/\n/',
      '/\r/',
      '/\t/',
      '/\//',
      '/\!/'
       ];

       $rep = [
      '\\\'',
      '\\\"',
      '\\\<',
      '\\\>',
      '\\\&',
      '\\\n',
      '\\\r',
      '\\\t',
      '\\\/',
      '\\\!'
       ];

      return preg_replace($chars, $rep, $code);
   }
}

if ( ! function_exists('dd'))
{
    /**
     * Dump and die
     * @param array  $code
     * @return void
     */
     function dd($code)
     {
          var_dump($code);
          die;
     }
}

if ( ! function_exists('breadcrumbs'))
{
	function breadcrumbs($aBreadcrumbs = array(), $title = '', $controller_name = '', $modal_title = '', $modal_type = 'modal-md', $delimiter = '-')
	{
		$crumbs = '<div class="d-flex align-items-center">'."\n";
		$crumbs .= '<h3 class="m-subheader__title m-subheader__title--separator">'.$title.'</h3>'."";
		$crumbs .= '<div class="mr-auto">'."\n";
		$crumbs .= '<nav aria-label="breadcrumb">'."\n";
		$crumbs .= '<ol class="breadcrumb breadcrumb m-subheader__breadcrumbs m-nav m-nav--inline" style="margin: unset;">'."\n";
		$crumbs .= '<li class="m-nav__item m-nav__item--home"><a href="'.base_url().'" class="m-nav__link m-nav__link--icon"><i class="m-nav__link-icon la la-home"></i></a></li>'."\n";
		for($i=0;$i<count($aBreadcrumbs);$i++)
		{
			if(isset($delimiter) and trim($delimiter)!='')
				$crumbs .= '<li class="delimeter" ><span>'.$delimiter.'</span></li>'."\n";

			if(!isset($aBreadcrumbs[$i]['active']) or $aBreadcrumbs[$i]['active'] == false)
				$crumbs .= '<li class="breadcrumb-item" ><a class="" href="'.base_url($aBreadcrumbs[$i]['url']).'">'.$aBreadcrumbs[$i]['title'].'</a></li>'."\n";
			else
				$crumbs .= '<li class="breadcrumb-item active"  aria-current="page">'.$aBreadcrumbs[$i]['title'].'</li>'."\n";
		}
		$crumbs .= '</ol>'."\n";
		$crumbs .= '</nav>'."\n";
		$crumbs .= '</div>'."\n";
		if(!empty($controller_name))
		{
			$crumbs .= '<div><a data-toggle="mainmodal" data-modal-type="'.$modal_type.'" title="'.$modal_title.'" href="'.base_url().$controller_name.'/form" id="frmAdd" class="btn btn-success btn-sm m-btn m-btn--custom m-btn--icon btnAddNew"><span><i class="fa fa-plus"></i><span>Tambah Baru</span></span></a></div>'."\n";
		}
		$crumbs .= '</div>'."\n";
		
		return $crumbs;
	}
}

if ( ! function_exists('generate_html_button_search_list'))
{
	function generate_html_button_search_list()
	{
		// $html  = '<div class="m-separator m-separator--sm m-separator--dashed"></div>';
		$html  = '<div class="row">';
		$html .= '	<div class="col-lg-12">';
		$html .= '		<button class="btn btn-success btn-sm m-btn m-btn--icon" id="BtnSearch">';
		$html .= '			<span>';
		$html .= '				<i class="la la-search"></i>';
		$html .= '				<span>Search</span>';
		$html .= '			</span>';
		$html .= '		</button>';
		$html .= '		&nbsp;&nbsp;';
		$html .= '		<button class="btn btn-secondary btn-sm m-btn m-btn--icon" id="BtnReset">';
		$html .= '			<span>';
		$html .= '				<i class="la la-close"></i>';
		$html .= '				<span>Reset</span>';
		$html .= '			</span>';
		$html .= '		</button>';
		$html .= '	</div>';
		$html .= '</div>';

		return $html;
	}
}

if ( ! function_exists('init_table_list'))
{
	function init_table_list($list_table_name, $url)
	{
		$url = base_url($url).'?_='.time().rand(3,999);
		$js = 'data:{type:"remote",saveState:{cookie:!1,webstorage:!1},source:{read:{url:"'.$url.'",map:function(t){var e=t;return void 0!==t.data&&(e=t.data),e}}},pageSize:10,serverPaging:!0,serverFiltering:!0,serverSorting:!0},layout:{spinner:{overlayColor:"#000",opacity:0.10,type:"loader",state:"success",message:!0},theme:"default",class:"",scroll:!0,height: 550,footer:!1},sortable:!0,pagination:!0,search:{input:$("#generalSearch"),onEnter:!0},toolbar: {
            layout: ["pagination", "info"],
            placement: ["top"],
            items: {
                pagination: {
                    type: "default",
                    pages: {
                        desktop: {
                            layout: "default",
                            pagesNumber: 6
                        },
                        tablet: {
                            layout: "default",
                            pagesNumber: 3
                        },
                        mobile: {
                            layout: "compact"
                        }
                    },
                    navigation: {
                        prev: !0,
                        next: !0,
                        first: !0,
                        last: !0
                    }
                },
                info: !0
            }
        },translate: {
            records: {
                processing: "Harap tunggu, sedang diproses...",
                noRecords: "Data tidak ditemukan"
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first: "First",
                            prev: "Previous",
                            next: "Next",
                            last: "Last",
                            more: "More pages",
                            input: "Halaman",
                            select: "Jumlah record per page"
                        },
                        info: "informasi {{start}} - {{end}} dari {{total}} "
                    }
                }
            }
        },';

		// $js  =	'data: {'."\n";
		// $js .=	'	type: "remote",'."\n";
		// $js .=	'	saveState: {'."\n";
		// $js .=	'		cookie: !1,'."\n";
		// $js .=	'		webstorage: !1'."\n";
		// $js .=	'	},'."\n";
		// $js .=	'	source: {'."\n";
		// $js .=	'		read: {'."\n";
		// $js .=	'			url: "'.$url.'",'."\n";
		// $js .=	'			map: function(t) {'."\n";
		// $js .=	'				var e = t;'."\n";
		// $js .=	'				return void 0 !== t.data && (e = t.data), e'."\n";
		// $js .=	'			}'."\n";
		// $js .=	'		}'."\n";
		// $js .=	'	},'."\n";
		// $js .=	'	pageSize: 10,'."\n";
		// $js .=	'	serverPaging: !0,'."\n";
		// $js .=	'	serverFiltering: !0,'."\n";
		// $js .=	'	serverSorting: !0'."\n";
		// $js .=	'},'."\n";
		// $js .=	'layout: {'."\n";
		// $js .=	'	spinner: {'."\n";
		// $js .=	'		overlayColor: "#000",'."\n";
		// $js .=	'		opacity: 0.10,'."\n";
		// $js .=	'		type: "loader",'."\n";
		// $js .=	'		state: "success",'."\n";
		// $js .=	'		message: !0'."\n";
		// $js .=	'	},'."\n";
		// $js .=	'	theme: "default",'."\n";
		// $js .=	'	class: "",'."\n";
		// $js .=	'	scroll: !0,'."\n";
		// $js .=	'	footer: !1'."\n";
		// $js .=	'},'."\n";
		// $js .=	'sortable: !0,'."\n";
		// $js .=	'pagination: !0,'."\n";
		// $js .=	'toolbar: {'."\n";
		// $js .=	'	items: {'."\n";
		// $js .=	'		pagination: {'."\n";
		// $js .=	'			pageSizeSelect: [10, 20, 30, 50, 100]'."\n";
		// $js .=	'		}'."\n";
		// $js .=	'	}'."\n";
		// $js .=	'},'."\n";
		// $js .=	'search: {'."\n";
		// $js .=	' input: $("#generalSearch"),'."\n";
		// $js .=	'	onEnter: !0'."\n";
		// $js .=	'},'."\n";

		return $js;
	}
}