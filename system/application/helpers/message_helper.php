<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (! function_exists('alert_message'))
{
	function alert_message($divId='', $type='', $message='', $msg_for='1', $icon='la-warning')
	{
		## Class type message :
		# 1. alert-success  	|  background-color:#45ccb1; 	| #ICON : la-check
		# 2. alert-warning		|  background-color:#ffc241; 	| #ICON : la-warning
		# 3. alert-danger		|  background-color:#f66e84; 	| #ICON : la-warning
		# 4. alert-primary		|  background-color:#717ee2; 	| #ICON : la-info-circle
		# 5. alert-dismissible	|  background-color:#fff;    	| #ICON : la-info-circle
		# 6. alert-accent		|  background-color:##00e0fb;	| #ICON : la-info-circle
		# 7. alert-info			|  background-color:##53b0f8;	| #ICON : la-info-circle
		
		## Message for
		# 1. for=1; submit succes redirect to list with show message.
		# 2. for=2; submit form_validation check.

		$html = "<div class='alert ".$type." alert-dismissible fade show m-alert m-alert--square m-alert--air' role='alert'>
					<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
					</button>
					".$message."
				</div>";
		
		if($msg_for == '2')
		{
			$html = "<div class='m-alert m-alert--icon m-alert--air m-alert--square alert ".$type." alert-dismissible m--hide' role='alert' id='".$divId."'>
						<div class='m-alert__icon'>
							<i class='la ".$icon."'></i>
						</div>
						<div class='m-alert__text'>
							".$message."
						</div>
						<div class='m-alert__close'>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
							</button>
						</div>
					</div>";
		}
		// return "<span class='label label-".$type." font-weight-100'>".$message."</span>";
		return $html;
	}
}