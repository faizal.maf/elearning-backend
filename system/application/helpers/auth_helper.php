<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Validates the session. Returns true if user is logged or redirect
 * to login page if it does not.
 *
 * @return mixed boolean
 */
function validate_session()
{
	$CI = get_instance();
	return $CI->auth->validate_session();
}

/**
 * Validates the session. Returns true true or false if user is logged or not.
 *
 * @return mixed boolean
 */
function check_session()
{
	$CI = get_instance();
	return $CI->auth->check_session();
}


// Logger For login

/**
 * Saves a log on database (table hsapp_log).
 *
 * @param string text_log
 * @param string action
 * @param string table
 * @param array additional data 	// anything you consider relevant
 * @return void
 */
function db_log($text_log = '', $action = '', $table = '', $additional_data = array())
{
	$CI =& get_instance();
	$CI->auth->db_log($text_log, $action, $table, $additional_data = array());
}

/**
 * Saves an error log on database (table hsapp_log_error).
 *
 * @param string error_type
 * @param string header
 * @param string message
 * @param string status_code
 * @return void
*/
function log_error($error_type = '', $header = '', $message = '', $status_code = '')
{
	$CI =& get_instance();
	$CI->auth->log_error($error_type, $header, $message, $status_code);
}
