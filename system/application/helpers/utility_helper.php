<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('generate_trxid'))
{
	function generate_trxid()
	{
		date_default_timezone_set("Asia/Jakarta");
		// date_default_timezone_set('UTC');
		$times = time() * 1000;
		$time = sprintf('%013.0f', microtime(1)*1000);
		// $time = "1532608344300";
		// JK1Y17F4 // 26/05/2018
		$q = (int)$time;
		$a = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		$rem = 0;
		$hexdec = "";
		while($q != 0)
		{
			$rem = $q%36;
			$hexdec = $a[$rem].$hexdec;
			$q = floor($q/36);
		}

		return $hexdec;
	}
}

if ( ! function_exists('logit'))
{
	function logit($str)
	{
		$CI =& get_instance();
		$client_ip = $CI->input->ip_address();
		$function_name = $CI->uri->uri_string();
		$uniqid = uniqid();
		if(!is_dir("logs")){
			mkdir("logs");
		}
		$logfile = date('Ymd').".log";
		$log = fopen(FCPATH."logs/$logfile","a");
		$write = date('Y-m-d H:i:s')." - ".$uniqid." - ".$client_ip." --> ".$function_name." --> ".$str."\n";
		fwrite($log, $write);
		fclose($log);
	}
}

if ( ! function_exists('get_param'))
{
	function get_param($url)
	{
		$query_str = parse_url($url, PHP_URL_QUERY);
		parse_str($query_str, $query_params);
		
		 $args = $query_params;
		
		return json_encode($args);
	}
}

if ( ! function_exists('get_3rd_url'))
{
	function get_3rd_url($url)
	{
		$url_schema = parse_url($url, PHP_URL_SCHEME);
		$url_host   = parse_url($url, PHP_URL_HOST);
		$url_path   = parse_url($url, PHP_URL_PATH);
		
		return $url_schema.'://'.$url_host.$url_path;
	}
}

if ( ! function_exists('generate_token_register'))
{
	function generate_token_register($id, $fname, $lname, $email, $phone)
	{
		$memb_id = $id;
		$memb_fname = $fname;
		$memb_lname = $lname;
		$memb_email = $email;
		$memb_mobile = $phone;

		$md5 = strtoupper(md5($memb_id . $memb_fname .$memb_lname . $memb_email . $memb_mobile));

		$code[] = substr ($md5, 0, 5);
		$code[] = substr ($md5, 5, 5);
		$code[] = substr ($md5, 10, 5);
		$code[] = substr ($md5, 15, 5);
		$code[] = substr ($md5, 20, 5);

		$membcode = implode ("-", $code);
		if (strlen($membcode) == "29")
		{
			return ($membcode); 
		} 
		else
		{
			return (FALSE);
		}
	}
}

/* Convert Date from 31/12/2013 to 2013-12-31 or in reverse 2013-12-31 to 31/12/2013
- use for save to database or display
- ex : convert_date('d/m/Y', $tgl, 'Y-m-d');
*/
if ( ! function_exists('convert_date'))
{
	function convert_date($date_format='d/m/Y', $date='', $date_format_result='Y-m-d')
	{
		if($date == '0000-00-00')
			$date = '';
			
		$result = $date;
		if (!empty($date))
		{			
			$date_new = DateTime::createFromFormat($date_format, $date);
			$result = $date_new->format($date_format_result);
		}
		
		return $result;
	}
}

if ( ! function_exists('generate_id_old'))
{
	function generate_id_old($table_name, $field_id, $prefix_id = '', $length_sequence_id = 4)
	{
		/* Cara Pakai
			echo generate_id_old($this->model_name,'501',5);
		*/
		$CI =& get_instance();
		$CI->db->select_max($field_id);
		$CI->db->from($table_name);
		if(!empty($prefix_id))
			$CI->db->where("LEFT(`".$field_id."`, LENGTH(`".$field_id."`) - ".$length_sequence_id.") = '".$prefix_id."'");
		
		$sql = $CI->db->get();
		$sequence_no = 1;
		if($sql !== FALSE){
			$result = $sql->row_array();
			if(isset($result[$field_id]) && !empty($result[$field_id]))
			{
				$sequence_no = substr($result[$field_id], (strlen($result[$field_id]) - $length_sequence_id), $length_sequence_id);
				if(is_numeric($sequence_no))
				{
					$sequence_no = $sequence_no + 1;
				}
			}
		}
		
		$sequence_no = sprintf("%0".$length_sequence_id."s",$sequence_no);
		
		return $prefix_id.$sequence_no;
	}
}

if (! function_exists('compact_string'))
{
	function compact_string($string)
	{
		$string = trim(preg_replace('/\s\s+/', '', $string));
		return $string;
	}
}

/* End of file utility_helper.php */
/* Location: ./application/helpers/utility_helper.php */