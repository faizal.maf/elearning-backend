var PortletTools = {
    init: function() {
        var e;
        toastr.options.showDuration = 1e3,
        function() {
			var e = new mPortlet("m_portlet");
			if(e.length)
			{
				e.on("reload", function(e) {
					mApp.block(e.getSelf(), {
						type: "loader",
						state: "success",
						message: "Please wait..."
					}),
					setTimeout(function() {
						mApp.unblock(e.getSelf())
					}, 1e3)
				})
			}
        }()
    }
};

var BootstrapSelect = {
    init: function() {
        $(".m-bootstrap-select").selectpicker()
    }
};

// For handle tree menus and icon change sub nav to dot icon.
// (c) : jonih 10
$(function() {
	PortletTools.init();
	BootstrapSelect.init();
	MYForm();
	var k = $("ul.m-menu__subnav > li.m-menu__item.m-menu__item--submenu > a span").prev();
		if (k.removeClass("m-menu__link-icon flaticon-suitcase")){
			k.addClass("m-menu__link-bullet m-menu__link-bullet--dot").append("<span></span>");
		}

        for (var i = window.location, e = $("ul#sidebarnav a").filter(function() {
            return this.href == i
        }).parent().addClass("m-menu__item--active"); ; ) {
			if (!e.is("li"))
                break;
            e = e.parent().addClass("m-menu__item--active");
			if (!e.is("ul.m-menu__subnav.m-menu__item--active"))
                break;
			e = e.parent().parent().addClass("m-menu__item--expanded m-menu__item--open");
		}
		
		// if($("ul.m-menu__subnav.m-menu__item--active > li.m-menu__item.m-menu__item--active").addClass("m-animate-blink")); add blink menu active
	
	$(".custom-file-input").on("change", function() {
		var fileName = $(this).val().split("\\").pop();
			$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
	});
});

function MYForm() {
	$('.myForm input, textarea').on('keyup', function () {
		// console.log($(this));
        $(this).removeClass('is-invalid');
        $(this).attr('aria-invalid', 'false');
		$(this).parents('.form-group').removeClass('has-danger');
		$(this).next("div .form-control-feedback").remove();
	});

	$('.myForm select').on('change', function () { 
		if( !$(this).val() ) {
			$(this).addClass('is-invalid');
        	$(this).attr('aria-invalid', 'true');
			$(this).parents('.form-group').addClass('has-danger');
			$(this).next("div .form-control-feedback").remove();
		}
		else
		{
        	$(this).removeClass('is-invalid');
        	$(this).attr('aria-invalid', 'false');
			$(this).parents('.form-group').removeClass('has-danger');
			$(this).next("div .form-control-feedback").remove();
		}
    });
}

function progressModal(type)
{
	mApp.unblock("#ModalAjaxID .modal-content");

	if(type == "start")
	{
		mApp.block("#ModalAjaxID .modal-content", {
			overlayColor: "#000000",
			opacity: 0.2,
			type: "loader",
			state: "success",
			message: "Please wait, data is being process..."
		});
	}
}

function popupYoutube() {
	$('.popup-youtube').magnificPopup({
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		iframe: {
			patterns: {
				youtube: {
					index: 'youtube.com/',
					id: 'v=',
					src: '//www.youtube.com/embed/%id%?autoplay=1'
				},
			}
		}
	});
}

function popupImagesDefault() {
	// Image popups
	$('.image-popup-no-margins').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
}

function popupImagesFit() {
	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile mfp-with-zoom',
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
}

function onDemandScript( url, callback ) {
	callback = (typeof callback != 'undefined') ? callback : {};

	$.ajax({
			type: "GET",
			url: url,
			success: callback,
			dataType: "script",
			cache: true
		});    
}

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()_+-={}[]:;\'<>?,./|\\";

  for (var i = 0; i < 30; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};

// Example Call :
// make_toast();
// toastr.success("", "Change saved!");
// toastr.info("", "Change saved!");
// toastr.warning("", "Change saved!");
// toastr.error("", "Change saved!");
function make_toast(type, msg) {
	toastr.options = {
		"closeButton": false,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "500",
		"timeOut": "1000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};

	if(type=='success')
		toastr.success("", msg);
	else if(type=='info')
		toastr.info("", msg);
	else if(type=='warning')
		toastr.warning("", msg);
	else if(type=='error')
		toastr.error("", msg);
}

function funcDelete(url, id, trID) {
	swal({
		title: "Apakah anda yakin?",
		text: "Data akan dihapus secara permanen!",
		type: "warning",
		showCancelButton: !0,
		confirmButtonClass: "btn m-btn--square btn-danger",
		confirmButtonText: "Ya, Hapus!",
		cancelButtonText: "Batal"
	}).then(function (e) {
		e.value ? swal({
			title: "Berhasil dihapus!",
			// text: "Your data has been deleted.",
			type: "success",
			timer: 1e3,
			showConfirmButton: !1
		}).then(function (e) {
			$.post(url, {id: id}, function (theResponse) {});
			"timer" === e.dismiss && console.log("I was closed by the timer");
			$("[data-row='" + trID + "']").fadeOut('slow')
		}) : "cancel" === e.dismiss && swalCancel()
	})
}
//------- set message cancel fow swal
function swalCancel(){
	swal("Batal Hapus", "", "error")
}

// Upload image global summernote
function uploadImage(url,image) {
	var data = new FormData();
	data.append("image", image);
	$.ajax({
		url: url+"welcome/upload_image",
		cache: false,
		contentType: false,
		processData: false,
		data: data,
		type: "POST",
		success: function(url) {
			$('#content, #answer, #info').summernote("insertImage", url);
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function deleteImage(url,src) {
	$.ajax( {
		data: {src : src},
		type: "POST",
		url: url+"welcome/delete_image",
		cache: false,
		success: function(response) {
			console.log(response);
		}
	});
}